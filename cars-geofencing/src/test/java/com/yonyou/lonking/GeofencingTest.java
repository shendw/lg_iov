package com.yonyou.lonking;

import com.yonyou.lonking.client.AMAPClient;
import com.yonyou.lonking.service.IGeoFencingService;
import com.yonyou.lonking.utils.ResponseData;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shendawei
 * @创建时间 2021/1/13
 * @描述
 **/
@SpringBootTest(classes = GeoFencingApplication.class)
@RunWith(SpringRunner.class)
@Slf4j
public class GeofencingTest {

    @Autowired
    private IGeoFencingService iGeoFencingService;

    @Autowired
    private AMAPClient amapClient;

    @Value("${amapkey}")
    private String amapkey;

    @Test
    public void create(){
        Object address = amapClient.address(amapkey, "合肥市肥东县撮镇镇龙塘工业园新安村合马路北");
        return;
    }
}
