package com.yonyou.lonking.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yonyou.lonking.GeoFencingApplication;
import com.yonyou.lonking.dto.IovTerminalCarDto;
import com.yonyou.lonking.service.IIovTerminalCarService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shendawei
 * @创建时间 2021/1/21
 * @描述
 **/
@SpringBootTest(classes = GeoFencingApplication.class)
@RunWith(SpringRunner.class)
@Slf4j
public class IovTerminalCarServiceImplTest {

    @Autowired
    private IIovTerminalCarService iovTerminalCarServiceImpl;

  @Test
  public void selectEnableByPage() {}

  @Test
  public void selectCarInfoByPage() {
      Page<IovTerminalCarDto> iovTerminalCarDtoPage = iovTerminalCarServiceImpl.selectCarInfoByPage(new Page(1, 10), null, null);
      return;
  }
}
