package com.yonyou.lonking.service.impl;

import com.yonyou.lonking.GeoFencingApplication;
import com.yonyou.lonking.entity.IovGeofencing;
import com.yonyou.lonking.service.IIovGeofencingService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shendawei
 * @创建时间 2021/1/13
 * @描述
 **/
@SpringBootTest(classes = GeoFencingApplication.class)
@RunWith(SpringRunner.class)
@Slf4j
public class IovGeoFencingServiceImplTest {
    @Autowired
    private IIovGeofencingService iovGeofencingServiceImpl;

    @Test
    public void create(){
        IovGeofencing iovGeofencing = new IovGeofencing();
        iovGeofencing.setCenter("115.672126,38.817129");
        iovGeofencing.setEnable(0L);
        iovGeofencing.setName("测试围栏");
        iovGeofencing.setRadius("1000");
        iovGeofencing.setPkTerminalCar("ssssss");
        boolean save = iovGeofencingServiceImpl.save(iovGeofencing);
        return;
    }
}
