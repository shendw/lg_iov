package com.yonyou.lonking.service.impl;

import com.yonyou.lonking.GeoFencingApplication;
import com.yonyou.lonking.utils.ResponseData;
import com.yonyou.lonking.service.IGeoFencingService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shendawei
 * @创建时间 2021/1/13
 * @描述
 **/
@SpringBootTest(classes = GeoFencingApplication.class)
@RunWith(SpringRunner.class)
@Slf4j
public class GeoFencingServiceImplTest {

    @Autowired
    private IGeoFencingService iGeoFencingService;

  @Test
  public void createGeofencing() {
      ResponseData geofencing = iGeoFencingService.createGeofencing();
  }

  @Test
  public void getGeoFencing() {
      ResponseData geoFencing = iGeoFencingService.getGeoFencing();
  }

  @Test
  public void updateGeoFencing() {
      iGeoFencingService.updateGeoFencing("c4d997a4-62f8-41d5-8b6a-3a90801705c1","测试围栏名称","115.662126,38.817129","2000");
  }
}
