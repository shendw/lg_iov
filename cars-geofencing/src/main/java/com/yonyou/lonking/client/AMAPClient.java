package com.yonyou.lonking.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @author shendawei
 * @创建时间 2021/1/12
 * @描述
 **/
@FeignClient(value = "amap",url = "https://restapi.amap.com/")
public interface AMAPClient {

    @RequestMapping(value = "/v4/geofence/meta",method = RequestMethod.POST)
    Object createFencing(@RequestParam("key") String key, @RequestBody Map<String,String> params);

//    @RequestMapping(value = "/v4/geofence/meta",method = RequestMethod.GET)
//    Object getFencing(@RequestParam("key") String key, @RequestBody Map<String,String> params);

    @RequestMapping(value = "/v4/geofence/meta",method = RequestMethod.POST)
    Object updateFencing(@RequestParam("key") String key,@RequestParam("gid") String gid,@RequestParam("method") String method, @RequestBody Map<String,String> params);

    @RequestMapping(value = "/v4/geofence/meta",method = RequestMethod.POST)
    Object deleteFencing(@RequestParam("key") String key,@RequestParam("gid") String gid,@RequestParam("delete") String method);

    @RequestMapping(value = "/v3/geocode/geo",method = RequestMethod.GET)
    Map<String,Object> address(@RequestParam("key") String key,@RequestParam("address") String address);

}
