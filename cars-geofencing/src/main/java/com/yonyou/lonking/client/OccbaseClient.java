package com.yonyou.lonking.client;

import com.yonyou.lonking.dto.occ.CustomerAddressDto;
import com.yonyou.lonking.dto.occ.CustomerDto;
import com.yonyou.lonking.dto.occ.FeignPage;
import com.yonyou.lonking.dto.occ.ShopDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * author： yanrunfa
 * data: 2018/2/5 11:57
 */
@FeignClient(value = "occ-base")
public interface OccbaseClient {
    /**
     * 获取指定编码的客户DTO。
     *
     * @param code 客户DTO的编码。
     * @return 客户DTO。
     */
    @GetMapping("/api/base/customer/get-by-code")
    ResponseEntity<CustomerDto> getByCode(@RequestParam("code") String code);

    @GetMapping(value = "/base/customers",consumes = "application/json;charset=UTF-8")
    FeignPage<CustomerDto> getCustomers(@SpringQueryMap Map<String,Object> params);

    @GetMapping("/base/customer-addresses/findByCustomerId")
    List<CustomerAddressDto> getCustomerAddress(@RequestParam("customerId")String customerId, @RequestParam("search_AUTH_APPCODE")String search_AUTH_APPCODE);

    @GetMapping("/base/shops")
    FeignPage<ShopDto> getShop(@SpringQueryMap Map<String,Object> params);
}
