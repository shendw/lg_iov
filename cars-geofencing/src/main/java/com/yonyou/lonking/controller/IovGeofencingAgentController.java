package com.yonyou.lonking.controller;


import com.yonyou.lonking.entity.IovGeofencingAgent;
import com.yonyou.lonking.service.IIovGeofencingAgentService;
import com.yonyou.lonking.utils.ResponseCode;
import com.yonyou.lonking.utils.ResponseData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author shendw
 * @since 2021-01-22
 */
@Api(tags = "代理商电子围栏")
@RestController
@RequestMapping("/lonking/iov-geofencing-agent")
@Validated
public class IovGeofencingAgentController {

    @Autowired
    private IIovGeofencingAgentService iIovGeofencingAgentServiceImpl;

    @ApiOperation("查询围栏")
    @GetMapping("/findGeofencingAgent")
    public ResponseData findGeofencingAgent(String costomerId){
        List<IovGeofencingAgent> geoFencingByAgent = iIovGeofencingAgentServiceImpl.findGeoFencingByAgent(costomerId);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),geoFencingByAgent);
    }

    @ApiOperation("创建围栏")
    @PostMapping
    public ResponseData createGeofencingAgent(@RequestBody@Validated IovGeofencingAgent iovGeofencingAgent){
        boolean flag = iIovGeofencingAgentServiceImpl.createGeoFencingByAgent(iovGeofencingAgent);
        if (flag){
            return ResponseData.result(ResponseCode.SUCCESS_INSERT.getCode(),ResponseCode.SUCCESS_INSERT.getDesc());
        }else{
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @ApiOperation(value = "更新围栏")
    @RequestMapping(value = "/updateGeoFencingAgent",method = RequestMethod.GET)
    public ResponseData updateGeoFencingAgent(@ApiParam(name = "id",value = "围栏主键",required = true)@RequestParam String id,
                                              @ApiParam(name = "center",value = "围栏坐标中心",required = true)@RequestParam@Pattern(regexp = "^\\d+(\\.\\d{6})\\,\\d+(\\.\\d{6})$",message = "坐标格式出错") String center,
                                              @ApiParam(name = "radius",value = "围栏半径",required = true)@RequestParam @Pattern(regexp = "^\\d{1,6}$",message = "半径格式错误")String radius){
        boolean flag = iIovGeofencingAgentServiceImpl.updateGeoFencingByAgent(id, center,radius);
        if (flag){
            return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(),ResponseCode.SUCCESS_UPDATE.getDesc());
        }else{
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @ApiOperation("删除围栏")
    @RequestMapping(value = "/deleteGeoFencingAgent",method = RequestMethod.GET)
    public ResponseData deleteGeoFencingAgent(@ApiParam(name = "id",value = "车辆围栏主键",required = true)@RequestParam String id){
        boolean flag = iIovGeofencingAgentServiceImpl.deleteGeoFencingByAgent(id);
        if (flag){
            return ResponseData.result(ResponseCode.SUCCESS_DELETE.getCode(),ResponseCode.SUCCESS_DELETE.getDesc());
        }else{
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }
}
