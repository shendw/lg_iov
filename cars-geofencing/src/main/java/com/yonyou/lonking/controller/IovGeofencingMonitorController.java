package com.yonyou.lonking.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yonyou.lonking.entity.IovGeofencingMonitor;
import com.yonyou.lonking.service.IIovGeofencingMonitorService;
import com.yonyou.lonking.utils.ResponseCode;
import com.yonyou.lonking.utils.ResponseData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author shendw
 * @since 2021-01-25
 */
@Api(tags = "电子围栏监控记录")
@RestController
@RequestMapping("/lonking/iov-geofencing-monitor")
public class IovGeofencingMonitorController {

    @Autowired
    private IIovGeofencingMonitorService iovGeofencingMonitorServiceImpl;

    @ApiOperation("获取电子围栏监控记录")
    @GetMapping("/getMonitor")
    public ResponseData getMonitor(@ApiParam(name = "pageNum",value = "页码",required = true,defaultValue = "1") @RequestParam int pageNum,
                                   @ApiParam(name ="pageSize",value = "每页数量",required = true,defaultValue = "10") @RequestParam int pageSize,
                                   @ApiParam(name = "terminalid",value = "终端号")@RequestParam(required = false) String terminalid,
                                   @ApiParam(name = "carno",value = "整机编号")@RequestParam(required = false) String carno,
                                   @ApiParam(name = "agentName",value = "代理商名称")@RequestParam(required = false) String agentName){
        Page<IovGeofencingMonitor> monitor = iovGeofencingMonitorServiceImpl.getMonitor(new Page(pageNum, pageSize), carno, terminalid, agentName);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),monitor);
    }
}
