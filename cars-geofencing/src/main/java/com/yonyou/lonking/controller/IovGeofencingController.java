package com.yonyou.lonking.controller;


import com.yonyou.lonking.entity.IovGeofencing;
import com.yonyou.lonking.service.IIovGeofencingService;
import com.yonyou.lonking.utils.ResponseCode;
import com.yonyou.lonking.utils.ResponseData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author shendw
 * @since 2021-01-16
 */
@Api(tags = "电子围栏")
@RestController
@RequestMapping("/lonking/iov-geofencing")
@Validated
public class IovGeofencingController {

    @Autowired
    private IIovGeofencingService iovGeofencingServiceImpl;

    @ApiOperation("通过整机创建围栏")
    @RequestMapping(value = "/createGeoFencingByCar",method = RequestMethod.GET)
    public ResponseData createGeoFencingByCar(@ApiParam(name = "pkTerminalCar",value = "车辆终端绑定关系主键",required = true)@RequestParam String pkTerminalCar,
                                              @ApiParam(name = "name",value = "围栏名称",required = true)@RequestParam String name,
                                              @ApiParam(name = "center",value = "围栏坐标中心",required = true)@RequestParam @Pattern(regexp = "^\\d+(\\.\\d{6})\\,\\d+(\\.\\d{6})$",message = "坐标格式出错") String center,
                                              @ApiParam(name = "radius",value = "围栏半径",required = true)@RequestParam @Pattern(regexp = "^\\d{1,4}$",message = "半径格式错误")String radius){
        boolean flag = iovGeofencingServiceImpl.createGeoFencingByCar(pkTerminalCar, name, center, radius);
        if (flag){
            return ResponseData.result(ResponseCode.SUCCESS_INSERT.getCode(),ResponseCode.SUCCESS_INSERT.getDesc());
        }else{
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @ApiOperation("通过整机删除围栏")
    @RequestMapping(value = "/deleteGeoFencingByCar",method = RequestMethod.GET)
    public ResponseData deleteGeoFencingByCar(@ApiParam(name = "id",value = "车辆围栏主键",required = true)@RequestParam String id){
        boolean flag = iovGeofencingServiceImpl.deleteGeoFencingByCar(id);
        if (flag){
            return ResponseData.result(ResponseCode.SUCCESS_DELETE.getCode(),ResponseCode.SUCCESS_DELETE.getDesc());
        }else{
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @ApiOperation(value = "通过整机更新围栏")
    @RequestMapping(value = "/updateGeoFencingByCar",method = RequestMethod.GET)
    public ResponseData updateGeoFencingByCar(@ApiParam(name = "id",value = "围栏主键",required = true)@RequestParam String id,
                                              @ApiParam(name = "center",value = "围栏坐标中心",required = true)@RequestParam @Pattern(regexp = "^\\d+(\\.\\d{6})\\,\\d+(\\.\\d{6})$",message = "坐标格式出错") String center,
                                              @ApiParam(name = "radius",value = "围栏半径",required = true)@RequestParam @Pattern(regexp = "^\\d{1,4}$",message = "半径格式错误")String radius){
        boolean flag = iovGeofencingServiceImpl.updateGeoFencingByCar(id, center,radius);
        if (flag){
            return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(),ResponseCode.SUCCESS_UPDATE.getDesc());
        }else{
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @ApiOperation("通过整机查询围栏")
    @RequestMapping(value = "/findGeoFencingByCar",method = RequestMethod.GET)
    public ResponseData findGeoFencingByCar(@ApiParam(name = "pkTerminalCar",value = "车辆终端绑定关系主键",required = true)@RequestParam String pkTerminalCar){
        List<IovGeofencing> geoFencingByCar = iovGeofencingServiceImpl.findGeoFencingByCar(pkTerminalCar);
        return ResponseData.result(ResponseCode.SUCCESS_INSERT.getCode(),geoFencingByCar);
    }
}
