package com.yonyou.lonking.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yonyou.lonking.dto.IovTerminalCarDto;
import com.yonyou.lonking.entity.IovTerminalCar;
import com.yonyou.lonking.service.IIovTerminalCarService;
import com.yonyou.lonking.utils.ResponseCode;
import com.yonyou.lonking.utils.ResponseData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import oracle.jdbc.proxy.annotation.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author shendw
 * @since 2021-01-18
 */
@Api(tags = "车辆查询")
@RestController
@RequestMapping("/lonking/iov-terminal-car")
public class IovTerminalCarController {

    @Autowired
    private IIovTerminalCarService iIovTerminalCarServiceImpl;

    @ApiOperation("车辆信息查询")
    @GetMapping("/findTerminalCar")
    public ResponseData findTerminalCar(@ApiParam(name = "pageNum",value = "页码",required = true,defaultValue = "1") @RequestParam int pageNum,
                                        @ApiParam(name ="pageSize",value = "每页数量",required = true,defaultValue = "10") @RequestParam int pageSize,
                                        @ApiParam(name = "terminalid",value = "终端号")@RequestParam(required = false) String terminalid,
                                        @ApiParam(name = "carno",value = "整机编号")@RequestParam(required = false) String carno){
        Page<IovTerminalCarDto> page = iIovTerminalCarServiceImpl.selectCarInfoByPage(new Page<>(pageNum, pageSize),terminalid,carno);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),page);
    }

    @ApiOperation("查询代理商下的库存车的信息")
    @GetMapping("/findTerminalCarByAgentName")
    public ResponseData findTerminalCarByAgentName(@ApiParam(name = "agentName",value = "代理商名称",required = true)@RequestParam String agentName){
        List<IovTerminalCarDto> iovCarDocByAgentName = iIovTerminalCarServiceImpl.getIovCarDocByAgentName(agentName);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),iovCarDocByAgentName);
    }
}
