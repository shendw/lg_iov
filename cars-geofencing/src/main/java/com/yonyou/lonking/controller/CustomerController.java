package com.yonyou.lonking.controller;

import com.yonyou.lonking.dto.occ.CustomerDto;
import com.yonyou.lonking.dto.occ.FeignPage;
import com.yonyou.lonking.service.ICustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shendawei
 * @创建时间 2021/1/21
 * @描述
 **/
@Api(tags = "代理商档案")
@RestController
@RequestMapping("/lonking/customer")
public class CustomerController {

    @Autowired
    private ICustomerService customerServiceImpl;

    @ApiOperation("代理商列表查询")
    @GetMapping()
    public FeignPage<CustomerDto> getCustomer(@ApiParam(name = "pageNum",value = "页码",required = true,defaultValue = "1") @RequestParam int pageNum,
                                              @ApiParam(name ="pageSize",value = "每页数量",required = true,defaultValue = "10") @RequestParam int pageSize,
                                              @ApiParam(name = "name",value = "客户名称")@RequestParam(required = false) String name){
        FeignPage<CustomerDto> customerList = customerServiceImpl.getCustomerList(pageNum, pageSize, name);
        return customerList;
    }
}
