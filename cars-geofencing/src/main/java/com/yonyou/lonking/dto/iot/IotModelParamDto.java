package com.yonyou.lonking.dto.iot;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2020/7/30
 * @描述
 **/
@Data
public class IotModelParamDto {

    private String id;
    private Long origin;
    private String device;
    private String name;
    private String value;
    private String tag;
    private String type;
}
