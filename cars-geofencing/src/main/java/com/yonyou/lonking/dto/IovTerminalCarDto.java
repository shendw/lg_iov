package com.yonyou.lonking.dto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2021/1/21
 * @描述
 **/
@Data
public class IovTerminalCarDto {

    private String pkTerminalCar;

    /**
     * 终端ID
     */
    private String terminalid;

    /**
     * 整车编号
     */
    private String carno;

    private String location;

    private long status;

    private String serviceAgent;
}
