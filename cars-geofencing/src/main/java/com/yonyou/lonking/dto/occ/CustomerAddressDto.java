package com.yonyou.lonking.dto.occ;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @ClassName: CustomerAddressDto
 * @Description: 客户收货地址
 * @author kongzhp3
 * @date 2017年6月21日 下午3:24:49
 */
@Data
public class CustomerAddressDto{
	@Size(max = 40)
	protected String id;

	protected Integer dr = 0;

	protected Date ts;

	@Size(max = 20)
	protected String creator;

	@Size(max = 19)
	protected Date creationTime;

	@Size(max = 20)
	protected String modifier;

	@ApiModelProperty(value = "修改时间", position = 7)
	@Size(max = 19)
	protected Date modifiedTime;
	/**
	 * 客户/经销商
	 */
	@ApiModelProperty(value = "客户/经销商", position = 101)
	@Size(max = 40)
	private String customerId;

	@ApiModelProperty(value = "客户/经销商编码", position = 102)
	private String customerCode;

	@ApiModelProperty(value = "客户/经销商名称", position = 103)
	private String customerName;

	@ApiModelProperty(value = "客户收货地址编码", position = 104)
	@Size(max = 40)
	private String code;

	@ApiModelProperty(value = "客户收货地址名称", position = 105)
	@Size(max = 200)
	private String name;

	/**
	 * 国家
	 */
	@ApiModelProperty(value = "国家主键", position = 106)
	@Size(max = 40)
	private String countryId;

	@ApiModelProperty(value = "国家编码", position = 107)
	private String countryCode;

	@ApiModelProperty(value = "国家名称", position = 108)
	private String countryName;

	/**
	 * 省
	 */
	@ApiModelProperty(value = "省主键", position = 109)
	@Size(max = 40)
	private String provinceId;
	@ApiModelProperty(value = "省名称", position = 110)
	private String provinceName;
	@ApiModelProperty(value = "省编码", position = 111)
	private String provinceCode;

	/**
	 * 市
	 */
	@ApiModelProperty(value = "市主键", position = 112)
	@Size(max = 40)
	private String cityId;

	@ApiModelProperty(value = "市名称", position = 113)
	private String cityName;
	@ApiModelProperty(value = "市编码", position = 114)
	private String cityCode;

	/**
	 * 区
	 */
	@ApiModelProperty(value = "区主键", position = 115)
	@Size(max = 40)
	private String countyId;
	@ApiModelProperty(value = "区/县名称", position = 116)
	private String countyName;
	@ApiModelProperty(value = "区/县编码", position = 117)
	private String countyCode;

	/**
	 * 镇
	 */
	@ApiModelProperty(value = "镇主键", position = 118)
	@Size(max = 40)
	private String townId;
	@ApiModelProperty(value = "街道/镇编码", position = 119)
	private String townCode;

	@ApiModelProperty(value = "街道/镇名称", position = 120)
	private String townName;

	/**
	 * 经度
	 */
	@ApiModelProperty(value = "经度", position = 121)
	@Size(max = 40)
	private String longitude;

	/**
	 * 纬度
	 */
	@ApiModelProperty(value = "纬度", position = 122)
	@Size(max = 40)
	private String latitude;

	/**
	 * 标准物流周期
	 */
	@ApiModelProperty(value = "标准物流周期", position = 123)
	private Integer standardLogisticPeriod;

	/**
	 * 收货第一联系人姓名
	 */
	@ApiModelProperty(value = "收货第一联系人姓名", position = 124)
	@Size(max = 64)
	private String firstReceiver;

	/**
	 * 收货第一联系人电话
	 */
	@ApiModelProperty(value = "收货第一联系人电话", position = 125)
	private String firstReceiverTel;

	/**
	 * 收货第一联系人手机
	 */
	@ApiModelProperty(value = "收货第一联系人手机", position = 126)
	private String firstReceiverPhone;

	/**
	 * 收货第二联系人姓名
	 */
	@ApiModelProperty(value = "收货第二联系人姓名", position = 127)
	@Size(max = 64)
	private String secondReceiver;

	/**
	 * 收货第二联系人电话
	 */
	@ApiModelProperty(value = "收货第二联系人电话", position = 128)
	private String secondReceiverTel;

	/**
	 * 收货第二联系人手机
	 */
	@ApiModelProperty(value = "收货第二联系人手机", position = 129)
	private String secondReceiverPhone;

	/**
	 * 收货第三联系人姓名
	 */
	@ApiModelProperty(value = "收货第三联系人姓名", position = 130)
	@Size(max = 64)
	private String thirdReceiver;

	/**
	 * 收货第三联系人电话
	 */
	@ApiModelProperty(value = "收货第三联系人电话", position = 131)
	private String thirdReceiverTel;

	/**
	 * 收货第三联系人手机
	 */
	@ApiModelProperty(value = "收货第三联系人手机", position = 132)
	private String thirdReceiverPhone;
	/**
	 * 传真
	 */
	@ApiModelProperty(value = "传真", position = 133)
	private String fax;

	/**
	 * 详细地址
	 */
	@ApiModelProperty(value = "详细地址", position = 134)
	@Size(max = 200)
	private String detailAddr;

	/**
	 * 是否默认
	 */
	@ApiModelProperty(value = "是否默认", position = 135)
	@Size(max = 1)
	private Integer isDefault = 0;

	/**
	 * 原系统
	 */
	@ApiModelProperty(value = "原系统", position = 136)
	@Size(max = 40)
	private String srcSystem;

	/**
	 * 原系统ID
	 */
	@ApiModelProperty(value = "原系统主键", position = 137)
	@Size(max = 40)
	private String srcSystemId;

	/**
	 * 原系统code
	 */
	@ApiModelProperty(value = "原系统编码", position = 138)
	@Size(max = 50)
	private String srcSystemCode;

	/**
	 * 原系统name
	 */
	@ApiModelProperty(value = "原系统名称", position = 139)
	@Size(max = 200)
	private String srcSystemName;

	/**
	 * 扩展字段01
	 */
	@ApiModelProperty(value = "扩展字段01", position = 201)
	@Size(max = 100)
	private String ext01;

	/**
	 * 扩展字段02
	 */
	@ApiModelProperty(value = "扩展字段02", position = 202)
	@Size(max = 100)
	private String ext02;

	/**
	 * 扩展字段03
	 */
	@ApiModelProperty(value = "扩展字段03", position = 203)
	@Size(max = 100)
	private String ext03;

	/**
	 * 扩展字段04
	 */
	@ApiModelProperty(value = "扩展字段04", position = 204)
	@Size(max = 100)
	private String ext04;

	/**
	 * 扩展字段05
	 */
	@ApiModelProperty(value = "扩展字段05", position = 205)
	@Size(max = 100)
	private String ext05;

	/**
	 * 扩展字段06
	 */
	@ApiModelProperty(value = "扩展字段06", position = 206)
	@Size(max = 100)
	private String ext06;

	/**
	 * 扩展字段07
	 */
	@ApiModelProperty(value = "扩展字段07", position = 207)
	@Size(max = 100)
	private String ext07;

	/**
	 * 扩展字段08
	 */
	@ApiModelProperty(value = "扩展字段08", position = 208)
	@Size(max = 100)
	private String ext08;

	/**
	 * 扩展字段09
	 */
	@ApiModelProperty(value = "扩展字段09", position = 209)
	@Size(max = 100)
	private String ext09;

	/**
	 * 扩展字段10
	 */
	@ApiModelProperty(value = "扩展字段10", position = 210)
	@Size(max = 100)
	private String ext10;
}
