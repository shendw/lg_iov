package com.yonyou.lonking.dto.occ;

import com.yonyou.lonking.dto.IovTerminalCarDto;
import com.yonyou.lonking.entity.IovGeofencingAgent;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 客户的数据传输对象类
 *
 * @author xugm
 * @date 2018-4-13
 */
@Data
public class CustomerDto {
    @Size(max = 40)
    protected String id;

    protected Integer dr = 0;

    protected Date ts;

    @Size(max = 20)
    protected String creator;

    @Size(max = 19)
    protected Date creationTime;

    @Size(max = 20)
    protected String modifier;

    @ApiModelProperty(value = "修改时间", position = 7)
    @Size(max = 19)
    protected Date modifiedTime;

    private String customerSourceId;
    private String customerSourceCode;

    private String customerSourceName;


    private String cusReqFormId;

    private String cusReqFormCode;


    private String code;


    private String name;


    private String abbr;


    private String remark;


    private String businessScope;


    private String marketAreaId;


    private String marketAreaCode;


    private String marketAreaName;


    private String customerCategoryId;

    private String customerCategoryCode;


    private String customerCategoryName;


    private String channelTypeId;

    private String channelTypeCode;

    private String channelTypeName;

    private String customerLevelId;

    private String customerLevelCode;

    private String customerLevelName;

    private String customerTypeId;

    private String customerTypeCode;

    private String customerTypeName;

    private String organizationId;

    private String organizationCode;

    private String organizationName;

    private Integer isChannelCustomer;

    private String credentialsTypeCode;

    private String credentialsTypeName;

    private String credentialsNo;

    private String legalPerson;

    private String taxRegNo;

    private String countryId;

    private String countryCode;

    private String countryName;

    private String provinceId;

    private String provinceName;

    private String provinceCode;

    private String cityId;

    private String cityCode;

    private String cityName;


    private String countyId;

    private String countyName;

    private String countyCode;

    private String townId;

    private String townName;

    private String townCode;

    private String detailAddr;

    private BigDecimal longitude;

    private BigDecimal latitude;

    private String postalCode;

    private Integer isFrozen;

    private Integer isEnable;

    private String payerCustomerId;

    private String payerCustomerCode;

    private String payerCustomerName;

    private String superiorCustomerId;

    private String superiorCustomerName;

    private String customerRankId;
    private String customerRankCode;
    private String customerRankName;
//    List<CustomerContactDto> customerContacts = new ArrayList<>();
//    List<CustomerAddressDto> customerAddresses = new ArrayList<>();
//    List<CustomerAccountDto> customerAccounts = new ArrayList<>();
    private Integer isServiceProvider;
//    private String srcSystem;
//    private String srcSystemId;
//    private String srcSystemCode;
//    private String srcSystemName;
//    private String ownerOrganizationId;
//    private String ownerOrganizationCode;
//    private String ownerOrganizationName;
//    private String ext01;
//    private String ext02;
//    private String ext03;
//    private String ext04;
//    private String ext05;
//    private String ext06;
//    private String ext07;
//    private String ext08;
//    private String ext09;
//    private String ext10;
    private List<IovGeofencingAgent> iovGeofencingAgents = new ArrayList<>();
    private List<IovTerminalCarDto> iovTerminalCarDtos = new ArrayList<>();
}
