package com.yonyou.lonking.dto.occ;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 门店的数据传输对象类
 *
 * @author gongdb
 * @date 2018-04-20 15:18:53
 */
@ApiModel(description = "门店数据传输对象")
@Data
public class ShopDto{

    @Size(max = 40)
    protected String id;

    protected Integer dr = 0;

    protected Date ts;

    @Size(max = 20)
    protected String creator;

    @Size(max = 19)
    protected Date creationTime;

    @Size(max = 20)
    protected String modifier;

    @ApiModelProperty(value = "修改时间", position = 7)
    @Size(max = 19)
    protected Date modifiedTime;
    /**
     * 所属组织
     */
    @Size(max = 40)
    @ApiModelProperty(value = "所属组织编码主键", position = 101, required = true)
    private String organizationId;
    @ApiModelProperty(value = "所属组织编码", position = 102, required = true)
    private String organizationCode;
    @ApiModelProperty(value = "所属组织名称", position = 103, required = true)
    private String organizationName;

    /**
     * 所属客户
     */
    @Size(max = 40)
    @ApiModelProperty(value = "所属客户主键", position = 104, required = true)
    private String customerId;
    @ApiModelProperty(value = "所属客户编码", position = 105, required = true)
    private String customerCode;
    @ApiModelProperty(value = "所属客户名称", position = 106, required = true)
    private String customerName;

    /**
     * 编码
     */

    @Size(max = 50)
    @ApiModelProperty(value = "编码", position = 107, required = true)
    private String code;

    /**
     * 名称
     */

    @Size(max = 400)
    @ApiModelProperty(value = "名称", position = 108, required = true)
    private String name;

    /**
     * 备注
     */

    @Size(max = 200)
    @ApiModelProperty(value = "备注", position = 109, required = true)
    private String remark;

    /**
     * 店铺性质
     */

    @Size(max = 50)
    @ApiModelProperty(value = "店铺性质编号", position = 110, required = true)
    private String shopNatureCode;
    @ApiModelProperty(value = "店铺性质主键", position = 111, required = true)
    private String shopNatureId;
    @ApiModelProperty(value = "店铺性质名称", position = 112, required = true)
    private String shopNatureName;

    /**
     * 店铺类型
     */

    @Size(max = 50)
    @ApiModelProperty(value = "店铺类型编号", position = 113, required = true)
    private String shopTypeCode;
    @ApiModelProperty(value = "店铺类型主键", position = 114, required = true)
    private String shopTypeId;
    @ApiModelProperty(value = "店铺类型名称", position = 115, required = true)
    private String shopTypeName;

    /**
     * 所在国家
     */

    @Size(max = 40)
    @ApiModelProperty(value = "所在国家主键", position = 116, required = true)
    private String countryId;
    @ApiModelProperty(value = "所在国家编码", position = 117, required = true)
    private String countryCode;
    @ApiModelProperty(value = "所在国家名称", position = 118, required = true)
    private String countryName;

    /**
     * 所在省份
     */

    @Size(max = 40)
    @ApiModelProperty(value = "所在省份主键", position = 119, required = true)
    private String provinceId;
    @ApiModelProperty(value = "所在省份编码", position = 120, required = true)
    private String provinceCode;
    @ApiModelProperty(value = "所在省份名称", position = 121, required = true)
    private String provinceName;

    /**
     * 所在城市
     */

    @Size(max = 40)
    @ApiModelProperty(value = "所在城市主键", position = 122, required = true)
    private String cityId;
    @ApiModelProperty(value = "所在城市编码", position = 123, required = true)
    private String cityCode;
    @ApiModelProperty(value = "所在城市名称", position = 124, required = true)
    private String cityName;

    /**
     * 所在区/县
     */

    @Size(max = 40)
    @ApiModelProperty(value = "所在区/县主键", position = 125, required = true)
    private String countyId;
    @ApiModelProperty(value = "所在区/县编码", position = 126, required = true)
    private String countyCode;
    @ApiModelProperty(value = "所在区/县名称", position = 127, required = true)
    private String countyName;

    /**
     * 所在街道/镇
     */

    @Size(max = 40)
    @ApiModelProperty(value = "所在街道/镇主键", position = 128, required = true)
    private String townId;
    @ApiModelProperty(value = "所在街道/镇编码", position = 129, required = true)
    private String townCode;
    @ApiModelProperty(value = "所在街道/镇名称", position = 130, required = true)
    private String townName;

    /**
     * 详细地址
     */

    @Size(max = 200)
    @ApiModelProperty(value = "详细地址", position = 131, required = true)
    private String address;

    /**
     * 法人代表
     */

    @Size(max = 40)
    @ApiModelProperty(value = "法人代表", position = 132, required = true)
    private String legalPerson;

    /**
     * 证件类型
     */

    @Size(max = 40)
    @ApiModelProperty(value = "证件类型主键", position = 133, required = true)
    private String credentialsTypeId;
    @ApiModelProperty(value = "证件类型编号", position = 134, required = true)
    private String credentialsTypeCode;
    @ApiModelProperty(value = "证件类型名称", position = 135, required = true)
    private String credentialsTypeName;

    /**
     * 证件号码
     */

    @Size(max = 40)
    @ApiModelProperty(value = "证件号码", position = 136, required = true)
    private String credentialsNo;

    /**
     * 税务登记号
     */

    @Size(max = 40)
    @ApiModelProperty(value = "税务登记号", position = 137, required = true)
    private String taxRegNo;

    /**
     * 营业面积
     */
    @ApiModelProperty(value = "营业面积", position = 138, required = true)
    private BigDecimal businessArea;

    /**
     * 邮政编码
     */

    @Size(max = 10)
    @ApiModelProperty(value = "邮政编码", position = 139, required = true)
    private String postalCode;

    /**
     * 经度
     */
    @ApiModelProperty(value = "经度", position = 140, required = true)
    private BigDecimal longitude;

    /**
     * 纬度
     */
    @ApiModelProperty(value = "纬度", position = 141, required = true)
    private BigDecimal latitude;

    /**
     * 经营范围
     */

    @Size(max = 400)
    @ApiModelProperty(value = "经营范围", position = 141, required = true)
    private String businessScope;

    /**
     * 来源
     */
    @Size(max = 50)
    @ApiModelProperty(value = "来源编码", position = 142, required = true)
    private String sourceCode;
    @ApiModelProperty(value = "来源主键", position = 143, required = true)
    private String sourceId;
    @ApiModelProperty(value = "来源名称", position = 144, required = true)
    private String sourceName;

//    /**
//     * 申请单编码
//     */
//    @Size(max = 50)
//    @Display("申请单编码")
//    @FieldDisplay(showName = "申请单编码")
    //private String reqFormCode;

    /**
     * 启用状态
     */
    @ApiModelProperty(value = "启用状态", position = 145, required = true)
    private Integer isEnable;

    /**
     * 审批状态
     */
    @ApiModelProperty(value = "审批状态", position = 146, required = true)
    private Integer state;

    /**
     * 申请单编码
     */
    @ApiModelProperty(value = "申请单编码主键", position = 147, required = true)
    private String cusReqFormId;
    @ApiModelProperty(value = "申请单编码", position = 148, required = true)
    private String cusReqFormCode;


    /**
     * 扩展字段01
     */
    @Size(max = 100)
    @ApiModelProperty(value = "扩展字段01", position = 201, required = true)
    private String ext01;

    /**
     * 扩展字段02
     */
    @Size(max = 100)
    @ApiModelProperty(value = "扩展字段02", position = 202, required = true)
    private String ext02;

    /**
     * 扩展字段03
     */
    @Size(max = 100)
    @ApiModelProperty(value = "扩展字段03", position = 203, required = true)
    private String ext03;

    /**
     * 扩展字段04
     */
    @Size(max = 100)
    @ApiModelProperty(value = "扩展字段04", position = 204, required = true)
    private String ext04;

    /**
     * 扩展字段05
     */
    @Size(max = 100)
    @ApiModelProperty(value = "扩展字段05", position = 205, required = true)
    private String ext05;

    /**
     * 扩展字段06
     */
    @Size(max = 100)
    @ApiModelProperty(value = "扩展字段06", position = 206, required = true)
    private String ext06;

    /**
     * 扩展字段07
     */
    @Size(max = 100)
    @ApiModelProperty(value = "扩展字段07", position = 207, required = true)
    private String ext07;

    /**
     * 扩展字段08
     */
    @Size(max = 100)
    @ApiModelProperty(value = "扩展字段08", position = 208, required = true)
    private String ext08;

    /**
     * 扩展字段09
     */
    @Size(max = 100)
    @ApiModelProperty(value = "扩展字段09", position = 209, required = true)
    private String ext09;

    /**
     * 扩展字段10
     */
    @Size(max = 100)
    @ApiModelProperty(value = "扩展字段10", position = 210, required = true)
    private String ext10;
}
