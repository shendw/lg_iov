package com.yonyou.lonking.mapper;

import com.yonyou.lonking.entity.IovGeofencingAgent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author shendw
 * @since 2021-01-22
 */
public interface IovGeofencingAgentMapper extends BaseMapper<IovGeofencingAgent> {

}
