package com.yonyou.lonking.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yonyou.lonking.dto.IovTerminalCarDto;
import com.yonyou.lonking.entity.IovTerminalCar;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author shendw
 * @since 2021-01-18
 */
public interface IovTerminalCarMapper extends BaseMapper<IovTerminalCar> {

    @Select("select itc.PK_TERMINAL_CAR,itc.terminalid,itc.carno,iti.location from IOV_TERMINAL_CAR itc left join IOV_TERMINAL_INFO iti on itc.terminalid = iti.id ${ew.customSqlSegment}")
    Page<IovTerminalCarDto> selectIovTerminalCar(IPage<IovTerminalCarDto> page, @Param(Constants.WRAPPER) Wrapper<IovTerminalCar> queryWrapper);


    @Select("select itc.PK_TERMINAL_CAR,itc.terminalid,itc.carno,ic.service_agent,ic.status  from IOV_TERMINAL_CAR itc left join IOV_CARDOC ic on itc.carno = ic.carno where itc.terminalid = #{terminalid} and itc.ENABLESTATE = 0")
    List<IovTerminalCarDto> getIovCarDocByTerminalid(String terminalid);

    @Select("select itc.PK_TERMINAL_CAR,itc.terminalid,itc.carno,ic.service_agent,ic.status,iti.location  from IOV_CARDOC ic left join IOV_TERMINAL_CAR itc on itc.carno = ic.carno left join IOV_TERMINAL_INFO iti on itc.terminalid = iti.id where ic.service_agent = #{agentName} and itc.ENABLESTATE = 0")
    List<IovTerminalCarDto> getIovCarDocByAgentName(String agentName);
}
