package com.yonyou.lonking.mapper;

import com.yonyou.lonking.entity.IovGeofencingMonitor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author shendw
 * @since 2021-01-25
 */
public interface IovGeofencingMonitorMapper extends BaseMapper<IovGeofencingMonitor> {

}
