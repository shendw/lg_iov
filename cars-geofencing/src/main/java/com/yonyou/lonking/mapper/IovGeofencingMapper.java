package com.yonyou.lonking.mapper;

import com.yonyou.lonking.entity.IovGeofencing;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author shendw
 * @since 2021-01-16
 */
public interface IovGeofencingMapper extends BaseMapper<IovGeofencing> {

}
