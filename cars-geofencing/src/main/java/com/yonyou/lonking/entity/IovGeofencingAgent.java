package com.yonyou.lonking.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Pattern;

/**
 * <p>
 * 
 * </p>
 *
 * @author shendw
 * @since 2021-01-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("IOV_GEOFENCING_AGENT")
@ApiModel()
public class IovGeofencingAgent implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(hidden = true)
    @TableId("ID")
    private String id;

    @ApiModelProperty(name = "agentCode",value = "代理商编码",required = true)
    @TableField("AGENT_CODE")
    private String agentCode;

    @ApiModelProperty(name = "name",value = "围栏名称",required = true)
    @TableField("NAME")
    private String name;

    @ApiModelProperty(name = "center",value = "围栏坐标中心",required = true)
    @TableField("CENTER")
    @Pattern(regexp = "^\\d+(\\.\\d{6})\\,\\d+(\\.\\d{6})$",message = "坐标格式出错")
    private String center;

    @ApiModelProperty(name = "radius",value = "围栏半径",required = true)
    @TableField("RADIUS")
    @Pattern(regexp = "^\\d{1,4}$",message = "半径格式错误")
    private String radius;

    @ApiModelProperty(hidden = true)
    @TableField("ENABLE")
    private Long enable;

    @ApiModelProperty(hidden = true)
    @TableField("CREATE_TIME")
    private String createTime;

    @ApiModelProperty(hidden = true)
    @TableField("POINTS")
    private String points;

    @ApiModelProperty(hidden = true)
    @TableField("DR")
    private Long dr;

    @ApiModelProperty(name = "agentName",value = "代理商名称",required = true)
    @TableField("AGENT_NAME")
    private String agentName;

    @ApiModelProperty(name = "agentId",value = "代理商主键",required = true)
    @TableField("AGENT_ID")
    private String agentId;


}
