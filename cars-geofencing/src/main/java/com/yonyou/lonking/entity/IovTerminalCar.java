package com.yonyou.lonking.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author shendw
 * @since 2021-01-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("IOV_TERMINAL_CAR")
public class IovTerminalCar implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("PK_TERMINAL_CAR")
    private String pkTerminalCar;

    /**
     * 终端ID
     */
    @TableField("TERMINALID")
    private String terminalid;

    /**
     * 整车编号
     */
    @TableField("CARNO")
    private String carno;

    /**
     * 绑定日期
     */
    @TableField("BIND_TIME")
    private String bindTime;

    /**
     * 启用状态
     */
    @TableField("ENABLESTATE")
    private Long enablestate;

    @TableField("DR")
    private Long dr;

    @TableField("CREATIONTIME")
    private String creationtime;

    @TableField("CREATOR")
    private String creator;

    @TableField("MODIFIEDTIME")
    private String modifiedtime;

    @TableField("MODIFIER")
    private String modifier;

    @TableField("VERSION")
    private Long version;

    @TableField("PK_ORG")
    private String pkOrg;

    /**
     * 解绑日期
     */
    @TableField("UNBUNDTIME")
    private String unbundtime;


}
