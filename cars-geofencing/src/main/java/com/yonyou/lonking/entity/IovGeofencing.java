package com.yonyou.lonking.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author shendw
 * @since 2021-01-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("IOV_GEOFENCING")
public class IovGeofencing implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("ID")
    private String id;

    @TableField("PK_TERMINAL_CAR")
    @NotNull
    private String pkTerminalCar;

    @TableField("NAME")
    @NotNull
    private String name;

    @TableField("CENTER")
    @NotNull
    private String center;

    @TableField("RADIUS")
    @NotNull
    private String radius;

    @TableField("ENABLE")
    private Long enable;

    @TableField("CREATE_TIME")
    private String createTime;

    @TableField("POINTS")
    private String points;

    @TableLogic
    private Long dr;


}
