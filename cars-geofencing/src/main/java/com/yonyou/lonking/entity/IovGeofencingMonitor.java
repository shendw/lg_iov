package com.yonyou.lonking.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author shendw
 * @since 2021-01-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("IOV_GEOFENCING_MONITOR")
public class IovGeofencingMonitor implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("ID")
    private String id;

    /**
     * 终端号
     */
    @TableField("TERMINALID")
    private String terminalid;

    /**
     * 整机编号
     */
    @TableField("CARNO")
    private String carno;

    /**
     * 代理商名称
     */
    @TableField("AGENT_NAME")
    private String agentName;

    /**
     * 代理商编码
     */
    @TableField("AGENT_CODE")
    private String agentCode;

    /**
     * 代理商主键
     */
    @TableField("AGENT_ID")
    private String agentId;

    /**
     * 围栏类型 terminal,agent
     */
    @TableField("GEOFENCING_TYPE")
    private String geofencingType;

    /**
     * 时间
     */
    @TableField("CREATE_TIME")
    private String createTime;

    /**
     * 动作 in,out
     */
    @TableField("ACTION")
    private String action;

    /**
     * 围栏ID
     */
    @TableField("GEOFENCING_ID")
    private String geofencingId;

    /**
     * 围栏名称
     */
    @TableField("GEOFENCING_NAME")
    private String geofencingName;

    /**
     * 逻辑删除
     */
    @TableField("DR")
    private Long dr;

    @TableField("PK_TERMINAL_CAR")
    private String pkTerminalCar;

}
