package com.yonyou.lonking.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.yonyou.lonking.client.OccbaseClient;
import com.yonyou.lonking.dto.occ.CustomerDto;
import com.yonyou.lonking.dto.occ.FeignPage;
import com.yonyou.lonking.service.ICustomerService;
import com.yonyou.lonking.service.IIovGeofencingAgentService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Size;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

/**
 * @author shendawei
 * @创建时间 2021/1/21
 * @描述
 **/
@Service
public class CustomerServiceImpl implements ICustomerService {

    @Autowired
    private OccbaseClient occbaseClient;

    @Autowired
    private IIovGeofencingAgentService iovGeofencingAgentServiceImpl;

    @Autowired
    private IovTerminalCarServiceImpl iovTerminalCarServiceImpl;

    @Override
    public FeignPage<CustomerDto> getCustomerList(int pageNum, int pageSize, String name) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("page",pageNum-1);
        params.put("size",pageSize);
        params.put("search_EQ_isChannelCustomer",1);
        params.put("search_EQ_state",3);
        params.put("search_AUTH_APPCODE","customer");
        if (StringUtils.isNotBlank(name)){
            try {
                params.put("search_LIKE_name", URLEncoder.encode("%"+name+"%", "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        FeignPage<CustomerDto> customers = occbaseClient.getCustomers(params);
        List<CustomerDto> content = customers.getContent();
        if (CollectionUtil.isNotEmpty(content)){
            content.forEach(x ->{
                @Size(max = 40) String id = x.getId();
                x.setIovGeofencingAgents(iovGeofencingAgentServiceImpl.findGeoFencingByAgent(id));
                x.setIovTerminalCarDtos(iovTerminalCarServiceImpl.getIovCarDocByAgentName(x.getName()));
            });
        }
        return customers;
    }
}
