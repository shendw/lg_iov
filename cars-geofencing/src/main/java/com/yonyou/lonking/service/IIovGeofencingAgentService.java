package com.yonyou.lonking.service;

import com.yonyou.lonking.entity.IovGeofencingAgent;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author shendw
 * @since 2021-01-22
 */
public interface IIovGeofencingAgentService extends IService<IovGeofencingAgent> {
    List<IovGeofencingAgent> findGeoFencingByAgent(String agentId);

    List<IovGeofencingAgent> findGeoFencingByAgentName(String name);

    boolean createGeoFencingByAgent(IovGeofencingAgent iovGeofencingAgent);

    boolean deleteGeoFencingByAgent(String id);

    boolean updateGeoFencingByAgent(String id,String center,String radius);
}
