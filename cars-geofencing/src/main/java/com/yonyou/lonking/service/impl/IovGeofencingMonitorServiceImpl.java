package com.yonyou.lonking.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yonyou.lonking.entity.IovGeofencingMonitor;
import com.yonyou.lonking.mapper.IovGeofencingMonitorMapper;
import com.yonyou.lonking.service.IIovGeofencingMonitorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author shendw
 * @since 2021-01-25
 */
@Service
public class IovGeofencingMonitorServiceImpl extends ServiceImpl<IovGeofencingMonitorMapper, IovGeofencingMonitor> implements IIovGeofencingMonitorService {
    @Override
    public IovGeofencingMonitor getLastGeoFencingMonitor(String geofencingType, String pkTerminalCar, String agentName) {
        LambdaQueryWrapper<IovGeofencingMonitor> queryWrapper = Wrappers.lambdaQuery(IovGeofencingMonitor.class);
        queryWrapper.eq(IovGeofencingMonitor::getGeofencingType,geofencingType);
        queryWrapper.eq(StringUtils.isNotBlank(pkTerminalCar),IovGeofencingMonitor::getPkTerminalCar,pkTerminalCar);
        queryWrapper.eq(StringUtils.isNotBlank(agentName),IovGeofencingMonitor::getAgentName,agentName);
        queryWrapper.orderByDesc(IovGeofencingMonitor::getCreateTime);
        queryWrapper.last(") where rownum = 1");
        queryWrapper.first("select * from (");
        return getOne(queryWrapper);
    }

    @Override
    public Page<IovGeofencingMonitor> getMonitor(Page page, String carno, String terminalId, String agentName) {
        LambdaQueryWrapper<IovGeofencingMonitor> queryWrapper = Wrappers.lambdaQuery(IovGeofencingMonitor.class);
        queryWrapper.like(StringUtils.isNotBlank(carno),IovGeofencingMonitor::getCarno,carno);
        queryWrapper.like(StringUtils.isNotBlank(terminalId),IovGeofencingMonitor::getTerminalid,terminalId);
        queryWrapper.like(StringUtils.isNotBlank(agentName),IovGeofencingMonitor::getAgentName,agentName);
        queryWrapper.orderByDesc(IovGeofencingMonitor::getCreateTime);
        return page(page,queryWrapper);
    }
}
