package com.yonyou.lonking.service;

import com.yonyou.lonking.dto.occ.CustomerDto;
import com.yonyou.lonking.dto.occ.FeignPage;

/**
 * @author shendawei
 * @创建时间 2021/1/21
 * @描述
 **/
public interface ICustomerService {
    FeignPage<CustomerDto> getCustomerList(int pageNum, int pageSize, String name);
}
