package com.yonyou.lonking.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yonyou.lonking.dto.IovTerminalCarDto;
import com.yonyou.lonking.entity.IovTerminalCar;
import com.yonyou.lonking.mapper.IovTerminalCarMapper;
import com.yonyou.lonking.service.IIovTerminalCarService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.sql.Wrapper;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author shendw
 * @since 2021-01-18
 */
@Service
public class IovTerminalCarServiceImpl extends ServiceImpl<IovTerminalCarMapper, IovTerminalCar> implements IIovTerminalCarService {
    @Override
    public Page<IovTerminalCar> selectEnableByPage(Page page,String terminalid,String carno) {
        LambdaQueryWrapper<IovTerminalCar> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(IovTerminalCar::getEnablestate,0L);
        if (StringUtils.isNotEmpty(terminalid)){
            queryWrapper.like(IovTerminalCar::getTerminalid,terminalid);
        }
        if (StringUtils.isNotEmpty(carno)){
            queryWrapper.like(IovTerminalCar::getCarno,carno);
        }
        return this.page(page,queryWrapper);
    }

    @Override
    public Page<IovTerminalCarDto> selectCarInfoByPage(Page page, String terminalid, String carno) {
        QueryWrapper<IovTerminalCar> queryWrapper = new QueryWrapper();
        queryWrapper.eq("itc.ENABLESTATE",0L);
        queryWrapper.like(StringUtils.isNotEmpty(terminalid),"itc.terminalid",terminalid);
        queryWrapper.like(StringUtils.isNotEmpty(carno),"itc.carno",carno);
        return baseMapper.selectIovTerminalCar(page,queryWrapper);
    }

    @Override
    public List<IovTerminalCarDto> getIovCarDocByTerminalid(String terminalid) {
        return baseMapper.getIovCarDocByTerminalid(terminalid);
    }

    @Override
    public List<IovTerminalCarDto> getIovCarDocByAgentName(String agentName) {
        return baseMapper.getIovCarDocByAgentName(agentName);
    }
}
