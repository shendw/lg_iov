package com.yonyou.lonking.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yonyou.lonking.dto.IovTerminalCarDto;
import com.yonyou.lonking.entity.IovTerminalCar;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author shendw
 * @since 2021-01-18
 */
public interface IIovTerminalCarService extends IService<IovTerminalCar> {
    Page<IovTerminalCar> selectEnableByPage(Page page,String terminalid,String carno);

    Page<IovTerminalCarDto> selectCarInfoByPage(Page page, String terminalid, String carno);

    List<IovTerminalCarDto> getIovCarDocByTerminalid(String terminalid);

    List<IovTerminalCarDto> getIovCarDocByAgentName(String agentName);
}
