package com.yonyou.lonking.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.yonyou.lonking.client.AMAPClient;
import com.yonyou.lonking.client.OccbaseClient;
import com.yonyou.lonking.dto.occ.CustomerAddressDto;
import com.yonyou.lonking.dto.occ.CustomerDto;
import com.yonyou.lonking.dto.occ.FeignPage;
import com.yonyou.lonking.dto.occ.ShopDto;
import com.yonyou.lonking.entity.IovGeofencing;
import com.yonyou.lonking.entity.IovGeofencingAgent;
import com.yonyou.lonking.enums.IsEnable;
import com.yonyou.lonking.exception.BusinessException;
import com.yonyou.lonking.mapper.IovGeofencingAgentMapper;
import com.yonyou.lonking.service.ICustomerService;
import com.yonyou.lonking.service.IIovGeofencingAgentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yonyou.lonking.utils.ResponseCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Size;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author shendw
 * @since 2021-01-22
 */
@Service
@Slf4j
public class IovGeofencingAgentServiceImpl extends ServiceImpl<IovGeofencingAgentMapper, IovGeofencingAgent> implements IIovGeofencingAgentService {

    @Autowired
    private OccbaseClient occbaseClient;

    @Autowired
    private AMAPClient amapClient;

    @Value("${amapkey}")
    private String amapkey;

    @Value("${geofencing.radius}")
    private String radius;

    @Autowired
    private ICustomerService customerServiceImpl;

    @Override
    public List<IovGeofencingAgent> findGeoFencingByAgent(String agentId) {
        LambdaQueryWrapper<IovGeofencingAgent> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(IovGeofencingAgent::getAgentId,agentId);
        List<IovGeofencingAgent> geofencingAgents = list(queryWrapper);
        //如果没找到代理商的围栏，需要初始化围栏
        if (CollectionUtil.isEmpty(geofencingAgents)){
            initGeoFencingAgent(agentId);
//            List<CustomerAddressDto> customer = occbaseClient.getCustomerAddress(agentId, "customer");
//            if (CollectionUtil.isNotEmpty(customer)){
//                customer.forEach(x ->{
//                    if (StringUtils.isNotBlank(x.getDetailAddr())){
//                        Map<String,Object> address = amapClient.address(amapkey,x.getDetailAddr());
//                        if ("OK".equals(address.get("info"))){
//                            ArrayList geocodes = (ArrayList)address.get("geocodes");
//                            String location = (String)((Map<String, Object>) geocodes.get(0)).get("location");
//                            IovGeofencingAgent iovGeofencingAgent = new IovGeofencingAgent();
//                            iovGeofencingAgent.setName(x.getDetailAddr());
//                            iovGeofencingAgent.setAgentCode(x.getCustomerCode());
//                            iovGeofencingAgent.setAgentName(x.getCustomerName());
//                            iovGeofencingAgent.setAgentId(x.getCustomerId());
//                            iovGeofencingAgent.setCenter(location);
//                            iovGeofencingAgent.setRadius("5000");
//                            save(iovGeofencingAgent);
//                        }
//                    }
//                });
//            }
            geofencingAgents = list(queryWrapper);
        }
        return geofencingAgents;
    }

    @Override
    public List<IovGeofencingAgent> findGeoFencingByAgentName(String name) {
        LambdaQueryWrapper<IovGeofencingAgent> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(IovGeofencingAgent::getAgentName,name);
        List<IovGeofencingAgent> geofencingAgents = list(queryWrapper);
        //如果没找到代理商的围栏，需要初始化围栏
        if (CollectionUtil.isEmpty(geofencingAgents)){
            FeignPage<CustomerDto> customerList = customerServiceImpl.getCustomerList(1, 10, name);
            List<CustomerDto> content = customerList.getContent();
            if (CollectionUtil.isNotEmpty(content)){
                @Size(max = 40) String agentId = content.get(0).getId();
                initGeoFencingAgent(agentId);
//                List<CustomerAddressDto> customer = occbaseClient.getCustomerAddress(agentId, "customer");
//                if (CollectionUtil.isNotEmpty(customer)){
//                    customer.forEach(x ->{
//                        if (StringUtils.isNotBlank(x.getDetailAddr())){
//                            Map<String,Object> address = amapClient.address(amapkey,x.getDetailAddr());
//                            if ("OK".equals(address.get("info"))){
//                                ArrayList geocodes = (ArrayList)address.get("geocodes");
//                                String location = (String)((Map<String, Object>) geocodes.get(0)).get("location");
//                                IovGeofencingAgent iovGeofencingAgent = new IovGeofencingAgent();
//                                iovGeofencingAgent.setName(x.getDetailAddr());
//                                iovGeofencingAgent.setAgentCode(x.getCustomerCode());
//                                iovGeofencingAgent.setAgentName(x.getCustomerName());
//                                iovGeofencingAgent.setAgentId(x.getCustomerId());
//                                iovGeofencingAgent.setCenter(location);
//                                iovGeofencingAgent.setRadius("5000");
//                                save(iovGeofencingAgent);
//                            }
//                        }
//                    });
//                }
                geofencingAgents = list(queryWrapper);
            }
        }
        return geofencingAgents;
    }


    private void initGeoFencingAgent(String agentId){
        HashMap<String, Object> params = new HashMap<>();
        params.put("page",0);
        params.put("size",100);
        params.put("search_AUTH_APPCODE","shop");
        params.put("search_EQ_customer.id", agentId);
        FeignPage<ShopDto> shop = occbaseClient.getShop(params);
        if (shop.hasContent() && shop.getContentSize() > 0 ){
            List<ShopDto> shopDtos = shop.getContent();
            shopDtos.forEach(x ->{
                if (StringUtils.isNotBlank(x.getAddress())){
                    Map<String,Object> address = amapClient.address(amapkey,x.getAddress());
                    if ("OK".equals(address.get("info"))){
                        ArrayList geocodes = (ArrayList)address.get("geocodes");
                        if (CollectionUtil.isEmpty(geocodes)){
                            log.error("网点地址解析失败,网点地址：{},网点名称：{}",x.getAddress(),x.getName());
                            return;
                        }
                        String location = (String)((Map<String, Object>) geocodes.get(0)).get("location");
                        IovGeofencingAgent iovGeofencingAgent = new IovGeofencingAgent();
//                        iovGeofencingAgent.setName(x.getAddress());
                        iovGeofencingAgent.setName(x.getName());
                        iovGeofencingAgent.setAgentCode(x.getCustomerCode());
                        iovGeofencingAgent.setAgentName(x.getCustomerName());
                        iovGeofencingAgent.setAgentId(x.getCustomerId());
                        iovGeofencingAgent.setCenter(location);
                        iovGeofencingAgent.setRadius(radius);
                        save(iovGeofencingAgent);
                    }
                }
            });
        }
    }

    @Override
    public boolean createGeoFencingByAgent(IovGeofencingAgent iovGeofencingAgent) {
        LambdaQueryWrapper<IovGeofencingAgent> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(IovGeofencingAgent::getName,iovGeofencingAgent.getName()).eq(IovGeofencingAgent::getAgentId,iovGeofencingAgent.getAgentId());
        List<IovGeofencingAgent> list = list(queryWrapper);
        if (CollectionUtil.isNotEmpty(list)){
            throw new BusinessException(ResponseCode.GEOFENCING_IS_EXIST);
        }
        iovGeofencingAgent.setEnable(IsEnable.ENABLE.getValue());
        iovGeofencingAgent.setCreateTime(DateUtil.formatDateTime(new Date()));
        return save(iovGeofencingAgent);
    }

    @Override
    public boolean deleteGeoFencingByAgent(String id) {
        return removeById(id);
    }

    @Override
    public boolean updateGeoFencingByAgent(String id, String center, String radius) {
        IovGeofencingAgent iovGeofencingAgent = new IovGeofencingAgent();
        iovGeofencingAgent.setId(id);
        iovGeofencingAgent.setCenter(center);
        iovGeofencingAgent.setRadius(radius);
        return saveOrUpdate(iovGeofencingAgent);
    }
}
