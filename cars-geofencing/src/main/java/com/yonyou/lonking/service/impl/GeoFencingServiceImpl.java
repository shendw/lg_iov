package com.yonyou.lonking.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yonyou.lonking.utils.ResponseData;
import com.yonyou.lonking.client.AMAPClient;
import com.yonyou.lonking.httpclient.HttpGetWithEntity;
import com.yonyou.lonking.service.IGeoFencingService;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;

/**
 * @author shendawei
 * @创建时间 2021/1/12
 * @描述
 **/
@Service("iGeoFencingService")
public class GeoFencingServiceImpl implements IGeoFencingService {

    @Autowired
    private AMAPClient amapClient;

    @Value("${amapkey}")
    private String amapkey;

    public ResponseData createGeofencing() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("name","测试围栏名称");
        params.put("center","115.672126,38.817129");
        params.put("radius","1000");
        params.put("enable","true");
        params.put("valid_time","2050-05-19");
        params.put("repeat","Mon,Tues,Wed,Thur,Fri,Sat,Sun");
        params.put("time","00:00,23:59");
        params.put("alert_condition","enter;leave");
        Object result = amapClient.createFencing(amapkey, params);
        return null;
    }

    public ResponseData getGeoFencing() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("name","测试围栏名称");
        try {
            URI uri = new URI("https://restapi.amap.com/v4/geofence/meta?key="+amapkey);
            HttpGetWithEntity oHttpGet = new HttpGetWithEntity(uri);
            HttpEntity httpEntity = new StringEntity(JSON.toJSONString(params), ContentType.APPLICATION_JSON);
            oHttpGet.setEntity(httpEntity);
            CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            CloseableHttpResponse response = httpClient.execute(oHttpGet);
            HttpEntity responseEntity = response.getEntity();
            if (responseEntity != null) {
                String s = EntityUtils.toString(responseEntity);
                JSONObject jsonObject = JSON.parseObject(s);
                JSONObject data = jsonObject.getJSONObject("data");
                JSONArray rs_list = data.getJSONArray("rs_list");
                if (rs_list != null && rs_list.size() > 0){
                  for (int i = 0; i < rs_list.size(); i++) {
                      JSONObject rs = rs_list.getJSONObject(i);
                      String center = rs.getString("center");
                      BigDecimal radius = rs.getBigDecimal("radius");
                      String gid = rs.getString("gid");
                  }
                }
                return null;
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ResponseData updateGeoFencing(String gid, String name, String center, String radius) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("name","测试围栏名称");
        params.put("center",center);
        params.put("radius",radius);
        Object patch = amapClient.updateFencing(amapkey, gid, "patch", params);
        return null;
    }

    public ResponseData deleteFeoFencing(String gid) {
        Object delete = amapClient.deleteFencing(amapkey, gid, "delete");
        return null;
    }
}
