package com.yonyou.lonking.service;

import com.yonyou.lonking.utils.ResponseData;

/**
 * @author shendawei
 * @创建时间 2021/1/12
 * @描述
 **/
public interface IGeoFencingService {
    ResponseData createGeofencing();

    ResponseData getGeoFencing();

    ResponseData updateGeoFencing(String gid,String name,String center,String radius);

    ResponseData deleteFeoFencing(String gid);
}
