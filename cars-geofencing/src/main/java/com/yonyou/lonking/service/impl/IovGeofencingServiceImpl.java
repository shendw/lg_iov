package com.yonyou.lonking.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yonyou.lonking.entity.IovGeofencing;
import com.yonyou.lonking.entity.IovGeofencingAgent;
import com.yonyou.lonking.enums.IsEnable;
import com.yonyou.lonking.exception.BusinessException;
import com.yonyou.lonking.mapper.IovGeofencingMapper;
import com.yonyou.lonking.service.IIovGeofencingService;
import com.yonyou.lonking.utils.ResponseCode;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author shendw
 * @since 2021-01-16
 */
@Service
public class IovGeofencingServiceImpl extends ServiceImpl<IovGeofencingMapper, IovGeofencing> implements IIovGeofencingService {

    @Override
    public boolean createGeoFencingByCar(String pkTerminalCar, String name, String center, String radius) {
        LambdaQueryWrapper<IovGeofencing> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(IovGeofencing::getName,name).eq(IovGeofencing::getPkTerminalCar,pkTerminalCar);
        List<IovGeofencing> list = list(queryWrapper);
        if (CollectionUtil.isNotEmpty(list)){
            throw new BusinessException(ResponseCode.GEOFENCING_IS_EXIST);
        }
        IovGeofencing iovGeofencing = new IovGeofencing();
        iovGeofencing.setPkTerminalCar(pkTerminalCar);
        iovGeofencing.setName(name);
        iovGeofencing.setCenter(center);
        iovGeofencing.setRadius(radius);
        iovGeofencing.setEnable(IsEnable.DISABLE.getValue());
        iovGeofencing.setCreateTime(DateUtil.formatDateTime(new Date()));
        return this.save(iovGeofencing);
    }

    @Override
    public boolean deleteGeoFencingByCar(String id) {
        return removeById(id);
    }

    @Override
    public boolean updateGeoFencingByCar(String id, String center, String radius) {
        IovGeofencing iovGeofencing = new IovGeofencing();
        iovGeofencing.setId(id);
        iovGeofencing.setCenter(center);
        iovGeofencing.setRadius(radius);
        return saveOrUpdate(iovGeofencing);
    }

    @Override
    public List<IovGeofencing> findGeoFencingByCar(String pkTerminalCar) {
        LambdaQueryWrapper<IovGeofencing> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(IovGeofencing::getPkTerminalCar,pkTerminalCar);
        return list(queryWrapper);
    }
}
