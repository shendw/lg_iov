package com.yonyou.lonking.service;

import com.yonyou.lonking.entity.IovGeofencing;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yonyou.lonking.entity.IovGeofencingAgent;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author shendw
 * @since 2021-01-16
 */
public interface IIovGeofencingService extends IService<IovGeofencing> {

    boolean createGeoFencingByCar(String pkTerminalCar,String name,String center,String radius);

    boolean deleteGeoFencingByCar(String id);

    boolean updateGeoFencingByCar(String id,String center,String radius);

    List<IovGeofencing> findGeoFencingByCar(String pkTerminalCar);
}

