package com.yonyou.lonking.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yonyou.lonking.entity.IovGeofencingMonitor;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author shendw
 * @since 2021-01-25
 */
public interface IIovGeofencingMonitorService extends IService<IovGeofencingMonitor> {
    /**
     *
     * @param geofencingType terminal,agent
     * @param pkTerminalCar
     * @param agentName
     * @return
     */
    IovGeofencingMonitor getLastGeoFencingMonitor(String geofencingType,String pkTerminalCar,String agentName);

    Page<IovGeofencingMonitor> getMonitor(Page page,String carno,String terminalId,String agentName);
}
