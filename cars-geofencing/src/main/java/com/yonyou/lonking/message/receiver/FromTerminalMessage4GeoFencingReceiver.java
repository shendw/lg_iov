package com.yonyou.lonking.message.receiver;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateUtil;
import com.yonyou.lonking.dto.IovTerminalCarDto;
import com.yonyou.lonking.dto.iot.IotModelParamDto;
import com.yonyou.lonking.entity.IovGeofencing;
import com.yonyou.lonking.entity.IovGeofencingAgent;
import com.yonyou.lonking.entity.IovGeofencingMonitor;
import com.yonyou.lonking.enums.CarProcessStatus;
import com.yonyou.lonking.message.sink.FromTerminalMessage4GeoFencingSink;
import com.yonyou.lonking.service.*;
import com.yonyou.lonking.utils.GeoFencingUtils;
import com.yonyou.lonking.utils.TransLocationsUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.MessageHeaders;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author shendawei
 * @创建时间 2021/1/25
 * @描述
 **/
@EnableBinding(FromTerminalMessage4GeoFencingSink.class)
@Slf4j
public class FromTerminalMessage4GeoFencingReceiver {

    @Autowired
    private IIovGeofencingService iovGeofencingServiceImpl;

    @Autowired
    private IIovTerminalCarService iovTerminalCarServiceImpl;

    @Autowired
    private IIovGeofencingAgentService iovGeofencingAgentServiceImpl;

    @Autowired
    private IIovGeofencingMonitorService iovGeofencingMonitorServiceImpl;

    @StreamListener(FromTerminalMessage4GeoFencingSink.INPUT)
    public void receive(Map<String, IotModelParamDto> payload, MessageHeaders messageHeaders) {
        for (Map.Entry<String, IotModelParamDto> x : payload.entrySet()) {
            IotModelParamDto iotModelParamDto = x.getValue();
            String terminalid = iotModelParamDto.getDevice();
            String value = iotModelParamDto.getValue();
            String name = iotModelParamDto.getName();
            if ("经纬度".equals(name)) {
                if (value.contains("0.000000")) {
                    break;
                }
                Double[] lonAndLat = TransLocationsUtil.calWGS84toGCJ02(value);
                if (lonAndLat != null && lonAndLat.length == 2) {
                    List<IovTerminalCarDto> iovCarDocByTerminalids = iovTerminalCarServiceImpl.getIovCarDocByTerminalid(terminalid);
                    if (CollectionUtil.isEmpty(iovCarDocByTerminalids)) {
                        break;
                    }
                    for (IovTerminalCarDto iovCarDocByTerminalid : iovCarDocByTerminalids) {
                        String pkTerminalCar = iovCarDocByTerminalid.getPkTerminalCar();
                        //代理商纬度
                        //判断车辆的状态，要是代理商库存的,判断代理商是否有围栏
                        //如果没有围栏直接跳过
                        //如果有围栏，获取该车历史最新的监控记录
                        //有记录说明激活过代理商围栏，根据获取到的记录判断该条数据是否需要更新一条监控记录
                        //没有记录说明没有激活过代理商围栏，只判断是否在代理商围栏内，如果在围栏内，则记录一条进入围栏的记录
                        if (CarProcessStatus.STOCK.getCode() == iovCarDocByTerminalid.getStatus()) {
                            String serviceAgent = iovCarDocByTerminalid.getServiceAgent();
                            List<IovGeofencingAgent> geoFencingByAgentName = iovGeofencingAgentServiceImpl.findGeoFencingByAgentName(serviceAgent);
                            if (CollectionUtil.isNotEmpty(geoFencingByAgentName)) {
                                IovGeofencingMonitor agentMonitor = iovGeofencingMonitorServiceImpl.getLastGeoFencingMonitor("agent", null, serviceAgent);
                                //如果没有代理商围栏的记录，判断该坐标是否在代理商围栏内，如果在记录一条。
                                if (agentMonitor != null && "in".equals(agentMonitor.getAction())) {
                                    String geofencingId = agentMonitor.getGeofencingId();
                                    geoFencingByAgentName.stream().forEach(iovGeofencingAgent -> {
                                        if (geofencingId.equals(iovGeofencingAgent.getId())) {
                                            String center = iovGeofencingAgent.getCenter();
                                            String radius = iovGeofencingAgent.getRadius();
                                            Double[] centerFormat = TransLocationsUtil.getLonAndLat(center);
                                            if (!GeoFencingUtils.isInCircle(lonAndLat[0], lonAndLat[1], centerFormat[0], centerFormat[1], Double.parseDouble(radius))) {
                                                saveAgentMonitor("out", iovGeofencingAgent, iovCarDocByTerminalid);
                                            }
                                        }
                                    });
                                } else {
                                    geoFencingByAgentName.stream().forEach(geofencingAgent -> {
                                        String center = geofencingAgent.getCenter();
                                        String radius = geofencingAgent.getRadius();
                                        Double[] centerFormat = TransLocationsUtil.getLonAndLat(center);
                                        if (GeoFencingUtils.isInCircle(lonAndLat[0], lonAndLat[1], centerFormat[0], centerFormat[1], Double.parseDouble(radius))) {
                                            saveAgentMonitor("in", geofencingAgent, iovCarDocByTerminalid);
                                        }
                                    });
                                }
                            }
                        }
                        //车辆纬度
                        //1.判断车辆有没有电子围栏
                        //2.判断车辆有没有历史记录
                        List<IovGeofencing> geoFencingByCar = iovGeofencingServiceImpl.findGeoFencingByCar(pkTerminalCar);
                        if (CollectionUtil.isNotEmpty(geoFencingByCar)) {
                            IovGeofencingMonitor terminalGeofencingMonitor = iovGeofencingMonitorServiceImpl.getLastGeoFencingMonitor("terminal", pkTerminalCar, null);
                            if (terminalGeofencingMonitor != null && "in".equals(terminalGeofencingMonitor.getAction())) {
                                String geofencingId = terminalGeofencingMonitor.getGeofencingId();
                                geoFencingByCar.stream().forEach(geoFencing -> {
                                    if (geofencingId.equals(geoFencing.getId())) {
                                        String center = geoFencing.getCenter();
                                        String radius = geoFencing.getRadius();
                                        Double[] centerFormat = TransLocationsUtil.getLonAndLat(center);
                                        if (!GeoFencingUtils.isInCircle(lonAndLat[0], lonAndLat[1], centerFormat[0], centerFormat[1], Double.parseDouble(radius))) {
                                            saveTerminalMonitor("out", geoFencing, iovCarDocByTerminalid);
                                        }
                                    }
                                });
                            } else {
                                geoFencingByCar.stream().forEach(geoFencing -> {
                                    String center = geoFencing.getCenter();
                                    String radius = geoFencing.getRadius();
                                    Double[] centerFormat = TransLocationsUtil.getLonAndLat(center);
                                    if (GeoFencingUtils.isInCircle(lonAndLat[0], lonAndLat[1], centerFormat[0], centerFormat[1], Double.parseDouble(radius))) {
                                        saveTerminalMonitor("in", geoFencing, iovCarDocByTerminalid);
                                    }
                                });
                            }
                        }
                    }
                }
            }
        }
    }

    private void saveAgentMonitor(String action, IovGeofencingAgent iovGeofencingAgent, IovTerminalCarDto iovCarDocByTerminalid) {
        IovGeofencingMonitor iovGeofencingMonitor = new IovGeofencingMonitor();
        iovGeofencingMonitor.setAction(action);
        iovGeofencingMonitor.setAgentCode(iovGeofencingAgent.getAgentCode());
        iovGeofencingMonitor.setAgentId(iovGeofencingAgent.getAgentId());
        iovGeofencingMonitor.setAgentName(iovGeofencingAgent.getAgentName());
        iovGeofencingMonitor.setCarno(iovCarDocByTerminalid.getCarno());
        iovGeofencingMonitor.setTerminalid(iovCarDocByTerminalid.getTerminalid());
        iovGeofencingMonitor.setGeofencingType("agent");
        iovGeofencingMonitor.setPkTerminalCar(iovCarDocByTerminalid.getPkTerminalCar());
        iovGeofencingMonitor.setCreateTime(DateUtil.formatDateTime(new Date()));
        iovGeofencingMonitor.setGeofencingId(iovGeofencingAgent.getId());
        iovGeofencingMonitor.setGeofencingName(iovGeofencingAgent.getName());
        iovGeofencingMonitorServiceImpl.save(iovGeofencingMonitor);
    }

    private void saveTerminalMonitor(String action, IovGeofencing geofencing, IovTerminalCarDto iovCarDocByTerminalid) {
        IovGeofencingMonitor iovGeofencingMonitor = new IovGeofencingMonitor();
        iovGeofencingMonitor.setAction(action);
        iovGeofencingMonitor.setCarno(iovCarDocByTerminalid.getCarno());
        iovGeofencingMonitor.setTerminalid(iovCarDocByTerminalid.getTerminalid());
        iovGeofencingMonitor.setGeofencingType("terminal");
        iovGeofencingMonitor.setPkTerminalCar(iovCarDocByTerminalid.getPkTerminalCar());
        iovGeofencingMonitor.setCreateTime(DateUtil.formatDateTime(new Date()));
        iovGeofencingMonitor.setGeofencingId(geofencing.getId());
        iovGeofencingMonitor.setGeofencingName(geofencing.getName());
        iovGeofencingMonitorServiceImpl.save(iovGeofencingMonitor);
    }
}
