package com.yonyou.lonking.enums;

import lombok.Getter;

/**
 * @author shendawei
 * @创建时间 2021/1/22
 * @描述
 **/
@Getter
public enum IsEnable {

    ENABLE(1L),
    DISABLE(0L);

    private long value;

    IsEnable(long value){
        this.value = value;
    }
}
