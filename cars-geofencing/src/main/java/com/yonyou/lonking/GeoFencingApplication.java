package com.yonyou.lonking;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringCloudApplication
@EnableTransactionManagement
@EnableFeignClients(basePackages = "com.yonyou.lonking.*")
@MapperScan(basePackages = "com.yonyou.lonking.mapper")
public class GeoFencingApplication {
    public static void main(String[] args) {
        SpringApplication.run(GeoFencingApplication.class, args);
    }

}
