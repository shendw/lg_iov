package com.yonyou.longgong;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringCloudApplication
@EnableTransactionManagement
@EnableFeignClients(basePackages = "com.yonyou.longgong.*.client")
@MapperScan(basePackages = "com.yonyou.longgong")
public class CarsServerApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CarsServerApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(CarsServerApplication.class, args);
    }

}
