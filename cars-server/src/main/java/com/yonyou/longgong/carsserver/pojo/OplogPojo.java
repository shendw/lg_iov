package com.yonyou.longgong.carsserver.pojo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OplogPojo implements Serializable {
    public static final Long IS_DELETED = Dr.IS_DELETED.value();

    public static final Long NOT_DELETED = Dr.NOT_DELETED.value();

    private String pkOplog;

    @Excel(name = "终端ID")
    private String terminalid;

    @Excel(name = "通讯通道")
    private String channel;

    @Excel(name = "操作时间")
    private String optime;

    @Excel(name = "方向")
    private String direction;

    @Excel(name = "报文内容")
    private String detail;

    @Excel(name = "命令关键字")
    private String opkeyword;

    @Excel(name = "是否操作成功")
    private String issuccess;

    private String creationtime;

    private String creator;

    private String modifiedtime;

    private String modifier;

    private Long dr;

    private Long version;

    private String pkOrg;

    public OplogPojo(String terminalid, String channel, String optime, String direction, String detail, String opkeyword, String issuccess) {
        this.terminalid = terminalid;
        this.channel = channel;
        this.optime = optime;
        this.direction = direction;
        this.detail = detail;
        this.opkeyword = opkeyword;
        this.issuccess = issuccess;
    }

    private static final long serialVersionUID = 1L;

    public void andLogicalDeleted(boolean deleted) {
        setDr(deleted ? Dr.IS_DELETED.value() : Dr.NOT_DELETED.value());
    }

    public enum Dr {
        NOT_DELETED(new Long("0"), "未删除"),
        IS_DELETED(new Long("1"), "已删除");

        private final Long value;

        private final String name;

        Dr(Long value, String name) {
            this.value = value;
            this.name = name;
        }

        public Long getValue() {
            return this.value;
        }

        public Long value() {
            return this.value;
        }

        public String getName() {
            return this.name;
        }
    }

    public enum Column {
        pkOplog("PK_OPLOG", "pkOplog", "VARCHAR", false),
        terminalid("TERMINALID", "terminalid", "VARCHAR", false),
        channel("CHANNEL", "channel", "VARCHAR", false),
        optime("OPTIME", "optime", "CHAR", false),
        direction("DIRECTION", "direction", "VARCHAR", false),
        detail("DETAIL", "detail", "VARCHAR", false),
        creationtime("CREATIONTIME", "creationtime", "CHAR", false),
        creator("CREATOR", "creator", "VARCHAR", false),
        modifiedtime("MODIFIEDTIME", "modifiedtime", "CHAR", false),
        modifier("MODIFIER", "modifier", "VARCHAR", false),
        dr("DR", "dr", "DECIMAL", false),
        version("VERSION", "version", "DECIMAL", false),
        pkOrg("PK_ORG", "pkOrg", "VARCHAR", false),
        opkeyword("OPKEYWORD", "opkeyword", "VARCHAR", false),
        issuccess("ISSUCCESS", "issuccess", "VARCHAR", false);

        private static final String BEGINNING_DELIMITER = "\"";

        private static final String ENDING_DELIMITER = "\"";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}