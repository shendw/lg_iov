package com.yonyou.longgong.carsserver.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dao.CardocPojoMapper;
import com.yonyou.longgong.carsserver.dao.TerminalCarPojoMapper;
import com.yonyou.longgong.carsserver.dao.TerminalPojoMapper;
import com.yonyou.longgong.carsserver.dto.CarInfoSelectDto;
import com.yonyou.longgong.carsserver.entity.UserIdentity;
import com.yonyou.longgong.carsserver.enums.CarWorkStatus;
import com.yonyou.longgong.carsserver.enums.TerminalCarStatus;
import com.yonyou.longgong.carsserver.example.CardocPojoExample;
import com.yonyou.longgong.carsserver.example.TerminalCarPojoExample;
import com.yonyou.longgong.carsserver.example.TerminalPojoExample;
import com.yonyou.longgong.carsserver.exception.BusinessException;
import com.yonyou.longgong.carsserver.pojo.CardocPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalCarPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalPojo;
import com.yonyou.longgong.carsserver.service.ICarInfoService;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.StringUtil;
import com.yonyou.longgong.carsserver.utils.UserIdentityUtil;
import com.yonyou.longgong.carsserver.vo.CarInfoVo;
import com.yonyou.longgong.iotserver.common.Const;
import com.yonyou.longgong.iotserver.service.IDeviceService;
import com.yonyou.longgong.iotserver.utils.LocateByAmapUtils;
import com.yonyou.longgong.iotserver.utils.RedisUtils;
import com.yonyou.longgong.iotserver.utils.TransLocationsUtil;
import com.yonyou.longgong.iotserver.vo.DeviceDataVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author shendawei
 * @创建时间 2019-11-11
 * @描述
 **/
@Service("iCarInfoService")
public class CarInfoServiceImpl implements ICarInfoService {

    @Autowired
    private TerminalPojoMapper terminalPojoMapper;

    @Autowired
    private TerminalCarPojoMapper terminalCarPojoMapper;

    @Autowired
    private CardocPojoMapper cardocPojoMapper;

    @Autowired
    private IDeviceService deviceService;

    @Autowired
    private UserIdentityUtil userIdentityUtil;

    @Autowired
    private RedisUtils redis;

    @Override
    public PageInfo<CarInfoVo> selectCarInfo(CarInfoSelectDto carInfoSelectDto,String userId,String loginName) {
        UserIdentity userIdentity = userIdentityUtil.getUserIdentity(userId, loginName);
        HashMap<String, Object> paramMap = new HashMap<>(16);
        List<String> orgs = userIdentity.getOrgs();
        paramMap.put("orgs",orgs);
        String saleAgent = carInfoSelectDto.getSaleAgent();
        paramMap.put("saleagent",saleAgent);
        //如果是渠道商，那必须过滤渠道商
        String agentName = userIdentity.getAgentName();
        if (agentName != null){
            paramMap.put("saleagent",agentName);
        }
        paramMap.put("agentname",agentName);
        String terminalid = carInfoSelectDto.getTerminalid();
        paramMap.put("terminalid",terminalid);
        String simid = carInfoSelectDto.getSimid();
        paramMap.put("simid",simid);
        String terminalVersion = carInfoSelectDto.getTerminalVersion();
        paramMap.put("terminalversion",terminalVersion);
        String carmodel = carInfoSelectDto.getMachineType();
        paramMap.put("carmodel",carmodel);
        String carNo = carInfoSelectDto.getCarNo();
        paramMap.put("carno",carNo);
        String customer = carInfoSelectDto.getCustomer();
        paramMap.put("customer",customer);
        String status = carInfoSelectDto.getProcessStatus();
        paramMap.put("status",status);
        String workStatus = carInfoSelectDto.getWorkStatus();
        paramMap.put("workstatus",workStatus);
        //新老平台过滤
        paramMap.put("isnew",carInfoSelectDto.getIsnew());
        //最新通讯时间
        paramMap.put("lasttimebegin",carInfoSelectDto.getLastTimeBegin());
        paramMap.put("lasttimeend",carInfoSelectDto.getLastTimeEnd());
        //最新登陆时间
        paramMap.put("lastloginbegin",carInfoSelectDto.getStartTime());
        paramMap.put("lastloginend",carInfoSelectDto.getEndTime());
        //锁车状态
        paramMap.put("lockstatus",carInfoSelectDto.getLockStatus());
        paramMap.put("machinemodel",carInfoSelectDto.getMachinemodel());
        //通过过滤完的整车编号去执行最终结果
        PageHelper.startPage(carInfoSelectDto.getPageNum(), carInfoSelectDto.getPageSize());
        List<CarInfoVo> carInfoVos = cardocPojoMapper.select4CarInfo(paramMap);
        PageInfo<CarInfoVo> cardocPojoPageInfo = new PageInfo<>(carInfoVos);
        //查找绑定关系,并绑定数据
        if (!CollectionUtils.isEmpty(carInfoVos)){
            for (CarInfoVo carInfoVo : carInfoVos) {
                String coordinates = carInfoVo.getCoordinates();
                coordinates = TransLocationsUtil.transLocation(coordinates);
                if (StringUtils.isNotEmpty(coordinates)){
                    String[] locationinfo = coordinates.split(",");
                    carInfoVo.setLongitude(locationinfo[0]);
                    carInfoVo.setLatitude(locationinfo[1]);
                    carInfoVo.setLocation(LocateByAmapUtils.getAddress(coordinates));
                }
                /*String finalTerminal = carInfoVo.getTerminalid();
                if (StringUtils.isNotEmpty(finalTerminal)){
                    Map<Object, Object> dataMap = redis.hmget(Const.PREFIX_TERMINAL+finalTerminal);
                    if (dataMap != null && !dataMap.isEmpty()){
                        Object location = dataMap.get(Const.LOCATION);
                        if (location != null){
                            String coordinates = location.toString();
                            coordinates = TransLocationsUtil.transLocation(coordinates);
                            if (StringUtils.isNotEmpty(coordinates)){
                                String[] locationinfo = coordinates.split(",");
                                carInfoVo.setLongitude(locationinfo[0]);
                                carInfoVo.setLatitude(locationinfo[1]);
                                carInfoVo.setLocation(LocateByAmapUtils.getAddress(coordinates));
                            }
                        }
                        Object worktime = dataMap.get(Const.WORKTIME);
                        if (worktime != null){
                            carInfoVo.setWorktime(worktime.toString());
                        }
                    }
                    //拼装iot信息
//                    DeviceDataVO deviceData = deviceService.getDeviceData(finalTerminal);
//                    if (deviceData != null) {
//                        String coordinates = deviceData.getCoordinates();
//                        if (StringUtils.isNotBlank(coordinates)) {
//                            String[] locationinfo = coordinates.split(",");
//                            carInfoVo.setLongitude(locationinfo[0]);
//                            carInfoVo.setLatitude(locationinfo[1]);
//                            carInfoVo.setLocation(LocateByAmapUtils.getAddress(deviceData.getCoordinates()));
//                        }
//                        carInfoVo.setLocationTime(deviceData.getGps_time());
//                        carInfoVo.setChannel(deviceData.getCommunication_mode());
//                        carInfoVo.setWorktime(deviceData.getEngine_running_duration());
//                        carInfoVo.setLastloginTime(deviceData.getLatest_landing_time());
//                    }
                }*/
            }
        }
        return cardocPojoPageInfo;
    }

    @Override
    public List<CarInfoVo> selectCarInfoByExcel(CarInfoSelectDto carInfoSelectDto,String userId,String loginName) {
        UserIdentity userIdentity = userIdentityUtil.getUserIdentity(userId, loginName);
        HashMap<String, Object> paramMap = new HashMap<>(16);
        List<String> orgs = userIdentity.getOrgs();
        paramMap.put("orgs",orgs);
        String saleAgent = carInfoSelectDto.getSaleAgent();
        paramMap.put("saleagent",saleAgent);
        //如果是渠道商，那必须过滤渠道商
        String agentName = userIdentity.getAgentName();
        if (agentName != null){
            paramMap.put("saleagent",agentName);
        }
        paramMap.put("agentname",agentName);
        String terminalid = carInfoSelectDto.getTerminalid();
        paramMap.put("terminalid",terminalid);
        String simid = carInfoSelectDto.getSimid();
        paramMap.put("simid",simid);
        String terminalVersion = carInfoSelectDto.getTerminalVersion();
        paramMap.put("terminalversion",terminalVersion);
        String carmodel = carInfoSelectDto.getMachineType();
        paramMap.put("carmodel",carmodel);
        String carNo = carInfoSelectDto.getCarNo();
        paramMap.put("carno",carNo);
        String customer = carInfoSelectDto.getCustomer();
        paramMap.put("customer",customer);
        String status = carInfoSelectDto.getProcessStatus();
        paramMap.put("status",status);
        String workStatus = carInfoSelectDto.getWorkStatus();
        paramMap.put("workstatus",workStatus);
        //新老平台过滤
        paramMap.put("isnew",carInfoSelectDto.getIsnew());
        //最新通讯时间
        paramMap.put("lasttimebegin",carInfoSelectDto.getLastTimeBegin());
        paramMap.put("lasttimeend",carInfoSelectDto.getLastTimeEnd());
        //最新登陆时间
        paramMap.put("lastloginbegin",carInfoSelectDto.getStartTime());
        paramMap.put("lastloginend",carInfoSelectDto.getEndTime());
        //锁车状态
        paramMap.put("lockstatus",carInfoSelectDto.getLockStatus());
        //通过过滤完的整车编号去执行最终结果
        List<CarInfoVo> carInfoVos = cardocPojoMapper.select4CarInfo(paramMap);
        //查找绑定关系,并绑定数据
        if (!CollectionUtils.isEmpty(carInfoVos)){
            for (CarInfoVo carInfoVo : carInfoVos) {
                String finalTerminal = carInfoVo.getTerminalid();
                if (StringUtils.isNotEmpty(finalTerminal)){
                    Object worktime = redis.hget(Const.PREFIX_TERMINAL + finalTerminal, Const.WORKTIME);
                    if (worktime != null){
                        carInfoVo.setWorktime(worktime.toString());
                    }
                }
            }
        }
        return carInfoVos;
    }

    @Override
    public Integer updateRemark(CardocPojo cardocPojo) {
        if (StringUtils.isBlank(cardocPojo.getCarno()) || StringUtils.isBlank(cardocPojo.getRemark())) {
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        cardocPojoExample.createCriteria().andLogicalDeleted(false).andCarnoEqualTo(cardocPojo.getCarno());
        return cardocPojoMapper.updateByExampleSelective(cardocPojo, cardocPojoExample, CardocPojo.Column.remark);
    }

    private String getWorkStatus(String mElectrify,String mStart,String mShutdown,String throttleGear){
        String trueStr = "true";
        String falseStr = "false";
        if (trueStr.equals(mElectrify) && falseStr.equals(mStart)){
            return CarWorkStatus.ELECTRIFY.getCode();
        }else if (falseStr.equals(mElectrify) && falseStr.equals(mStart)){
            return CarWorkStatus.STOP.getCode();
        }else if (trueStr.equals(mElectrify) && trueStr.equals(mStart) && "1".equals(throttleGear)){
            return CarWorkStatus.SLOWSPEED.getCode();
        }else if (trueStr.equals(mElectrify) && trueStr.equals(mStart)){
            return CarWorkStatus.START.getCode();
        }
        return null;
    }
}
