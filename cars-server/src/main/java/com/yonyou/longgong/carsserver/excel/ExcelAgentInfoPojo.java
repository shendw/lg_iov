package com.yonyou.longgong.carsserver.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/**
 * @author yushilin
 * @version 1.0
 * @description: TODO
 * @date 2021/5/7 10:27
 */
@Data
public class ExcelAgentInfoPojo {

    @Excel(name = "代理商编码")
    private String agentCode;

    @Excel(name = "代理商名称")
    private String agentName;

    @Excel(name = "公司地址")
    private String agentAddress;

    /**
     * 坐标点
     * 经度，纬度
     */
    @Excel(name = "坐标点")
    private String location;

}
