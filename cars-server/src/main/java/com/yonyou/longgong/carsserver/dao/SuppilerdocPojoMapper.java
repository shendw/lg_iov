package com.yonyou.longgong.carsserver.dao;

import com.yonyou.longgong.carsserver.example.SuppilerdocPojoExample;
import com.yonyou.longgong.carsserver.pojo.SuppilerdocPojo;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SuppilerdocPojoMapper {
    long countByExample(SuppilerdocPojoExample example);

    int deleteWithVersionByExample(@Param("version") Long version, @Param("example") SuppilerdocPojoExample example);

    int deleteByExample(SuppilerdocPojoExample example);

    int deleteWithVersionByPrimaryKey(@Param("version") Long version, @Param("key") String pkSupplier);

    int deleteByPrimaryKey(String pkSupplier);

    int insert(SuppilerdocPojo record);

    int insertSelective(@Param("record") SuppilerdocPojo record, @Param("selective") SuppilerdocPojo.Column ... selective);

    List<SuppilerdocPojo> selectByExample(SuppilerdocPojoExample example);

    SuppilerdocPojo selectByPrimaryKey(String pkSupplier);

    SuppilerdocPojo selectByPrimaryKeyWithLogicalDelete(@Param("pkSupplier") String pkSupplier, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    int updateWithVersionByExample(@Param("version") Long version, @Param("record") SuppilerdocPojo record, @Param("example") SuppilerdocPojoExample example);

    int updateWithVersionByExampleSelective(@Param("version") Long version, @Param("record") SuppilerdocPojo record, @Param("example") SuppilerdocPojoExample example, @Param("selective") SuppilerdocPojo.Column ... selective);

    int updateByExampleSelective(@Param("record") SuppilerdocPojo record, @Param("example") SuppilerdocPojoExample example, @Param("selective") SuppilerdocPojo.Column ... selective);

    int updateByExample(@Param("record") SuppilerdocPojo record, @Param("example") SuppilerdocPojoExample example);

    int updateWithVersionByPrimaryKey(@Param("version") Long version, @Param("record") SuppilerdocPojo record);

    int updateWithVersionByPrimaryKeySelective(@Param("version") Long version, @Param("record") SuppilerdocPojo record, @Param("selective") SuppilerdocPojo.Column ... selective);

    int updateByPrimaryKeySelective(@Param("record") SuppilerdocPojo record, @Param("selective") SuppilerdocPojo.Column ... selective);

    int updateByPrimaryKey(SuppilerdocPojo record);

    int logicalDeleteByExample(@Param("example") SuppilerdocPojoExample example);

    int logicalDeleteWithVersionByExample(@Param("version") Long version, @Param("nextVersion") Long nextVersion, @Param("example") SuppilerdocPojoExample example);

    int logicalDeleteByPrimaryKey(String pkSupplier);

    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Long version, @Param("nextVersion") Long nextVersion, @Param("key") String pkSupplier);
}