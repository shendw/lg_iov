package com.yonyou.longgong.carsserver.occdto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-12-06
 * @描述
 **/
@Data
public class OccUserIdentityDto {

    private String avator;
    private String businessPersonCode;
    private String businessPersonId;
    private String businessPersonName;
    private String channelCustomerCode;
    private String channelCustomerId;
    private String channelCustomerName;
    private String creationTime;
    private String creator;
    private String dr;
    private String dtype;
    private String email;
    private String groupId;
    private String groupName;
    private String id;
    private String img;
    private String isLock;
    private String label;
    private String loginName;
    private String modifiedTime;
    private String modifier;
    private String name;
    private String organizationId;
    private String organizationName;
    private String persistStatus;
    private String phone;
    private String promptMessage;
    private String registerDate;
    private String remark;
    private String roles;
    private String states;
    private String tenantId;
    private String ts;
    private String type;

}
