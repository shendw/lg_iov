package com.yonyou.longgong.carsserver.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-12-09
 * @描述
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserIdentity {

    List<String> orgs;

    String agentName;

    String agentCode;
}
