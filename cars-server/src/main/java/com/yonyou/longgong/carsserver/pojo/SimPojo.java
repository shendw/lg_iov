package com.yonyou.longgong.carsserver.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SimPojo implements Serializable {
    public SimPojo() {
    }

    public SimPojo(@NotNull String simid, @NotNull String serial, @NotNull String startTime) {
        this.simid = simid;
        this.serial = serial;
        this.startTime = startTime;
    }

    public static final Long IS_DELETED = Dr.IS_DELETED.value();

    public static final Long NOT_DELETED = Dr.NOT_DELETED.value();

    private String pkSim;

    @Excel(name = "sim卡号", isImportField = "true")
    @NotNull
    private String simid;

    @Excel(name = "sim卡串号", isImportField = "true")
    @NotNull
    private String serial;

    @Excel(name = "开卡日期", isImportField = "true")
    @NotNull
    private String startTime;

    private String messageMonth;

    private String flowMonth;

    private String messageTotal;

    private String flowTotal;

    @Excel(name = "最后通讯时间")
    private String lastTime;

    private String carno;

    private String clientid;

    private String bindTime;

    private String creationtime;

    private String creator;

    private String modifiedtime;

    private String modifier;

    private Long dr;

    private Long version;

    private String pkOrg;

    private static final long serialVersionUID = 1L;

    public void andLogicalDeleted(boolean deleted) {
        setDr(deleted ? Dr.IS_DELETED.value() : Dr.NOT_DELETED.value());
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SimPojo other = (SimPojo) that;
        return (this.getPkSim() == null ? other.getPkSim() == null : this.getPkSim().equals(other.getPkSim()))
            && (this.getSimid() == null ? other.getSimid() == null : this.getSimid().equals(other.getSimid()))
            && (this.getSerial() == null ? other.getSerial() == null : this.getSerial().equals(other.getSerial()))
            && (this.getStartTime() == null ? other.getStartTime() == null : this.getStartTime().equals(other.getStartTime()))
            && (this.getMessageMonth() == null ? other.getMessageMonth() == null : this.getMessageMonth().equals(other.getMessageMonth()))
            && (this.getFlowMonth() == null ? other.getFlowMonth() == null : this.getFlowMonth().equals(other.getFlowMonth()))
            && (this.getMessageTotal() == null ? other.getMessageTotal() == null : this.getMessageTotal().equals(other.getMessageTotal()))
            && (this.getFlowTotal() == null ? other.getFlowTotal() == null : this.getFlowTotal().equals(other.getFlowTotal()))
            && (this.getLastTime() == null ? other.getLastTime() == null : this.getLastTime().equals(other.getLastTime()))
            && (this.getCarno() == null ? other.getCarno() == null : this.getCarno().equals(other.getCarno()))
            && (this.getClientid() == null ? other.getClientid() == null : this.getClientid().equals(other.getClientid()))
            && (this.getBindTime() == null ? other.getBindTime() == null : this.getBindTime().equals(other.getBindTime()))
            && (this.getCreationtime() == null ? other.getCreationtime() == null : this.getCreationtime().equals(other.getCreationtime()))
            && (this.getCreator() == null ? other.getCreator() == null : this.getCreator().equals(other.getCreator()))
            && (this.getModifiedtime() == null ? other.getModifiedtime() == null : this.getModifiedtime().equals(other.getModifiedtime()))
            && (this.getModifier() == null ? other.getModifier() == null : this.getModifier().equals(other.getModifier()))
            && (this.getDr() == null ? other.getDr() == null : this.getDr().equals(other.getDr()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()))
            && (this.getPkOrg() == null ? other.getPkOrg() == null : this.getPkOrg().equals(other.getPkOrg()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getPkSim() == null) ? 0 : getPkSim().hashCode());
        result = prime * result + ((getSimid() == null) ? 0 : getSimid().hashCode());
        result = prime * result + ((getSerial() == null) ? 0 : getSerial().hashCode());
        result = prime * result + ((getStartTime() == null) ? 0 : getStartTime().hashCode());
        result = prime * result + ((getMessageMonth() == null) ? 0 : getMessageMonth().hashCode());
        result = prime * result + ((getFlowMonth() == null) ? 0 : getFlowMonth().hashCode());
        result = prime * result + ((getMessageTotal() == null) ? 0 : getMessageTotal().hashCode());
        result = prime * result + ((getFlowTotal() == null) ? 0 : getFlowTotal().hashCode());
        result = prime * result + ((getLastTime() == null) ? 0 : getLastTime().hashCode());
        result = prime * result + ((getCarno() == null) ? 0 : getCarno().hashCode());
        result = prime * result + ((getClientid() == null) ? 0 : getClientid().hashCode());
        result = prime * result + ((getBindTime() == null) ? 0 : getBindTime().hashCode());
        result = prime * result + ((getCreationtime() == null) ? 0 : getCreationtime().hashCode());
        result = prime * result + ((getCreator() == null) ? 0 : getCreator().hashCode());
        result = prime * result + ((getModifiedtime() == null) ? 0 : getModifiedtime().hashCode());
        result = prime * result + ((getModifier() == null) ? 0 : getModifier().hashCode());
        result = prime * result + ((getDr() == null) ? 0 : getDr().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        result = prime * result + ((getPkOrg() == null) ? 0 : getPkOrg().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", pkSim=").append(pkSim);
        sb.append(", simid=").append(simid);
        sb.append(", serial=").append(serial);
        sb.append(", startTime=").append(startTime);
        sb.append(", messageMonth=").append(messageMonth);
        sb.append(", flowMonth=").append(flowMonth);
        sb.append(", messageTotal=").append(messageTotal);
        sb.append(", flowTotal=").append(flowTotal);
        sb.append(", lastTime=").append(lastTime);
        sb.append(", carno=").append(carno);
        sb.append(", clientid=").append(clientid);
        sb.append(", bindTime=").append(bindTime);
        sb.append(", creationtime=").append(creationtime);
        sb.append(", creator=").append(creator);
        sb.append(", modifiedtime=").append(modifiedtime);
        sb.append(", modifier=").append(modifier);
        sb.append(", dr=").append(dr);
        sb.append(", version=").append(version);
        sb.append(", pkOrg=").append(pkOrg);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    public enum Dr {
        NOT_DELETED(new Long("0"), "未删除"),
        IS_DELETED(new Long("1"), "已删除");

        private final Long value;

        private final String name;

        Dr(Long value, String name) {
            this.value = value;
            this.name = name;
        }

        public Long getValue() {
            return this.value;
        }

        public Long value() {
            return this.value;
        }

        public String getName() {
            return this.name;
        }
    }

    public enum Column {
        pkSim("PK_SIM", "pkSim", "VARCHAR", false),
        simid("SIMID", "simid", "VARCHAR", false),
        serial("SERIAL", "serial", "VARCHAR", false),
        startTime("START_TIME", "startTime", "CHAR", false),
        messageMonth("MESSAGE_MONTH", "messageMonth", "VARCHAR", false),
        flowMonth("FLOW_MONTH", "flowMonth", "VARCHAR", false),
        messageTotal("MESSAGE_TOTAL", "messageTotal", "VARCHAR", false),
        flowTotal("FLOW_TOTAL", "flowTotal", "VARCHAR", false),
        lastTime("LAST_TIME", "lastTime", "CHAR", false),
        carno("CARNO", "carno", "VARCHAR", false),
        clientid("CLIENTID", "clientid", "VARCHAR", false),
        bindTime("BIND_TIME", "bindTime", "CHAR", false),
        creationtime("CREATIONTIME", "creationtime", "CHAR", false),
        creator("CREATOR", "creator", "VARCHAR", false),
        modifiedtime("MODIFIEDTIME", "modifiedtime", "CHAR", false),
        modifier("MODIFIER", "modifier", "VARCHAR", false),
        dr("DR", "dr", "DECIMAL", false),
        version("VERSION", "version", "DECIMAL", false),
        pkOrg("PK_ORG", "pkOrg", "VARCHAR", false);

        private static final String BEGINNING_DELIMITER = "\"";

        private static final String ENDING_DELIMITER = "\"";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}