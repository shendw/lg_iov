package com.yonyou.longgong.carsserver.dto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-11-06
 * @描述
 **/
@Data
public class TerminalSelectDto {

    /**
     * 单位
     */
    private String pkOrg;
    /**
     * 整车编号
     */
    private String carNo;
    /**
     * 终端id
     */
    private String terminalid;
    /**
     * sim卡号
     */
    private String simid;
    /**
     * 流程状态
     */
    private String status;
    /**
     * 工作状态
     */
    private String workstatus;
    /**
     * 通讯通道
     */
    private String channel;
    /**
     * 终端生成日期
     */
    private String productiontimeLow;
    /**
     * 终端生成日期
     */
    private String productiontimeHigh;
    /**
     * 开始日期
     */
    private String finishedtimeLow;
    /**
     * 开始日期
     */
    private String finishedtimeHigh;
    /**
     * 开始日期
     */
    private String lasttimeLow;
    /**
     * 开始日期
     */
    private String lasttimeHigh;

    private Integer pageSize = 10;

    private Integer pageNum = 1;
}
