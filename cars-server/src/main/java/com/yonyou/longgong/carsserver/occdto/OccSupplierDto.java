package com.yonyou.longgong.carsserver.occdto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-11-18
 * @描述
 **/
@Data
public class OccSupplierDto {
    /**
     * 供应商简称
     */
    private String abbreviation;
    /**
     * 联系地址
     */
    private String address;
    /**
     * 承运商类型
     */
    private String carrierType;
    /**
     * 供应商编码
     */
    private String code;
    /**
     * 联系人
     */
    private String contact;
    /**
     * 创建时间
     */
    private String creationTime;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 关联客商编码
     */
    private String customerCode;
    /**
     * 关联客商id
     */
    private String customerId;
    /**
     * 关联客商名称
     */
    private String customerName;
    /**
     * 删除标识
     */
    private Integer dr;
    /**
     * 扩展字段01
     */
    private String ext01;
    /**
     * 扩展字段02
     */
    private String ext02;
    /**
     * 扩展字段03
     */
    private String ext03;
    /**
     * 扩展字段04
     */
    private String ext04;
    /**
     * 扩展字段05
     */
    private String ext05;
    /**
     * 扩展字段06
     */
    private String ext06;
    /**
     * 扩展字段07
     */
    private String ext07;
    /**
     * 扩展字段08
     */
    private String ext08;
    /**
     * 扩展字段09
     */
    private String ext09;
    /**
     * 扩展字段10
     */
    private String ext10;
    /**
     * 主键
     */
    private String id;
    /**
     * 内部单位编码
     */
    private String innerOrgCode;
    /**
     * 内部单位id
     */
    private String innerOrgId;
    /**
     * 内部单位名称
     */
    private String innerOrgName;
    /**
     * 是否承运商
     */
    private Integer isCarrier;
    /**
     * 状态
     */
    private Integer isEnable;
    /**
     * 修改时间
     */
    private String modifiedTime;
    /**
     * 修改人
     */
    private String modifier;
    /**
     * 供应商名字
     */
    private String name;
    /**
     * 持久化状态
     */
    private String persistStatus;
    /**
     * 备注
     */
    private String remark;
    /**
     * 原系统
     */
    private String srcSystem;
    /**
     * 原系统记录code
     */
    private String srcSystemCode;
    /**
     * 原系统记录id
     */
    private String srcSystemId;
    /**
     * 原系统记录名称
     */
    private String srcSystemName;
    /**
     * 供应商账户集合
     */
    private OccSupplierAccountDto[] supplierAccounts;
    /**
     * 供应商分类编码
     */
    private String supplierCategoryCode;
    /**
     * 供应商分类id
     */
    private String supplierCategoryId;
    /**
     * 供应商分类名称
     */
    private String supplierCategoryName;
    /**
     * 供应商类型编码
     */
    private String supplierTypeCode;
    /**
     * 供应商类型id
     */
    private String supplierTypeId;
    /**
     * 供应商类型名称
     */
    private String supplierTypeName;
    /**
     * 联系人电话
     */
    private String tel;
    /**
     * 时间戳
     */
    private String ts;
}
