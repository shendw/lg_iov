package com.yonyou.longgong.carsserver.dao;

import com.yonyou.longgong.carsserver.example.SimPojoExample;
import com.yonyou.longgong.carsserver.pojo.SimPojo;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SimPojoMapper {
    long countByExample(SimPojoExample example);

    int deleteWithVersionByExample(@Param("version") Long version, @Param("example") SimPojoExample example);

    int deleteByExample(SimPojoExample example);

    int deleteWithVersionByPrimaryKey(@Param("version") Long version, @Param("key") String pkSim);

    int deleteByPrimaryKey(String pkSim);

    int insert(List<SimPojo> record);

    int insertSelective(@Param("record") SimPojo record, @Param("selective") SimPojo.Column ... selective);

    List<SimPojo> selectByExample(SimPojoExample example);

    SimPojo selectByPrimaryKey(String pkSim);

    SimPojo selectByPrimaryKeyWithLogicalDelete(@Param("pkSim") String pkSim, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    int updateWithVersionByExample(@Param("version") Long version, @Param("record") SimPojo record, @Param("example") SimPojoExample example);

    int updateWithVersionByExampleSelective(@Param("version") Long version, @Param("record") SimPojo record, @Param("example") SimPojoExample example, @Param("selective") SimPojo.Column ... selective);

    int updateByExampleSelective(@Param("record") SimPojo record, @Param("example") SimPojoExample example, @Param("selective") SimPojo.Column ... selective);

    int updateByExample(@Param("record") SimPojo record, @Param("example") SimPojoExample example);

    int updateWithVersionByPrimaryKey(@Param("version") Long version, @Param("record") SimPojo record);

    int updateWithVersionByPrimaryKeySelective(@Param("version") Long version, @Param("record") SimPojo record, @Param("selective") SimPojo.Column ... selective);

    int updateByPrimaryKeySelective(@Param("record") SimPojo record, @Param("selective") SimPojo.Column ... selective);

    int updateByPrimaryKey(SimPojo record);

    int logicalDeleteByExample(@Param("example") SimPojoExample example);

    int logicalDeleteWithVersionByExample(@Param("version") Long version, @Param("nextVersion") Long nextVersion, @Param("example") SimPojoExample example);

    int logicalDeleteByPrimaryKey(String pkSim);

    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Long version, @Param("nextVersion") Long nextVersion, @Param("key") String pkSim);
}