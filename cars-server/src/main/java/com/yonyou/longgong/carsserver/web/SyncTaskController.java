package com.yonyou.longgong.carsserver.web;

import com.alibaba.fastjson.JSONObject;
import com.yonyou.longgong.carsserver.service.ISyncTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shendawei
 * @创建时间 2020/4/28
 * @描述
 **/
@RestController
@RequestMapping(value = "/syncTask")
@Slf4j
public class SyncTaskController {

    @Autowired
    private ISyncTaskService iSyncTaskService;

    @RequestMapping("/getU9InfoNotSaleToSale")
    public JSONObject getU9InfoNotSaleToSale(){
        return iSyncTaskService.getU9InfoNotSaleToSale();
    }

    @RequestMapping("/getU9InfoNotSale")
    public JSONObject getU9InfoNotSale(){
        return iSyncTaskService.getU9InfoNotSale();
    }

    @RequestMapping("/getU9Info")
    public JSONObject getU9Info(){
        return iSyncTaskService.getU9Info();
    }
}
