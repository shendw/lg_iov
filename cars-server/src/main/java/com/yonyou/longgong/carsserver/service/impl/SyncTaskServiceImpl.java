package com.yonyou.longgong.carsserver.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.yonyou.longgong.carsserver.dao.CardocPojoMapper;
import com.yonyou.longgong.carsserver.enums.CarProcessStatus;
import com.yonyou.longgong.carsserver.example.CardocPojoExample;
import com.yonyou.longgong.carsserver.pojo.CardocPojo;
import com.yonyou.longgong.carsserver.service.ICardocService;
import com.yonyou.longgong.carsserver.service.ISyncTaskService;
import com.yonyou.lonking.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;

/**
 * @author shendawei
 * @创建时间 2020/4/28
 * @描述
 **/
@Service("iSyncTaskService")
@Slf4j
public class SyncTaskServiceImpl implements ISyncTaskService {

    @Autowired
    private ICardocService iCardocService;

    @Autowired
    private CardocPojoMapper cardocPojoMapper;
    
    
    private static final double sizeDouble = 50.0;
    
    private static final int sizeInt =  50;

    @Override
    public JSONObject getU9InfoNotSaleToSale() {
        try {
            CardocPojoExample cardocPojoExample = new CardocPojoExample();
            cardocPojoExample.createCriteria()
                    .andLogicalDeleted(false)
                    .andStatusIn(Arrays.asList(CarProcessStatus.TEST.getCode(),CarProcessStatus.INLINE.getCode(),CarProcessStatus.STOCK.getCode(),CarProcessStatus.AUTOBIND.getCode()));
            List<CardocPojo> cardocPojos = cardocPojoMapper.selectByExample(cardocPojoExample);
            if (CollectionUtils.isEmpty(cardocPojos)){
                return JsonUtils.responseOccResult(true,"找不到库存车信息",null);
            }
            log.info("开始同步库存车已售信息，共{}条",cardocPojos.size());
            int times = (int)Math.ceil( cardocPojos.size()/sizeDouble );
            for(int i=0; i<times; i++ ){
                Integer integer = iCardocService.syncCardocFromU9(cardocPojos.subList(i * sizeInt, Math.min((i + 1) * sizeInt, cardocPojos.size())));
            }
            log.info("同步库存车已售信息已完成");
            return JsonUtils.responseOccResult(true,"同步成功",null);
        }catch (Exception e){
            return JsonUtils.responseOccResult(false,"同步失败",e);
        }
    }

    @Override
    public JSONObject getU9InfoNotSale() {
        try {
            CardocPojoExample cardocPojoExample = new CardocPojoExample();
            cardocPojoExample.createCriteria()
                    .andLogicalDeleted(false)
                    .andStatusIn(Arrays.asList(CarProcessStatus.OUTLINE.getCode(),CarProcessStatus.TEST.getCode(),CarProcessStatus.INLINE.getCode(),CarProcessStatus.STOCK.getCode(),CarProcessStatus.AUTOBIND.getCode()));
            List<CardocPojo> cardocPojos = cardocPojoMapper.selectByExample(cardocPojoExample);
            if (CollectionUtils.isEmpty(cardocPojos)){
                return JsonUtils.responseOccResult(true,"找不到库存车信息",null);
            }
            log.info("开始同步库存车库存信息，共{}条",cardocPojos.size());
            int times = (int)Math.ceil( cardocPojos.size()/sizeDouble );
            for(int i=0; i<times; i++ ){
                Integer integer = iCardocService.syncCardocStockFromU9(cardocPojos.subList(i * sizeInt, Math.min((i + 1) * sizeInt, cardocPojos.size())));
            }
            log.info("同步库存车库存信息已完成");
            return JsonUtils.responseOccResult(true,"同步成功",null);
        }catch (Exception e){
            return JsonUtils.responseOccResult(false,"同步失败",e);
        }
    }

    @Override
    public JSONObject getU9Info() {
        try {
            CardocPojoExample cardocPojoExample = new CardocPojoExample();
            cardocPojoExample.createCriteria().andLogicalDeleted(false).andStatusEqualTo(CarProcessStatus.SOLD.getCode());
            List<CardocPojo> cardocPojos = cardocPojoMapper.selectByExample(cardocPojoExample);
            if (CollectionUtils.isEmpty(cardocPojos)){
                return JsonUtils.responseOccResult(true,"找不到已售车信息",null);
            }
            log.info("开始同步已售车信息，共{}条",cardocPojos.size());
            int times = (int)Math.ceil( cardocPojos.size()/sizeDouble );
            for(int i=0; i<times; i++ ){
                Integer row = iCardocService.syncCardocFromU9(cardocPojos.subList(i * sizeInt, Math.min((i + 1) * sizeInt, cardocPojos.size())));
            }
            log.info("同步已售车已售信息已完成");
            return JsonUtils.responseOccResult(true,"同步成功",null);
        }catch (Exception e){
            return JsonUtils.responseOccResult(false,"同步失败",e);
        }
    }
}
