package com.yonyou.longgong.carsserver.service;

import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dto.CardocSelectDto;
import com.yonyou.longgong.carsserver.excel.ExcelVerifyCardocPojoOfMode;
import com.yonyou.longgong.carsserver.exception.BusinessException;
import com.yonyou.longgong.carsserver.pojo.CardocPojo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-11-07
 * @描述
 **/
public interface ICardocService {
    Integer insertCardoc(CardocPojo cardocPojo);

    /**
     * iot插入车辆数据-自动绑定
     * @param cardocPojo
     * @param terminalid
     * @return
     */
    Integer iotInsertCardoc(CardocPojo cardocPojo,String terminalid);
    Integer delCardoc(List<CardocPojo> cardocPojos);
    Integer updateCardoc(CardocPojo cardocPojo);
    PageInfo<CardocPojo> selectCardoc(CardocSelectDto cardocSelectDto);
    PageInfo<CardocPojo> selectCarDocByCarNo(String carno);
    PageInfo<CardocPojo> selectCardoc(CardocSelectDto cardocSelectDto,String userId,String loginName);
    List<CardocPojo> selectCardocByExcel(CardocSelectDto cardocSelectDto,String userId,String loginName);
    List<String> selectCarno();

    /**
     * 从U9中拿已售的数据
     * @param cardocPojos
     * @return
     */
    Integer syncCardocFromU9(List<CardocPojo> cardocPojos);

    /**
     * 从U9中拿库存车的数据
     * @param cardocPojos
     * @return
     */
    Integer syncCardocStockFromU9(List<CardocPojo> cardocPojos);

    /**
     * 导入车辆，终端信息
     * @param excelVerifyCardocPojoOfMode
     * @return
     */
    String insertCardocByExcel(ExcelVerifyCardocPojoOfMode excelVerifyCardocPojoOfMode);

    /**
     * 检查excel
     * @param carExcel
     * @return
     */
    ExcelImportResult<ExcelVerifyCardocPojoOfMode> checkExcel(MultipartFile carExcel);

    /**
     * 车辆信息档案编辑功能
     * @param cardocPojo
     * @return
     */
    Integer changeCardoc(CardocPojo cardocPojo);

    Integer updateLockStatus(String carno,String lockcode);
}
