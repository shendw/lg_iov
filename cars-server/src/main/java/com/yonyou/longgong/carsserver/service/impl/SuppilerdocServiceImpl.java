package com.yonyou.longgong.carsserver.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dao.SuppilerdocPojoMapper;
import com.yonyou.longgong.carsserver.dto.SuppilerdocSelectDto;
import com.yonyou.longgong.carsserver.example.SuppilerdocPojoExample;
import com.yonyou.longgong.carsserver.exception.BusinessException;
import com.yonyou.longgong.carsserver.pojo.SuppilerdocPojo;
import com.yonyou.longgong.carsserver.service.ISuppilerdocService;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.UUIDUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author shendawei
 * @创建时间 2019-11-13
 * @描述
 **/
@Service(value = "iSuppilerdocService")
public class SuppilerdocServiceImpl implements ISuppilerdocService {

    @Autowired
    private SuppilerdocPojoMapper suppilerdocPojoMapper;

    @Override
    public Integer insertSuppilerdoc(SuppilerdocPojo suppilerdocPojo) {
        SuppilerdocPojoExample suppilerdocPojoExample = new SuppilerdocPojoExample();
        suppilerdocPojoExample.createCriteria().andLogicalDeleted(false).andCodeEqualTo(suppilerdocPojo.getCode());
        long count = suppilerdocPojoMapper.countByExample(suppilerdocPojoExample);
        if (count>0){
            throw new BusinessException(ResponseCode.SUPPILERDOC_IS_EXIST);
        }
        suppilerdocPojoExample.clear();
//        suppilerdocPojoExample.setOrderByClause("no");
//        suppilerdocPojoExample.createCriteria().andLogicalDeleted(false);
        long no = suppilerdocPojoMapper.countByExample(suppilerdocPojoExample);
        suppilerdocPojo.setPkSupplier(UUIDUtils.getUniqueIdByUUId());
        suppilerdocPojo.setVersion(1L);
        suppilerdocPojo.setDr(0L);
        suppilerdocPojo.setNo(String.valueOf(no+1));
        return suppilerdocPojoMapper.insertSelective(suppilerdocPojo);
    }

    @Override
    public Integer delSuppilerdoc(List<SuppilerdocPojo> suppilerdocPojos) {
        SuppilerdocPojoExample suppilerdocPojoExample = new SuppilerdocPojoExample();
        suppilerdocPojoExample.createCriteria().andLogicalDeleted(false).andPkSupplierIn(suppilerdocPojos.stream().map(SuppilerdocPojo::getPkSupplier).collect(Collectors.toList()));
        return suppilerdocPojoMapper.logicalDeleteByExample(suppilerdocPojoExample);
    }

    @Override
    public Integer updateSuppilerdoc(SuppilerdocPojo suppilerdocPojo) {
        SuppilerdocPojoExample suppilerdocPojoExample = new SuppilerdocPojoExample();
        suppilerdocPojoExample.createCriteria().andLogicalDeleted(false).andCodeEqualTo(suppilerdocPojo.getCode());
        List<SuppilerdocPojo> suppilerdocPojos = suppilerdocPojoMapper.selectByExample(suppilerdocPojoExample);
        if (!CollectionUtils.isEmpty(suppilerdocPojos) && !suppilerdocPojos.get(0).getPkSupplier().equals(suppilerdocPojo.getPkSupplier())){
            throw new BusinessException(ResponseCode.SUPPILERDOC_CODE_IS_EXIST);
        }
        return suppilerdocPojoMapper.updateWithVersionByPrimaryKeySelective(suppilerdocPojo.getVersion(),suppilerdocPojo);
    }

    @Override
    public PageInfo<SuppilerdocPojo> selectSuppilerdoc(SuppilerdocSelectDto suppilerdocSelectDto) {
        SuppilerdocPojoExample suppilerdocPojoExample = selectRule(suppilerdocSelectDto);
        PageHelper.startPage(suppilerdocSelectDto.getPageNum(),suppilerdocSelectDto.getPageSize());
        List<SuppilerdocPojo> suppilerdocPojos = suppilerdocPojoMapper.selectByExample(suppilerdocPojoExample);
        return new PageInfo<>(suppilerdocPojos);
    }

    @Override
    public List<SuppilerdocPojo> selectSuppilerdocByExcel(SuppilerdocSelectDto suppilerdocSelectDto) {
        SuppilerdocPojoExample suppilerdocPojoExample = selectRule(suppilerdocSelectDto);
        return suppilerdocPojoMapper.selectByExample(suppilerdocPojoExample);
    }

    private SuppilerdocPojoExample selectRule(SuppilerdocSelectDto suppilerdocSelectDto){
        SuppilerdocPojoExample suppilerdocPojoExample = new SuppilerdocPojoExample();
        SuppilerdocPojoExample.Criteria criteria = suppilerdocPojoExample.createCriteria();
        if (StringUtils.isNotBlank(suppilerdocSelectDto.getCode())){
            criteria.andCodeEqualTo(suppilerdocSelectDto.getCode());
        }
        if (StringUtils.isNotBlank(suppilerdocSelectDto.getName())){
            criteria.andNameLike("%"+suppilerdocSelectDto.getName()+"%");
        }
        criteria.andLogicalDeleted(false);
        return suppilerdocPojoExample;
    }

    @Override
    public SuppilerdocPojo selectSuppilerByKeyword(String keyword) {
        SuppilerdocPojoExample suppilerdocPojoExample = new SuppilerdocPojoExample();
        suppilerdocPojoExample.createCriteria().andLogicalDeleted(false).andKeywordEqualTo(keyword);
        List<SuppilerdocPojo> suppilerdocPojos = suppilerdocPojoMapper.selectByExample(suppilerdocPojoExample);
        return CollectionUtils.isEmpty(suppilerdocPojos)?null:suppilerdocPojos.get(0);
    }
}
