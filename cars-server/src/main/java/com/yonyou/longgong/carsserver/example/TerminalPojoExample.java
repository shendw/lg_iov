package com.yonyou.longgong.carsserver.example;

import com.yonyou.longgong.carsserver.pojo.TerminalPojo;
import java.util.ArrayList;
import java.util.List;

public class TerminalPojoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TerminalPojoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPkTerminalIsNull() {
            addCriterion("PK_TERMINAL is null");
            return (Criteria) this;
        }

        public Criteria andPkTerminalIsNotNull() {
            addCriterion("PK_TERMINAL is not null");
            return (Criteria) this;
        }

        public Criteria andPkTerminalEqualTo(String value) {
            addCriterion("PK_TERMINAL =", value, "pkTerminal");
            return (Criteria) this;
        }

        public Criteria andPkTerminalNotEqualTo(String value) {
            addCriterion("PK_TERMINAL <>", value, "pkTerminal");
            return (Criteria) this;
        }

        public Criteria andPkTerminalGreaterThan(String value) {
            addCriterion("PK_TERMINAL >", value, "pkTerminal");
            return (Criteria) this;
        }

        public Criteria andPkTerminalGreaterThanOrEqualTo(String value) {
            addCriterion("PK_TERMINAL >=", value, "pkTerminal");
            return (Criteria) this;
        }

        public Criteria andPkTerminalLessThan(String value) {
            addCriterion("PK_TERMINAL <", value, "pkTerminal");
            return (Criteria) this;
        }

        public Criteria andPkTerminalLessThanOrEqualTo(String value) {
            addCriterion("PK_TERMINAL <=", value, "pkTerminal");
            return (Criteria) this;
        }

        public Criteria andPkTerminalLike(String value) {
            addCriterion("PK_TERMINAL like", value, "pkTerminal");
            return (Criteria) this;
        }

        public Criteria andPkTerminalNotLike(String value) {
            addCriterion("PK_TERMINAL not like", value, "pkTerminal");
            return (Criteria) this;
        }

        public Criteria andPkTerminalIn(List<String> values) {
            addCriterion("PK_TERMINAL in", values, "pkTerminal");
            return (Criteria) this;
        }

        public Criteria andPkTerminalNotIn(List<String> values) {
            addCriterion("PK_TERMINAL not in", values, "pkTerminal");
            return (Criteria) this;
        }

        public Criteria andPkTerminalBetween(String value1, String value2) {
            addCriterion("PK_TERMINAL between", value1, value2, "pkTerminal");
            return (Criteria) this;
        }

        public Criteria andPkTerminalNotBetween(String value1, String value2) {
            addCriterion("PK_TERMINAL not between", value1, value2, "pkTerminal");
            return (Criteria) this;
        }

        public Criteria andTerminalidIsNull() {
            addCriterion("TERMINALID is null");
            return (Criteria) this;
        }

        public Criteria andTerminalidIsNotNull() {
            addCriterion("TERMINALID is not null");
            return (Criteria) this;
        }

        public Criteria andTerminalidEqualTo(String value) {
            addCriterion("TERMINALID =", value, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidNotEqualTo(String value) {
            addCriterion("TERMINALID <>", value, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidGreaterThan(String value) {
            addCriterion("TERMINALID >", value, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidGreaterThanOrEqualTo(String value) {
            addCriterion("TERMINALID >=", value, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidLessThan(String value) {
            addCriterion("TERMINALID <", value, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidLessThanOrEqualTo(String value) {
            addCriterion("TERMINALID <=", value, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidLike(String value) {
            addCriterion("TERMINALID like", value, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidNotLike(String value) {
            addCriterion("TERMINALID not like", value, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidIn(List<String> values) {
            addCriterion("TERMINALID in", values, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidNotIn(List<String> values) {
            addCriterion("TERMINALID not in", values, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidBetween(String value1, String value2) {
            addCriterion("TERMINALID between", value1, value2, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidNotBetween(String value1, String value2) {
            addCriterion("TERMINALID not between", value1, value2, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalVersionIsNull() {
            addCriterion("TERMINAL_VERSION is null");
            return (Criteria) this;
        }

        public Criteria andTerminalVersionIsNotNull() {
            addCriterion("TERMINAL_VERSION is not null");
            return (Criteria) this;
        }

        public Criteria andTerminalVersionEqualTo(String value) {
            addCriterion("TERMINAL_VERSION =", value, "terminalVersion");
            return (Criteria) this;
        }

        public Criteria andTerminalVersionNotEqualTo(String value) {
            addCriterion("TERMINAL_VERSION <>", value, "terminalVersion");
            return (Criteria) this;
        }

        public Criteria andTerminalVersionGreaterThan(String value) {
            addCriterion("TERMINAL_VERSION >", value, "terminalVersion");
            return (Criteria) this;
        }

        public Criteria andTerminalVersionGreaterThanOrEqualTo(String value) {
            addCriterion("TERMINAL_VERSION >=", value, "terminalVersion");
            return (Criteria) this;
        }

        public Criteria andTerminalVersionLessThan(String value) {
            addCriterion("TERMINAL_VERSION <", value, "terminalVersion");
            return (Criteria) this;
        }

        public Criteria andTerminalVersionLessThanOrEqualTo(String value) {
            addCriterion("TERMINAL_VERSION <=", value, "terminalVersion");
            return (Criteria) this;
        }

        public Criteria andTerminalVersionLike(String value) {
            addCriterion("TERMINAL_VERSION like", value, "terminalVersion");
            return (Criteria) this;
        }

        public Criteria andTerminalVersionNotLike(String value) {
            addCriterion("TERMINAL_VERSION not like", value, "terminalVersion");
            return (Criteria) this;
        }

        public Criteria andTerminalVersionIn(List<String> values) {
            addCriterion("TERMINAL_VERSION in", values, "terminalVersion");
            return (Criteria) this;
        }

        public Criteria andTerminalVersionNotIn(List<String> values) {
            addCriterion("TERMINAL_VERSION not in", values, "terminalVersion");
            return (Criteria) this;
        }

        public Criteria andTerminalVersionBetween(String value1, String value2) {
            addCriterion("TERMINAL_VERSION between", value1, value2, "terminalVersion");
            return (Criteria) this;
        }

        public Criteria andTerminalVersionNotBetween(String value1, String value2) {
            addCriterion("TERMINAL_VERSION not between", value1, value2, "terminalVersion");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("STATUS is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Long value) {
            addCriterion("STATUS =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Long value) {
            addCriterion("STATUS <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Long value) {
            addCriterion("STATUS >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Long value) {
            addCriterion("STATUS >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Long value) {
            addCriterion("STATUS <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Long value) {
            addCriterion("STATUS <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Long> values) {
            addCriterion("STATUS in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Long> values) {
            addCriterion("STATUS not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Long value1, Long value2) {
            addCriterion("STATUS between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Long value1, Long value2) {
            addCriterion("STATUS not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andProductiontimeIsNull() {
            addCriterion("PRODUCTIONTIME is null");
            return (Criteria) this;
        }

        public Criteria andProductiontimeIsNotNull() {
            addCriterion("PRODUCTIONTIME is not null");
            return (Criteria) this;
        }

        public Criteria andProductiontimeEqualTo(String value) {
            addCriterion("PRODUCTIONTIME =", value, "productiontime");
            return (Criteria) this;
        }

        public Criteria andProductiontimeNotEqualTo(String value) {
            addCriterion("PRODUCTIONTIME <>", value, "productiontime");
            return (Criteria) this;
        }

        public Criteria andProductiontimeGreaterThan(String value) {
            addCriterion("PRODUCTIONTIME >", value, "productiontime");
            return (Criteria) this;
        }

        public Criteria andProductiontimeGreaterThanOrEqualTo(String value) {
            addCriterion("PRODUCTIONTIME >=", value, "productiontime");
            return (Criteria) this;
        }

        public Criteria andProductiontimeLessThan(String value) {
            addCriterion("PRODUCTIONTIME <", value, "productiontime");
            return (Criteria) this;
        }

        public Criteria andProductiontimeLessThanOrEqualTo(String value) {
            addCriterion("PRODUCTIONTIME <=", value, "productiontime");
            return (Criteria) this;
        }

        public Criteria andProductiontimeLike(String value) {
            addCriterion("PRODUCTIONTIME like", value, "productiontime");
            return (Criteria) this;
        }

        public Criteria andProductiontimeNotLike(String value) {
            addCriterion("PRODUCTIONTIME not like", value, "productiontime");
            return (Criteria) this;
        }

        public Criteria andProductiontimeIn(List<String> values) {
            addCriterion("PRODUCTIONTIME in", values, "productiontime");
            return (Criteria) this;
        }

        public Criteria andProductiontimeNotIn(List<String> values) {
            addCriterion("PRODUCTIONTIME not in", values, "productiontime");
            return (Criteria) this;
        }

        public Criteria andProductiontimeBetween(String value1, String value2) {
            addCriterion("PRODUCTIONTIME between", value1, value2, "productiontime");
            return (Criteria) this;
        }

        public Criteria andProductiontimeNotBetween(String value1, String value2) {
            addCriterion("PRODUCTIONTIME not between", value1, value2, "productiontime");
            return (Criteria) this;
        }

        public Criteria andFinishedtimeIsNull() {
            addCriterion("FINISHEDTIME is null");
            return (Criteria) this;
        }

        public Criteria andFinishedtimeIsNotNull() {
            addCriterion("FINISHEDTIME is not null");
            return (Criteria) this;
        }

        public Criteria andFinishedtimeEqualTo(String value) {
            addCriterion("FINISHEDTIME =", value, "finishedtime");
            return (Criteria) this;
        }

        public Criteria andFinishedtimeNotEqualTo(String value) {
            addCriterion("FINISHEDTIME <>", value, "finishedtime");
            return (Criteria) this;
        }

        public Criteria andFinishedtimeGreaterThan(String value) {
            addCriterion("FINISHEDTIME >", value, "finishedtime");
            return (Criteria) this;
        }

        public Criteria andFinishedtimeGreaterThanOrEqualTo(String value) {
            addCriterion("FINISHEDTIME >=", value, "finishedtime");
            return (Criteria) this;
        }

        public Criteria andFinishedtimeLessThan(String value) {
            addCriterion("FINISHEDTIME <", value, "finishedtime");
            return (Criteria) this;
        }

        public Criteria andFinishedtimeLessThanOrEqualTo(String value) {
            addCriterion("FINISHEDTIME <=", value, "finishedtime");
            return (Criteria) this;
        }

        public Criteria andFinishedtimeLike(String value) {
            addCriterion("FINISHEDTIME like", value, "finishedtime");
            return (Criteria) this;
        }

        public Criteria andFinishedtimeNotLike(String value) {
            addCriterion("FINISHEDTIME not like", value, "finishedtime");
            return (Criteria) this;
        }

        public Criteria andFinishedtimeIn(List<String> values) {
            addCriterion("FINISHEDTIME in", values, "finishedtime");
            return (Criteria) this;
        }

        public Criteria andFinishedtimeNotIn(List<String> values) {
            addCriterion("FINISHEDTIME not in", values, "finishedtime");
            return (Criteria) this;
        }

        public Criteria andFinishedtimeBetween(String value1, String value2) {
            addCriterion("FINISHEDTIME between", value1, value2, "finishedtime");
            return (Criteria) this;
        }

        public Criteria andFinishedtimeNotBetween(String value1, String value2) {
            addCriterion("FINISHEDTIME not between", value1, value2, "finishedtime");
            return (Criteria) this;
        }

        public Criteria andPendingtimeIsNull() {
            addCriterion("PENDINGTIME is null");
            return (Criteria) this;
        }

        public Criteria andPendingtimeIsNotNull() {
            addCriterion("PENDINGTIME is not null");
            return (Criteria) this;
        }

        public Criteria andPendingtimeEqualTo(String value) {
            addCriterion("PENDINGTIME =", value, "pendingtime");
            return (Criteria) this;
        }

        public Criteria andPendingtimeNotEqualTo(String value) {
            addCriterion("PENDINGTIME <>", value, "pendingtime");
            return (Criteria) this;
        }

        public Criteria andPendingtimeGreaterThan(String value) {
            addCriterion("PENDINGTIME >", value, "pendingtime");
            return (Criteria) this;
        }

        public Criteria andPendingtimeGreaterThanOrEqualTo(String value) {
            addCriterion("PENDINGTIME >=", value, "pendingtime");
            return (Criteria) this;
        }

        public Criteria andPendingtimeLessThan(String value) {
            addCriterion("PENDINGTIME <", value, "pendingtime");
            return (Criteria) this;
        }

        public Criteria andPendingtimeLessThanOrEqualTo(String value) {
            addCriterion("PENDINGTIME <=", value, "pendingtime");
            return (Criteria) this;
        }

        public Criteria andPendingtimeLike(String value) {
            addCriterion("PENDINGTIME like", value, "pendingtime");
            return (Criteria) this;
        }

        public Criteria andPendingtimeNotLike(String value) {
            addCriterion("PENDINGTIME not like", value, "pendingtime");
            return (Criteria) this;
        }

        public Criteria andPendingtimeIn(List<String> values) {
            addCriterion("PENDINGTIME in", values, "pendingtime");
            return (Criteria) this;
        }

        public Criteria andPendingtimeNotIn(List<String> values) {
            addCriterion("PENDINGTIME not in", values, "pendingtime");
            return (Criteria) this;
        }

        public Criteria andPendingtimeBetween(String value1, String value2) {
            addCriterion("PENDINGTIME between", value1, value2, "pendingtime");
            return (Criteria) this;
        }

        public Criteria andPendingtimeNotBetween(String value1, String value2) {
            addCriterion("PENDINGTIME not between", value1, value2, "pendingtime");
            return (Criteria) this;
        }

        public Criteria andRepairedtimeIsNull() {
            addCriterion("REPAIREDTIME is null");
            return (Criteria) this;
        }

        public Criteria andRepairedtimeIsNotNull() {
            addCriterion("REPAIREDTIME is not null");
            return (Criteria) this;
        }

        public Criteria andRepairedtimeEqualTo(String value) {
            addCriterion("REPAIREDTIME =", value, "repairedtime");
            return (Criteria) this;
        }

        public Criteria andRepairedtimeNotEqualTo(String value) {
            addCriterion("REPAIREDTIME <>", value, "repairedtime");
            return (Criteria) this;
        }

        public Criteria andRepairedtimeGreaterThan(String value) {
            addCriterion("REPAIREDTIME >", value, "repairedtime");
            return (Criteria) this;
        }

        public Criteria andRepairedtimeGreaterThanOrEqualTo(String value) {
            addCriterion("REPAIREDTIME >=", value, "repairedtime");
            return (Criteria) this;
        }

        public Criteria andRepairedtimeLessThan(String value) {
            addCriterion("REPAIREDTIME <", value, "repairedtime");
            return (Criteria) this;
        }

        public Criteria andRepairedtimeLessThanOrEqualTo(String value) {
            addCriterion("REPAIREDTIME <=", value, "repairedtime");
            return (Criteria) this;
        }

        public Criteria andRepairedtimeLike(String value) {
            addCriterion("REPAIREDTIME like", value, "repairedtime");
            return (Criteria) this;
        }

        public Criteria andRepairedtimeNotLike(String value) {
            addCriterion("REPAIREDTIME not like", value, "repairedtime");
            return (Criteria) this;
        }

        public Criteria andRepairedtimeIn(List<String> values) {
            addCriterion("REPAIREDTIME in", values, "repairedtime");
            return (Criteria) this;
        }

        public Criteria andRepairedtimeNotIn(List<String> values) {
            addCriterion("REPAIREDTIME not in", values, "repairedtime");
            return (Criteria) this;
        }

        public Criteria andRepairedtimeBetween(String value1, String value2) {
            addCriterion("REPAIREDTIME between", value1, value2, "repairedtime");
            return (Criteria) this;
        }

        public Criteria andRepairedtimeNotBetween(String value1, String value2) {
            addCriterion("REPAIREDTIME not between", value1, value2, "repairedtime");
            return (Criteria) this;
        }

        public Criteria andScraptimeIsNull() {
            addCriterion("SCRAPTIME is null");
            return (Criteria) this;
        }

        public Criteria andScraptimeIsNotNull() {
            addCriterion("SCRAPTIME is not null");
            return (Criteria) this;
        }

        public Criteria andScraptimeEqualTo(String value) {
            addCriterion("SCRAPTIME =", value, "scraptime");
            return (Criteria) this;
        }

        public Criteria andScraptimeNotEqualTo(String value) {
            addCriterion("SCRAPTIME <>", value, "scraptime");
            return (Criteria) this;
        }

        public Criteria andScraptimeGreaterThan(String value) {
            addCriterion("SCRAPTIME >", value, "scraptime");
            return (Criteria) this;
        }

        public Criteria andScraptimeGreaterThanOrEqualTo(String value) {
            addCriterion("SCRAPTIME >=", value, "scraptime");
            return (Criteria) this;
        }

        public Criteria andScraptimeLessThan(String value) {
            addCriterion("SCRAPTIME <", value, "scraptime");
            return (Criteria) this;
        }

        public Criteria andScraptimeLessThanOrEqualTo(String value) {
            addCriterion("SCRAPTIME <=", value, "scraptime");
            return (Criteria) this;
        }

        public Criteria andScraptimeLike(String value) {
            addCriterion("SCRAPTIME like", value, "scraptime");
            return (Criteria) this;
        }

        public Criteria andScraptimeNotLike(String value) {
            addCriterion("SCRAPTIME not like", value, "scraptime");
            return (Criteria) this;
        }

        public Criteria andScraptimeIn(List<String> values) {
            addCriterion("SCRAPTIME in", values, "scraptime");
            return (Criteria) this;
        }

        public Criteria andScraptimeNotIn(List<String> values) {
            addCriterion("SCRAPTIME not in", values, "scraptime");
            return (Criteria) this;
        }

        public Criteria andScraptimeBetween(String value1, String value2) {
            addCriterion("SCRAPTIME between", value1, value2, "scraptime");
            return (Criteria) this;
        }

        public Criteria andScraptimeNotBetween(String value1, String value2) {
            addCriterion("SCRAPTIME not between", value1, value2, "scraptime");
            return (Criteria) this;
        }

        public Criteria andLastTimeIsNull() {
            addCriterion("LAST_TIME is null");
            return (Criteria) this;
        }

        public Criteria andLastTimeIsNotNull() {
            addCriterion("LAST_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andLastTimeEqualTo(String value) {
            addCriterion("LAST_TIME =", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeNotEqualTo(String value) {
            addCriterion("LAST_TIME <>", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeGreaterThan(String value) {
            addCriterion("LAST_TIME >", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeGreaterThanOrEqualTo(String value) {
            addCriterion("LAST_TIME >=", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeLessThan(String value) {
            addCriterion("LAST_TIME <", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeLessThanOrEqualTo(String value) {
            addCriterion("LAST_TIME <=", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeLike(String value) {
            addCriterion("LAST_TIME like", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeNotLike(String value) {
            addCriterion("LAST_TIME not like", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeIn(List<String> values) {
            addCriterion("LAST_TIME in", values, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeNotIn(List<String> values) {
            addCriterion("LAST_TIME not in", values, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeBetween(String value1, String value2) {
            addCriterion("LAST_TIME between", value1, value2, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeNotBetween(String value1, String value2) {
            addCriterion("LAST_TIME not between", value1, value2, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastloginTimeIsNull() {
            addCriterion("LASTLOGIN_TIME is null");
            return (Criteria) this;
        }

        public Criteria andLastloginTimeIsNotNull() {
            addCriterion("LASTLOGIN_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andLastloginTimeEqualTo(String value) {
            addCriterion("LASTLOGIN_TIME =", value, "lastloginTime");
            return (Criteria) this;
        }

        public Criteria andLastloginTimeNotEqualTo(String value) {
            addCriterion("LASTLOGIN_TIME <>", value, "lastloginTime");
            return (Criteria) this;
        }

        public Criteria andLastloginTimeGreaterThan(String value) {
            addCriterion("LASTLOGIN_TIME >", value, "lastloginTime");
            return (Criteria) this;
        }

        public Criteria andLastloginTimeGreaterThanOrEqualTo(String value) {
            addCriterion("LASTLOGIN_TIME >=", value, "lastloginTime");
            return (Criteria) this;
        }

        public Criteria andLastloginTimeLessThan(String value) {
            addCriterion("LASTLOGIN_TIME <", value, "lastloginTime");
            return (Criteria) this;
        }

        public Criteria andLastloginTimeLessThanOrEqualTo(String value) {
            addCriterion("LASTLOGIN_TIME <=", value, "lastloginTime");
            return (Criteria) this;
        }

        public Criteria andLastloginTimeLike(String value) {
            addCriterion("LASTLOGIN_TIME like", value, "lastloginTime");
            return (Criteria) this;
        }

        public Criteria andLastloginTimeNotLike(String value) {
            addCriterion("LASTLOGIN_TIME not like", value, "lastloginTime");
            return (Criteria) this;
        }

        public Criteria andLastloginTimeIn(List<String> values) {
            addCriterion("LASTLOGIN_TIME in", values, "lastloginTime");
            return (Criteria) this;
        }

        public Criteria andLastloginTimeNotIn(List<String> values) {
            addCriterion("LASTLOGIN_TIME not in", values, "lastloginTime");
            return (Criteria) this;
        }

        public Criteria andLastloginTimeBetween(String value1, String value2) {
            addCriterion("LASTLOGIN_TIME between", value1, value2, "lastloginTime");
            return (Criteria) this;
        }

        public Criteria andLastloginTimeNotBetween(String value1, String value2) {
            addCriterion("LASTLOGIN_TIME not between", value1, value2, "lastloginTime");
            return (Criteria) this;
        }

        public Criteria andWorkstatusIsNull() {
            addCriterion("WORKSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andWorkstatusIsNotNull() {
            addCriterion("WORKSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andWorkstatusEqualTo(Long value) {
            addCriterion("WORKSTATUS =", value, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusNotEqualTo(Long value) {
            addCriterion("WORKSTATUS <>", value, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusGreaterThan(Long value) {
            addCriterion("WORKSTATUS >", value, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusGreaterThanOrEqualTo(Long value) {
            addCriterion("WORKSTATUS >=", value, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusLessThan(Long value) {
            addCriterion("WORKSTATUS <", value, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusLessThanOrEqualTo(Long value) {
            addCriterion("WORKSTATUS <=", value, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusIn(List<Long> values) {
            addCriterion("WORKSTATUS in", values, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusNotIn(List<Long> values) {
            addCriterion("WORKSTATUS not in", values, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusBetween(Long value1, Long value2) {
            addCriterion("WORKSTATUS between", value1, value2, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusNotBetween(Long value1, Long value2) {
            addCriterion("WORKSTATUS not between", value1, value2, "workstatus");
            return (Criteria) this;
        }

        public Criteria andChannelIsNull() {
            addCriterion("CHANNEL is null");
            return (Criteria) this;
        }

        public Criteria andChannelIsNotNull() {
            addCriterion("CHANNEL is not null");
            return (Criteria) this;
        }

        public Criteria andChannelEqualTo(String value) {
            addCriterion("CHANNEL =", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelNotEqualTo(String value) {
            addCriterion("CHANNEL <>", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelGreaterThan(String value) {
            addCriterion("CHANNEL >", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelGreaterThanOrEqualTo(String value) {
            addCriterion("CHANNEL >=", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelLessThan(String value) {
            addCriterion("CHANNEL <", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelLessThanOrEqualTo(String value) {
            addCriterion("CHANNEL <=", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelLike(String value) {
            addCriterion("CHANNEL like", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelNotLike(String value) {
            addCriterion("CHANNEL not like", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelIn(List<String> values) {
            addCriterion("CHANNEL in", values, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelNotIn(List<String> values) {
            addCriterion("CHANNEL not in", values, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelBetween(String value1, String value2) {
            addCriterion("CHANNEL between", value1, value2, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelNotBetween(String value1, String value2) {
            addCriterion("CHANNEL not between", value1, value2, "channel");
            return (Criteria) this;
        }

        public Criteria andServiceAgentIsNull() {
            addCriterion("SERVICE_AGENT is null");
            return (Criteria) this;
        }

        public Criteria andServiceAgentIsNotNull() {
            addCriterion("SERVICE_AGENT is not null");
            return (Criteria) this;
        }

        public Criteria andServiceAgentEqualTo(String value) {
            addCriterion("SERVICE_AGENT =", value, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentNotEqualTo(String value) {
            addCriterion("SERVICE_AGENT <>", value, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentGreaterThan(String value) {
            addCriterion("SERVICE_AGENT >", value, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentGreaterThanOrEqualTo(String value) {
            addCriterion("SERVICE_AGENT >=", value, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentLessThan(String value) {
            addCriterion("SERVICE_AGENT <", value, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentLessThanOrEqualTo(String value) {
            addCriterion("SERVICE_AGENT <=", value, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentLike(String value) {
            addCriterion("SERVICE_AGENT like", value, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentNotLike(String value) {
            addCriterion("SERVICE_AGENT not like", value, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentIn(List<String> values) {
            addCriterion("SERVICE_AGENT in", values, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentNotIn(List<String> values) {
            addCriterion("SERVICE_AGENT not in", values, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentBetween(String value1, String value2) {
            addCriterion("SERVICE_AGENT between", value1, value2, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentNotBetween(String value1, String value2) {
            addCriterion("SERVICE_AGENT not between", value1, value2, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentIsNull() {
            addCriterion("SALE_AGENT is null");
            return (Criteria) this;
        }

        public Criteria andSaleAgentIsNotNull() {
            addCriterion("SALE_AGENT is not null");
            return (Criteria) this;
        }

        public Criteria andSaleAgentEqualTo(String value) {
            addCriterion("SALE_AGENT =", value, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentNotEqualTo(String value) {
            addCriterion("SALE_AGENT <>", value, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentGreaterThan(String value) {
            addCriterion("SALE_AGENT >", value, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentGreaterThanOrEqualTo(String value) {
            addCriterion("SALE_AGENT >=", value, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentLessThan(String value) {
            addCriterion("SALE_AGENT <", value, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentLessThanOrEqualTo(String value) {
            addCriterion("SALE_AGENT <=", value, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentLike(String value) {
            addCriterion("SALE_AGENT like", value, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentNotLike(String value) {
            addCriterion("SALE_AGENT not like", value, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentIn(List<String> values) {
            addCriterion("SALE_AGENT in", values, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentNotIn(List<String> values) {
            addCriterion("SALE_AGENT not in", values, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentBetween(String value1, String value2) {
            addCriterion("SALE_AGENT between", value1, value2, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentNotBetween(String value1, String value2) {
            addCriterion("SALE_AGENT not between", value1, value2, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSuppilercodeIsNull() {
            addCriterion("SUPPILERCODE is null");
            return (Criteria) this;
        }

        public Criteria andSuppilercodeIsNotNull() {
            addCriterion("SUPPILERCODE is not null");
            return (Criteria) this;
        }

        public Criteria andSuppilercodeEqualTo(String value) {
            addCriterion("SUPPILERCODE =", value, "suppilercode");
            return (Criteria) this;
        }

        public Criteria andSuppilercodeNotEqualTo(String value) {
            addCriterion("SUPPILERCODE <>", value, "suppilercode");
            return (Criteria) this;
        }

        public Criteria andSuppilercodeGreaterThan(String value) {
            addCriterion("SUPPILERCODE >", value, "suppilercode");
            return (Criteria) this;
        }

        public Criteria andSuppilercodeGreaterThanOrEqualTo(String value) {
            addCriterion("SUPPILERCODE >=", value, "suppilercode");
            return (Criteria) this;
        }

        public Criteria andSuppilercodeLessThan(String value) {
            addCriterion("SUPPILERCODE <", value, "suppilercode");
            return (Criteria) this;
        }

        public Criteria andSuppilercodeLessThanOrEqualTo(String value) {
            addCriterion("SUPPILERCODE <=", value, "suppilercode");
            return (Criteria) this;
        }

        public Criteria andSuppilercodeLike(String value) {
            addCriterion("SUPPILERCODE like", value, "suppilercode");
            return (Criteria) this;
        }

        public Criteria andSuppilercodeNotLike(String value) {
            addCriterion("SUPPILERCODE not like", value, "suppilercode");
            return (Criteria) this;
        }

        public Criteria andSuppilercodeIn(List<String> values) {
            addCriterion("SUPPILERCODE in", values, "suppilercode");
            return (Criteria) this;
        }

        public Criteria andSuppilercodeNotIn(List<String> values) {
            addCriterion("SUPPILERCODE not in", values, "suppilercode");
            return (Criteria) this;
        }

        public Criteria andSuppilercodeBetween(String value1, String value2) {
            addCriterion("SUPPILERCODE between", value1, value2, "suppilercode");
            return (Criteria) this;
        }

        public Criteria andSuppilercodeNotBetween(String value1, String value2) {
            addCriterion("SUPPILERCODE not between", value1, value2, "suppilercode");
            return (Criteria) this;
        }

        public Criteria andSuppilernameIsNull() {
            addCriterion("SUPPILERNAME is null");
            return (Criteria) this;
        }

        public Criteria andSuppilernameIsNotNull() {
            addCriterion("SUPPILERNAME is not null");
            return (Criteria) this;
        }

        public Criteria andSuppilernameEqualTo(String value) {
            addCriterion("SUPPILERNAME =", value, "suppilername");
            return (Criteria) this;
        }

        public Criteria andSuppilernameNotEqualTo(String value) {
            addCriterion("SUPPILERNAME <>", value, "suppilername");
            return (Criteria) this;
        }

        public Criteria andSuppilernameGreaterThan(String value) {
            addCriterion("SUPPILERNAME >", value, "suppilername");
            return (Criteria) this;
        }

        public Criteria andSuppilernameGreaterThanOrEqualTo(String value) {
            addCriterion("SUPPILERNAME >=", value, "suppilername");
            return (Criteria) this;
        }

        public Criteria andSuppilernameLessThan(String value) {
            addCriterion("SUPPILERNAME <", value, "suppilername");
            return (Criteria) this;
        }

        public Criteria andSuppilernameLessThanOrEqualTo(String value) {
            addCriterion("SUPPILERNAME <=", value, "suppilername");
            return (Criteria) this;
        }

        public Criteria andSuppilernameLike(String value) {
            addCriterion("SUPPILERNAME like", value, "suppilername");
            return (Criteria) this;
        }

        public Criteria andSuppilernameNotLike(String value) {
            addCriterion("SUPPILERNAME not like", value, "suppilername");
            return (Criteria) this;
        }

        public Criteria andSuppilernameIn(List<String> values) {
            addCriterion("SUPPILERNAME in", values, "suppilername");
            return (Criteria) this;
        }

        public Criteria andSuppilernameNotIn(List<String> values) {
            addCriterion("SUPPILERNAME not in", values, "suppilername");
            return (Criteria) this;
        }

        public Criteria andSuppilernameBetween(String value1, String value2) {
            addCriterion("SUPPILERNAME between", value1, value2, "suppilername");
            return (Criteria) this;
        }

        public Criteria andSuppilernameNotBetween(String value1, String value2) {
            addCriterion("SUPPILERNAME not between", value1, value2, "suppilername");
            return (Criteria) this;
        }

        public Criteria andCreationtimeIsNull() {
            addCriterion("CREATIONTIME is null");
            return (Criteria) this;
        }

        public Criteria andCreationtimeIsNotNull() {
            addCriterion("CREATIONTIME is not null");
            return (Criteria) this;
        }

        public Criteria andCreationtimeEqualTo(String value) {
            addCriterion("CREATIONTIME =", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeNotEqualTo(String value) {
            addCriterion("CREATIONTIME <>", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeGreaterThan(String value) {
            addCriterion("CREATIONTIME >", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeGreaterThanOrEqualTo(String value) {
            addCriterion("CREATIONTIME >=", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeLessThan(String value) {
            addCriterion("CREATIONTIME <", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeLessThanOrEqualTo(String value) {
            addCriterion("CREATIONTIME <=", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeLike(String value) {
            addCriterion("CREATIONTIME like", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeNotLike(String value) {
            addCriterion("CREATIONTIME not like", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeIn(List<String> values) {
            addCriterion("CREATIONTIME in", values, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeNotIn(List<String> values) {
            addCriterion("CREATIONTIME not in", values, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeBetween(String value1, String value2) {
            addCriterion("CREATIONTIME between", value1, value2, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeNotBetween(String value1, String value2) {
            addCriterion("CREATIONTIME not between", value1, value2, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNull() {
            addCriterion("CREATOR is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNotNull() {
            addCriterion("CREATOR is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorEqualTo(String value) {
            addCriterion("CREATOR =", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotEqualTo(String value) {
            addCriterion("CREATOR <>", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThan(String value) {
            addCriterion("CREATOR >", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThanOrEqualTo(String value) {
            addCriterion("CREATOR >=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThan(String value) {
            addCriterion("CREATOR <", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThanOrEqualTo(String value) {
            addCriterion("CREATOR <=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLike(String value) {
            addCriterion("CREATOR like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotLike(String value) {
            addCriterion("CREATOR not like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorIn(List<String> values) {
            addCriterion("CREATOR in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotIn(List<String> values) {
            addCriterion("CREATOR not in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorBetween(String value1, String value2) {
            addCriterion("CREATOR between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotBetween(String value1, String value2) {
            addCriterion("CREATOR not between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeIsNull() {
            addCriterion("MODIFIEDTIME is null");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeIsNotNull() {
            addCriterion("MODIFIEDTIME is not null");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeEqualTo(String value) {
            addCriterion("MODIFIEDTIME =", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeNotEqualTo(String value) {
            addCriterion("MODIFIEDTIME <>", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeGreaterThan(String value) {
            addCriterion("MODIFIEDTIME >", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeGreaterThanOrEqualTo(String value) {
            addCriterion("MODIFIEDTIME >=", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeLessThan(String value) {
            addCriterion("MODIFIEDTIME <", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeLessThanOrEqualTo(String value) {
            addCriterion("MODIFIEDTIME <=", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeLike(String value) {
            addCriterion("MODIFIEDTIME like", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeNotLike(String value) {
            addCriterion("MODIFIEDTIME not like", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeIn(List<String> values) {
            addCriterion("MODIFIEDTIME in", values, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeNotIn(List<String> values) {
            addCriterion("MODIFIEDTIME not in", values, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeBetween(String value1, String value2) {
            addCriterion("MODIFIEDTIME between", value1, value2, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeNotBetween(String value1, String value2) {
            addCriterion("MODIFIEDTIME not between", value1, value2, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifierIsNull() {
            addCriterion("MODIFIER is null");
            return (Criteria) this;
        }

        public Criteria andModifierIsNotNull() {
            addCriterion("MODIFIER is not null");
            return (Criteria) this;
        }

        public Criteria andModifierEqualTo(String value) {
            addCriterion("MODIFIER =", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierNotEqualTo(String value) {
            addCriterion("MODIFIER <>", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierGreaterThan(String value) {
            addCriterion("MODIFIER >", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierGreaterThanOrEqualTo(String value) {
            addCriterion("MODIFIER >=", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierLessThan(String value) {
            addCriterion("MODIFIER <", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierLessThanOrEqualTo(String value) {
            addCriterion("MODIFIER <=", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierLike(String value) {
            addCriterion("MODIFIER like", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierNotLike(String value) {
            addCriterion("MODIFIER not like", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierIn(List<String> values) {
            addCriterion("MODIFIER in", values, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierNotIn(List<String> values) {
            addCriterion("MODIFIER not in", values, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierBetween(String value1, String value2) {
            addCriterion("MODIFIER between", value1, value2, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierNotBetween(String value1, String value2) {
            addCriterion("MODIFIER not between", value1, value2, "modifier");
            return (Criteria) this;
        }

        public Criteria andDrIsNull() {
            addCriterion("DR is null");
            return (Criteria) this;
        }

        public Criteria andDrIsNotNull() {
            addCriterion("DR is not null");
            return (Criteria) this;
        }

        public Criteria andDrEqualTo(Long value) {
            addCriterion("DR =", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrNotEqualTo(Long value) {
            addCriterion("DR <>", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrGreaterThan(Long value) {
            addCriterion("DR >", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrGreaterThanOrEqualTo(Long value) {
            addCriterion("DR >=", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrLessThan(Long value) {
            addCriterion("DR <", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrLessThanOrEqualTo(Long value) {
            addCriterion("DR <=", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrIn(List<Long> values) {
            addCriterion("DR in", values, "dr");
            return (Criteria) this;
        }

        public Criteria andDrNotIn(List<Long> values) {
            addCriterion("DR not in", values, "dr");
            return (Criteria) this;
        }

        public Criteria andDrBetween(Long value1, Long value2) {
            addCriterion("DR between", value1, value2, "dr");
            return (Criteria) this;
        }

        public Criteria andDrNotBetween(Long value1, Long value2) {
            addCriterion("DR not between", value1, value2, "dr");
            return (Criteria) this;
        }

        public Criteria andVersionIsNull() {
            addCriterion("VERSION is null");
            return (Criteria) this;
        }

        public Criteria andVersionIsNotNull() {
            addCriterion("VERSION is not null");
            return (Criteria) this;
        }

        public Criteria andVersionEqualTo(Long value) {
            addCriterion("VERSION =", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotEqualTo(Long value) {
            addCriterion("VERSION <>", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionGreaterThan(Long value) {
            addCriterion("VERSION >", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionGreaterThanOrEqualTo(Long value) {
            addCriterion("VERSION >=", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionLessThan(Long value) {
            addCriterion("VERSION <", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionLessThanOrEqualTo(Long value) {
            addCriterion("VERSION <=", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionIn(List<Long> values) {
            addCriterion("VERSION in", values, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotIn(List<Long> values) {
            addCriterion("VERSION not in", values, "version");
            return (Criteria) this;
        }

        public Criteria andVersionBetween(Long value1, Long value2) {
            addCriterion("VERSION between", value1, value2, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotBetween(Long value1, Long value2) {
            addCriterion("VERSION not between", value1, value2, "version");
            return (Criteria) this;
        }

        public Criteria andPkOrgIsNull() {
            addCriterion("PK_ORG is null");
            return (Criteria) this;
        }

        public Criteria andPkOrgIsNotNull() {
            addCriterion("PK_ORG is not null");
            return (Criteria) this;
        }

        public Criteria andPkOrgEqualTo(String value) {
            addCriterion("PK_ORG =", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgNotEqualTo(String value) {
            addCriterion("PK_ORG <>", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgGreaterThan(String value) {
            addCriterion("PK_ORG >", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgGreaterThanOrEqualTo(String value) {
            addCriterion("PK_ORG >=", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgLessThan(String value) {
            addCriterion("PK_ORG <", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgLessThanOrEqualTo(String value) {
            addCriterion("PK_ORG <=", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgLike(String value) {
            addCriterion("PK_ORG like", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgNotLike(String value) {
            addCriterion("PK_ORG not like", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgIn(List<String> values) {
            addCriterion("PK_ORG in", values, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgNotIn(List<String> values) {
            addCriterion("PK_ORG not in", values, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgBetween(String value1, String value2) {
            addCriterion("PK_ORG between", value1, value2, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgNotBetween(String value1, String value2) {
            addCriterion("PK_ORG not between", value1, value2, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andSimidIsNull() {
            addCriterion("SIMID is null");
            return (Criteria) this;
        }

        public Criteria andSimidIsNotNull() {
            addCriterion("SIMID is not null");
            return (Criteria) this;
        }

        public Criteria andSimidEqualTo(String value) {
            addCriterion("SIMID =", value, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidNotEqualTo(String value) {
            addCriterion("SIMID <>", value, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidGreaterThan(String value) {
            addCriterion("SIMID >", value, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidGreaterThanOrEqualTo(String value) {
            addCriterion("SIMID >=", value, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidLessThan(String value) {
            addCriterion("SIMID <", value, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidLessThanOrEqualTo(String value) {
            addCriterion("SIMID <=", value, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidLike(String value) {
            addCriterion("SIMID like", value, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidNotLike(String value) {
            addCriterion("SIMID not like", value, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidIn(List<String> values) {
            addCriterion("SIMID in", values, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidNotIn(List<String> values) {
            addCriterion("SIMID not in", values, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidBetween(String value1, String value2) {
            addCriterion("SIMID between", value1, value2, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidNotBetween(String value1, String value2) {
            addCriterion("SIMID not between", value1, value2, "simid");
            return (Criteria) this;
        }

        public Criteria andIsnewIsNull() {
            addCriterion("ISNEW is null");
            return (Criteria) this;
        }

        public Criteria andIsnewIsNotNull() {
            addCriterion("ISNEW is not null");
            return (Criteria) this;
        }

        public Criteria andIsnewEqualTo(String value) {
            addCriterion("ISNEW =", value, "isnew");
            return (Criteria) this;
        }

        public Criteria andIsnewNotEqualTo(String value) {
            addCriterion("ISNEW <>", value, "isnew");
            return (Criteria) this;
        }

        public Criteria andIsnewGreaterThan(String value) {
            addCriterion("ISNEW >", value, "isnew");
            return (Criteria) this;
        }

        public Criteria andIsnewGreaterThanOrEqualTo(String value) {
            addCriterion("ISNEW >=", value, "isnew");
            return (Criteria) this;
        }

        public Criteria andIsnewLessThan(String value) {
            addCriterion("ISNEW <", value, "isnew");
            return (Criteria) this;
        }

        public Criteria andIsnewLessThanOrEqualTo(String value) {
            addCriterion("ISNEW <=", value, "isnew");
            return (Criteria) this;
        }

        public Criteria andIsnewLike(String value) {
            addCriterion("ISNEW like", value, "isnew");
            return (Criteria) this;
        }

        public Criteria andIsnewNotLike(String value) {
            addCriterion("ISNEW not like", value, "isnew");
            return (Criteria) this;
        }

        public Criteria andIsnewIn(List<String> values) {
            addCriterion("ISNEW in", values, "isnew");
            return (Criteria) this;
        }

        public Criteria andIsnewNotIn(List<String> values) {
            addCriterion("ISNEW not in", values, "isnew");
            return (Criteria) this;
        }

        public Criteria andIsnewBetween(String value1, String value2) {
            addCriterion("ISNEW between", value1, value2, "isnew");
            return (Criteria) this;
        }

        public Criteria andIsnewNotBetween(String value1, String value2) {
            addCriterion("ISNEW not between", value1, value2, "isnew");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }

        public Criteria andLogicalDeleted(boolean deleted) {
            return deleted ? andDrEqualTo(TerminalPojo.Dr.IS_DELETED.value()) : andDrNotEqualTo(TerminalPojo.Dr.IS_DELETED.value());
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}