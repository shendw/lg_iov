package com.yonyou.longgong.carsserver.dto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-11-07
 * @描述
 **/
@Data
public class CardocSelectDto {
    /**
     * 车型型号
     */
    private String carmodel;
    /**
     * 机型
     */
    private String machinemodel;
    /**
     * 整车编号
     */
    private String carno;
    /**
     * 车辆流程状态
     */
    private String status;

    private Integer pageSize = 10;

    private Integer pageNum = 1;
}
