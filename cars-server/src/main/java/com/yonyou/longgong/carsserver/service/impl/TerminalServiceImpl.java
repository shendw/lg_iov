package com.yonyou.longgong.carsserver.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dao.TerminalCarPojoMapper;
import com.yonyou.longgong.carsserver.dao.TerminalPojoMapper;
import com.yonyou.longgong.carsserver.dto.TerminalSelectDto;
import com.yonyou.longgong.carsserver.entity.UserIdentity;
import com.yonyou.longgong.carsserver.enums.TerminalCarStatus;
import com.yonyou.longgong.carsserver.enums.TerminalStatus;
import com.yonyou.longgong.carsserver.example.TerminalCarPojoExample;
import com.yonyou.longgong.carsserver.example.TerminalPojoExample;
import com.yonyou.longgong.carsserver.excel.ExcelVerifyCardocHandlerImpl;
import com.yonyou.longgong.carsserver.excel.ExcelVerifyCardocPojoOfMode;
import com.yonyou.longgong.carsserver.excel.ExcelVerifyTerminalHandlerImpl;
import com.yonyou.longgong.carsserver.excel.ExcelVerifyTerminalPojoOfMode;
import com.yonyou.longgong.carsserver.exception.BusinessException;
import com.yonyou.longgong.carsserver.pojo.SuppilerdocPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalCarPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalPojo;
import com.yonyou.longgong.carsserver.service.ISimService;
import com.yonyou.longgong.carsserver.service.ISuppilerdocService;
import com.yonyou.longgong.carsserver.service.ITerminalService;
import com.yonyou.longgong.carsserver.utils.DataTimeUtil;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.UUIDUtils;
import com.yonyou.longgong.carsserver.utils.UserIdentityUtil;
import com.yonyou.longgong.carsserver.vo.TerminalFinishedVO;
import com.yonyou.longgong.iotserver.service.IDeviceService;
import com.yonyou.longgong.iotserver.vo.DeviceDataVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author shendawei
 * @创建时间 2019-11-06
 * @描述
 **/
@Service("iTerminalService")
public class TerminalServiceImpl implements ITerminalService {

    @Autowired
    private TerminalPojoMapper terminalPojoMapper;

    @Autowired
    private TerminalCarPojoMapper terminalCarPojoMapper;

    @Autowired
    private IDeviceService deviceService;

    @Autowired
    private ISuppilerdocService iSuppilerdocService;

    @Autowired
    private ISimService iSimService;

    @Autowired
    private UserIdentityUtil userIdentityUtil;

    @Override
    public PageInfo<TerminalPojo> selectTerminalsTest(TerminalSelectDto selectDto,String userId,String loginName) {
        return selectTerminals(selectDto,true,userId,loginName);
    }

    @Override
    public PageInfo<TerminalFinishedVO> selectTerminalsFinished(TerminalSelectDto selectDto, String userId, String loginName) {
        List<String> status = new ArrayList<>();
        if(StringUtils.isEmpty(selectDto.getStatus())){
            status.add(TerminalStatus.WORK.getCode().toString());
            status.add(TerminalStatus.FINISHED.getCode().toString());
        }else {
            status.add(selectDto.getStatus());
        }
        //通过登录名去获取用户身份信息，如果没有会提前抛出异常
        UserIdentity userIdentity = userIdentityUtil.getUserIdentity(userId, loginName);
        String agentCode = userIdentity.getAgentCode();
        HashMap<String, Object> param = new HashMap<>();
        param.put("carno",selectDto.getCarNo());
        param.put("status",status);
        param.put("terminalid",selectDto.getTerminalid());
        param.put("simid",selectDto.getSimid());
        param.put("channel",selectDto.getChannel());
        String productiontimeLow = selectDto.getProductiontimeLow();
        if (StringUtils.isNotBlank(productiontimeLow)){
            productiontimeLow +=DataTimeUtil.DEFAULTSTARTTIME;
        }
        param.put("productiontimebegin", productiontimeLow);
        String productiontimeHigh = selectDto.getProductiontimeHigh();
        if (StringUtils.isNotBlank(productiontimeHigh)){
            productiontimeHigh +=DataTimeUtil.DEFAULTENDTIME;
        }
        param.put("productiontimeend", productiontimeHigh);
        param.put("agentcode",agentCode);
        PageHelper.startPage(selectDto.getPageNum(),selectDto.getPageSize());
        List<TerminalFinishedVO> terminalFinishedVOS = terminalPojoMapper.select4Finished(param);
        return new PageInfo<>(terminalFinishedVOS);
    }

    @Override
    public PageInfo<TerminalPojo> selectTerminalsProcess(TerminalSelectDto selectDto,String userId,String loginName) {
        if(StringUtils.isEmpty(selectDto.getStatus())){
            String status = TerminalStatus.WORK.getCode() + "," + TerminalStatus.FINISHED.getCode();
            selectDto.setStatus(status);
        }
        return selectTerminals(selectDto,false,userId,loginName);
    }

    private PageInfo<TerminalPojo> selectTerminals(TerminalSelectDto selectDto,Boolean isTest,String userId,String loginName){
        //通过登录名去获取用户身份信息，如果没有会提前抛出异常
        UserIdentity userIdentity = userIdentityUtil.getUserIdentity(userId, loginName);
        String agentCode = userIdentity.getAgentCode();
        if (StringUtils.isNotBlank(selectDto.getCarNo())){
            TerminalCarPojoExample terminalCarPojoExample = new TerminalCarPojoExample();
            terminalCarPojoExample.createCriteria().andCarnoEqualTo(selectDto.getCarNo()).andLogicalDeleted(false).andEnablestateEqualTo(TerminalCarStatus.EFFECTIVE.getCode());
            List<TerminalCarPojo> terminalCarPojos = terminalCarPojoMapper.selectByExample(terminalCarPojoExample);
            if (CollectionUtils.isEmpty(terminalCarPojos)){
                return new PageInfo<>();
            }
            PageHelper.startPage(selectDto.getPageNum(),selectDto.getPageSize());
            TerminalPojoExample terminalPojoExample = new TerminalPojoExample();
            TerminalPojoExample.Criteria criteria = terminalPojoExample.createCriteria().andLogicalDeleted(false).andTerminalidEqualTo(terminalCarPojos.get(0).getTerminalid());
            //如果渠道商信息不为空，通过渠道商编码去过滤数据
            if (StringUtils.isNotBlank(agentCode)){
                criteria.andSuppilercodeEqualTo(agentCode);
            }
            List<TerminalPojo> terminalPojos = terminalPojoMapper.selectByExample(terminalPojoExample);
            if (CollectionUtils.isEmpty(terminalPojos)){
                return new PageInfo<>();
            }
            terminalPojos.get(0).setTerminalBodyPojos(terminalCarPojos);
            return new PageInfo<>(terminalPojos);
        }
        TerminalPojoExample terminalPojoExample = selectRule(selectDto,agentCode);
        PageHelper.startPage(selectDto.getPageNum(),selectDto.getPageSize());
        List<TerminalPojo> terminalPojos = terminalPojoMapper.selectByExample(terminalPojoExample);
        PageInfo<TerminalPojo> terminalPojoPageInfo = new PageInfo<>(terminalPojos);
        if(!CollectionUtils.isEmpty(terminalPojos)){
            TerminalCarPojoExample terminalCarPojoExample = new TerminalCarPojoExample();
            for (TerminalPojo terminalPojo:
                    terminalPojos) {
                terminalCarPojoExample.clear();
                terminalCarPojoExample.createCriteria().andLogicalDeleted(false).andTerminalidEqualTo(terminalPojo.getTerminalid());
                List<TerminalCarPojo> terminalCarPojos = terminalCarPojoMapper.selectByExample(terminalCarPojoExample);
                terminalPojo.setTerminalBodyPojos(terminalCarPojos);
                //绑iot的数据
//                DeviceDataVO deviceData = deviceService.getDeviceData(terminalPojo.getTerminalid(),isTest);
//                if (deviceData != null){
//                    terminalPojo.setChannel(deviceData.getCommunication_mode());
//                    terminalPojo.setWorkstatus("true".equals(deviceData.getMachine_start())?0L:1L);
//                    terminalPojo.setLastTime(deviceData.getLast_time());
//                    terminalPojo.setLastloginTime(deviceData.getLatest_landing_time());
//                }
            }
        }
        //20200306 新增对最新通讯时间排序
        Collections.sort(terminalPojos, new Comparator<TerminalPojo>() {
            @Override
            public int compare(TerminalPojo o1, TerminalPojo o2) {
                String o1LastTime = o1.getLastTime() == null?"0":o1.getLastTime();
                String o2LastTime = o2.getLastTime() == null?"0":o2.getLastTime();
                return o2LastTime.compareTo(o1LastTime);
            }
        });
        return terminalPojoPageInfo;
    }

    @Override
    public List<TerminalPojo> selectTerminalsByExcel(TerminalSelectDto selectDto,String userId,String loginName) {
        //通过登录名去获取用户身份信息，如果没有会提前抛出异常
        UserIdentity userIdentity = userIdentityUtil.getUserIdentity(userId, loginName);
        String agentCode = userIdentity.getAgentCode();
        if (StringUtils.isNotBlank(selectDto.getCarNo())){
            TerminalCarPojoExample terminalCarPojoExample = new TerminalCarPojoExample();
            terminalCarPojoExample.createCriteria().andCarnoEqualTo(selectDto.getCarNo()).andLogicalDeleted(false).andEnablestateEqualTo(TerminalCarStatus.EFFECTIVE.getCode());
            List<TerminalCarPojo> terminalCarPojos = terminalCarPojoMapper.selectByExample(terminalCarPojoExample);
            if (CollectionUtils.isEmpty(terminalCarPojos)){
                return new ArrayList<>();
            }
            PageHelper.startPage(selectDto.getPageNum(),selectDto.getPageSize());
            TerminalPojoExample terminalPojoExample = new TerminalPojoExample();
            TerminalPojoExample.Criteria criteria = terminalPojoExample.createCriteria().andLogicalDeleted(false).andTerminalidEqualTo(terminalCarPojos.get(0).getTerminalid());
            //如果渠道商信息不为空，通过渠道商编码去过滤数据
            if (StringUtils.isNotBlank(agentCode)){
                criteria.andSuppilercodeEqualTo(agentCode);
            }
            List<TerminalPojo> terminalPojos = terminalPojoMapper.selectByExample(terminalPojoExample);
            if (CollectionUtils.isEmpty(terminalPojos)){
                return new ArrayList<>();
            }
            terminalPojos.get(0).setTerminalBodyPojos(terminalCarPojos);
            return terminalPojos;
        }
        TerminalPojoExample terminalPojoExample = selectRule(selectDto,agentCode);
        List<TerminalPojo> terminalPojos = terminalPojoMapper.selectByExample(terminalPojoExample);
        if(!CollectionUtils.isEmpty(terminalPojos)){
            TerminalCarPojoExample terminalCarPojoExample = new TerminalCarPojoExample();
            for (TerminalPojo terminalPojo:
                    terminalPojos) {
                terminalCarPojoExample.clear();
                terminalCarPojoExample.createCriteria().andLogicalDeleted(false).andTerminalidEqualTo(terminalPojo.getTerminalid());
                List<TerminalCarPojo> terminalCarPojos = terminalCarPojoMapper.selectByExample(terminalCarPojoExample);
                terminalPojo.setTerminalBodyPojos(terminalCarPojos);
            }
        }
        return terminalPojos;
    }

    private TerminalPojoExample selectRule(TerminalSelectDto selectDto,String agentCode){
        TerminalPojoExample terminalPojoExample = new TerminalPojoExample();
        TerminalPojoExample.Criteria criteria = terminalPojoExample.createCriteria();
        if (StringUtils.isNotBlank(selectDto.getPkOrg())){
            criteria.andPkOrgEqualTo(selectDto.getPkOrg());
        }
        if (StringUtils.isNotBlank(selectDto.getTerminalid())){
            criteria.andTerminalidLike("%"+selectDto.getTerminalid()+"%");
        }
        if (StringUtils.isNotBlank(selectDto.getSimid())){
            criteria.andSimidLike("%"+selectDto.getSimid()+"%");
        }
        if (StringUtils.isNotBlank(selectDto.getStatus())){
            String[] status = selectDto.getStatus().split(",");
            if (status.length>1){
                List<Long> statu = Arrays.stream(status).map(item -> Long.parseLong(item)).collect(Collectors.toList());
                criteria.andStatusIn(statu);
            }else {
                criteria.andStatusEqualTo(Long.parseLong(selectDto.getStatus()));
            }
        }
        if (StringUtils.isNotBlank(selectDto.getWorkstatus())){
            criteria.andWorkstatusEqualTo(Long.parseLong(selectDto.getWorkstatus()));
        }
        if (StringUtils.isNotBlank(selectDto.getChannel())){
            criteria.andChannelEqualTo(selectDto.getChannel());
        }
        if (StringUtils.isNotBlank(selectDto.getProductiontimeLow()) && StringUtils.isNotBlank(selectDto.getProductiontimeHigh())){
            criteria.andProductiontimeBetween(selectDto.getProductiontimeLow(),selectDto.getProductiontimeHigh());
        }else if (StringUtils.isNotBlank(selectDto.getProductiontimeLow())){
            criteria.andProductiontimeGreaterThanOrEqualTo(selectDto.getProductiontimeLow());
        }else if (StringUtils.isNotBlank(selectDto.getProductiontimeHigh())){
            criteria.andProductiontimeLessThanOrEqualTo(selectDto.getProductiontimeHigh());
        }
        if (StringUtils.isNotBlank(selectDto.getFinishedtimeLow()) && StringUtils.isNotBlank(selectDto.getFinishedtimeHigh())){
            criteria.andFinishedtimeBetween(selectDto.getFinishedtimeLow(),selectDto.getFinishedtimeHigh());
        }else if (StringUtils.isNotBlank(selectDto.getFinishedtimeLow())){
            criteria.andFinishedtimeGreaterThanOrEqualTo(selectDto.getFinishedtimeLow());
        }else if (StringUtils.isNotBlank(selectDto.getFinishedtimeHigh())){
            criteria.andFinishedtimeLessThanOrEqualTo(selectDto.getFinishedtimeHigh());
        }
        if (StringUtils.isNotBlank(selectDto.getLasttimeLow())){
            criteria.andLastTimeGreaterThanOrEqualTo(selectDto.getLasttimeLow());
        }
        if (StringUtils.isNotBlank(selectDto.getLasttimeHigh())){
            criteria.andLastTimeLessThanOrEqualTo(selectDto.getLasttimeHigh());
        }
        if (StringUtils.isNotBlank(agentCode)){
            criteria.andSuppilercodeEqualTo(agentCode);
        }
        criteria.andLogicalDeleted(false);
        terminalPojoExample.setOrderByClause(" LAST_TIME desc nulls  last");
        return terminalPojoExample;
    }

    @Override
    public Integer updateTermianals(TerminalPojo terminalPojo) {
        if (StringUtils.isBlank(terminalPojo.getPkTerminal())){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        return terminalPojoMapper.updateWithVersionByPrimaryKeySelective(terminalPojo.getVersion(), terminalPojo);
    }

    @Override
    public Integer changeStatus(TerminalPojo terminalPojo) {
        if (StringUtils.isBlank(terminalPojo.getTerminalid())){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        TerminalPojoExample terminalPojoExample = new TerminalPojoExample();
        terminalPojoExample.createCriteria().andLogicalDeleted(false).andTerminalidEqualTo(terminalPojo.getTerminalid());
        List<TerminalPojo> terminalPojos = terminalPojoMapper.selectByExample(terminalPojoExample);
        if (CollectionUtils.isEmpty(terminalPojos)){
            throw new BusinessException(ResponseCode.TERMINALID_IS_NULL);
        }
        TerminalPojo terminal = terminalPojos.get(0);
        if (!TerminalStatus.DEFEAT.getCode().equals(terminal.getStatus())){
            throw new BusinessException(ResponseCode.TERMINALSTATUS_ERROR);
        }
        terminal.setStatus(TerminalStatus.FINISHED.getCode());
        terminal.setFinishedtime(DataTimeUtil.dateToStr(new Date()));
        return terminalPojoMapper.updateByPrimaryKeySelective(terminal,TerminalPojo.Column.status);
    }

    @Override
    public String selectTerminalidBySimid(String simid) {
        TerminalPojoExample terminalPojoExample = new TerminalPojoExample();
        terminalPojoExample.createCriteria().andLogicalDeleted(false).andSimidEqualTo(simid);
        List<TerminalPojo> terminalPojos = terminalPojoMapper.selectByExample(terminalPojoExample);
        if (CollectionUtils.isEmpty(terminalPojos)){
            return null;
        }
        return terminalPojos.get(0).getTerminalid();
    }

    @Override
    public TerminalPojo selectTerminalByTerminalid(String terminalid) {
        TerminalPojoExample terminalPojoExample = new TerminalPojoExample();
        terminalPojoExample.createCriteria().andLogicalDeleted(false).andTerminalidEqualTo(terminalid);
        List<TerminalPojo> terminalPojos = terminalPojoMapper.selectByExample(terminalPojoExample);
        if (CollectionUtils.isEmpty(terminalPojos)){
            return null;
        }
        return terminalPojos.get(0);
    }

    @Override
    public Integer insertTerminal(TerminalPojo terminalPojo) {
        terminalPojo.setPkTerminal(UUIDUtils.getUniqueIdByUUId());
        SuppilerdocPojo suppilerdocPojo = iSuppilerdocService.selectSuppilerByKeyword(terminalPojo.getTerminalid().substring(0, 1));
        if (suppilerdocPojo != null){
            terminalPojo.setSuppilercode(suppilerdocPojo.getCode());
            terminalPojo.setSuppilername(suppilerdocPojo.getName());
        }
        return terminalPojoMapper.insertSelective(terminalPojo);
    }

    @Override
    public ExcelImportResult<ExcelVerifyTerminalPojoOfMode> checkExcel(MultipartFile terminalExcel) {
        try {
            if (terminalExcel == null) {
                throw new BusinessException(ResponseCode.SIMEXCEL_IS_NULL);
            }
            String fileName = terminalExcel.getOriginalFilename();
            if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
                // 文件格式不正确
                throw new BusinessException(ResponseCode.SIMEXCEL_IS_ILLEGAL);
            }
            ImportParams params = new ImportParams();
            params.setNeedVerify(true);
            params.setVerifyHandler(new ExcelVerifyTerminalHandlerImpl());
            params.setImportFields(new String[] { "终端", "版本","SIM卡号"});
            ExcelImportResult<ExcelVerifyTerminalPojoOfMode> result = ExcelImportUtil.importExcelMore(terminalExcel.getInputStream(), ExcelVerifyTerminalPojoOfMode.class, params);
            return result;
        }catch (Exception e){
            throw new BusinessException(ResponseCode.SIMEXCEL_IS_ERROR);
        }
    }

    @Override
    public String insertTerminalByExcel(ExcelVerifyTerminalPojoOfMode excelVerifyTerminalPojoOfMode) {
        @NotNull String simid = excelVerifyTerminalPojoOfMode.getSimid();
        @NotNull String terminalid = excelVerifyTerminalPojoOfMode.getTerminalid();
        @NotNull String terminalVersion = excelVerifyTerminalPojoOfMode.getTerminalVersion();
        //判断sim卡档案是否存在
        Integer row = iSimService.updateSim(simid, terminalid);
        if (row == 0) {
            return "该sim卡信息没有维护，请去sim卡档案维护！";
        }
        //判断有没有该终端id
        TerminalPojo existTerminalPojo = this.selectTerminalByTerminalid(terminalid);
        if (existTerminalPojo == null){
            TerminalPojo terminalPojo = new TerminalPojo();
            terminalPojo.setTerminalid(terminalid);
            terminalPojo.setTerminalVersion(terminalVersion);
            terminalPojo.setSimid(simid);
            terminalPojo.setStatus(TerminalStatus.FINISHED.getCode());
            terminalPojo.setCreationtime(DataTimeUtil.dateToStr(new Date()));
            Integer insertTerminalRow = this.insertTerminal(terminalPojo);
            if (insertTerminalRow == 0) {
                return "终端信息插入失败！";
            }
        }else {
            return "终端已存在！";
        }
        return null;
    }
}
