package com.yonyou.longgong.carsserver.service;

import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dto.BindErrorSelectDto;
import com.yonyou.longgong.carsserver.pojo.BindErrorInfoPojo;

/**
 * @author shendawei
 * @创建时间 2020/4/20
 * @描述
 **/
public interface IBindErrorService {
    /**
     * 查询终端绑定的错误信息
     * @return
     */
    PageInfo<BindErrorInfoPojo> selectBindError(BindErrorSelectDto bindErrorSelectDto);

    /**
     * 插入自动绑定异常信息
     * @param carno
     * @param terminalid
     * @param errorInfo
     * @return
     */
    Integer insertBindErrorInfo(String carno,String terminalid,String errorInfo);
}
