package com.yonyou.longgong.carsserver.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.handler.inter.IExcelModel;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author shendawei
 * @创建时间 2019/12/26
 * @描述
 **/
@Data
public class ExcelVerifyTerminalPojoOfMode implements IExcelModel {

    @Excel(name = "错误信息",orderNum = "4")
    private String errorMsg;

    @Excel(name = "终端",isImportField = "true",orderNum = "1")
    @NotNull
    private String terminalid;

    @Excel(name = "版本",isImportField = "true",orderNum = "2")
    @NotNull
    private String terminalVersion;

    @Excel(name = "SIM卡号",isImportField = "true",orderNum = "3")
    @NotNull
    private String simid;
}
