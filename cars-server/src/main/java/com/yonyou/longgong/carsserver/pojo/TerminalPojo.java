package com.yonyou.longgong.carsserver.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.*;

@Data
@ExcelTarget("terminalPojo")
public class TerminalPojo implements Serializable {
    public TerminalPojo(String pkTerminal, String terminalid, Long status, String pkOrg, String simid) {
        this.pkTerminal = pkTerminal;
        this.terminalid = terminalid;
        this.status = status;
        this.pkOrg = pkOrg;
        this.simid = simid;
    }

    public TerminalPojo(String pkTerminal, String terminalid, String terminalVersion, Long status, String pkOrg, String simid) {
        this.pkTerminal = pkTerminal;
        this.terminalid = terminalid;
        this.terminalVersion = terminalVersion;
        this.status = status;
        this.pkOrg = pkOrg;
        this.simid = simid;
    }

    public TerminalPojo() {
    }

    public static final Long IS_DELETED = Dr.IS_DELETED.value();

    public static final Long NOT_DELETED = Dr.NOT_DELETED.value();

    private String pkTerminal;

    @Excel(name = "sim卡号",needMerge = true)
    private String simid;

    @Excel(name = "终端版本",needMerge = true)
    private String terminalVersion;

    @Excel(name = "流程状态",needMerge = true)
    private Long status;

    @Excel(name = "终端id",needMerge = true)
    private String terminalid;

    @Excel(name = "生产日期",needMerge = true)
    private String productiontime;

    @Excel(name = "成品日期",needMerge = true)
    private String finishedtime;

    //    @Excel(name = "待修日期",needMerge = true)
    private String pendingtime;

    //    @Excel(name = "返修日期",needMerge = true)
    private String repairedtime;

    //    @Excel(name = "报废日期",needMerge = true)
    private String scraptime;

    @Excel(name = "最后通讯时间",needMerge = true)
    private String lastTime;

    @Excel(name = "最后登陆时间",needMerge = true)
    private String lastloginTime;

    //    @Excel(name = "工作状态",needMerge = true)
    private Long workstatus;

    @Excel(name = "通讯通道",needMerge = true)
    private String channel;

    //    @Excel(name = "服务代理商",needMerge = true)
    private String serviceAgent;

    //    @Excel(name = "销售代理商",needMerge = true)
    private String saleAgent;

    @Excel(name = "供应商编码",needMerge = true)
    private String suppilercode;

    @Excel(name = "供应商名称",needMerge = true)
    private String suppilername;

    private String creationtime;

    private String creator;

    private String modifiedtime;

    private String modifier;

    private Long dr;

    private Long version;

    private String pkOrg;

    @ExcelCollection(name = "终端绑定信息",orderNum = "5")
    private List<TerminalCarPojo> terminalBodyPojos;


    private String isnew;

    private static final long serialVersionUID = 1L;

    public void andLogicalDeleted(boolean deleted) {
        setDr(deleted ? Dr.IS_DELETED.value() : Dr.NOT_DELETED.value());
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        TerminalPojo other = (TerminalPojo) that;
        return (this.getPkTerminal() == null ? other.getPkTerminal() == null : this.getPkTerminal().equals(other.getPkTerminal()))
            && (this.getTerminalid() == null ? other.getTerminalid() == null : this.getTerminalid().equals(other.getTerminalid()))
            && (this.getTerminalVersion() == null ? other.getTerminalVersion() == null : this.getTerminalVersion().equals(other.getTerminalVersion()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getProductiontime() == null ? other.getProductiontime() == null : this.getProductiontime().equals(other.getProductiontime()))
            && (this.getFinishedtime() == null ? other.getFinishedtime() == null : this.getFinishedtime().equals(other.getFinishedtime()))
            && (this.getPendingtime() == null ? other.getPendingtime() == null : this.getPendingtime().equals(other.getPendingtime()))
            && (this.getRepairedtime() == null ? other.getRepairedtime() == null : this.getRepairedtime().equals(other.getRepairedtime()))
            && (this.getScraptime() == null ? other.getScraptime() == null : this.getScraptime().equals(other.getScraptime()))
            && (this.getLastTime() == null ? other.getLastTime() == null : this.getLastTime().equals(other.getLastTime()))
            && (this.getLastloginTime() == null ? other.getLastloginTime() == null : this.getLastloginTime().equals(other.getLastloginTime()))
            && (this.getWorkstatus() == null ? other.getWorkstatus() == null : this.getWorkstatus().equals(other.getWorkstatus()))
            && (this.getChannel() == null ? other.getChannel() == null : this.getChannel().equals(other.getChannel()))
            && (this.getServiceAgent() == null ? other.getServiceAgent() == null : this.getServiceAgent().equals(other.getServiceAgent()))
            && (this.getSaleAgent() == null ? other.getSaleAgent() == null : this.getSaleAgent().equals(other.getSaleAgent()))
            && (this.getSuppilercode() == null ? other.getSuppilercode() == null : this.getSuppilercode().equals(other.getSuppilercode()))
            && (this.getSuppilername() == null ? other.getSuppilername() == null : this.getSuppilername().equals(other.getSuppilername()))
            && (this.getCreationtime() == null ? other.getCreationtime() == null : this.getCreationtime().equals(other.getCreationtime()))
            && (this.getCreator() == null ? other.getCreator() == null : this.getCreator().equals(other.getCreator()))
            && (this.getModifiedtime() == null ? other.getModifiedtime() == null : this.getModifiedtime().equals(other.getModifiedtime()))
            && (this.getModifier() == null ? other.getModifier() == null : this.getModifier().equals(other.getModifier()))
            && (this.getDr() == null ? other.getDr() == null : this.getDr().equals(other.getDr()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()))
            && (this.getPkOrg() == null ? other.getPkOrg() == null : this.getPkOrg().equals(other.getPkOrg()))
            && (this.getSimid() == null ? other.getSimid() == null : this.getSimid().equals(other.getSimid()))
            && (this.getIsnew() == null ? other.getIsnew() == null : this.getIsnew().equals(other.getIsnew()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getPkTerminal() == null) ? 0 : getPkTerminal().hashCode());
        result = prime * result + ((getTerminalid() == null) ? 0 : getTerminalid().hashCode());
        result = prime * result + ((getTerminalVersion() == null) ? 0 : getTerminalVersion().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getProductiontime() == null) ? 0 : getProductiontime().hashCode());
        result = prime * result + ((getFinishedtime() == null) ? 0 : getFinishedtime().hashCode());
        result = prime * result + ((getPendingtime() == null) ? 0 : getPendingtime().hashCode());
        result = prime * result + ((getRepairedtime() == null) ? 0 : getRepairedtime().hashCode());
        result = prime * result + ((getScraptime() == null) ? 0 : getScraptime().hashCode());
        result = prime * result + ((getLastTime() == null) ? 0 : getLastTime().hashCode());
        result = prime * result + ((getLastloginTime() == null) ? 0 : getLastloginTime().hashCode());
        result = prime * result + ((getWorkstatus() == null) ? 0 : getWorkstatus().hashCode());
        result = prime * result + ((getChannel() == null) ? 0 : getChannel().hashCode());
        result = prime * result + ((getServiceAgent() == null) ? 0 : getServiceAgent().hashCode());
        result = prime * result + ((getSaleAgent() == null) ? 0 : getSaleAgent().hashCode());
        result = prime * result + ((getSuppilercode() == null) ? 0 : getSuppilercode().hashCode());
        result = prime * result + ((getSuppilername() == null) ? 0 : getSuppilername().hashCode());
        result = prime * result + ((getCreationtime() == null) ? 0 : getCreationtime().hashCode());
        result = prime * result + ((getCreator() == null) ? 0 : getCreator().hashCode());
        result = prime * result + ((getModifiedtime() == null) ? 0 : getModifiedtime().hashCode());
        result = prime * result + ((getModifier() == null) ? 0 : getModifier().hashCode());
        result = prime * result + ((getDr() == null) ? 0 : getDr().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        result = prime * result + ((getPkOrg() == null) ? 0 : getPkOrg().hashCode());
        result = prime * result + ((getSimid() == null) ? 0 : getSimid().hashCode());
        result = prime * result + ((getIsnew() == null) ? 0 : getIsnew().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", pkTerminal=").append(pkTerminal);
        sb.append(", terminalid=").append(terminalid);
        sb.append(", terminalVersion=").append(terminalVersion);
        sb.append(", status=").append(status);
        sb.append(", productiontime=").append(productiontime);
        sb.append(", finishedtime=").append(finishedtime);
        sb.append(", pendingtime=").append(pendingtime);
        sb.append(", repairedtime=").append(repairedtime);
        sb.append(", scraptime=").append(scraptime);
        sb.append(", lastTime=").append(lastTime);
        sb.append(", lastloginTime=").append(lastloginTime);
        sb.append(", workstatus=").append(workstatus);
        sb.append(", channel=").append(channel);
        sb.append(", serviceAgent=").append(serviceAgent);
        sb.append(", saleAgent=").append(saleAgent);
        sb.append(", suppilercode=").append(suppilercode);
        sb.append(", suppilername=").append(suppilername);
        sb.append(", creationtime=").append(creationtime);
        sb.append(", creator=").append(creator);
        sb.append(", modifiedtime=").append(modifiedtime);
        sb.append(", modifier=").append(modifier);
        sb.append(", dr=").append(dr);
        sb.append(", version=").append(version);
        sb.append(", pkOrg=").append(pkOrg);
        sb.append(", simid=").append(simid);
        sb.append(", isnew=").append(isnew);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    public enum Dr {
        NOT_DELETED(new Long("0"), "未删除"),
        IS_DELETED(new Long("1"), "已删除");

        private final Long value;

        private final String name;

        Dr(Long value, String name) {
            this.value = value;
            this.name = name;
        }

        public Long getValue() {
            return this.value;
        }

        public Long value() {
            return this.value;
        }

        public String getName() {
            return this.name;
        }
    }

    public enum Column {
        pkTerminal("PK_TERMINAL", "pkTerminal", "VARCHAR", false),
        terminalid("TERMINALID", "terminalid", "VARCHAR", false),
        terminalVersion("TERMINAL_VERSION", "terminalVersion", "VARCHAR", false),
        status("STATUS", "status", "DECIMAL", false),
        productiontime("PRODUCTIONTIME", "productiontime", "CHAR", false),
        finishedtime("FINISHEDTIME", "finishedtime", "CHAR", false),
        pendingtime("PENDINGTIME", "pendingtime", "CHAR", false),
        repairedtime("REPAIREDTIME", "repairedtime", "CHAR", false),
        scraptime("SCRAPTIME", "scraptime", "CHAR", false),
        lastTime("LAST_TIME", "lastTime", "CHAR", false),
        lastloginTime("LASTLOGIN_TIME", "lastloginTime", "CHAR", false),
        workstatus("WORKSTATUS", "workstatus", "DECIMAL", false),
        channel("CHANNEL", "channel", "VARCHAR", false),
        serviceAgent("SERVICE_AGENT", "serviceAgent", "VARCHAR", false),
        saleAgent("SALE_AGENT", "saleAgent", "VARCHAR", false),
        suppilercode("SUPPILERCODE", "suppilercode", "VARCHAR", false),
        suppilername("SUPPILERNAME", "suppilername", "VARCHAR", false),
        creationtime("CREATIONTIME", "creationtime", "CHAR", false),
        creator("CREATOR", "creator", "VARCHAR", false),
        modifiedtime("MODIFIEDTIME", "modifiedtime", "CHAR", false),
        modifier("MODIFIER", "modifier", "VARCHAR", false),
        dr("DR", "dr", "DECIMAL", false),
        version("VERSION", "version", "DECIMAL", false),
        pkOrg("PK_ORG", "pkOrg", "VARCHAR", false),
        simid("SIMID", "simid", "VARCHAR", false),
        isnew("ISNEW", "isnew", "VARCHAR", false);

        private static final String BEGINNING_DELIMITER = "\"";

        private static final String ENDING_DELIMITER = "\"";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}