package com.yonyou.longgong.carsserver.web;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.client.OccBaseClient;
import com.yonyou.longgong.carsserver.dto.SuppilerdocSelectDto;
import com.yonyou.longgong.carsserver.occdto.OccReferDto;
import com.yonyou.longgong.carsserver.pojo.SuppilerdocPojo;
import com.yonyou.longgong.carsserver.service.ISuppilerdocService;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-11-13
 * @描述
 **/
@RestController
@RequestMapping(value = "/supplier")
public class SuppilerdocController {

    @Autowired
    private ISuppilerdocService iSuppilerdocService;

    @Autowired
    private OccBaseClient occBaseClient;

    @PostMapping(value = "/getSupplierdoc")
    public ResponseData getSuppilerdoc(SuppilerdocSelectDto suppilerdocSelectDto){
        PageInfo<SuppilerdocPojo> suppilerdocPojoPageInfo = iSuppilerdocService.selectSuppilerdoc(suppilerdocSelectDto);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),suppilerdocPojoPageInfo);
    }

    @RequestMapping(value = "/getSupplierList")
    public ResponseData getSuppilerList(){
        OccReferDto[] supplierList = occBaseClient.getSupplierList();
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),supplierList);
    }

    @PostMapping(value = "/addSupplierdoc")
    public ResponseData addSuppilerdoc(SuppilerdocPojo suppilerdocPojo){
        Integer row = iSuppilerdocService.insertSuppilerdoc(suppilerdocPojo);
        if (row>0){
            return ResponseData.result(ResponseCode.SUCCESS_INSERT.getCode(),ResponseCode.SUCCESS_INSERT.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @PostMapping(value = "/delSupplierdoc")
    public ResponseData delSuppilerdoc(@RequestBody List<SuppilerdocPojo> suppilerdocPojos){
        Integer row = iSuppilerdocService.delSuppilerdoc(suppilerdocPojos);
        if (row>0){
            return ResponseData.result(ResponseCode.SUCCESS_DELETE.getCode(),ResponseCode.SUCCESS_DELETE.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @PostMapping(value = "/updateSupplierdoc")
    public ResponseData updateSuppilerdoc(SuppilerdocPojo suppilerdocPojo){
        Integer row = iSuppilerdocService.updateSuppilerdoc(suppilerdocPojo);
        if (row>0){
            return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(),ResponseCode.SUCCESS_UPDATE.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @RequestMapping(value = "/excelExport")
    public void export(SuppilerdocSelectDto suppilerdocSelectDto, HttpServletResponse response){
        try{
            List<SuppilerdocPojo> suppilerdocPojos = iSuppilerdocService.selectSuppilerdocByExcel(suppilerdocSelectDto);
            Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams("供应商档案", "供应商档案"), SuppilerdocPojo.class, suppilerdocPojos);
            // 告诉浏览器用什么软件可以打开此文件
            response.setHeader("content-Type", "application/vnd.ms-excel");
            // 下载文件的默认名称
            response.setHeader("Content-Disposition", "attachment;filename=simdoc.xls");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            sheets.write(response.getOutputStream());
            return;
        }catch (IOException e){

        }
    }
}
