package com.yonyou.longgong.carsserver.dao;

import com.yonyou.longgong.carsserver.example.TerminalCarPojoExample;
import com.yonyou.longgong.carsserver.pojo.TerminalCarPojo;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TerminalCarPojoMapper {
    long countByExample(TerminalCarPojoExample example);

    int deleteWithVersionByExample(@Param("version") Long version, @Param("example") TerminalCarPojoExample example);

    int deleteByExample(TerminalCarPojoExample example);

    int deleteWithVersionByPrimaryKey(@Param("version") Long version, @Param("key") String pkTerminalCar);

    int deleteByPrimaryKey(String pkTerminalCar);

    int insert(TerminalCarPojo record);

    int insertSelective(@Param("record") TerminalCarPojo record, @Param("selective") TerminalCarPojo.Column ... selective);

    List<TerminalCarPojo> selectByExample(TerminalCarPojoExample example);

    TerminalCarPojo selectByPrimaryKey(String pkTerminalCar);

    TerminalCarPojo selectByPrimaryKeyWithLogicalDelete(@Param("pkTerminalCar") String pkTerminalCar, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    int updateWithVersionByExample(@Param("version") Long version, @Param("record") TerminalCarPojo record, @Param("example") TerminalCarPojoExample example);

    int updateWithVersionByExampleSelective(@Param("version") Long version, @Param("record") TerminalCarPojo record, @Param("example") TerminalCarPojoExample example, @Param("selective") TerminalCarPojo.Column ... selective);

    int updateByExampleSelective(@Param("record") TerminalCarPojo record, @Param("example") TerminalCarPojoExample example, @Param("selective") TerminalCarPojo.Column ... selective);

    int updateByExample(@Param("record") TerminalCarPojo record, @Param("example") TerminalCarPojoExample example);

    int updateWithVersionByPrimaryKey(@Param("version") Long version, @Param("record") TerminalCarPojo record);

    int updateWithVersionByPrimaryKeySelective(@Param("version") Long version, @Param("record") TerminalCarPojo record, @Param("selective") TerminalCarPojo.Column ... selective);

    int updateByPrimaryKeySelective(@Param("record") TerminalCarPojo record, @Param("selective") TerminalCarPojo.Column ... selective);

    int updateByPrimaryKey(TerminalCarPojo record);

    int logicalDeleteByExample(@Param("example") TerminalCarPojoExample example);

    int logicalDeleteWithVersionByExample(@Param("version") Long version, @Param("nextVersion") Long nextVersion, @Param("example") TerminalCarPojoExample example);

    int logicalDeleteByPrimaryKey(String pkTerminalCar);

    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Long version, @Param("nextVersion") Long nextVersion, @Param("key") String pkTerminalCar);
}