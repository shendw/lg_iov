package com.yonyou.longgong.carsserver.dto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-11-11
 * @描述
 **/
@Data
public class CarInfoSelectDto {
    /**
     * 机器类型
     */
    private String machineType;
    /**
     * 整车编号
     */
    private String carNo;
    /**
     * 终端ID
     */
    private String terminalid;
    /**
     * sim卡ID
     */
    private String simid;
    /**
     * 客户
     */
    private String customer;
    /**
     * 工作状态
     */
    private String workStatus;
    /**
     * 销售代理商
     */
    private String saleAgent;
    /**
     * 最早登陆时间
     */
    private String startTime;
    /**
     * 最晚登陆时间
     */
    private String endTime;
    /**
     * 叉车流程状态，渠道云取
     */
    private String processStatus;
    /**
     * 通讯通道
     */
    private String channel;
    /**
     * 工作时间
     */
    private String worktimeLow;
    /**
     * 工作时间
     */
    private String worktimeHigh;
    /**
     * 通讯错误
     */
    private String channelError;
    /**
     * 终端版本
     */
    private String terminalVersion;

    private String lockStatus;
    /**
     * 新老平台
     */
    private String isnew;
    /**
     * 最新通讯时间
     */
    private String lastTimeBegin;
    private String lastTimeEnd;

    private Integer pageSize = 10;

    private Integer pageNum = 1;

    private String machinemodel;
}
