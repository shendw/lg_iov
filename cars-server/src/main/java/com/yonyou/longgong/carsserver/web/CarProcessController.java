package com.yonyou.longgong.carsserver.web;

import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dto.CardocSelectDto;
import com.yonyou.longgong.carsserver.enums.CarProcessStatus;
import com.yonyou.longgong.carsserver.pojo.CardocPojo;
import com.yonyou.longgong.carsserver.service.ICarProcessService;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shendawei
 * @创建时间 2019-12-19
 * @描述
 **/
@RestController
@RequestMapping("/carProcess")
public class CarProcessController {

    @Autowired
    private ICarProcessService iCarProcessService;

    @RequestMapping("/carOutline")
    public ResponseData carOutline(String carno,
                                   @CookieValue(value = "userId") String userId,
                                   @CookieValue(value = "_A_P_userLoginName") String loginName){
        Integer row = iCarProcessService.carOutline(carno,loginName);
        if (row>0){
            return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(),ResponseCode.SUCCESS_UPDATE.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @RequestMapping("/carTest")
    public ResponseData carTest(String carno,
                                @CookieValue(value = "userId") String userId,
                                @CookieValue(value = "_A_P_userLoginName") String loginName){
        Integer row = iCarProcessService.carTest(carno,loginName);
        if (row>0){
            return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(),ResponseCode.SUCCESS_UPDATE.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @PostMapping(value = "/getCarProcess/outline")
    public ResponseData getCardocOutline(CardocSelectDto cardocSelectDto,
                                  @CookieValue(value = "userId") String userId,
                                  @CookieValue(value = "_A_P_userLoginName") String loginName){
        return iCarProcessService.selectCardocOutline(cardocSelectDto,userId,loginName);
    }

    @PostMapping(value = "/getCarProcess/debug")
    public ResponseData getCardocDebug(CardocSelectDto cardocSelectDto,
                                  @CookieValue(value = "userId") String userId,
                                  @CookieValue(value = "_A_P_userLoginName") String loginName){
        return iCarProcessService.selectCardocDebug(cardocSelectDto,userId,loginName);
    }
}
