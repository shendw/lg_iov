package com.yonyou.longgong.carsserver.client;

import com.yonyou.longgong.carsserver.u9dto.U9CardocPojo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-11-29
 * @描述
 **/
//@FeignClient(name = "u9-data",url = "10.10.10.152:10000")
@FeignClient(name = "u9-data")
public interface U9dataClient {

    @RequestMapping("/u9-data/cardoc/getCardoc")
    List<U9CardocPojo> getU9data(@RequestBody List<U9CardocPojo> u9CardocPojos);

    @RequestMapping("/u9-data/cardoc/getCardocStock")
    List<U9CardocPojo> getCardocStock(@RequestBody List<U9CardocPojo> u9CardocPojos);
}
