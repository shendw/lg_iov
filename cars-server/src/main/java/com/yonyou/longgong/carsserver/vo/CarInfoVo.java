package com.yonyou.longgong.carsserver.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-11-11
 * @描述
 **/
@Data
public class CarInfoVo {
    /**
     * 产品类型
     */
//    @Excel(name = "产品类型")
    private String carmodel;
    /**
     * 机型
     */
//    @Excel(name = "机型")
    private String machinemodel;
    /**
     * 整机编号
     */
    @Excel(name = "整机编号")
    private String carNo;
    /**
     * 服务代理商
     */
    @Excel(name = "服务代理商")
    private String serviceAgent;
    /**
     * 销售代理商
     */
    @Excel(name = "销售代理商")
    private String saleAgent;
    /**
     * 客户
     */
    @Excel(name = "客户")
    private String customer;
    /**
     * 终端id
     */
//    @Excel(name = "终端id")
    private String terminalid;
    /**
     * sim卡id
     */
//    @Excel(name = "sim卡id")
    private String simid;
    /**
     * 工作状态
     */
//    @Excel(name = "工作状态")
    private String workStatus;
    /**
     * 锁车状态
     */
//    @Excel(name = "锁车状态")
    private String lockStatus;
    /**
     * 最近登陆时间
     */
    @Excel(name = "最近登陆时间")
    private String lastloginTime;
    /***
     * 最近通讯时间
     */
    @Excel(name = "最近通讯时间")
    private String lasttime;
    /**
     * 备注
     */
    @Excel(name = "备注")
    private String remark;
    /**
     * 工作时间
     */
    @Excel(name = "工作时间")
    private String worktime;
    /**
     * 流程状态
     */
//    @Excel(name = "流程状态")
    private String status;

//    @Excel(name = "自动绑定时间")
    private String autobindtime;

    private String outlineer;

//    @Excel(name = "下线时间")
    private String outlinetime;

    private String debuger;

//    @Excel(name = "调试时间")
    private String debugtime;
    /**
     * 终端版本
     */
//    @Excel(name = "终端版本")
    private String terminalVersion;
    /**
     * 通讯通道
     */
//    @Excel(name = "通讯通道")
    private String channel;
    /**
     * 位置信息
     */
//    @Excel(name = "位置信息")
    private String location;

    private String locationTime;
    /**
     * 经度
     */
//    @Excel(name = "经度")
    private String longitude;
    /**
     * 纬度
     */
//    @Excel(name = "纬度")
    private String latitude;

    private String isnew;

    private String pkTerminalCar;

    private String pkCardoc;

    //经纬度
    private String coordinates;
}
