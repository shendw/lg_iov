package com.yonyou.longgong.carsserver.excel;

import cn.afterturn.easypoi.excel.entity.result.ExcelVerifyHandlerResult;
import cn.afterturn.easypoi.handler.inter.IExcelVerifyHandler;
import com.yonyou.longgong.carsserver.service.ITerminalService;
import com.yonyou.longgong.carsserver.utils.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;

/**
 * @author shendawei
 * @创建时间 2019/12/26
 * @描述
 **/
@DependsOn("springUtils")
public class ExcelVerifyTerminalHandlerImpl implements IExcelVerifyHandler<ExcelVerifyTerminalPojoOfMode> {

    @Autowired
    private ITerminalService iTerminalService = SpringUtils.getBean("iTerminalService");

    @Override
    public ExcelVerifyHandlerResult verifyHandler(ExcelVerifyTerminalPojoOfMode excelVerifyTerminalPojoOfMode) {
        String msg = iTerminalService.insertTerminalByExcel(excelVerifyTerminalPojoOfMode);
        if (msg != null) {
            StringBuilder builder = new StringBuilder();
            builder.append(msg);
            return new ExcelVerifyHandlerResult(false, builder.toString());
        }
        return new ExcelVerifyHandlerResult(true);
    }
}
