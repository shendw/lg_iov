package com.yonyou.longgong.carsserver.web;

import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dto.BindErrorSelectDto;
import com.yonyou.longgong.carsserver.pojo.BindErrorInfoPojo;
import com.yonyou.longgong.carsserver.service.IBindErrorService;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shendawei
 * @创建时间 2020/4/20
 * @描述
 **/
@RestController
@RequestMapping("/bindErrorInfo")
public class BindErrorInfoController {

    @Autowired
    private IBindErrorService iBindErrorService;

    @RequestMapping("/getBindErrorInfo")
    public ResponseData getBindErrorInfo(BindErrorSelectDto bindErrorSelectDto){
        PageInfo<BindErrorInfoPojo> bindErrorInfoPojoPageInfo = iBindErrorService.selectBindError(bindErrorSelectDto);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),bindErrorInfoPojoPageInfo);
    }
}
