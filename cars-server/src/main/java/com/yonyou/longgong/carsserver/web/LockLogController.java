package com.yonyou.longgong.carsserver.web;

import com.yonyou.longgong.carsserver.dto.LockLogDto;
import com.yonyou.longgong.carsserver.service.ILockLogService;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shendawei
 * @创建时间 2020/4/26
 * @描述
 **/
@RestController
@RequestMapping("/lockLog")
public class LockLogController {

    @Autowired
    private ILockLogService iLockLogService;

    @RequestMapping("/getLockLog")
    public ResponseData getLockLog(LockLogDto lockLogDto){
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),iLockLogService.selectLocklog(lockLogDto));
    }
}
