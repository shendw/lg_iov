package com.yonyou.longgong.carsserver.dao;

import com.yonyou.longgong.carsserver.example.BindErrorInfoPojoExample;
import com.yonyou.longgong.carsserver.pojo.BindErrorInfoPojo;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BindErrorInfoPojoMapper {
    long countByExample(BindErrorInfoPojoExample example);

    int deleteByExample(BindErrorInfoPojoExample example);

    int deleteByPrimaryKey(String pkBinderror);

    int insert(BindErrorInfoPojo record);

    int insertSelective(@Param("record") BindErrorInfoPojo record, @Param("selective") BindErrorInfoPojo.Column ... selective);

    List<BindErrorInfoPojo> selectByExample(BindErrorInfoPojoExample example);

    BindErrorInfoPojo selectByPrimaryKey(String pkBinderror);

    BindErrorInfoPojo selectByPrimaryKeyWithLogicalDelete(@Param("pkBinderror") String pkBinderror, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    int updateByExampleSelective(@Param("record") BindErrorInfoPojo record, @Param("example") BindErrorInfoPojoExample example, @Param("selective") BindErrorInfoPojo.Column ... selective);

    int updateByExample(@Param("record") BindErrorInfoPojo record, @Param("example") BindErrorInfoPojoExample example);

    int updateByPrimaryKeySelective(@Param("record") BindErrorInfoPojo record, @Param("selective") BindErrorInfoPojo.Column ... selective);

    int updateByPrimaryKey(BindErrorInfoPojo record);

    int logicalDeleteByExample(@Param("example") BindErrorInfoPojoExample example);

    int logicalDeleteByPrimaryKey(String pkBinderror);
}