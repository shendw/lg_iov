package com.yonyou.longgong.carsserver.web;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dto.CardocSelectDto;
import com.yonyou.longgong.carsserver.dto.SimSelectDto;
import com.yonyou.longgong.carsserver.excel.ExcelVerifyCardocPojoOfMode;
import com.yonyou.longgong.carsserver.excel.ExcelVerifySimPojoOfMode;
import com.yonyou.longgong.carsserver.pojo.CardocPojo;
import com.yonyou.longgong.carsserver.pojo.SimPojo;
import com.yonyou.longgong.carsserver.service.ICardocService;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author shendawei
 * @创建时间 2019-11-07
 * @描述
 **/
@RestController
@RequestMapping(value = "/cardoc")
@Slf4j
public class CardocController {

    @Autowired
    private ICardocService iCardocService;

    @PostMapping(value = "/getCardoc")
    public ResponseData getCardoc(CardocSelectDto cardocSelectDto,
                                  @CookieValue(value = "userId") String userId,
                                  @CookieValue(value = "_A_P_userLoginName") String loginName){
        PageInfo<CardocPojo> pojoPageInfo = iCardocService.selectCardoc(cardocSelectDto,userId,loginName);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),pojoPageInfo,ResponseCode.SUCCESS.getDesc());
    }

    @PostMapping(value = "/addCardoc")
    public ResponseData addCardoc(CardocPojo cardocPojo){
        Integer row = iCardocService.insertCardoc(cardocPojo);
        if (row>0){
            return ResponseData.result(ResponseCode.SUCCESS_INSERT.getCode(),ResponseCode.SUCCESS_INSERT.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @PostMapping(value = "/updateCardoc")
    public ResponseData updateCardoc(CardocPojo cardocPojo){
        Integer row = iCardocService.updateCardoc(cardocPojo);
        if (row>0){
            return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(),ResponseCode.SUCCESS_UPDATE.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @PostMapping(value = "/delCardoc")
    public ResponseData delCardoc(@RequestBody List<CardocPojo> cardocPojos){
        Integer row = iCardocService.delCardoc(cardocPojos);
        if (row>0){
            return ResponseData.result(ResponseCode.SUCCESS_DELETE.getCode(),ResponseCode.SUCCESS_DELETE.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @RequestMapping(value = "/excelExport")
    public void export(CardocSelectDto cardocSelectDto, HttpServletResponse response,
                       @CookieValue(value = "userId") String userId,
                       @CookieValue(value = "_A_P_userLoginName") String loginName){
        try{
            List<CardocPojo> cardocPojos = iCardocService.selectCardocByExcel(cardocSelectDto,userId,loginName);
            Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams("车辆档案", "车辆档案"), CardocPojo.class, cardocPojos);
            // 告诉浏览器用什么软件可以打开此文件
            response.setHeader("content-Type", "application/vnd.ms-excel");
            // 下载文件的默认名称
            response.setHeader("Content-Disposition", "attachment;filename=simdoc.xls");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            sheets.write(response.getOutputStream());
            return;
        }catch (IOException e){

        }
    }

    @RequestMapping(value = "/getAllCarno")
    public ResponseData getAllCarno(){
        List<String> carnos = iCardocService.selectCarno();
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),carnos);
    }

    @RequestMapping(value = "/syncCardoc")
    public ResponseData syncCardoc(@RequestBody List<CardocPojo> cardocPojos){
        Integer row = iCardocService.syncCardocFromU9(cardocPojos);
        if (row>0){
            return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(),ResponseCode.SUCCESS_UPDATE.getDesc());
        }else if (row == -1){
            return ResponseData.result(ResponseCode.CARDOC_U9DATA_IS_NULL.getCode(),ResponseCode.CARDOC_U9DATA_IS_NULL.getDesc());
        }else if (row == -2){
            return ResponseData.result(ResponseCode.CARDOC_U9DATA_IS_MISS.getCode(),ResponseCode.CARDOC_U9DATA_IS_MISS.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @PostMapping(value = "/excelInsert")
    public void batchInsertCar(@RequestParam("carExcel") MultipartFile carExcel, HttpServletResponse response){
        try {
            log.info("begin carExcel insert!");
            long startTime = System.currentTimeMillis();
            ExcelImportResult<ExcelVerifyCardocPojoOfMode> excelImportResult = iCardocService.checkExcel(carExcel);
            long endTime = System.currentTimeMillis();
            log.info("carExcel check end fail.size {},success.size {},time {}"
                    ,excelImportResult.getFailList().size()
                    ,excelImportResult.getList().size()
                    ,endTime-startTime);
            if (!CollectionUtils.isEmpty(excelImportResult.getFailList())){
                // 告诉浏览器用什么软件可以打开此文件
                response.setHeader("content-Type", "application/vnd.ms-excel");
                // 下载文件的默认名称
                response.setHeader("Content-Disposition", "attachment;filename=cardoc.xls");
                response.setHeader("Status","50001");
                response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
                response.setHeader("Access-Control-Expose-Headers", "Status");
                List<ExcelVerifyCardocPojoOfMode> failList = excelImportResult.getFailList();
                Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams(null, "车辆档案错误信息"), ExcelVerifyCardocPojoOfMode.class, failList);
                sheets.write(response.getOutputStream());
            }else {
                response.setHeader("Status","20000");
                response.setHeader("Access-Control-Expose-Headers", "Status");
            }
        }catch (IOException e){
            response.setHeader("Status","50000");
            response.setHeader("Access-Control-Expose-Headers", "Status");
        }
    }

    @RequestMapping(value = "/changeStatus")
    public ResponseData changeStatus(CardocPojo cardocPojo){
        Integer row = iCardocService.changeCardoc(cardocPojo);
        if (row>0){
            return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(),ResponseCode.SUCCESS_UPDATE.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }
}
