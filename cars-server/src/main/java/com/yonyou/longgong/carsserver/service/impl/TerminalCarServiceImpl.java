package com.yonyou.longgong.carsserver.service.impl;

import com.yonyou.longgong.carsserver.dao.CardocPojoMapper;
import com.yonyou.longgong.carsserver.dao.TerminalCarPojoMapper;
import com.yonyou.longgong.carsserver.dao.TerminalPojoMapper;
import com.yonyou.longgong.carsserver.enums.CarProcessStatus;
import com.yonyou.longgong.carsserver.enums.TerminalCarStatus;
import com.yonyou.longgong.carsserver.enums.TerminalStatus;
import com.yonyou.longgong.carsserver.example.CardocPojoExample;
import com.yonyou.longgong.carsserver.example.TerminalCarPojoExample;
import com.yonyou.longgong.carsserver.example.TerminalPojoExample;
import com.yonyou.longgong.carsserver.exception.BusinessException;
import com.yonyou.longgong.carsserver.pojo.CardocPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalCarPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalPojo;
import com.yonyou.longgong.carsserver.service.IBindErrorService;
import com.yonyou.longgong.carsserver.service.ICardocService;
import com.yonyou.longgong.carsserver.service.ITerminalCarService;
import com.yonyou.longgong.carsserver.utils.DataTimeUtil;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.UUIDUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author shendawei
 * @创建时间 2019-11-07
 * @描述
 **/
@Service("iTerminalCarService")
@Slf4j
public class TerminalCarServiceImpl implements ITerminalCarService {

    @Autowired
    private TerminalCarPojoMapper terminalCarPojoMapper;

    @Autowired
    private TerminalPojoMapper terminalPojoMapper;

    @Autowired
    private CardocPojoMapper cardocPojoMapper;

    @Autowired
    private ICardocService iCardocService;

    @Autowired
    private IBindErrorService iBindErrorService;

    @Override
    public Integer insertTerminalCar(TerminalCarPojo terminalCarPojo) {
        log.info("收到绑定请求，终端号:{},整机编号:{}",terminalCarPojo.getTerminalid(),terminalCarPojo.getCarno());
        if (StringUtils.isBlank(terminalCarPojo.getTerminalid()) || StringUtils.isBlank(terminalCarPojo.getCarno())){
            throw new BusinessException((ResponseCode.ERROR_PARAM_IS_NULL));
        }
        //加><
        terminalCarPojo.setCarno(">"+terminalCarPojo.getCarno().trim()+"<");
        //查询终端是否存在
        TerminalPojoExample terminalPojoExample = new TerminalPojoExample();
        terminalPojoExample.createCriteria().andLogicalDeleted(false).andTerminalidEqualTo(terminalCarPojo.getTerminalid());
        List<TerminalPojo> terminalPojos = terminalPojoMapper.selectByExample(terminalPojoExample);
        if (CollectionUtils.isEmpty(terminalPojos)){
            throw new BusinessException(ResponseCode.TERMINALID_IS_NULL);
        }
        TerminalPojo terminalPojo = terminalPojos.get(0);
        //检查下是不是已绑当前车辆，被绑的车是不是绑了其它终端
        checkEnablestatus(terminalCarPojo);
        //校验终端的状态是不是未标定
        if (terminalPojo.getStatus().equals(TerminalStatus.DEFEAT.getCode())){
            iBindErrorService.insertBindErrorInfo(terminalCarPojo.getCarno(),terminalCarPojo.getTerminalid(),ResponseCode.TERMINALSTATUS_ERROR.getDesc());
            throw new BusinessException(ResponseCode.TERMINALSTATUS_ERROR);
        }
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        cardocPojoExample.createCriteria().andLogicalDeleted(false).andCarnoEqualTo(terminalCarPojo.getCarno());
        List<CardocPojo> cardocPojos = cardocPojoMapper.selectByExample(cardocPojoExample);
        //如果没有车辆档案自动生成车辆档案
        String nowTime = DataTimeUtil.dateToStr(new Date());
        if (CollectionUtils.isEmpty(cardocPojos)){
            CardocPojo cardocPojo = new CardocPojo();
            cardocPojo.setCarno(terminalCarPojo.getCarno());
            cardocPojo.setStatus(CarProcessStatus.AUTOBIND.getCode());
            cardocPojo.setCreationtime(nowTime);
            cardocPojo.setAutobindtime(nowTime);
            Integer row = iCardocService.iotInsertCardoc(cardocPojo,terminalCarPojo.getTerminalid());
            if (row == 0){
                throw new BusinessException(ResponseCode.CARDOC_INSERT_ERROR);
            }
        }
        terminalCarPojo.setPkTerminalCar(UUIDUtils.getUniqueIdByUUId());
        terminalCarPojo.setDr(0L);
        terminalCarPojo.setVersion(1L);
        terminalCarPojo.setBindTime(nowTime);
        if (terminalCarPojo.getEnablestate() == null){
            terminalCarPojo.setEnablestate(TerminalCarStatus.DEFEAT.getCode());
        }
        //更改状态为装机件
        terminalPojo.setStatus(TerminalStatus.WORK.getCode());
        terminalPojoMapper.updateByPrimaryKeySelective(terminalPojo,TerminalPojo.Column.status);
        return terminalCarPojoMapper.insertSelective(terminalCarPojo);
    }

    @Override
    public Integer unbundTerminalCar(String pkTerminalCar) {
        if (StringUtils.isBlank(pkTerminalCar)){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        TerminalCarPojo terminalCarPojo = terminalCarPojoMapper.selectByPrimaryKey(pkTerminalCar);
        if (terminalCarPojo != null){
            terminalCarPojo.setEnablestate(TerminalCarStatus.INVALID.getCode());
            terminalCarPojo.setUnbundtime(DataTimeUtil.dateToStr(new Date()));
            //解绑前先把终端变成成品
            String terminalid = terminalCarPojo.getTerminalid();
            TerminalPojoExample terminalPojoExample = new TerminalPojoExample();
            terminalPojoExample.createCriteria().andLogicalDeleted(false).andTerminalidEqualTo(terminalid);
            TerminalPojo terminalPojo = new TerminalPojo();
            terminalPojo.setStatus(TerminalStatus.FINISHED.getCode());
            int i = terminalPojoMapper.updateByExampleSelective(terminalPojo, terminalPojoExample, TerminalPojo.Column.status);
            if (i >0 ){
                return terminalCarPojoMapper.updateByPrimaryKeySelective(terminalCarPojo
                        ,TerminalCarPojo.Column.enablestate,TerminalCarPojo.Column.unbundtime);
            }
        }
        return 0;
    }

    @Override
    public List<TerminalCarPojo> selectTerminalCar(TerminalCarPojo terminalCarPojo) {
        if (StringUtils.isBlank(terminalCarPojo.getCarno()) && StringUtils.isBlank(terminalCarPojo.getTerminalid())){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        TerminalCarPojoExample terminalCarPojoExample = new TerminalCarPojoExample();
        TerminalCarPojoExample.Criteria criteria = terminalCarPojoExample.createCriteria();
        if (StringUtils.isNotBlank(terminalCarPojo.getCarno())){
            criteria.andCarnoEqualTo(terminalCarPojo.getCarno());
        }
        if (StringUtils.isNotBlank(terminalCarPojo.getTerminalid())){
            criteria.andTerminalidEqualTo(terminalCarPojo.getTerminalid());
        }
        if(terminalCarPojo.getEnablestate() != null){
            criteria.andEnablestateEqualTo(terminalCarPojo.getEnablestate());
        }
        criteria.andLogicalDeleted(false);
        //根据启用状态排序
        terminalCarPojoExample.setOrderByClause(TerminalCarPojo.Column.enablestate.getJavaProperty());
        return terminalCarPojoMapper.selectByExample(terminalCarPojoExample);
    }

    @Override
    public List<String> selectTerminalid(Long enablestate) {
        TerminalCarPojoExample terminalCarPojoExample = new TerminalCarPojoExample();
        terminalCarPojoExample.createCriteria().andLogicalDeleted(false).andEnablestateEqualTo(enablestate);
        List<TerminalCarPojo> terminalCarPojos = terminalCarPojoMapper.selectByExample(terminalCarPojoExample);
        if (CollectionUtils.isEmpty(terminalCarPojos)){
            return null;
        }
        return terminalCarPojos.stream().map(TerminalCarPojo::getTerminalid).collect(Collectors.toList());
    }

    private void checkEnablestatus(TerminalCarPojo terminalCarPojo){
        String terminalid = terminalCarPojo.getTerminalid();
        String carno = terminalCarPojo.getCarno();
        TerminalCarPojoExample terminalCarPojoExample = new TerminalCarPojoExample();
            terminalCarPojoExample.createCriteria()
                    .andLogicalDeleted(false)
                    .andTerminalidEqualTo(terminalid)
                    .andEnablestateEqualTo(TerminalCarStatus.DEFEAT.getCode());
            List<TerminalCarPojo> terminalties = terminalCarPojoMapper.selectByExample(terminalCarPojoExample);
            if (!CollectionUtils.isEmpty(terminalties)){
                String oldCarno = terminalties.get(0).getCarno();
                if (carno.equals(oldCarno)){
                    //已经绑定当前车辆，给iot返回状态码20000
                    throw new BusinessException(ResponseCode.IOV_ALREADY_BIND);
                }else {
                    //如果本次绑定的整机编号不一致，判断是否还没入库
                    CardocPojoExample cardocPojoExample = new CardocPojoExample();
                    cardocPojoExample.createCriteria().andLogicalDeleted(false).andCarnoEqualTo(oldCarno);
                    List<CardocPojo> cardocPojos = cardocPojoMapper.selectByExample(cardocPojoExample);
                    if (!CollectionUtils.isEmpty(cardocPojos)){
                        Long status = cardocPojos.get(0).getStatus();
                        //如果老的整机编号已经入库之后，报警
                        if (status > CarProcessStatus.TEST.getCode()){
                            iBindErrorService.insertBindErrorInfo(carno,terminalid,ResponseCode.TERMINAL_CAR_CHANGE_CAR.getDesc());
                            throw new BusinessException(ResponseCode.TERMINAL_CAR_CHANGE_CAR);
                        }
                    }
                }
            }
            terminalCarPojoExample.clear();
            terminalCarPojoExample.createCriteria()
                    .andLogicalDeleted(false)
                    .andCarnoEqualTo(carno)
                    .andEnablestateEqualTo(TerminalCarStatus.DEFEAT.getCode());
            List<TerminalCarPojo> carNoTies = terminalCarPojoMapper.selectByExample(terminalCarPojoExample);
            if (!CollectionUtils.isEmpty(carNoTies)){
                String oldTerminal = carNoTies.get(0).getTerminalid();
                if (oldTerminal.equals(terminalid)){
                    throw new BusinessException(ResponseCode.IOV_ALREADY_BIND);
                }else {
                    iBindErrorService.insertBindErrorInfo(carno,terminalid,ResponseCode.TERMINAL_CAR_CHANGE_TERMINAL.getDesc());
                    throw new BusinessException(ResponseCode.TERMINAL_CAR_CHANGE_TERMINAL);
                }
            }
    }
}
