package com.yonyou.longgong.carsserver.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import lombok.Data;

@Data
public class BindErrorInfoPojo implements Serializable {
    public static final Long IS_DELETED = Dr.IS_DELETED.value();

    public static final Long NOT_DELETED = Dr.NOT_DELETED.value();

    private String pkBinderror;

    private String terminalid;

    private String carno;

    private String errorinfo;

    private String remark;

    private String ishandle;

    private Long dr;

    private String handler;

    private String hsndletime;

    private String createtime;

    private String ts;

    private static final long serialVersionUID = 1L;

    public void andLogicalDeleted(boolean deleted) {
        setDr(deleted ? Dr.IS_DELETED.value() : Dr.NOT_DELETED.value());
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        BindErrorInfoPojo other = (BindErrorInfoPojo) that;
        return (this.getPkBinderror() == null ? other.getPkBinderror() == null : this.getPkBinderror().equals(other.getPkBinderror()))
            && (this.getTerminalid() == null ? other.getTerminalid() == null : this.getTerminalid().equals(other.getTerminalid()))
            && (this.getCarno() == null ? other.getCarno() == null : this.getCarno().equals(other.getCarno()))
            && (this.getErrorinfo() == null ? other.getErrorinfo() == null : this.getErrorinfo().equals(other.getErrorinfo()))
            && (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()))
            && (this.getIshandle() == null ? other.getIshandle() == null : this.getIshandle().equals(other.getIshandle()))
            && (this.getDr() == null ? other.getDr() == null : this.getDr().equals(other.getDr()))
            && (this.getHandler() == null ? other.getHandler() == null : this.getHandler().equals(other.getHandler()))
            && (this.getHsndletime() == null ? other.getHsndletime() == null : this.getHsndletime().equals(other.getHsndletime()))
            && (this.getCreatetime() == null ? other.getCreatetime() == null : this.getCreatetime().equals(other.getCreatetime()))
            && (this.getTs() == null ? other.getTs() == null : this.getTs().equals(other.getTs()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getPkBinderror() == null) ? 0 : getPkBinderror().hashCode());
        result = prime * result + ((getTerminalid() == null) ? 0 : getTerminalid().hashCode());
        result = prime * result + ((getCarno() == null) ? 0 : getCarno().hashCode());
        result = prime * result + ((getErrorinfo() == null) ? 0 : getErrorinfo().hashCode());
        result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
        result = prime * result + ((getIshandle() == null) ? 0 : getIshandle().hashCode());
        result = prime * result + ((getDr() == null) ? 0 : getDr().hashCode());
        result = prime * result + ((getHandler() == null) ? 0 : getHandler().hashCode());
        result = prime * result + ((getHsndletime() == null) ? 0 : getHsndletime().hashCode());
        result = prime * result + ((getCreatetime() == null) ? 0 : getCreatetime().hashCode());
        result = prime * result + ((getTs() == null) ? 0 : getTs().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", pkBinderror=").append(pkBinderror);
        sb.append(", terminalid=").append(terminalid);
        sb.append(", carno=").append(carno);
        sb.append(", errorinfo=").append(errorinfo);
        sb.append(", remark=").append(remark);
        sb.append(", ishandle=").append(ishandle);
        sb.append(", dr=").append(dr);
        sb.append(", handler=").append(handler);
        sb.append(", hsndletime=").append(hsndletime);
        sb.append(", createtime=").append(createtime);
        sb.append(", ts=").append(ts);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    public enum Dr {
        NOT_DELETED(new Long("0"), "未删除"),
        IS_DELETED(new Long("1"), "已删除");

        private final Long value;

        private final String name;

        Dr(Long value, String name) {
            this.value = value;
            this.name = name;
        }

        public Long getValue() {
            return this.value;
        }

        public Long value() {
            return this.value;
        }

        public String getName() {
            return this.name;
        }
    }

    public enum Column {
        pkBinderror("PK_BINDERROR", "pkBinderror", "VARCHAR", false),
        terminalid("TERMINALID", "terminalid", "VARCHAR", false),
        carno("CARNO", "carno", "VARCHAR", false),
        errorinfo("ERRORINFO", "errorinfo", "VARCHAR", false),
        remark("REMARK", "remark", "VARCHAR", false),
        ishandle("ISHANDLE", "ishandle", "VARCHAR", false),
        dr("DR", "dr", "DECIMAL", false),
        handler("HANDLER", "handler", "VARCHAR", false),
        hsndletime("HSNDLETIME", "hsndletime", "CHAR", false),
        createtime("CREATETIME", "createtime", "CHAR", false),
        ts("TS", "ts", "CHAR", false);

        private static final String BEGINNING_DELIMITER = "\"";

        private static final String ENDING_DELIMITER = "\"";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}