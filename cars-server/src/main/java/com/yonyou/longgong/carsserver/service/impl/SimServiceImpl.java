package com.yonyou.longgong.carsserver.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.client.OccBaseClient;
import com.yonyou.longgong.carsserver.dao.SimPojoMapper;
import com.yonyou.longgong.carsserver.dao.TerminalPojoMapper;
import com.yonyou.longgong.carsserver.dto.SimSelectDto;
import com.yonyou.longgong.carsserver.dto.SuppilerdocSelectDto;
import com.yonyou.longgong.carsserver.enums.TerminalStatus;
import com.yonyou.longgong.carsserver.example.SimPojoExample;
import com.yonyou.longgong.carsserver.example.TerminalPojoExample;
import com.yonyou.longgong.carsserver.excel.ExcelVerifySimHandlerImpl;
import com.yonyou.longgong.carsserver.excel.ExcelVerifySimPojoOfMode;
import com.yonyou.longgong.carsserver.exception.BusinessException;
import com.yonyou.longgong.carsserver.occdto.CustomerDto;
import com.yonyou.longgong.carsserver.pojo.SimPojo;
import com.yonyou.longgong.carsserver.pojo.SuppilerdocPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalPojo;
import com.yonyou.longgong.carsserver.service.ISimService;
import com.yonyou.longgong.carsserver.service.ISuppilerdocService;
import com.yonyou.longgong.carsserver.utils.DataTimeUtil;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.UUIDUtils;
import com.yonyou.longgong.iotserver.service.IDeviceService;
import com.yonyou.longgong.iotserver.vo.DeviceDataVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author shendawei
 * @创建时间 2019-11-04
 * @描述
 **/
@Slf4j
@Service("iSimService")
public class SimServiceImpl implements ISimService {

    @Autowired
    private SimPojoMapper simPojoMapper;

    @Autowired
    private TerminalPojoMapper terminalPojoMapper;

    @Autowired
    private IDeviceService deviceService;

    @Autowired
    private ISuppilerdocService iSuppilerdocService;

    @Autowired
    private OccBaseClient occBaseClient;

    @Override
    public Integer insertSim(List<SimPojo> simPojos) {
        SimPojoExample simPojoExample = new SimPojoExample();
        simPojoExample.createCriteria().andSimidIn(simPojos.stream().map(SimPojo::getSimid).collect(Collectors.toList())).andLogicalDeleted(false);
        long count = simPojoMapper.countByExample(simPojoExample);
        if (count > 0 ){
            throw new BusinessException(ResponseCode.SIMID_IS_EXIST);
        }
        simPojoExample.clear();
        simPojoExample.createCriteria().andLogicalDeleted(false).andSerialIn(simPojos.stream().map(SimPojo::getSerial).collect(Collectors.toList()));
        long serialCount = simPojoMapper.countByExample(simPojoExample);
        if (serialCount>0){
            throw new BusinessException(ResponseCode.SIMSERIAL_IS_EXIST);
        }
        simPojos.forEach(simPojo -> {
            simPojo.setPkSim(UUIDUtils.getUniqueIdByUUId());
            simPojo.setVersion(1L);
            simPojo.setDr(0L);
        });
        int insert = simPojoMapper.insert(simPojos);
        return insert;
    }

    @Override
    public Integer insertSimByExcel(List<SimPojo> simPojos) {
        simPojos.forEach(simPojo -> {
            simPojo.setPkSim(UUIDUtils.getUniqueIdByUUId());
            simPojo.setVersion(1L);
            simPojo.setDr(0L);
        });
        int insert = simPojoMapper.insert(simPojos);
        return insert;
    }

    @Override
    public Integer delSim(List<SimPojo> simPojos) {
        SimPojoExample simPojoExample = new SimPojoExample();
        simPojoExample.createCriteria().andPkSimIn(simPojos.stream().map(SimPojo::getPkSim).collect(Collectors.toList()));
        return simPojoMapper.logicalDeleteByExample(simPojoExample);
    }

    @Override
    public Integer updateSim(SimPojo simPojo) {
        SimPojoExample simPojoExample = new SimPojoExample();
        simPojoExample.createCriteria().andSimidEqualTo(simPojo.getSimid()).andLogicalDeleted(false);
        SimPojoExample.Criteria criteria = simPojoExample.createCriteria();
        criteria.andLogicalDeleted(false).andSerialEqualTo(simPojo.getSerial());
        simPojoExample.or(criteria);
        List<SimPojo> simPojos = simPojoMapper.selectByExample(simPojoExample);
        if (!CollectionUtils.isEmpty(simPojos)){
            for (SimPojo simPojoCheck:
                 simPojos) {
                if (!simPojoCheck.getPkSim().equals(simPojo.getPkSim())){
                    throw new BusinessException(ResponseCode.SIMID_IS_EXIST);
                }
            }
        }
        return simPojoMapper.updateByPrimaryKeySelective(simPojo);
    }

    @Override
    public Integer updateSim(String simid,String terminalid) {
        SimPojoExample simPojoExample = new SimPojoExample();
        simPojoExample.createCriteria().andLogicalDeleted(false).andSimidEqualTo(simid);
        SimPojo simPojo = new SimPojo();
        simPojo.setClientid(terminalid);
        simPojo.setBindTime(DataTimeUtil.dateToStr(new Date()));
        return simPojoMapper.updateByExampleSelective(simPojo,simPojoExample,SimPojo.Column.clientid,SimPojo.Column.bindTime);
    }

    @Override
    public PageInfo<SimPojo> selectSim(SimSelectDto simSelectDto) {
        PageHelper.startPage(simSelectDto.getPageNum(),simSelectDto.getPageSize());
        List<SimPojo> simPojos = simPojoMapper.selectByExample(selectRule(simSelectDto));
        PageInfo<SimPojo> simPojoPageInfo = new PageInfo<>(simPojos);
        if (!CollectionUtils.isEmpty(simPojos)){
            for (SimPojo simPojo:
            simPojos) {
                if (StringUtils.isNotBlank(simPojo.getClientid())){
                    DeviceDataVO deviceData = deviceService.getDeviceData(simPojo.getClientid());
                    if (deviceData != null){
                        simPojo.setLastTime(deviceData.getLast_time());
                    }
                }
            }
        }
        return simPojoPageInfo;
    }

    @Override
    public List<SimPojo> selectSimByExcel(SimSelectDto simSelectDto) {
        List<SimPojo> simPojos = simPojoMapper.selectByExample(selectRule(simSelectDto));
        if (!CollectionUtils.isEmpty(simPojos)){
            for (SimPojo simPojo:
                    simPojos) {
                if (StringUtils.isNotBlank(simPojo.getClientid())){
                    DeviceDataVO deviceData = deviceService.getDeviceData(simPojo.getClientid());
                    if (deviceData != null){
                        simPojo.setLastTime(deviceData.getLast_time());
                    }
                }
            }
        }
        return simPojos;
    }

    private SimPojoExample selectRule(SimSelectDto simSelectDto){
        SimPojoExample simPojoExample = new SimPojoExample();
        SimPojoExample.Criteria criteria = simPojoExample.createCriteria();
        if (StringUtils.isNotBlank(simSelectDto.getSimid())){
            criteria.andSimidLike("%"+simSelectDto.getSimid()+"%");
        }
        if (StringUtils.isNotBlank(simSelectDto.getSerial())){
            criteria.andSerialLike("%"+simSelectDto.getSerial()+"%");
        }
        if (StringUtils.isNotBlank(simSelectDto.getMessageLow()) && StringUtils.isNotBlank(simSelectDto.getMessageHigh())){
            criteria.andMessageMonthBetween(simSelectDto.getMessageLow(),simSelectDto.getMessageHigh());
        }else if (StringUtils.isNotBlank(simSelectDto.getMessageLow())){
            criteria.andMessageMonthGreaterThanOrEqualTo(simSelectDto.getMessageLow());
        }else if(StringUtils.isNotBlank(simSelectDto.getMessageHigh())){
            criteria.andMessageMonthLessThanOrEqualTo(simSelectDto.getMessageHigh());
        }
        if (StringUtils.isNotBlank(simSelectDto.getFlowLow()) && StringUtils.isNotBlank(simSelectDto.getFlowHigh())){
            criteria.andFlowMonthBetween(simSelectDto.getFlowLow(),simSelectDto.getFlowHigh());
        }else if (StringUtils.isNotBlank(simSelectDto.getFlowLow())){
            criteria.andFlowMonthGreaterThanOrEqualTo(simSelectDto.getFlowLow());
        }else if(StringUtils.isNotBlank(simSelectDto.getFlowHigh())){
            criteria.andFlowMonthLessThanOrEqualTo(simSelectDto.getFlowHigh());
        }
        criteria.andDrEqualTo(SimPojo.NOT_DELETED);
        return simPojoExample;
    }

    @Override
    public ExcelImportResult<ExcelVerifySimPojoOfMode> checkExcel(MultipartFile simExcel) {
        try {
            if (simExcel == null) {
                throw new BusinessException(ResponseCode.SIMEXCEL_IS_NULL);
            }
            String fileName = simExcel.getOriginalFilename();
            if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
                // 文件格式不正确
                throw new BusinessException(ResponseCode.SIMEXCEL_IS_ILLEGAL);
            }
            ImportParams params = new ImportParams();
            params.setTitleRows(1);
            params.setNeedVerify(true);
            //时间太长关闭校验
//            params.setVerifyHandler(new ExcelVerifySimHandlerImpl());
            params.setImportFields(new String[] { "sim卡号", "sim卡串号", "开卡日期"});
            ExcelImportResult<ExcelVerifySimPojoOfMode> result = ExcelImportUtil.importExcelMore(simExcel.getInputStream(), ExcelVerifySimPojoOfMode.class, params);
            return result;
        }catch (Exception e){
            throw new BusinessException(ResponseCode.SIMEXCEL_IS_ERROR);
        }
    }

    @Override
    public long countSim(String simid, String serial) {
        SimPojoExample simPojoExample = new SimPojoExample();
        SimPojoExample.Criteria criteria = simPojoExample.createCriteria();
        criteria.andSimidEqualTo(simid).andLogicalDeleted(false);
        SimPojoExample.Criteria criteria2 = simPojoExample.createCriteria().andLogicalDeleted(false).andSerialEqualTo(serial);
        simPojoExample.or(criteria2);
        return simPojoMapper.countByExample(simPojoExample);
    }

    @Override
    public SimPojo matchSim(String imsi) {
        if (StringUtils.isBlank(imsi)){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        SimPojoExample simPojoExample = new SimPojoExample();
        simPojoExample.createCriteria().andLogicalDeleted(false).andSerialEqualTo(imsi);
        List<SimPojo> simPojos = simPojoMapper.selectByExample(simPojoExample);
        if (CollectionUtils.isEmpty(simPojos)){
            return null;
        }
        SimPojo simPojo = simPojos.get(0);
        return simPojo;
    }

    @Override
    public String bindSim(String imsi, String originTerminalid, String terminalVersion,SimPojo simPojo) {
        if (StringUtils.isBlank(imsi) || StringUtils.isBlank(originTerminalid)){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        String preffix = originTerminalid.substring(0, 4);
        //获取供应商信息 TODO 后续改为去渠道云获取
//        SuppilerdocPojo suppilerdocPojo = iSuppilerdocService.selectSuppilerByKeyword(preffix.substring(0, 1));
        ResponseEntity<CustomerDto> customerDtoResponseEntity = occBaseClient.getByCode(preffix.substring(0, 2).toUpperCase());
        CustomerDto customerDto = null;
        if (customerDtoResponseEntity.hasBody()){
            customerDto = customerDtoResponseEntity.getBody();
            if (customerDto == null){
                log.error("没有获取到终端厂家匹配,编码:{}",preffix.substring(0, 2).toUpperCase());
                throw new BusinessException(ResponseCode.SUPPILERDOC_KEYWORD_IS_NULL);
            }
        }
        String newTerminalid = preffix+UUIDUtils.randomHexString(8);
        boolean flag = true;
        while (flag){
            TerminalPojoExample terminalPojoExample = new TerminalPojoExample();
            terminalPojoExample.createCriteria().andTerminalidEqualTo(newTerminalid);
            List<TerminalPojo> terminalPojos = terminalPojoMapper.selectByExample(terminalPojoExample);
            if (CollectionUtils.isEmpty(terminalPojos)){
                flag = false;
            }else {
                newTerminalid = preffix+UUIDUtils.randomHexString(8);
            }
        }

        simPojo.setBindTime(DataTimeUtil.dateToStr(new Date()));
        simPojo.setClientid(newTerminalid);
        int row1 = simPojoMapper.updateByPrimaryKeySelective(simPojo, SimPojo.Column.bindTime, SimPojo.Column.clientid);
        TerminalPojo terminalPojo = new TerminalPojo(UUIDUtils.getUniqueIdByUUId(), newTerminalid, terminalVersion, TerminalStatus.DEFEAT.getCode(), simPojo.getPkOrg(), simPojo.getSimid());
        terminalPojo.setSuppilercode(customerDto.getCode());
        terminalPojo.setSuppilername(customerDto.getAbbr());
        terminalPojo.setProductiontime(DataTimeUtil.dateToStr(new Date()));
        terminalPojo.setIsnew("1");
        int row2 = terminalPojoMapper.insertSelective(terminalPojo);
        if (row1>0 && row2 >0){
            return newTerminalid;
        }
        return null;
    }
}
