package com.yonyou.longgong.carsserver.service;

import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dto.TerminalSelectDto;
import com.yonyou.longgong.carsserver.excel.ExcelVerifyTerminalPojoOfMode;
import com.yonyou.longgong.carsserver.pojo.TerminalPojo;
import com.yonyou.longgong.carsserver.vo.TerminalFinishedVO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-11-06
 * @描述
 **/
public interface ITerminalService {
    PageInfo<TerminalPojo> selectTerminalsTest(TerminalSelectDto selectDto,String userId,String loginName);

    /**
     * 给终端成品列表使用的
     * @param selectDto
     * @param userId
     * @param loginName
     * @return
     */
    PageInfo<TerminalFinishedVO> selectTerminalsFinished(TerminalSelectDto selectDto, String userId, String loginName);
    PageInfo<TerminalPojo> selectTerminalsProcess(TerminalSelectDto selectDto,String userId,String loginName);
    List<TerminalPojo> selectTerminalsByExcel(TerminalSelectDto selectDto,String userId,String loginName);
    Integer updateTermianals(TerminalPojo terminalPojo);
    Integer changeStatus(TerminalPojo terminalPojo);
    String selectTerminalidBySimid(String simid);
    TerminalPojo selectTerminalByTerminalid(String terminalid);
    Integer insertTerminal(TerminalPojo terminalPojo);
    ExcelImportResult<ExcelVerifyTerminalPojoOfMode> checkExcel(MultipartFile terminalExcel);
    String insertTerminalByExcel(ExcelVerifyTerminalPojoOfMode excelVerifyTerminalPojoOfMode);
}
