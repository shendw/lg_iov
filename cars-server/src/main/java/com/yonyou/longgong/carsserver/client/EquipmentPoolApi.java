package com.yonyou.longgong.carsserver.client;

import com.yonyou.longgong.carsserver.occdto.EquipmentPoolDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 设备池对外REST接口。
 *
 * @author ts
 * @date 2020-08-10
 */
@FeignClient(name = "occ-base")
public interface EquipmentPoolApi {

    /**
     * 初始化设备池(数据写入)
     *
     * @param machineNumber
     * @param terminalId
     * @param storageStatus
     * @return
     */
    @PostMapping("/api/base/equipmentpool/init")
    ResponseEntity<EquipmentPoolDto> init(@RequestParam("machineNumber") String machineNumber, @RequestParam("terminalId") String terminalId, @RequestParam("storageStatus") String storageStatus);


    /**
     * 获取指定主键的设备池。
     *
     * @param id 设备池主键。
     * @return 指定主键的设备池。
     */
    @GetMapping("/api/base/equipmentpool/{id}")
    ResponseEntity<EquipmentPoolDto> getById(@PathVariable("id") String id);

    /**
     * 获取指定主键的设备池。
     *
     * @param id 设备池主键。
     * @return 指定主键的设备池。
     */
    @GetMapping("/api/base/equipmentpool/getById")
    EquipmentPoolDto getEquipmentPoolDtoById(@RequestParam("id") String id);


    /**
     * 获取指定整机编码的设备池。
     *
     * @param machineNumber 整机编码。
     * @return 指定整机编码的设备池。
     */
    @GetMapping("/api/base/equipmentpool/getByMachineNumber")
    ResponseEntity<EquipmentPoolDto> getByMachineNumber(@RequestParam("machineNumber") String machineNumber);


    /**
     * 根据整机编码去更新设备池信息
     */
    @PutMapping("/api/base/equipmentpool/updateEquipmentPool")
    ResponseEntity<EquipmentPoolDto> update(@RequestBody EquipmentPoolDto dto);

    /**
     * 根据整机编码去更新收货日期
     */
    @PutMapping("/api/base/equipmentpool/updateReceiveDate")
    ResponseEntity<EquipmentPoolDto> updateReceiveDate(@RequestBody EquipmentPoolDto dto);

    /**
     * 根据整机编码去更新维修状态
     */
    @PostMapping("/api/base/equipmentpool/updateMaintenanceStatus")
    ResponseEntity<EquipmentPoolDto> updateMaintenanceStatus(@RequestParam("machineNumber") String machineNumber, @RequestParam("maintenanceStatus") String maintenanceStatus);

    /**
     * 根据整机编码去更新保养状态
     */
    @PostMapping("/api/base/equipmentpool/updateMaintainStatus")
    ResponseEntity<EquipmentPoolDto> updateMaintainStatus(@RequestParam("machineNumber") String machineNumber, @RequestParam("maintainStatus") String maintainStatus);


    @GetMapping("/api/base/equipmentpool/get-by-ids")
    ResponseEntity<List<EquipmentPoolDto>> getByIds(@RequestParam("ids") String[] ids);


    /**
     * 获取指定终端状态（terminalId）的设备池数据
     *
     * @param terminalId 整机编码。
     * @return 指定终端状态的设备池。
     */
    @GetMapping("/api/base/equipmentpool/getByTerminalId")
    ResponseEntity<EquipmentPoolDto> getByTerminalId(@RequestParam("terminalId") String terminalId);


    /**
     * 根据整机编码去更新调拨日期
     */
    @PutMapping("/api/base/equipmentpool/updateTransferDate")
    ResponseEntity<EquipmentPoolDto> updateTransferDate(@RequestParam("machineNumber") String machineNumber, @RequestParam("transferDate") Date transferDate);


    /**
     * 根据终端状态去更新设备池销售以及售后信息
     */
    @PutMapping("/api/base/equipmentpool/updateSaleAndAfterSale")
    ResponseEntity<EquipmentPoolDto> updateSaleAndAfterSale(@RequestBody EquipmentPoolDto dto);


    /**
     * 根据终端状态去更新设备池销售以及售后信息
     */
    @PutMapping("/api/base/equipmentpool/machineReturn")
    ResponseEntity<EquipmentPoolDto> machineReturn(@RequestBody EquipmentPoolDto dto);


    /**
     * 根据设备池id取更新库存状态
     */
    @GetMapping("/api/base/equipmentpool/updateStorageStatus")
    ResponseEntity<EquipmentPoolDto> updateStorageStatus(@RequestParam("id") String id, @RequestParam("storageStatus") String storageStatus);


    /**
     * 根据设备池id取更新生命周期状态
     */
    @GetMapping("/api/base/equipmentpool/updateLifeCycleSataus")
    ResponseEntity<EquipmentPoolDto> updateLifeCycleSataus(@RequestParam("id") String id, @RequestParam("lifeCycleSataus") String lifeCycleSataus);

    /**
     * 根据产品线、产品分类、产品名称查找设备池
     *
     * @param productLine     产品线
     * @param productCategory 产品分类
     * @param product         产品名称
     * @return 设备池list
     */
    @GetMapping("/api/base/equipmentpool/getByProduct")
    ResponseEntity<List<EquipmentPoolDto>> getByProductLineAndProductCategoryAndProduct(@RequestParam("productLine") String productLine,
                                                                                        @RequestParam("productCategory") String productCategory,
                                                                                        @RequestParam("product") String product);

    /**
     * 查询整车列表
     *
     * @param contactsTel 联系电话
     * @param credentialsNo 证件号码
     * @return
     */
    @GetMapping("/api/base/equipmentpool/getList")
    ResponseEntity<List<EquipmentPoolDto>> getList(@RequestParam(value = "contactsTel", required = false) String contactsTel,
                                                   @RequestParam(value = "credentialsNo", required = false) String credentialsNo);

    /**
     * 查询整车分页列表
     */
    @GetMapping("/api/base/equipmentpool/getPage")
    ResponseEntity<Map<String, Object>> getPage(Pageable pageable,
                                                @RequestParam(value = "maintainStatus", required = false) String maintainStatus,
                                                @RequestParam(value = "repairStatus", required = false) String repairStatus,
                                                @RequestParam(value = "keywords", required = false) String keywords,
                                                @RequestParam(value = "status", required = false) Boolean status,
                                                @RequestParam(value = "agencyCode", required = false) String agencyCode,
                                                @RequestParam(value = "equipmentPoolIds", required = false) List<String> equipmentPoolIds,
                                                @RequestParam(value = "productLine", required = false) String productLine);

    /**
     * 整车设备详情
     * @param id
     * @return
     */
    @GetMapping("/api/base/equipmentpool/queryVehicleInfo/{id}")
    ResponseEntity<EquipmentPoolDto> queryVehicleInfo(@PathVariable String id);
}
