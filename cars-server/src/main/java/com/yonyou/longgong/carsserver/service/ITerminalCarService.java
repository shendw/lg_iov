package com.yonyou.longgong.carsserver.service;

import com.yonyou.longgong.carsserver.pojo.TerminalCarPojo;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-11-07
 * @描述
 **/
public interface ITerminalCarService {
    Integer insertTerminalCar(TerminalCarPojo terminalCarPojo);

    /**
     * 解绑操作，把enable改成1
     * @param pkTerminalCar
     * @return
     */
    Integer unbundTerminalCar(String pkTerminalCar);
    /**
     *@描述   查询终端和车辆绑定信息
     *@参数
     *@返回值
     *@创建人 sdw
     *@创建时间 2019-11-24
     *@修改人和其它信息
     */
    List<TerminalCarPojo> selectTerminalCar(TerminalCarPojo terminalCarPojo);
    /**
     *@描述 根据状态查询所有终端id
     *@参数 状态 0启用 1未启用
     *@返回值 终端id的集合
     *@创建人 sdw
     *@创建时间 2019-11-14
     *@修改人和其它信息
     */
    List<String> selectTerminalid(Long enablestate);
}
