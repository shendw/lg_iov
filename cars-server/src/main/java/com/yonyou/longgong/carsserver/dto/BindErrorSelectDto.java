package com.yonyou.longgong.carsserver.dto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2020/4/20
 * @描述
 **/
@Data
public class BindErrorSelectDto {
    private String terminalid;

    private String carno;

    private String ishandle;

    private String handler;

    private String hsndletime;

    private String createtimeBegin;

    private String createtimeEnd;

    private Integer pageSize = 10;

    private Integer pageNum = 1;
}
