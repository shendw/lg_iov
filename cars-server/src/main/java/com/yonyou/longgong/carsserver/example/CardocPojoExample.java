package com.yonyou.longgong.carsserver.example;

import com.yonyou.longgong.carsserver.pojo.CardocPojo;
import java.util.ArrayList;
import java.util.List;

public class CardocPojoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CardocPojoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPkCardocIsNull() {
            addCriterion("PK_CARDOC is null");
            return (Criteria) this;
        }

        public Criteria andPkCardocIsNotNull() {
            addCriterion("PK_CARDOC is not null");
            return (Criteria) this;
        }

        public Criteria andPkCardocEqualTo(String value) {
            addCriterion("PK_CARDOC =", value, "pkCardoc");
            return (Criteria) this;
        }

        public Criteria andPkCardocNotEqualTo(String value) {
            addCriterion("PK_CARDOC <>", value, "pkCardoc");
            return (Criteria) this;
        }

        public Criteria andPkCardocGreaterThan(String value) {
            addCriterion("PK_CARDOC >", value, "pkCardoc");
            return (Criteria) this;
        }

        public Criteria andPkCardocGreaterThanOrEqualTo(String value) {
            addCriterion("PK_CARDOC >=", value, "pkCardoc");
            return (Criteria) this;
        }

        public Criteria andPkCardocLessThan(String value) {
            addCriterion("PK_CARDOC <", value, "pkCardoc");
            return (Criteria) this;
        }

        public Criteria andPkCardocLessThanOrEqualTo(String value) {
            addCriterion("PK_CARDOC <=", value, "pkCardoc");
            return (Criteria) this;
        }

        public Criteria andPkCardocLike(String value) {
            addCriterion("PK_CARDOC like", value, "pkCardoc");
            return (Criteria) this;
        }

        public Criteria andPkCardocNotLike(String value) {
            addCriterion("PK_CARDOC not like", value, "pkCardoc");
            return (Criteria) this;
        }

        public Criteria andPkCardocIn(List<String> values) {
            addCriterion("PK_CARDOC in", values, "pkCardoc");
            return (Criteria) this;
        }

        public Criteria andPkCardocNotIn(List<String> values) {
            addCriterion("PK_CARDOC not in", values, "pkCardoc");
            return (Criteria) this;
        }

        public Criteria andPkCardocBetween(String value1, String value2) {
            addCriterion("PK_CARDOC between", value1, value2, "pkCardoc");
            return (Criteria) this;
        }

        public Criteria andPkCardocNotBetween(String value1, String value2) {
            addCriterion("PK_CARDOC not between", value1, value2, "pkCardoc");
            return (Criteria) this;
        }

        public Criteria andCarmodelIsNull() {
            addCriterion("CARMODEL is null");
            return (Criteria) this;
        }

        public Criteria andCarmodelIsNotNull() {
            addCriterion("CARMODEL is not null");
            return (Criteria) this;
        }

        public Criteria andCarmodelEqualTo(String value) {
            addCriterion("CARMODEL =", value, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelNotEqualTo(String value) {
            addCriterion("CARMODEL <>", value, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelGreaterThan(String value) {
            addCriterion("CARMODEL >", value, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelGreaterThanOrEqualTo(String value) {
            addCriterion("CARMODEL >=", value, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelLessThan(String value) {
            addCriterion("CARMODEL <", value, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelLessThanOrEqualTo(String value) {
            addCriterion("CARMODEL <=", value, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelLike(String value) {
            addCriterion("CARMODEL like", value, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelNotLike(String value) {
            addCriterion("CARMODEL not like", value, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelIn(List<String> values) {
            addCriterion("CARMODEL in", values, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelNotIn(List<String> values) {
            addCriterion("CARMODEL not in", values, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelBetween(String value1, String value2) {
            addCriterion("CARMODEL between", value1, value2, "carmodel");
            return (Criteria) this;
        }

        public Criteria andCarmodelNotBetween(String value1, String value2) {
            addCriterion("CARMODEL not between", value1, value2, "carmodel");
            return (Criteria) this;
        }

        public Criteria andMachinemodelIsNull() {
            addCriterion("MACHINEMODEL is null");
            return (Criteria) this;
        }

        public Criteria andMachinemodelIsNotNull() {
            addCriterion("MACHINEMODEL is not null");
            return (Criteria) this;
        }

        public Criteria andMachinemodelEqualTo(String value) {
            addCriterion("MACHINEMODEL =", value, "machinemodel");
            return (Criteria) this;
        }

        public Criteria andMachinemodelNotEqualTo(String value) {
            addCriterion("MACHINEMODEL <>", value, "machinemodel");
            return (Criteria) this;
        }

        public Criteria andMachinemodelGreaterThan(String value) {
            addCriterion("MACHINEMODEL >", value, "machinemodel");
            return (Criteria) this;
        }

        public Criteria andMachinemodelGreaterThanOrEqualTo(String value) {
            addCriterion("MACHINEMODEL >=", value, "machinemodel");
            return (Criteria) this;
        }

        public Criteria andMachinemodelLessThan(String value) {
            addCriterion("MACHINEMODEL <", value, "machinemodel");
            return (Criteria) this;
        }

        public Criteria andMachinemodelLessThanOrEqualTo(String value) {
            addCriterion("MACHINEMODEL <=", value, "machinemodel");
            return (Criteria) this;
        }

        public Criteria andMachinemodelLike(String value) {
            addCriterion("MACHINEMODEL like", value, "machinemodel");
            return (Criteria) this;
        }

        public Criteria andMachinemodelNotLike(String value) {
            addCriterion("MACHINEMODEL not like", value, "machinemodel");
            return (Criteria) this;
        }

        public Criteria andMachinemodelIn(List<String> values) {
            addCriterion("MACHINEMODEL in", values, "machinemodel");
            return (Criteria) this;
        }

        public Criteria andMachinemodelNotIn(List<String> values) {
            addCriterion("MACHINEMODEL not in", values, "machinemodel");
            return (Criteria) this;
        }

        public Criteria andMachinemodelBetween(String value1, String value2) {
            addCriterion("MACHINEMODEL between", value1, value2, "machinemodel");
            return (Criteria) this;
        }

        public Criteria andMachinemodelNotBetween(String value1, String value2) {
            addCriterion("MACHINEMODEL not between", value1, value2, "machinemodel");
            return (Criteria) this;
        }

        public Criteria andCarnoIsNull() {
            addCriterion("CARNO is null");
            return (Criteria) this;
        }

        public Criteria andCarnoIsNotNull() {
            addCriterion("CARNO is not null");
            return (Criteria) this;
        }

        public Criteria andCarnoEqualTo(String value) {
            addCriterion("CARNO =", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotEqualTo(String value) {
            addCriterion("CARNO <>", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoGreaterThan(String value) {
            addCriterion("CARNO >", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoGreaterThanOrEqualTo(String value) {
            addCriterion("CARNO >=", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoLessThan(String value) {
            addCriterion("CARNO <", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoLessThanOrEqualTo(String value) {
            addCriterion("CARNO <=", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoLike(String value) {
            addCriterion("CARNO like", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotLike(String value) {
            addCriterion("CARNO not like", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoIn(List<String> values) {
            addCriterion("CARNO in", values, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotIn(List<String> values) {
            addCriterion("CARNO not in", values, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoBetween(String value1, String value2) {
            addCriterion("CARNO between", value1, value2, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotBetween(String value1, String value2) {
            addCriterion("CARNO not between", value1, value2, "carno");
            return (Criteria) this;
        }

        public Criteria andServiceAgentIsNull() {
            addCriterion("SERVICE_AGENT is null");
            return (Criteria) this;
        }

        public Criteria andServiceAgentIsNotNull() {
            addCriterion("SERVICE_AGENT is not null");
            return (Criteria) this;
        }

        public Criteria andServiceAgentEqualTo(String value) {
            addCriterion("SERVICE_AGENT =", value, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentNotEqualTo(String value) {
            addCriterion("SERVICE_AGENT <>", value, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentGreaterThan(String value) {
            addCriterion("SERVICE_AGENT >", value, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentGreaterThanOrEqualTo(String value) {
            addCriterion("SERVICE_AGENT >=", value, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentLessThan(String value) {
            addCriterion("SERVICE_AGENT <", value, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentLessThanOrEqualTo(String value) {
            addCriterion("SERVICE_AGENT <=", value, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentLike(String value) {
            addCriterion("SERVICE_AGENT like", value, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentNotLike(String value) {
            addCriterion("SERVICE_AGENT not like", value, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentIn(List<String> values) {
            addCriterion("SERVICE_AGENT in", values, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentNotIn(List<String> values) {
            addCriterion("SERVICE_AGENT not in", values, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentBetween(String value1, String value2) {
            addCriterion("SERVICE_AGENT between", value1, value2, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andServiceAgentNotBetween(String value1, String value2) {
            addCriterion("SERVICE_AGENT not between", value1, value2, "serviceAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentIsNull() {
            addCriterion("SALE_AGENT is null");
            return (Criteria) this;
        }

        public Criteria andSaleAgentIsNotNull() {
            addCriterion("SALE_AGENT is not null");
            return (Criteria) this;
        }

        public Criteria andSaleAgentEqualTo(String value) {
            addCriterion("SALE_AGENT =", value, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentNotEqualTo(String value) {
            addCriterion("SALE_AGENT <>", value, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentGreaterThan(String value) {
            addCriterion("SALE_AGENT >", value, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentGreaterThanOrEqualTo(String value) {
            addCriterion("SALE_AGENT >=", value, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentLessThan(String value) {
            addCriterion("SALE_AGENT <", value, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentLessThanOrEqualTo(String value) {
            addCriterion("SALE_AGENT <=", value, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentLike(String value) {
            addCriterion("SALE_AGENT like", value, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentNotLike(String value) {
            addCriterion("SALE_AGENT not like", value, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentIn(List<String> values) {
            addCriterion("SALE_AGENT in", values, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentNotIn(List<String> values) {
            addCriterion("SALE_AGENT not in", values, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentBetween(String value1, String value2) {
            addCriterion("SALE_AGENT between", value1, value2, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andSaleAgentNotBetween(String value1, String value2) {
            addCriterion("SALE_AGENT not between", value1, value2, "saleAgent");
            return (Criteria) this;
        }

        public Criteria andCustomerIsNull() {
            addCriterion("CUSTOMER is null");
            return (Criteria) this;
        }

        public Criteria andCustomerIsNotNull() {
            addCriterion("CUSTOMER is not null");
            return (Criteria) this;
        }

        public Criteria andCustomerEqualTo(String value) {
            addCriterion("CUSTOMER =", value, "customer");
            return (Criteria) this;
        }

        public Criteria andCustomerNotEqualTo(String value) {
            addCriterion("CUSTOMER <>", value, "customer");
            return (Criteria) this;
        }

        public Criteria andCustomerGreaterThan(String value) {
            addCriterion("CUSTOMER >", value, "customer");
            return (Criteria) this;
        }

        public Criteria andCustomerGreaterThanOrEqualTo(String value) {
            addCriterion("CUSTOMER >=", value, "customer");
            return (Criteria) this;
        }

        public Criteria andCustomerLessThan(String value) {
            addCriterion("CUSTOMER <", value, "customer");
            return (Criteria) this;
        }

        public Criteria andCustomerLessThanOrEqualTo(String value) {
            addCriterion("CUSTOMER <=", value, "customer");
            return (Criteria) this;
        }

        public Criteria andCustomerLike(String value) {
            addCriterion("CUSTOMER like", value, "customer");
            return (Criteria) this;
        }

        public Criteria andCustomerNotLike(String value) {
            addCriterion("CUSTOMER not like", value, "customer");
            return (Criteria) this;
        }

        public Criteria andCustomerIn(List<String> values) {
            addCriterion("CUSTOMER in", values, "customer");
            return (Criteria) this;
        }

        public Criteria andCustomerNotIn(List<String> values) {
            addCriterion("CUSTOMER not in", values, "customer");
            return (Criteria) this;
        }

        public Criteria andCustomerBetween(String value1, String value2) {
            addCriterion("CUSTOMER between", value1, value2, "customer");
            return (Criteria) this;
        }

        public Criteria andCustomerNotBetween(String value1, String value2) {
            addCriterion("CUSTOMER not between", value1, value2, "customer");
            return (Criteria) this;
        }

        public Criteria andSerialIsNull() {
            addCriterion("SERIAL is null");
            return (Criteria) this;
        }

        public Criteria andSerialIsNotNull() {
            addCriterion("SERIAL is not null");
            return (Criteria) this;
        }

        public Criteria andSerialEqualTo(String value) {
            addCriterion("SERIAL =", value, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialNotEqualTo(String value) {
            addCriterion("SERIAL <>", value, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialGreaterThan(String value) {
            addCriterion("SERIAL >", value, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialGreaterThanOrEqualTo(String value) {
            addCriterion("SERIAL >=", value, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialLessThan(String value) {
            addCriterion("SERIAL <", value, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialLessThanOrEqualTo(String value) {
            addCriterion("SERIAL <=", value, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialLike(String value) {
            addCriterion("SERIAL like", value, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialNotLike(String value) {
            addCriterion("SERIAL not like", value, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialIn(List<String> values) {
            addCriterion("SERIAL in", values, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialNotIn(List<String> values) {
            addCriterion("SERIAL not in", values, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialBetween(String value1, String value2) {
            addCriterion("SERIAL between", value1, value2, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialNotBetween(String value1, String value2) {
            addCriterion("SERIAL not between", value1, value2, "serial");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("STATUS is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("STATUS is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Long value) {
            addCriterion("STATUS =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Long value) {
            addCriterion("STATUS <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Long value) {
            addCriterion("STATUS >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Long value) {
            addCriterion("STATUS >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Long value) {
            addCriterion("STATUS <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Long value) {
            addCriterion("STATUS <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Long> values) {
            addCriterion("STATUS in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Long> values) {
            addCriterion("STATUS not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Long value1, Long value2) {
            addCriterion("STATUS between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Long value1, Long value2) {
            addCriterion("STATUS not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andInstocktimeIsNull() {
            addCriterion("INSTOCKTIME is null");
            return (Criteria) this;
        }

        public Criteria andInstocktimeIsNotNull() {
            addCriterion("INSTOCKTIME is not null");
            return (Criteria) this;
        }

        public Criteria andInstocktimeEqualTo(String value) {
            addCriterion("INSTOCKTIME =", value, "instocktime");
            return (Criteria) this;
        }

        public Criteria andInstocktimeNotEqualTo(String value) {
            addCriterion("INSTOCKTIME <>", value, "instocktime");
            return (Criteria) this;
        }

        public Criteria andInstocktimeGreaterThan(String value) {
            addCriterion("INSTOCKTIME >", value, "instocktime");
            return (Criteria) this;
        }

        public Criteria andInstocktimeGreaterThanOrEqualTo(String value) {
            addCriterion("INSTOCKTIME >=", value, "instocktime");
            return (Criteria) this;
        }

        public Criteria andInstocktimeLessThan(String value) {
            addCriterion("INSTOCKTIME <", value, "instocktime");
            return (Criteria) this;
        }

        public Criteria andInstocktimeLessThanOrEqualTo(String value) {
            addCriterion("INSTOCKTIME <=", value, "instocktime");
            return (Criteria) this;
        }

        public Criteria andInstocktimeLike(String value) {
            addCriterion("INSTOCKTIME like", value, "instocktime");
            return (Criteria) this;
        }

        public Criteria andInstocktimeNotLike(String value) {
            addCriterion("INSTOCKTIME not like", value, "instocktime");
            return (Criteria) this;
        }

        public Criteria andInstocktimeIn(List<String> values) {
            addCriterion("INSTOCKTIME in", values, "instocktime");
            return (Criteria) this;
        }

        public Criteria andInstocktimeNotIn(List<String> values) {
            addCriterion("INSTOCKTIME not in", values, "instocktime");
            return (Criteria) this;
        }

        public Criteria andInstocktimeBetween(String value1, String value2) {
            addCriterion("INSTOCKTIME between", value1, value2, "instocktime");
            return (Criteria) this;
        }

        public Criteria andInstocktimeNotBetween(String value1, String value2) {
            addCriterion("INSTOCKTIME not between", value1, value2, "instocktime");
            return (Criteria) this;
        }

        public Criteria andOutstocktimeIsNull() {
            addCriterion("OUTSTOCKTIME is null");
            return (Criteria) this;
        }

        public Criteria andOutstocktimeIsNotNull() {
            addCriterion("OUTSTOCKTIME is not null");
            return (Criteria) this;
        }

        public Criteria andOutstocktimeEqualTo(String value) {
            addCriterion("OUTSTOCKTIME =", value, "outstocktime");
            return (Criteria) this;
        }

        public Criteria andOutstocktimeNotEqualTo(String value) {
            addCriterion("OUTSTOCKTIME <>", value, "outstocktime");
            return (Criteria) this;
        }

        public Criteria andOutstocktimeGreaterThan(String value) {
            addCriterion("OUTSTOCKTIME >", value, "outstocktime");
            return (Criteria) this;
        }

        public Criteria andOutstocktimeGreaterThanOrEqualTo(String value) {
            addCriterion("OUTSTOCKTIME >=", value, "outstocktime");
            return (Criteria) this;
        }

        public Criteria andOutstocktimeLessThan(String value) {
            addCriterion("OUTSTOCKTIME <", value, "outstocktime");
            return (Criteria) this;
        }

        public Criteria andOutstocktimeLessThanOrEqualTo(String value) {
            addCriterion("OUTSTOCKTIME <=", value, "outstocktime");
            return (Criteria) this;
        }

        public Criteria andOutstocktimeLike(String value) {
            addCriterion("OUTSTOCKTIME like", value, "outstocktime");
            return (Criteria) this;
        }

        public Criteria andOutstocktimeNotLike(String value) {
            addCriterion("OUTSTOCKTIME not like", value, "outstocktime");
            return (Criteria) this;
        }

        public Criteria andOutstocktimeIn(List<String> values) {
            addCriterion("OUTSTOCKTIME in", values, "outstocktime");
            return (Criteria) this;
        }

        public Criteria andOutstocktimeNotIn(List<String> values) {
            addCriterion("OUTSTOCKTIME not in", values, "outstocktime");
            return (Criteria) this;
        }

        public Criteria andOutstocktimeBetween(String value1, String value2) {
            addCriterion("OUTSTOCKTIME between", value1, value2, "outstocktime");
            return (Criteria) this;
        }

        public Criteria andOutstocktimeNotBetween(String value1, String value2) {
            addCriterion("OUTSTOCKTIME not between", value1, value2, "outstocktime");
            return (Criteria) this;
        }

        public Criteria andSaletimeIsNull() {
            addCriterion("SALETIME is null");
            return (Criteria) this;
        }

        public Criteria andSaletimeIsNotNull() {
            addCriterion("SALETIME is not null");
            return (Criteria) this;
        }

        public Criteria andSaletimeEqualTo(String value) {
            addCriterion("SALETIME =", value, "saletime");
            return (Criteria) this;
        }

        public Criteria andSaletimeNotEqualTo(String value) {
            addCriterion("SALETIME <>", value, "saletime");
            return (Criteria) this;
        }

        public Criteria andSaletimeGreaterThan(String value) {
            addCriterion("SALETIME >", value, "saletime");
            return (Criteria) this;
        }

        public Criteria andSaletimeGreaterThanOrEqualTo(String value) {
            addCriterion("SALETIME >=", value, "saletime");
            return (Criteria) this;
        }

        public Criteria andSaletimeLessThan(String value) {
            addCriterion("SALETIME <", value, "saletime");
            return (Criteria) this;
        }

        public Criteria andSaletimeLessThanOrEqualTo(String value) {
            addCriterion("SALETIME <=", value, "saletime");
            return (Criteria) this;
        }

        public Criteria andSaletimeLike(String value) {
            addCriterion("SALETIME like", value, "saletime");
            return (Criteria) this;
        }

        public Criteria andSaletimeNotLike(String value) {
            addCriterion("SALETIME not like", value, "saletime");
            return (Criteria) this;
        }

        public Criteria andSaletimeIn(List<String> values) {
            addCriterion("SALETIME in", values, "saletime");
            return (Criteria) this;
        }

        public Criteria andSaletimeNotIn(List<String> values) {
            addCriterion("SALETIME not in", values, "saletime");
            return (Criteria) this;
        }

        public Criteria andSaletimeBetween(String value1, String value2) {
            addCriterion("SALETIME between", value1, value2, "saletime");
            return (Criteria) this;
        }

        public Criteria andSaletimeNotBetween(String value1, String value2) {
            addCriterion("SALETIME not between", value1, value2, "saletime");
            return (Criteria) this;
        }

        public Criteria andSalecompanyIsNull() {
            addCriterion("SALECOMPANY is null");
            return (Criteria) this;
        }

        public Criteria andSalecompanyIsNotNull() {
            addCriterion("SALECOMPANY is not null");
            return (Criteria) this;
        }

        public Criteria andSalecompanyEqualTo(String value) {
            addCriterion("SALECOMPANY =", value, "salecompany");
            return (Criteria) this;
        }

        public Criteria andSalecompanyNotEqualTo(String value) {
            addCriterion("SALECOMPANY <>", value, "salecompany");
            return (Criteria) this;
        }

        public Criteria andSalecompanyGreaterThan(String value) {
            addCriterion("SALECOMPANY >", value, "salecompany");
            return (Criteria) this;
        }

        public Criteria andSalecompanyGreaterThanOrEqualTo(String value) {
            addCriterion("SALECOMPANY >=", value, "salecompany");
            return (Criteria) this;
        }

        public Criteria andSalecompanyLessThan(String value) {
            addCriterion("SALECOMPANY <", value, "salecompany");
            return (Criteria) this;
        }

        public Criteria andSalecompanyLessThanOrEqualTo(String value) {
            addCriterion("SALECOMPANY <=", value, "salecompany");
            return (Criteria) this;
        }

        public Criteria andSalecompanyLike(String value) {
            addCriterion("SALECOMPANY like", value, "salecompany");
            return (Criteria) this;
        }

        public Criteria andSalecompanyNotLike(String value) {
            addCriterion("SALECOMPANY not like", value, "salecompany");
            return (Criteria) this;
        }

        public Criteria andSalecompanyIn(List<String> values) {
            addCriterion("SALECOMPANY in", values, "salecompany");
            return (Criteria) this;
        }

        public Criteria andSalecompanyNotIn(List<String> values) {
            addCriterion("SALECOMPANY not in", values, "salecompany");
            return (Criteria) this;
        }

        public Criteria andSalecompanyBetween(String value1, String value2) {
            addCriterion("SALECOMPANY between", value1, value2, "salecompany");
            return (Criteria) this;
        }

        public Criteria andSalecompanyNotBetween(String value1, String value2) {
            addCriterion("SALECOMPANY not between", value1, value2, "salecompany");
            return (Criteria) this;
        }

        public Criteria andFinalcustomerIsNull() {
            addCriterion("FINALCUSTOMER is null");
            return (Criteria) this;
        }

        public Criteria andFinalcustomerIsNotNull() {
            addCriterion("FINALCUSTOMER is not null");
            return (Criteria) this;
        }

        public Criteria andFinalcustomerEqualTo(String value) {
            addCriterion("FINALCUSTOMER =", value, "finalcustomer");
            return (Criteria) this;
        }

        public Criteria andFinalcustomerNotEqualTo(String value) {
            addCriterion("FINALCUSTOMER <>", value, "finalcustomer");
            return (Criteria) this;
        }

        public Criteria andFinalcustomerGreaterThan(String value) {
            addCriterion("FINALCUSTOMER >", value, "finalcustomer");
            return (Criteria) this;
        }

        public Criteria andFinalcustomerGreaterThanOrEqualTo(String value) {
            addCriterion("FINALCUSTOMER >=", value, "finalcustomer");
            return (Criteria) this;
        }

        public Criteria andFinalcustomerLessThan(String value) {
            addCriterion("FINALCUSTOMER <", value, "finalcustomer");
            return (Criteria) this;
        }

        public Criteria andFinalcustomerLessThanOrEqualTo(String value) {
            addCriterion("FINALCUSTOMER <=", value, "finalcustomer");
            return (Criteria) this;
        }

        public Criteria andFinalcustomerLike(String value) {
            addCriterion("FINALCUSTOMER like", value, "finalcustomer");
            return (Criteria) this;
        }

        public Criteria andFinalcustomerNotLike(String value) {
            addCriterion("FINALCUSTOMER not like", value, "finalcustomer");
            return (Criteria) this;
        }

        public Criteria andFinalcustomerIn(List<String> values) {
            addCriterion("FINALCUSTOMER in", values, "finalcustomer");
            return (Criteria) this;
        }

        public Criteria andFinalcustomerNotIn(List<String> values) {
            addCriterion("FINALCUSTOMER not in", values, "finalcustomer");
            return (Criteria) this;
        }

        public Criteria andFinalcustomerBetween(String value1, String value2) {
            addCriterion("FINALCUSTOMER between", value1, value2, "finalcustomer");
            return (Criteria) this;
        }

        public Criteria andFinalcustomerNotBetween(String value1, String value2) {
            addCriterion("FINALCUSTOMER not between", value1, value2, "finalcustomer");
            return (Criteria) this;
        }

        public Criteria andLasttimeIsNull() {
            addCriterion("LASTTIME is null");
            return (Criteria) this;
        }

        public Criteria andLasttimeIsNotNull() {
            addCriterion("LASTTIME is not null");
            return (Criteria) this;
        }

        public Criteria andLasttimeEqualTo(String value) {
            addCriterion("LASTTIME =", value, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeNotEqualTo(String value) {
            addCriterion("LASTTIME <>", value, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeGreaterThan(String value) {
            addCriterion("LASTTIME >", value, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeGreaterThanOrEqualTo(String value) {
            addCriterion("LASTTIME >=", value, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeLessThan(String value) {
            addCriterion("LASTTIME <", value, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeLessThanOrEqualTo(String value) {
            addCriterion("LASTTIME <=", value, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeLike(String value) {
            addCriterion("LASTTIME like", value, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeNotLike(String value) {
            addCriterion("LASTTIME not like", value, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeIn(List<String> values) {
            addCriterion("LASTTIME in", values, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeNotIn(List<String> values) {
            addCriterion("LASTTIME not in", values, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeBetween(String value1, String value2) {
            addCriterion("LASTTIME between", value1, value2, "lasttime");
            return (Criteria) this;
        }

        public Criteria andLasttimeNotBetween(String value1, String value2) {
            addCriterion("LASTTIME not between", value1, value2, "lasttime");
            return (Criteria) this;
        }

        public Criteria andWorktimeIsNull() {
            addCriterion("WORKTIME is null");
            return (Criteria) this;
        }

        public Criteria andWorktimeIsNotNull() {
            addCriterion("WORKTIME is not null");
            return (Criteria) this;
        }

        public Criteria andWorktimeEqualTo(String value) {
            addCriterion("WORKTIME =", value, "worktime");
            return (Criteria) this;
        }

        public Criteria andWorktimeNotEqualTo(String value) {
            addCriterion("WORKTIME <>", value, "worktime");
            return (Criteria) this;
        }

        public Criteria andWorktimeGreaterThan(String value) {
            addCriterion("WORKTIME >", value, "worktime");
            return (Criteria) this;
        }

        public Criteria andWorktimeGreaterThanOrEqualTo(String value) {
            addCriterion("WORKTIME >=", value, "worktime");
            return (Criteria) this;
        }

        public Criteria andWorktimeLessThan(String value) {
            addCriterion("WORKTIME <", value, "worktime");
            return (Criteria) this;
        }

        public Criteria andWorktimeLessThanOrEqualTo(String value) {
            addCriterion("WORKTIME <=", value, "worktime");
            return (Criteria) this;
        }

        public Criteria andWorktimeLike(String value) {
            addCriterion("WORKTIME like", value, "worktime");
            return (Criteria) this;
        }

        public Criteria andWorktimeNotLike(String value) {
            addCriterion("WORKTIME not like", value, "worktime");
            return (Criteria) this;
        }

        public Criteria andWorktimeIn(List<String> values) {
            addCriterion("WORKTIME in", values, "worktime");
            return (Criteria) this;
        }

        public Criteria andWorktimeNotIn(List<String> values) {
            addCriterion("WORKTIME not in", values, "worktime");
            return (Criteria) this;
        }

        public Criteria andWorktimeBetween(String value1, String value2) {
            addCriterion("WORKTIME between", value1, value2, "worktime");
            return (Criteria) this;
        }

        public Criteria andWorktimeNotBetween(String value1, String value2) {
            addCriterion("WORKTIME not between", value1, value2, "worktime");
            return (Criteria) this;
        }

        public Criteria andLocationIsNull() {
            addCriterion("LOCATION is null");
            return (Criteria) this;
        }

        public Criteria andLocationIsNotNull() {
            addCriterion("LOCATION is not null");
            return (Criteria) this;
        }

        public Criteria andLocationEqualTo(String value) {
            addCriterion("LOCATION =", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationNotEqualTo(String value) {
            addCriterion("LOCATION <>", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationGreaterThan(String value) {
            addCriterion("LOCATION >", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationGreaterThanOrEqualTo(String value) {
            addCriterion("LOCATION >=", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationLessThan(String value) {
            addCriterion("LOCATION <", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationLessThanOrEqualTo(String value) {
            addCriterion("LOCATION <=", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationLike(String value) {
            addCriterion("LOCATION like", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationNotLike(String value) {
            addCriterion("LOCATION not like", value, "location");
            return (Criteria) this;
        }

        public Criteria andLocationIn(List<String> values) {
            addCriterion("LOCATION in", values, "location");
            return (Criteria) this;
        }

        public Criteria andLocationNotIn(List<String> values) {
            addCriterion("LOCATION not in", values, "location");
            return (Criteria) this;
        }

        public Criteria andLocationBetween(String value1, String value2) {
            addCriterion("LOCATION between", value1, value2, "location");
            return (Criteria) this;
        }

        public Criteria andLocationNotBetween(String value1, String value2) {
            addCriterion("LOCATION not between", value1, value2, "location");
            return (Criteria) this;
        }

        public Criteria andCreationtimeIsNull() {
            addCriterion("CREATIONTIME is null");
            return (Criteria) this;
        }

        public Criteria andCreationtimeIsNotNull() {
            addCriterion("CREATIONTIME is not null");
            return (Criteria) this;
        }

        public Criteria andCreationtimeEqualTo(String value) {
            addCriterion("CREATIONTIME =", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeNotEqualTo(String value) {
            addCriterion("CREATIONTIME <>", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeGreaterThan(String value) {
            addCriterion("CREATIONTIME >", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeGreaterThanOrEqualTo(String value) {
            addCriterion("CREATIONTIME >=", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeLessThan(String value) {
            addCriterion("CREATIONTIME <", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeLessThanOrEqualTo(String value) {
            addCriterion("CREATIONTIME <=", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeLike(String value) {
            addCriterion("CREATIONTIME like", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeNotLike(String value) {
            addCriterion("CREATIONTIME not like", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeIn(List<String> values) {
            addCriterion("CREATIONTIME in", values, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeNotIn(List<String> values) {
            addCriterion("CREATIONTIME not in", values, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeBetween(String value1, String value2) {
            addCriterion("CREATIONTIME between", value1, value2, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeNotBetween(String value1, String value2) {
            addCriterion("CREATIONTIME not between", value1, value2, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNull() {
            addCriterion("CREATOR is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNotNull() {
            addCriterion("CREATOR is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorEqualTo(String value) {
            addCriterion("CREATOR =", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotEqualTo(String value) {
            addCriterion("CREATOR <>", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThan(String value) {
            addCriterion("CREATOR >", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThanOrEqualTo(String value) {
            addCriterion("CREATOR >=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThan(String value) {
            addCriterion("CREATOR <", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThanOrEqualTo(String value) {
            addCriterion("CREATOR <=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLike(String value) {
            addCriterion("CREATOR like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotLike(String value) {
            addCriterion("CREATOR not like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorIn(List<String> values) {
            addCriterion("CREATOR in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotIn(List<String> values) {
            addCriterion("CREATOR not in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorBetween(String value1, String value2) {
            addCriterion("CREATOR between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotBetween(String value1, String value2) {
            addCriterion("CREATOR not between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeIsNull() {
            addCriterion("MODIFIEDTIME is null");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeIsNotNull() {
            addCriterion("MODIFIEDTIME is not null");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeEqualTo(String value) {
            addCriterion("MODIFIEDTIME =", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeNotEqualTo(String value) {
            addCriterion("MODIFIEDTIME <>", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeGreaterThan(String value) {
            addCriterion("MODIFIEDTIME >", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeGreaterThanOrEqualTo(String value) {
            addCriterion("MODIFIEDTIME >=", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeLessThan(String value) {
            addCriterion("MODIFIEDTIME <", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeLessThanOrEqualTo(String value) {
            addCriterion("MODIFIEDTIME <=", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeLike(String value) {
            addCriterion("MODIFIEDTIME like", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeNotLike(String value) {
            addCriterion("MODIFIEDTIME not like", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeIn(List<String> values) {
            addCriterion("MODIFIEDTIME in", values, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeNotIn(List<String> values) {
            addCriterion("MODIFIEDTIME not in", values, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeBetween(String value1, String value2) {
            addCriterion("MODIFIEDTIME between", value1, value2, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeNotBetween(String value1, String value2) {
            addCriterion("MODIFIEDTIME not between", value1, value2, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifierIsNull() {
            addCriterion("MODIFIER is null");
            return (Criteria) this;
        }

        public Criteria andModifierIsNotNull() {
            addCriterion("MODIFIER is not null");
            return (Criteria) this;
        }

        public Criteria andModifierEqualTo(String value) {
            addCriterion("MODIFIER =", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierNotEqualTo(String value) {
            addCriterion("MODIFIER <>", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierGreaterThan(String value) {
            addCriterion("MODIFIER >", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierGreaterThanOrEqualTo(String value) {
            addCriterion("MODIFIER >=", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierLessThan(String value) {
            addCriterion("MODIFIER <", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierLessThanOrEqualTo(String value) {
            addCriterion("MODIFIER <=", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierLike(String value) {
            addCriterion("MODIFIER like", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierNotLike(String value) {
            addCriterion("MODIFIER not like", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierIn(List<String> values) {
            addCriterion("MODIFIER in", values, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierNotIn(List<String> values) {
            addCriterion("MODIFIER not in", values, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierBetween(String value1, String value2) {
            addCriterion("MODIFIER between", value1, value2, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierNotBetween(String value1, String value2) {
            addCriterion("MODIFIER not between", value1, value2, "modifier");
            return (Criteria) this;
        }

        public Criteria andDrIsNull() {
            addCriterion("DR is null");
            return (Criteria) this;
        }

        public Criteria andDrIsNotNull() {
            addCriterion("DR is not null");
            return (Criteria) this;
        }

        public Criteria andDrEqualTo(Long value) {
            addCriterion("DR =", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrNotEqualTo(Long value) {
            addCriterion("DR <>", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrGreaterThan(Long value) {
            addCriterion("DR >", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrGreaterThanOrEqualTo(Long value) {
            addCriterion("DR >=", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrLessThan(Long value) {
            addCriterion("DR <", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrLessThanOrEqualTo(Long value) {
            addCriterion("DR <=", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrIn(List<Long> values) {
            addCriterion("DR in", values, "dr");
            return (Criteria) this;
        }

        public Criteria andDrNotIn(List<Long> values) {
            addCriterion("DR not in", values, "dr");
            return (Criteria) this;
        }

        public Criteria andDrBetween(Long value1, Long value2) {
            addCriterion("DR between", value1, value2, "dr");
            return (Criteria) this;
        }

        public Criteria andDrNotBetween(Long value1, Long value2) {
            addCriterion("DR not between", value1, value2, "dr");
            return (Criteria) this;
        }

        public Criteria andVersionIsNull() {
            addCriterion("VERSION is null");
            return (Criteria) this;
        }

        public Criteria andVersionIsNotNull() {
            addCriterion("VERSION is not null");
            return (Criteria) this;
        }

        public Criteria andVersionEqualTo(Long value) {
            addCriterion("VERSION =", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotEqualTo(Long value) {
            addCriterion("VERSION <>", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionGreaterThan(Long value) {
            addCriterion("VERSION >", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionGreaterThanOrEqualTo(Long value) {
            addCriterion("VERSION >=", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionLessThan(Long value) {
            addCriterion("VERSION <", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionLessThanOrEqualTo(Long value) {
            addCriterion("VERSION <=", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionIn(List<Long> values) {
            addCriterion("VERSION in", values, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotIn(List<Long> values) {
            addCriterion("VERSION not in", values, "version");
            return (Criteria) this;
        }

        public Criteria andVersionBetween(Long value1, Long value2) {
            addCriterion("VERSION between", value1, value2, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotBetween(Long value1, Long value2) {
            addCriterion("VERSION not between", value1, value2, "version");
            return (Criteria) this;
        }

        public Criteria andPkOrgIsNull() {
            addCriterion("PK_ORG is null");
            return (Criteria) this;
        }

        public Criteria andPkOrgIsNotNull() {
            addCriterion("PK_ORG is not null");
            return (Criteria) this;
        }

        public Criteria andPkOrgEqualTo(String value) {
            addCriterion("PK_ORG =", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgNotEqualTo(String value) {
            addCriterion("PK_ORG <>", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgGreaterThan(String value) {
            addCriterion("PK_ORG >", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgGreaterThanOrEqualTo(String value) {
            addCriterion("PK_ORG >=", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgLessThan(String value) {
            addCriterion("PK_ORG <", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgLessThanOrEqualTo(String value) {
            addCriterion("PK_ORG <=", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgLike(String value) {
            addCriterion("PK_ORG like", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgNotLike(String value) {
            addCriterion("PK_ORG not like", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgIn(List<String> values) {
            addCriterion("PK_ORG in", values, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgNotIn(List<String> values) {
            addCriterion("PK_ORG not in", values, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgBetween(String value1, String value2) {
            addCriterion("PK_ORG between", value1, value2, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgNotBetween(String value1, String value2) {
            addCriterion("PK_ORG not between", value1, value2, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("REMARK is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("REMARK =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("REMARK <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("REMARK >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("REMARK >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("REMARK <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("REMARK <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("REMARK like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("REMARK not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("REMARK in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("REMARK not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("REMARK between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("REMARK not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andWorkstatusIsNull() {
            addCriterion("WORKSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andWorkstatusIsNotNull() {
            addCriterion("WORKSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andWorkstatusEqualTo(String value) {
            addCriterion("WORKSTATUS =", value, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusNotEqualTo(String value) {
            addCriterion("WORKSTATUS <>", value, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusGreaterThan(String value) {
            addCriterion("WORKSTATUS >", value, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusGreaterThanOrEqualTo(String value) {
            addCriterion("WORKSTATUS >=", value, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusLessThan(String value) {
            addCriterion("WORKSTATUS <", value, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusLessThanOrEqualTo(String value) {
            addCriterion("WORKSTATUS <=", value, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusLike(String value) {
            addCriterion("WORKSTATUS like", value, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusNotLike(String value) {
            addCriterion("WORKSTATUS not like", value, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusIn(List<String> values) {
            addCriterion("WORKSTATUS in", values, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusNotIn(List<String> values) {
            addCriterion("WORKSTATUS not in", values, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusBetween(String value1, String value2) {
            addCriterion("WORKSTATUS between", value1, value2, "workstatus");
            return (Criteria) this;
        }

        public Criteria andWorkstatusNotBetween(String value1, String value2) {
            addCriterion("WORKSTATUS not between", value1, value2, "workstatus");
            return (Criteria) this;
        }

        public Criteria andLockstatusIsNull() {
            addCriterion("LOCKSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andLockstatusIsNotNull() {
            addCriterion("LOCKSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andLockstatusEqualTo(String value) {
            addCriterion("LOCKSTATUS =", value, "lockstatus");
            return (Criteria) this;
        }

        public Criteria andLockstatusNotEqualTo(String value) {
            addCriterion("LOCKSTATUS <>", value, "lockstatus");
            return (Criteria) this;
        }

        public Criteria andLockstatusGreaterThan(String value) {
            addCriterion("LOCKSTATUS >", value, "lockstatus");
            return (Criteria) this;
        }

        public Criteria andLockstatusGreaterThanOrEqualTo(String value) {
            addCriterion("LOCKSTATUS >=", value, "lockstatus");
            return (Criteria) this;
        }

        public Criteria andLockstatusLessThan(String value) {
            addCriterion("LOCKSTATUS <", value, "lockstatus");
            return (Criteria) this;
        }

        public Criteria andLockstatusLessThanOrEqualTo(String value) {
            addCriterion("LOCKSTATUS <=", value, "lockstatus");
            return (Criteria) this;
        }

        public Criteria andLockstatusLike(String value) {
            addCriterion("LOCKSTATUS like", value, "lockstatus");
            return (Criteria) this;
        }

        public Criteria andLockstatusNotLike(String value) {
            addCriterion("LOCKSTATUS not like", value, "lockstatus");
            return (Criteria) this;
        }

        public Criteria andLockstatusIn(List<String> values) {
            addCriterion("LOCKSTATUS in", values, "lockstatus");
            return (Criteria) this;
        }

        public Criteria andLockstatusNotIn(List<String> values) {
            addCriterion("LOCKSTATUS not in", values, "lockstatus");
            return (Criteria) this;
        }

        public Criteria andLockstatusBetween(String value1, String value2) {
            addCriterion("LOCKSTATUS between", value1, value2, "lockstatus");
            return (Criteria) this;
        }

        public Criteria andLockstatusNotBetween(String value1, String value2) {
            addCriterion("LOCKSTATUS not between", value1, value2, "lockstatus");
            return (Criteria) this;
        }

        public Criteria andOutlineerIsNull() {
            addCriterion("OUTLINEER is null");
            return (Criteria) this;
        }

        public Criteria andOutlineerIsNotNull() {
            addCriterion("OUTLINEER is not null");
            return (Criteria) this;
        }

        public Criteria andOutlineerEqualTo(String value) {
            addCriterion("OUTLINEER =", value, "outlineer");
            return (Criteria) this;
        }

        public Criteria andOutlineerNotEqualTo(String value) {
            addCriterion("OUTLINEER <>", value, "outlineer");
            return (Criteria) this;
        }

        public Criteria andOutlineerGreaterThan(String value) {
            addCriterion("OUTLINEER >", value, "outlineer");
            return (Criteria) this;
        }

        public Criteria andOutlineerGreaterThanOrEqualTo(String value) {
            addCriterion("OUTLINEER >=", value, "outlineer");
            return (Criteria) this;
        }

        public Criteria andOutlineerLessThan(String value) {
            addCriterion("OUTLINEER <", value, "outlineer");
            return (Criteria) this;
        }

        public Criteria andOutlineerLessThanOrEqualTo(String value) {
            addCriterion("OUTLINEER <=", value, "outlineer");
            return (Criteria) this;
        }

        public Criteria andOutlineerLike(String value) {
            addCriterion("OUTLINEER like", value, "outlineer");
            return (Criteria) this;
        }

        public Criteria andOutlineerNotLike(String value) {
            addCriterion("OUTLINEER not like", value, "outlineer");
            return (Criteria) this;
        }

        public Criteria andOutlineerIn(List<String> values) {
            addCriterion("OUTLINEER in", values, "outlineer");
            return (Criteria) this;
        }

        public Criteria andOutlineerNotIn(List<String> values) {
            addCriterion("OUTLINEER not in", values, "outlineer");
            return (Criteria) this;
        }

        public Criteria andOutlineerBetween(String value1, String value2) {
            addCriterion("OUTLINEER between", value1, value2, "outlineer");
            return (Criteria) this;
        }

        public Criteria andOutlineerNotBetween(String value1, String value2) {
            addCriterion("OUTLINEER not between", value1, value2, "outlineer");
            return (Criteria) this;
        }

        public Criteria andOutlinetimeIsNull() {
            addCriterion("OUTLINETIME is null");
            return (Criteria) this;
        }

        public Criteria andOutlinetimeIsNotNull() {
            addCriterion("OUTLINETIME is not null");
            return (Criteria) this;
        }

        public Criteria andOutlinetimeEqualTo(String value) {
            addCriterion("OUTLINETIME =", value, "outlinetime");
            return (Criteria) this;
        }

        public Criteria andOutlinetimeNotEqualTo(String value) {
            addCriterion("OUTLINETIME <>", value, "outlinetime");
            return (Criteria) this;
        }

        public Criteria andOutlinetimeGreaterThan(String value) {
            addCriterion("OUTLINETIME >", value, "outlinetime");
            return (Criteria) this;
        }

        public Criteria andOutlinetimeGreaterThanOrEqualTo(String value) {
            addCriterion("OUTLINETIME >=", value, "outlinetime");
            return (Criteria) this;
        }

        public Criteria andOutlinetimeLessThan(String value) {
            addCriterion("OUTLINETIME <", value, "outlinetime");
            return (Criteria) this;
        }

        public Criteria andOutlinetimeLessThanOrEqualTo(String value) {
            addCriterion("OUTLINETIME <=", value, "outlinetime");
            return (Criteria) this;
        }

        public Criteria andOutlinetimeLike(String value) {
            addCriterion("OUTLINETIME like", value, "outlinetime");
            return (Criteria) this;
        }

        public Criteria andOutlinetimeNotLike(String value) {
            addCriterion("OUTLINETIME not like", value, "outlinetime");
            return (Criteria) this;
        }

        public Criteria andOutlinetimeIn(List<String> values) {
            addCriterion("OUTLINETIME in", values, "outlinetime");
            return (Criteria) this;
        }

        public Criteria andOutlinetimeNotIn(List<String> values) {
            addCriterion("OUTLINETIME not in", values, "outlinetime");
            return (Criteria) this;
        }

        public Criteria andOutlinetimeBetween(String value1, String value2) {
            addCriterion("OUTLINETIME between", value1, value2, "outlinetime");
            return (Criteria) this;
        }

        public Criteria andOutlinetimeNotBetween(String value1, String value2) {
            addCriterion("OUTLINETIME not between", value1, value2, "outlinetime");
            return (Criteria) this;
        }

        public Criteria andDebugerIsNull() {
            addCriterion("DEBUGER is null");
            return (Criteria) this;
        }

        public Criteria andDebugerIsNotNull() {
            addCriterion("DEBUGER is not null");
            return (Criteria) this;
        }

        public Criteria andDebugerEqualTo(String value) {
            addCriterion("DEBUGER =", value, "debuger");
            return (Criteria) this;
        }

        public Criteria andDebugerNotEqualTo(String value) {
            addCriterion("DEBUGER <>", value, "debuger");
            return (Criteria) this;
        }

        public Criteria andDebugerGreaterThan(String value) {
            addCriterion("DEBUGER >", value, "debuger");
            return (Criteria) this;
        }

        public Criteria andDebugerGreaterThanOrEqualTo(String value) {
            addCriterion("DEBUGER >=", value, "debuger");
            return (Criteria) this;
        }

        public Criteria andDebugerLessThan(String value) {
            addCriterion("DEBUGER <", value, "debuger");
            return (Criteria) this;
        }

        public Criteria andDebugerLessThanOrEqualTo(String value) {
            addCriterion("DEBUGER <=", value, "debuger");
            return (Criteria) this;
        }

        public Criteria andDebugerLike(String value) {
            addCriterion("DEBUGER like", value, "debuger");
            return (Criteria) this;
        }

        public Criteria andDebugerNotLike(String value) {
            addCriterion("DEBUGER not like", value, "debuger");
            return (Criteria) this;
        }

        public Criteria andDebugerIn(List<String> values) {
            addCriterion("DEBUGER in", values, "debuger");
            return (Criteria) this;
        }

        public Criteria andDebugerNotIn(List<String> values) {
            addCriterion("DEBUGER not in", values, "debuger");
            return (Criteria) this;
        }

        public Criteria andDebugerBetween(String value1, String value2) {
            addCriterion("DEBUGER between", value1, value2, "debuger");
            return (Criteria) this;
        }

        public Criteria andDebugerNotBetween(String value1, String value2) {
            addCriterion("DEBUGER not between", value1, value2, "debuger");
            return (Criteria) this;
        }

        public Criteria andDebugtimeIsNull() {
            addCriterion("DEBUGTIME is null");
            return (Criteria) this;
        }

        public Criteria andDebugtimeIsNotNull() {
            addCriterion("DEBUGTIME is not null");
            return (Criteria) this;
        }

        public Criteria andDebugtimeEqualTo(String value) {
            addCriterion("DEBUGTIME =", value, "debugtime");
            return (Criteria) this;
        }

        public Criteria andDebugtimeNotEqualTo(String value) {
            addCriterion("DEBUGTIME <>", value, "debugtime");
            return (Criteria) this;
        }

        public Criteria andDebugtimeGreaterThan(String value) {
            addCriterion("DEBUGTIME >", value, "debugtime");
            return (Criteria) this;
        }

        public Criteria andDebugtimeGreaterThanOrEqualTo(String value) {
            addCriterion("DEBUGTIME >=", value, "debugtime");
            return (Criteria) this;
        }

        public Criteria andDebugtimeLessThan(String value) {
            addCriterion("DEBUGTIME <", value, "debugtime");
            return (Criteria) this;
        }

        public Criteria andDebugtimeLessThanOrEqualTo(String value) {
            addCriterion("DEBUGTIME <=", value, "debugtime");
            return (Criteria) this;
        }

        public Criteria andDebugtimeLike(String value) {
            addCriterion("DEBUGTIME like", value, "debugtime");
            return (Criteria) this;
        }

        public Criteria andDebugtimeNotLike(String value) {
            addCriterion("DEBUGTIME not like", value, "debugtime");
            return (Criteria) this;
        }

        public Criteria andDebugtimeIn(List<String> values) {
            addCriterion("DEBUGTIME in", values, "debugtime");
            return (Criteria) this;
        }

        public Criteria andDebugtimeNotIn(List<String> values) {
            addCriterion("DEBUGTIME not in", values, "debugtime");
            return (Criteria) this;
        }

        public Criteria andDebugtimeBetween(String value1, String value2) {
            addCriterion("DEBUGTIME between", value1, value2, "debugtime");
            return (Criteria) this;
        }

        public Criteria andDebugtimeNotBetween(String value1, String value2) {
            addCriterion("DEBUGTIME not between", value1, value2, "debugtime");
            return (Criteria) this;
        }

        public Criteria andAutobindtimeIsNull() {
            addCriterion("AUTOBINDTIME is null");
            return (Criteria) this;
        }

        public Criteria andAutobindtimeIsNotNull() {
            addCriterion("AUTOBINDTIME is not null");
            return (Criteria) this;
        }

        public Criteria andAutobindtimeEqualTo(String value) {
            addCriterion("AUTOBINDTIME =", value, "autobindtime");
            return (Criteria) this;
        }

        public Criteria andAutobindtimeNotEqualTo(String value) {
            addCriterion("AUTOBINDTIME <>", value, "autobindtime");
            return (Criteria) this;
        }

        public Criteria andAutobindtimeGreaterThan(String value) {
            addCriterion("AUTOBINDTIME >", value, "autobindtime");
            return (Criteria) this;
        }

        public Criteria andAutobindtimeGreaterThanOrEqualTo(String value) {
            addCriterion("AUTOBINDTIME >=", value, "autobindtime");
            return (Criteria) this;
        }

        public Criteria andAutobindtimeLessThan(String value) {
            addCriterion("AUTOBINDTIME <", value, "autobindtime");
            return (Criteria) this;
        }

        public Criteria andAutobindtimeLessThanOrEqualTo(String value) {
            addCriterion("AUTOBINDTIME <=", value, "autobindtime");
            return (Criteria) this;
        }

        public Criteria andAutobindtimeLike(String value) {
            addCriterion("AUTOBINDTIME like", value, "autobindtime");
            return (Criteria) this;
        }

        public Criteria andAutobindtimeNotLike(String value) {
            addCriterion("AUTOBINDTIME not like", value, "autobindtime");
            return (Criteria) this;
        }

        public Criteria andAutobindtimeIn(List<String> values) {
            addCriterion("AUTOBINDTIME in", values, "autobindtime");
            return (Criteria) this;
        }

        public Criteria andAutobindtimeNotIn(List<String> values) {
            addCriterion("AUTOBINDTIME not in", values, "autobindtime");
            return (Criteria) this;
        }

        public Criteria andAutobindtimeBetween(String value1, String value2) {
            addCriterion("AUTOBINDTIME between", value1, value2, "autobindtime");
            return (Criteria) this;
        }

        public Criteria andAutobindtimeNotBetween(String value1, String value2) {
            addCriterion("AUTOBINDTIME not between", value1, value2, "autobindtime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }

        public Criteria andLogicalDeleted(boolean deleted) {
            return deleted ? andDrEqualTo(CardocPojo.Dr.IS_DELETED.value()) : andDrNotEqualTo(CardocPojo.Dr.IS_DELETED.value());
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}