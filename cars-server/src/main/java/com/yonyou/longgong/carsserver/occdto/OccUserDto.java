package com.yonyou.longgong.carsserver.occdto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-12-06
 * @描述
 **/
@Data
public class OccUserDto {
      private String id;
      private String dr;
      private String ts;
      private String creator;
      private String creationTime;
      private String modifier;
      private String modifiedTime;
      private String persistStatus;
      private String promptMessage;
      private String loginName;
      private String name;
      private String roles;
      private String states;
      private String registerDate;
      private String avator;
      private String tenantId;
      private String type;
      private String label;
      private String phone;
      private String email;
      private String img;
      private String isLock;
      private String remark;
      private String organizationId;
      private String organizationName;
      private String groupId;
      private String groupName;
      private String channelCustomerId;
      private String channelCustomerCode;
      private String channelCustomerName;
      private String businessPersonId;
      private String businessPersonCode;
      private String businessPersonName;
      private String dtype;    
}
