package com.yonyou.longgong.carsserver.occdto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-11-20
 * @描述
 **/
@Data
public class OccReferDto {
    /**
     * 供应商编码
     */
    private String code;
    /**
     * 创建时间
     */
    private String creationTime;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 删除标识
     */
    private Integer dr;
    /**
     * 主键
     */
    private String id;
    /**
     *
     */
    private Integer isEnable;
    /**
     * 修改时间
     */
    private String modifiedTime;
    /**
     * 修改人
     */
    private String modifier;
    /**
     * 供应商名称
     */
    private String name;
    /**
     * 持久化状态
     */
    private String persistStatus;
    /**
     * 时间戳
     */
    private String ts;
}
