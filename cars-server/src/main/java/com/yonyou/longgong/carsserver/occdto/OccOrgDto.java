package com.yonyou.longgong.carsserver.occdto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-12-05
 * @描述 渠道云的组织对象
 **/
@Data
public class OccOrgDto {

    private String cityCode;
    private String cityId;
    private String cityName;
    private String code;
    private String countryCode;
    private String countryId;
    private String countryName;
    private String countyCode;
    private String countyId;
    private String countyName;
    private String creationTime;
    private String creator;
    private String customerCode;
    private String customerId;
    private String customerName;
    private String detailAddr;
    private String dr;
    private String ext01;
    private String ext02;
    private String ext03;
    private String ext04;
    private String ext05;
    private String ext06;
    private String ext07;
    private String ext08;
    private String ext09;
    private String ext10;
    private String id;
    private String isAdministration;
    private String isEnable;
    private String isFinance;
    private String isInventory;
    private String isLeaf;
    private String isLegalPersonCorp;
    private String isLogistrics;
    private String isPurchase;
    private String isSale;
    private String isService;
    private String modifiedTime;
    private String modifier;
    private String name;
    private String nodeLevel;
    private String orgFuncRels;
    private String organizationTypeCode;
    private String organizationTypeId;
    private String organizationTypeName;
    private String parentCode;
    private String parentId;
    private String parentName;
    private String persistStatus;
    private String postalCode;
    private String principalCode;
    private String principalId;
    private String principalName;
    private String promptMessage;
    private String provinceCode;
    private String provinceId;
    private String provinceName;
    private String remark;
    private String srcSystem;
    private String srcSystemCode;
    private String srcSystemId;
    private String srcSystemName;
    private String tel;
    private String townCode;
    private String townId;
    private String townName;
    private String treeCode;
    private String ts;
}
