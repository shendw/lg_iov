package com.yonyou.longgong.carsserver.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author shendawei
 * @创建时间 2019/12/27
 * @描述
 **/
public class StringUtil {
    private static Pattern NUMBER_PATTERN = Pattern.compile("[`~☆★!@#$%^&*()+=|{}':;,\\[\\]》·.<>/?~！@#￥%……（）——+|{}【】‘；：”“’。，、？]");


    public static String formatCarno(String carno){
        if (StringUtils.isEmpty(carno)){
            return null;
        }
        Matcher m = NUMBER_PATTERN.matcher(carno);
        carno = m.replaceAll("").trim().replace(" ", "").replace("\\", "");
        //去除姓名中的空格
        return ">"+carno+"<";
    }
}
