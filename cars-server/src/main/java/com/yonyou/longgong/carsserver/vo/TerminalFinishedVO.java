package com.yonyou.longgong.carsserver.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2020/4/22
 * @描述
 **/
@Data
public class TerminalFinishedVO {
    private String pkTerminal;

    @Excel(name = "sim卡号",needMerge = true)
    private String simid;

    @Excel(name = "终端版本",needMerge = true)
    private String terminalVersion;

    @Excel(name = "流程状态",needMerge = true)
    private Long status;

    @Excel(name = "终端id",needMerge = true)
    private String terminalid;

    @Excel(name = "生产日期",needMerge = true)
    private String productiontime;

    @Excel(name = "成品日期",needMerge = true)
    private String finishedtime;

    @Excel(name = "最后通讯时间",needMerge = true)
    private String lastTime;

    @Excel(name = "最后登陆时间",needMerge = true)
    private String lastloginTime;

    @Excel(name = "通讯通道",needMerge = true)
    private String channel;

    @Excel(name = "供应商编码",needMerge = true)
    private String suppilercode;

    @Excel(name = "供应商名称",needMerge = true)
    private String suppilername;

    private String isnew;

    private String carno;

    @Excel(name = "自动绑定时间")
    private String autobindtime;

    private String outlineer;

    @Excel(name = "下线时间")
    private String outlinetime;

    private String debuger;

    @Excel(name = "调试时间")
    private String debugtime;
}
