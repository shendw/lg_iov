package com.yonyou.longgong.carsserver.service;

import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dto.SimSelectDto;
import com.yonyou.longgong.carsserver.excel.ExcelVerifySimPojoOfMode;
import com.yonyou.longgong.carsserver.pojo.SimPojo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-11-04
 * @描述
 **/
public interface ISimService {
    Integer insertSim(List<SimPojo> simPojos);
    Integer insertSimByExcel(List<SimPojo> simPojos);
    Integer delSim(List<SimPojo> simPojos);
    Integer updateSim(SimPojo simPojo);
    /**
     *@描述 更新sim卡与终端的绑定关系的接口
     *@参数 sim卡号，终端id
     *@返回值
     *@创建人 sdw
     *@创建时间 2019/12/25
     *@修改人和其它信息
     */
    Integer updateSim(String simid,String terminalid);
    PageInfo<SimPojo> selectSim(SimSelectDto simSelectDto);
    List<SimPojo> selectSimByExcel(SimSelectDto simSelectDto);
    ExcelImportResult<ExcelVerifySimPojoOfMode> checkExcel(MultipartFile simExcel);
    long countSim(String simid,String serial);
    /**
     *@描述 iot调用匹配信息接口
     *@参数 sim卡信息
     *@返回值 终端id
     *@创建人 sdw
     *@创建时间 2019-11-06
     *@修改人和其它信息
     */
    SimPojo matchSim(String imsi);

    /**
     * 绑定sim卡和终端
     * @param imsi
     * @param originTerminalid
     * @param terminalVersion
     * @param simPojo
     * @return
     */
    String bindSim(String imsi,String originTerminalid,String terminalVersion,SimPojo simPojo);
}
