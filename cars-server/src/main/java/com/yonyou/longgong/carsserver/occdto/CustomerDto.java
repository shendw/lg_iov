package com.yonyou.longgong.carsserver.occdto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 客户的数据传输对象类
 *
 * @author xugm
 * @date 2018-4-13
 */
@Data
public class CustomerDto {
	
	
	
	
	

    private String customerSourceId;
    private String customerSourceCode;

    private String customerSourceName;


    private String cusReqFormId;

    private String cusReqFormCode;


    private String code;


    private String name;


    private String abbr;


    private String remark;


    private String businessScope;


    private String marketAreaId;


    private String marketAreaCode;


    private String marketAreaName;


    private String customerCategoryId;

    private String customerCategoryCode;


    private String customerCategoryName;


    private String channelTypeId;

    private String channelTypeCode;

    private String channelTypeName;

    private String customerLevelId;

    private String customerLevelCode;

    private String customerLevelName;

    private String customerTypeId;

    private String customerTypeCode;

    private String customerTypeName;

    private String organizationId;

    private String organizationCode;

    private String organizationName;

    private Integer isChannelCustomer;

    private String credentialsTypeCode;

    private String credentialsTypeName;

    private String credentialsNo;

    private String legalPerson;

    private String taxRegNo;

    private String countryId;

    private String countryCode;

    private String countryName;

    private String provinceId;

    private String provinceName;

    private String provinceCode;

    private String cityId;

    private String cityCode;

    private String cityName;


    private String countyId;

    private String countyName;

    private String countyCode;

    private String townId;

    private String townName;

    private String townCode;

    private String detailAddr;

    private BigDecimal longitude;

    private BigDecimal latitude;

    private String postalCode;

    private Integer isFrozen;

    private Integer isEnable;

    private String payerCustomerId;

    private String payerCustomerCode;

    private String payerCustomerName;

    private String superiorCustomerId;

    private String superiorCustomerName;

    private String customerRankId;
    private String customerRankCode;
    private String customerRankName;
//    List<CustomerContactDto> customerContacts = new ArrayList<>();
//    List<CustomerAddressDto> customerAddresses = new ArrayList<>();
//    List<CustomerAccountDto> customerAccounts = new ArrayList<>();
    private Integer isServiceProvider;
    private String srcSystem;
    private String srcSystemId;
    private String srcSystemCode;
    private String srcSystemName;
    private String ownerOrganizationId;
    private String ownerOrganizationCode;
    private String ownerOrganizationName;
    private String ext01;
    private String ext02;
    private String ext03;
    private String ext04;
    private String ext05;
    private String ext06;
    private String ext07;
    private String ext08;
    private String ext09;
    private String ext10;
}
