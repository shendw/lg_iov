package com.yonyou.longgong.carsserver.dao;

import com.yonyou.longgong.carsserver.example.OplogPojoExample;
import com.yonyou.longgong.carsserver.pojo.OplogPojo;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OplogPojoMapper {
    long countByExample(OplogPojoExample example);

    int deleteWithVersionByExample(@Param("version") Long version, @Param("example") OplogPojoExample example);

    int deleteByExample(OplogPojoExample example);

    int insert(OplogPojo record);

    int insertSelective(@Param("record") OplogPojo record, @Param("selective") OplogPojo.Column ... selective);

    List<OplogPojo> selectByExample(OplogPojoExample example);

    int updateWithVersionByExample(@Param("version") Long version, @Param("record") OplogPojo record, @Param("example") OplogPojoExample example);

    int updateWithVersionByExampleSelective(@Param("version") Long version, @Param("record") OplogPojo record, @Param("example") OplogPojoExample example, @Param("selective") OplogPojo.Column ... selective);

    int updateByExampleSelective(@Param("record") OplogPojo record, @Param("example") OplogPojoExample example, @Param("selective") OplogPojo.Column ... selective);

    int updateByExample(@Param("record") OplogPojo record, @Param("example") OplogPojoExample example);

    int logicalDeleteByExample(@Param("example") OplogPojoExample example);

    int logicalDeleteWithVersionByExample(@Param("version") Long version, @Param("nextVersion") Long nextVersion, @Param("example") OplogPojoExample example);
}