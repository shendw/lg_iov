package com.yonyou.longgong.carsserver.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author shendawei
 * @创建时间 2020/3/31
 * @描述
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrgDto {
    private String name;
    private String code;
    private String id;
}
