package com.yonyou.longgong.carsserver.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dao.OplogPojoMapper;
import com.yonyou.longgong.carsserver.dto.OplogSelectDto;
import com.yonyou.longgong.carsserver.example.OplogPojoExample;
import com.yonyou.longgong.carsserver.exception.BusinessException;
import com.yonyou.longgong.carsserver.pojo.OplogPojo;
import com.yonyou.longgong.carsserver.service.IOplogService;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.UUIDUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Comparator;
import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-11-23
 * @描述
 **/
@Service("iOplogService")
@Deprecated
public class OplogServiceImpl implements IOplogService {

    @Autowired
    private OplogPojoMapper oplogPojoMapper;

    @Override
    public Integer insertOplog(OplogPojo oplogPojo) {
        if (StringUtils.isBlank(oplogPojo.getPkOplog())){
            oplogPojo.setPkOplog(UUIDUtils.getUniqueIdByUUId());
        }
        return oplogPojoMapper.insertSelective(oplogPojo);
    }

    @Override
    public List<OplogPojo> selectOplog(OplogSelectDto oplogSelectDto) {
        OplogPojoExample oplogPojoExample = selectRule(oplogSelectDto);
        List<OplogPojo> oplogPojos = oplogPojoMapper.selectByExample(oplogPojoExample);
        if (CollectionUtils.isEmpty(oplogPojos)){
            oplogPojos.sort(Comparator.comparing(OplogPojo::getOptime).reversed());
        }
        return oplogPojos;
    }

    @Override
    public PageInfo<OplogPojo> selectOplogByPage(OplogSelectDto oplogSelectDto) {
        OplogPojoExample oplogPojoExample = selectRule(oplogSelectDto);
        PageHelper.startPage(oplogSelectDto.getPageNum(),oplogSelectDto.getPageSize());
        List<OplogPojo> oplogPojos = oplogPojoMapper.selectByExample(oplogPojoExample);
        if (CollectionUtils.isEmpty(oplogPojos)){
            oplogPojos.sort(Comparator.comparing(OplogPojo::getOptime).reversed());
        }
        PageInfo<OplogPojo> oplogPojoPageInfo = new PageInfo<>(oplogPojos);
        return oplogPojoPageInfo;
    }

    private OplogPojoExample selectRule(OplogSelectDto oplogSelectDto){
        if (StringUtils.isBlank(oplogSelectDto.getTerminalid())){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        OplogPojoExample oplogPojoExample = new OplogPojoExample();
        OplogPojoExample.Criteria criteria = oplogPojoExample.createCriteria();
        criteria.andLogicalDeleted(false).andTerminalidEqualTo(oplogSelectDto.getTerminalid());
        if (StringUtils.isNotBlank(oplogSelectDto.getChannel())){
            criteria.andChannelEqualTo(oplogSelectDto.getChannel());
        }
        if (StringUtils.isNotBlank(oplogSelectDto.getStarttime())){
            criteria.andOptimeGreaterThanOrEqualTo(oplogSelectDto.getStarttime());
        }
        if (StringUtils.isNotBlank(oplogSelectDto.getEndtime())){
            criteria.andOptimeLessThanOrEqualTo(oplogSelectDto.getEndtime());
        }
        return oplogPojoExample;
    }
}
