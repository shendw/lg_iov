package com.yonyou.longgong.carsserver.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dao.BindErrorInfoPojoMapper;
import com.yonyou.longgong.carsserver.dto.BindErrorSelectDto;
import com.yonyou.longgong.carsserver.example.BindErrorInfoPojoExample;
import com.yonyou.longgong.carsserver.pojo.BindErrorInfoPojo;
import com.yonyou.longgong.carsserver.service.IBindErrorService;
import com.yonyou.longgong.carsserver.utils.DataTimeUtil;
import com.yonyou.longgong.carsserver.utils.UUIDUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

/**
 * @author shendawei
 * @创建时间 2020/4/20
 * @描述
 **/
@Service("iBindErrorService")
public class BindErrorServiceImpl implements IBindErrorService {

    @Autowired
    private BindErrorInfoPojoMapper bindErrorInfoPojoMapper;

    @Override
    public PageInfo<BindErrorInfoPojo> selectBindError(BindErrorSelectDto bindErrorSelectDto) {
        BindErrorInfoPojoExample bindErrorInfoPojoExample = new BindErrorInfoPojoExample();
        BindErrorInfoPojoExample.Criteria criteria = bindErrorInfoPojoExample.createCriteria();
        criteria.andLogicalDeleted(false);
        if (StringUtils.isNotEmpty(bindErrorSelectDto.getCarno())){
            criteria.andCarnoLike("%"+bindErrorSelectDto.getCarno()+"%");
        }
        if (StringUtils.isNotEmpty(bindErrorSelectDto.getTerminalid())){
            criteria.andTerminalidLike("%"+bindErrorSelectDto.getTerminalid()+"%");
        }
        bindErrorInfoPojoExample.setOrderByClause(" CREATETIME desc ");
        PageHelper.startPage(bindErrorSelectDto.getPageNum(),bindErrorSelectDto.getPageSize());
        List<BindErrorInfoPojo> bindErrorInfoPojos = bindErrorInfoPojoMapper.selectByExample(bindErrorInfoPojoExample);
        PageInfo<BindErrorInfoPojo> bindErrorInfoPojoPageInfo = new PageInfo<>(bindErrorInfoPojos);
        return bindErrorInfoPojoPageInfo;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Integer insertBindErrorInfo(String carno,String terminalid,String errorInfo) {
        BindErrorInfoPojoExample bindErrorInfoPojoExample = new BindErrorInfoPojoExample();
        bindErrorInfoPojoExample.createCriteria()
                .andLogicalDeleted(false)
                .andCarnoEqualTo(carno)
                .andTerminalidEqualTo(terminalid);
        List<BindErrorInfoPojo> bindErrorInfoPojos = bindErrorInfoPojoMapper.selectByExample(bindErrorInfoPojoExample);
        if (CollectionUtils.isEmpty(bindErrorInfoPojos)){
            BindErrorInfoPojo bindErrorInfoPojo = new BindErrorInfoPojo();
            bindErrorInfoPojo.setCarno(carno);
            bindErrorInfoPojo.setTerminalid(terminalid);
            bindErrorInfoPojo.setErrorinfo(errorInfo);
            bindErrorInfoPojo.setPkBinderror(UUIDUtils.getUniqueIdByUUId());
            bindErrorInfoPojo.setCreatetime(DataTimeUtil.dateToStr(new Date()));
            return bindErrorInfoPojoMapper.insertSelective(bindErrorInfoPojo);
        }
        return null;
    }
}
