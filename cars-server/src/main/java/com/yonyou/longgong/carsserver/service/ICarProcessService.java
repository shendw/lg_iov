package com.yonyou.longgong.carsserver.service;

import com.yonyou.longgong.carsserver.dto.CardocSelectDto;
import com.yonyou.longgong.carsserver.utils.ResponseData;

/**
 * @author shendawei
 * @创建时间 2019-12-18
 * @描述
 **/
public interface ICarProcessService {
    /**
     * 车辆下线
     * @param carno
     * @return
     */
    Integer carOutline(String carno,String loginName);

    /**
     * 车辆调试
     * @param carno
     * @return
     */
    Integer carTest(String carno,String loginName);

    /**
     * 《车辆流程管理-下线》查询用
     * @param cardocSelectDto
     * @param userId
     * @param loginName
     * @return
     */
    ResponseData selectCardocOutline(CardocSelectDto cardocSelectDto, String userId, String loginName);

    /**
     * 《车辆流程管理-调试》查询用
     * @param cardocSelectDto
     * @param userId
     * @param loginName
     * @return
     */
    ResponseData selectCardocDebug(CardocSelectDto cardocSelectDto, String userId, String loginName);
}
