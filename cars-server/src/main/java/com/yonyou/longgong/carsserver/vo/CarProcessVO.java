package com.yonyou.longgong.carsserver.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2020/4/16
 * @描述
 **/
@Data
public class CarProcessVO {
    private String pkCardoc;

    @Excel(name = "车型型号",needMerge = true)
    private String carmodel;

    @Excel(name = "机型",needMerge = true)
    private String machinemodel;

    @Excel(name = "整车编号",needMerge = true)
    private String carno;

    @Excel(name = "流程状态",needMerge = true)
    private Long status;

    @Excel(name = "入库日期",needMerge = true)
    private String instocktime;


    @Excel(name = "工作时长",needMerge = true)
    private String worktime;

    @Excel(name = "位置信息",needMerge = true)
    private String location;

    private String creationtime;

    private String creator;

    private String modifiedtime;

    private String modifier;

    private Long dr;

    private Long version;

    private String pkOrg;

    @Excel(name = "备注",needMerge = true)
    private String remark;

    private String workstatus;

    private String lockstatus;

    private String outlineer;

    private String outlinetime;

    private String debuger;

    private String debugtime;

    private String autobindtime;

    @Excel(name = "终端id",needMerge = true)
    private String terminalid;

    @Excel(name = "sim卡号",needMerge = true)
    private String simid;

    @Excel(name = "最后通讯时间",needMerge = true)
    private String lastTime;

}
