package com.yonyou.longgong.carsserver.utils;

import lombok.Getter;

/**
 * @author shendawei
 * @创建时间 2019-11-04
 * @描述
 **/
@Getter
public enum ResponseCode {
    SUCCESS("20000", "执行成功！"),
    SUCCESS_MATCH("20000", "匹配成功！"),
    SUCCESS_BIND("20000", "绑定成功！"),
    SUCCESS_INSERT("20000", "新增成功！"),
    SUCCESS_UPDATE("20000", "更新成功！"),
    SUCCESS_DELETE("20000", "删除成功！"),

    ERROR_NODATA("40000", "未获取到数据！"),
    PERSONID_IS_NULL("40001", "该用户未维护人员信息！"),
    USERID_IS_NULL("40002", "未获取到当前登陆人信息，请重新登陆后重试！"),
    USERINFO_IS_NULL("40003", "未获取到当前登陆人信息，请重新登陆后重试！"),
    IDENTITY_IS_NULL("40004", "无身份信息，请联系管理员设置身份！"),
    ROLE_IS_NOT_IOV("40005", "非车联网人员，请联系管理员维护！"),
    ROLE_IS_NOT_CARPROCESS("40006", "非车联网人员，请联系管理员维护！"),

    ERROR("50000", "执行失败！"),
    ERROR_UPDATE("50000", "更新失败，请稍后再试！"),
    ERROR_LOCKED("50000", "该终端已存在下发的指令，请稍后重试！"),
    ERROR_PARAM_IS_NULL("50000", "缺失参数！"),
    ERROR_TIMEFORMAT("50000", "时间格式异常：yyyy-MM-dd HH:mm:ss"),
    ERROR_TIMEOUTRANGE("50000", "历史记录查询时间区间请保持在30天内。"),

    //业务类异常code
    SIMID_IS_EXIST("60001","已存在SIM卡号！"),
    SIMSERIAL_IS_NULL("60002","SIM卡串口号为空"),
    SIMEXCEL_IS_NULL("60003","excel文件为空"),
    SIMEXCEL_IS_ILLEGAL("60004","excel文件格式错误"),
    SIMEXCEL_IS_ERROR("60005","excel解析错误"),
    SIMMATCH_IS_ERROR("60006","sim档案匹配失败"),
    SIMMATCH_IS_EXIST("60007","该sim号已存在终端id"),
    SIMSERIAL_IS_EXIST("60007","该sim卡串号已存在"),

    CARNO_IS_EXIST("61000","已存在该整车编号"),
    CARNO_IS_NULL("61001","该整车编号不存在"),
    CARDOC_IS_NULL("61002","未查询到车辆档案"),
    CARDOC_INSERT_ERROR("61003","车辆档案新增错误"),
    CARDOC_U9DATA_IS_NULL("61004","U9视图中无该车辆信息，请核对后操作"),
    CARDOC_U9DATA_IS_MISS("61005","U9视图中缺失部分车辆信息，请核对后操作"),
    CARNO_IS_NOTFORMAT("61006","整机编号格式不正确"),
    CARNO_IS_NOTHAVEORG("61006","无法通过整机编号获取组织信息"),
    CARDOCSTATUS_ERROR("61007","车辆流程状态错误"),
    CARDOCSTATUS_IOVMANAGER("61008","你好，车联网管理员！"),
    CARDOCSTATUS_IOVPRODUCTION("61009","你好，车联网生产人员！"),
    CARDOCSTATUS_IOVQUALITY("61010","你好，车联网质检人员！"),

    TERMINALID_IS_EXIST("62000","已存在该那终端id"),
    TERMINALID_IS_NULL("62001","该终端id不存在"),
    TERMINALSTATUS_ERROR("62002","终端流程状态错误"),
    TERMINAL_CAR_TERMINAL_IS_BIND("63000","该终端存在已启用的绑定车辆，请解绑后重试"),
    TERMINAL_CAR_CARNO_IS_BIND("63001","该车辆存在已启用的绑定终端，请解绑后重试"),
    TERMINAL_CAR_CARNO_IS_NULL("63001","该车辆不存在已启用的绑定终端，请解绑后重试"),
    TERMINAL_CAR_IS_TOOMANY("63003","终端和车辆绑定关系异常"),
    TERMINAL_CAR_IS_NONE("63004","终端和车辆绑定关系异常"),

    TERMINAL_CAR_CHANGE_CAR("63010","终端换绑异常，已绑定入库或已售的车辆。"),
    TERMINAL_CAR_CHANGE_TERMINAL("63011","车辆换绑异常，已绑定启用终端。"),

    SUPPILERDOC_IS_EXIST("64000","该供应商档案已存在"),
    SUPPILERDOC_CODE_IS_EXIST("64001","该供应商编码已存在"),
    SUPPILERDOC_KEYWORD_IS_NULL("64002","未在系统中找到该供应商档案信息，请维护"),

    //7**** iot
    IOV_SITE_IS_NULL("70001","获取终端站点失败"),
    IOV_ALREADY_BIND("20000","该终端已经绑定当前车辆")
    ;


    private final String code;
    private final String desc;


    ResponseCode(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
