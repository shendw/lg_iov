package com.yonyou.longgong.carsserver.client;

import com.yonyou.longgong.carsserver.occdto.FeignPage;
import com.yonyou.longgong.carsserver.occdto.OccUserDto;
import com.yonyou.longgong.carsserver.occdto.OccUserIdentityDto;
import com.yonyou.longgong.carsserver.occdto.UserPrivilegeDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-12-06
 * @描述
 **/
@FeignClient(name = "occ-cmpt")
public interface OccCmptClient {

    /**
     *@描述   获取用户身份
     *@参数 %userlogin%
     *@返回值  用户身份信息
     *@创建人 sdw
     *@创建时间 2019-12-06
     *@修改人和其它信息
     */
    @GetMapping("/users")
    FeignPage<OccUserIdentityDto> getUserIdentityByUsername(@RequestParam String search_LIKE_loginName,
                                                            @RequestParam(value = "10") String size,
                                                            @RequestParam(value = "0") String page,
                                                            @RequestParam(value = "user-identity") String search_AUTH_APPCODE);

    /**
     *@描述 获取用户身份
     *@参数 用户id
     *@返回值
     *@创建人 sdw
     *@创建时间 2019-12-07
     *@修改人和其它信息
     */
    @GetMapping("/api/cmpt/privileges/get-user-by-login-name/{loginName}")
    OccUserDto getUserInfoByloginName(@RequestParam @PathVariable(name="loginName")String loginName);

    /**
     * 获取用户的权限集合。
     *
     * @param userId 用户主键。
     * @return 用户的权限集合。
     */
    @GetMapping("/api/cmpt/privileges/get-user-privilege")
    List<UserPrivilegeDto> getUserPrivilegesByUserId(@RequestParam("userId") String userId);
}
