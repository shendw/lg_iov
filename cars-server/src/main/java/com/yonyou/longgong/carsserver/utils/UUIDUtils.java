package com.yonyou.longgong.carsserver.utils;

import org.apache.logging.log4j.util.StringBuilderFormattable;

import java.util.Random;
import java.util.UUID;

/**
 * @author  shendawei
 * @创建时间 2019-08-29
 * @描述
 **/
public class UUIDUtils {
    private static final int SHORT_LENGTH = 8;

    public static String uuid() {
        String str = UUID.randomUUID().toString();
        String temp = str.replace("-","");
        return temp;
    }

    public static String getUniqueIdByUUId() {
        //最大支持1-9个集群机器部署
        int machineId = 1;
        int hashCodeV = UUID.randomUUID().toString().hashCode();
        if(hashCodeV < 0) {
            hashCodeV = - hashCodeV;
        }
        // 0 代表前面补充0
        // 19 代表长度为19
        // d 代表参数为正数型
        return machineId + String.format("%019d", hashCodeV);
    }



    public static String[] chars = new String[] { "a", "b", "c", "d", "e", "f",
            "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
            "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z" };


    public static String generateShortUuid() {
        StringBuffer shortBuffer = new StringBuffer();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        for (int i = 0; i < SHORT_LENGTH; i++) {
            String str = uuid.substring(i * 4, i * 4 + 4);
            int x = Integer.parseInt(str, 16);
            shortBuffer.append(chars[x % 0x3E]);
        }
        return shortBuffer.toString();

    }

    /**
     * 获取16进制随机数
     * @param len
     * @return
     * @throws
     */
    public static String randomHexString(int len)  {
        StringBuffer result = new StringBuffer();
        try {
            result.append(1);
            result.append(Math.random()>0.5?1:0);
            for(int i=0;i<len;i++) {
                result.append(Integer.toHexString(new Random().nextInt(16)));
            }
            StringBuffer temp = new StringBuffer();
            for(int i=0;i<3;i++) {
                int number = Math.random()>0.5?1:0;
                temp.append(number);
            }
            temp.append("10000");
            result.append(Integer.toHexString(Integer.valueOf(temp.toString(),2)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString().toUpperCase();
    }

    public static void main(String[] args) {
        String s = UUIDUtils.generateShortUuid();
        for(int i=0;i<10;i++) {
            String randomHexString = UUIDUtils.randomHexString(8);
            System.out.println(randomHexString);
        }
        System.out.println(s);
    }
}
