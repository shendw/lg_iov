package com.yonyou.longgong.carsserver.service;

import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dto.OplogSelectDto;
import com.yonyou.longgong.carsserver.pojo.OplogPojo;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-11-23
 * @描述
 **/
@Deprecated
public interface IOplogService {
    Integer insertOplog(OplogPojo oplogPojo);
    List<OplogPojo> selectOplog(OplogSelectDto oplogSelectDto);
    PageInfo<OplogPojo> selectOplogByPage(OplogSelectDto oplogSelectDto);
}
