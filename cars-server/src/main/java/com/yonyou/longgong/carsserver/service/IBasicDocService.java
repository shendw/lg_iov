package com.yonyou.longgong.carsserver.service;

import com.yonyou.longgong.carsserver.dto.OrgDto;
import com.yonyou.longgong.carsserver.occdto.OccOrgDto;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-12-05
 * @描述
 **/
public interface IBasicDocService {
    List<OccOrgDto> getOrgs();

    /**
     * 获取当前登陆人的组织参照
     * @param userId
     * @param loginName
     * @return
     */
    List<OrgDto> getOrgsByUser(String userId,String loginName);
}
