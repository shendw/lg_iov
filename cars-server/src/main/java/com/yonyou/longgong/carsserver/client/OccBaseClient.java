package com.yonyou.longgong.carsserver.client;

import com.yonyou.longgong.carsserver.occdto.*;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author shendawei
 * @创建时间 2019-11-18
 * @描述
 **/
@FeignClient(name = "occ-base")
public interface OccBaseClient {
    /**
     *@描述   获取供应商参照列表
     *@参数
     *@返回值 参照对象
     *@创建人 sdw
     *@创建时间 2019-11-21
     *@修改人和其它信息
     */
    @GetMapping("/api/base/suppliers/get-refer-list")
    OccReferDto[] getSupplierList();

    /**
     *@描述 获取用户参照的人员数据列表
     *@参数 查询条件的JSON字符串
     *@返回值 获取用户参照的人员数据列表
     *@创建人 sdw
     *@创建时间 2019-11-20
     *@修改人和其它信息
     */
    @GetMapping("/api/base/person/get-refer-list")
    OccReferDto[] getPersonRefer(String condition);

    /**
     *@描述 获取指定数据权限1001的人员主键
     *@参数 用户主键
     *@返回值 人员主键
     *@创建人 sdw
     *@创建时间 2019-11-20
     *@修改人和其它信息
     */
    @GetMapping("/api/base/person/data-rule-YZ1001")
    String[] getPersonId(String userId);

    /**
     *@描述
     *@参数 人员主键，person
     *@返回值 人员信息
     *@创建人 sdw
     *@创建时间 2019-11-20
     *@修改人和其它信息
     */
    @GetMapping("/base/person-posts/findByPersonId")
    OccPersonInfoDto[] getPersonInfo(@RequestParam String personId, @RequestParam String search_AUTH_APPCODE);

    /**
     *@描述 获取渠道云中维护的组织信息
     *@参数 查询参数
     *@返回值 组织信息列表
     *@创建人 sdw
     *@创建时间 2019-12-05
     *@修改人和其它信息
     */
    @GetMapping("/organizations")
    FeignPage<OccOrgDto> getOrgList(@RequestParam(value = "10000") String size,
                                         @RequestParam(value = "0") String page,
                                         @RequestParam(value = "organization") String search_AUTH_APPCODE);

    /**
     * 获取指定编码的客户DTO。
     *
     * @param code 客户DTO的编码。
     * @return 客户DTO。
     */
    @GetMapping("/api/base/customer/get-by-code")
    ResponseEntity<CustomerDto> getByCode(@RequestParam("code") String code);

}
