package com.yonyou.longgong.carsserver.enums;

import lombok.Getter;

/**
 * @author shendawei
 * @创建时间 2019-11-06
 * @描述
 **/
@Getter
public enum TerminalCarStatus {
    /**
     * 有效
     */
    DEFEAT(0L,"启用"),
    /**
     * 有效
     */
    EFFECTIVE(0L,"启用"),
    /**
     * 无效
     */
    INVALID(1L,"未启用"),
    ;

    private final Long code;
    private final String desc;

    TerminalCarStatus(Long code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
