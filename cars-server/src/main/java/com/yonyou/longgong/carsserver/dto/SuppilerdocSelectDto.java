package com.yonyou.longgong.carsserver.dto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-11-13
 * @描述
 **/
@Data
public class SuppilerdocSelectDto {

    private String code;

    private String name;

    private Integer pageSize = 10;

    private Integer pageNum = 1;
}
