package com.yonyou.longgong.carsserver.web;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yonyou.longgong.carsserver.excel.ExcelAgentInfoPojo;
import com.yonyou.longgong.carsserver.excel.ExcelVerifyCardocHandlerImpl;
import com.yonyou.longgong.carsserver.excel.ExcelVerifyCardocPojoOfMode;
import com.yonyou.longgong.carsserver.exception.BusinessException;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.StringUtil;
import com.yonyou.longgong.iotserver.utils.LocateByAmapUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author yushilin
 * @version 1.0
 * @description: 渠道云整机档案
 * @date 2021/5/7 10:12
 */
@RestController
@RequestMapping(value = "/channelCloudDoc")
@Slf4j
public class ChannelCloudDocController {

    @PostMapping(value = "/excelInsert")
    public String batchInsertCar(@RequestParam("carExcel") MultipartFile carExcel){
        try {
            log.info("begin carExcel insert!");
            if (carExcel == null) {
                throw new BusinessException(ResponseCode.SIMEXCEL_IS_NULL);
            }
            String fileName = carExcel.getOriginalFilename();
            if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
                // 文件格式不正确
                throw new BusinessException(ResponseCode.SIMEXCEL_IS_ILLEGAL);
            }
            ImportParams params = new ImportParams();
            //params.setTitleRows(1);
            params.setNeedVerify(true);
            //params.setVerifyHandler(new ExcelVerifyCardocHandlerImpl());
            params.setImportFields(new String[] { "代理商编码", "代理商名称","公司地址"});
            ExcelImportResult<ExcelAgentInfoPojo> importExcelMore = ExcelImportUtil.importExcelMore(carExcel.getInputStream(), ExcelAgentInfoPojo.class, params);
            List<ExcelAgentInfoPojo> list = importExcelMore.getList();
            for (ExcelAgentInfoPojo excelAgentInfoPojo : list) {
                if(!StringUtils.isEmpty(excelAgentInfoPojo.getAgentAddress()))
                excelAgentInfoPojo.setLocation(LocateByAmapUtils.getLocation(StringUtils.trimTrailingWhitespace(excelAgentInfoPojo.getAgentAddress())));
            }
            JSONArray jsonArray = new JSONArray();
            JSONArray jsonArray1 = new JSONArray();
            list.forEach(s ->{
                JSONObject colorJson = new JSONObject();
                colorJson.put("color","#F58158");
                JSONObject normalJson = new JSONObject();
                normalJson.put("normal",colorJson);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name",s.getAgentName());
                String[] split = s.getLocation().split(",");
                Double[] doubles = new Double[3];
                doubles[0] = Double.valueOf(split[0]);
                doubles[1] = Double.valueOf(split[1]);
                doubles[2] = 2d;
                jsonObject.put("value",doubles);
                jsonObject.put("symbolSize",2);
                jsonObject.put("itemStyle",normalJson);
                jsonArray.add(jsonObject);

                JSONObject moveLinesJson = new JSONObject();
                moveLinesJson.put("fromName","上海");
                moveLinesJson.put("toName",s.getAgentName());
                JSONArray arr = new JSONArray();
                Double[] doubles1 = new Double[2];
                doubles1[0] = 121.473701d;
                doubles1[1] = 31.230416d;
                Double[] doubles2 = new Double[2];
                doubles2[0] = Double.valueOf(split[0]);
                doubles2[1] = Double.valueOf(split[1]);
                arr.add(doubles1);
                arr.add(doubles2);
                moveLinesJson.put("coords",arr);
                jsonArray1.add(moveLinesJson);

            });


            JSONObject object = new JSONObject();
            object.put("city",jsonArray);
            object.put("moveLines",jsonArray1);

            return object.toJSONString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }



}
