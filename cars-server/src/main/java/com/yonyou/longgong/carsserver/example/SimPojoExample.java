package com.yonyou.longgong.carsserver.example;

import com.yonyou.longgong.carsserver.pojo.SimPojo;
import java.util.ArrayList;
import java.util.List;

public class SimPojoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public SimPojoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPkSimIsNull() {
            addCriterion("PK_SIM is null");
            return (Criteria) this;
        }

        public Criteria andPkSimIsNotNull() {
            addCriterion("PK_SIM is not null");
            return (Criteria) this;
        }

        public Criteria andPkSimEqualTo(String value) {
            addCriterion("PK_SIM =", value, "pkSim");
            return (Criteria) this;
        }

        public Criteria andPkSimNotEqualTo(String value) {
            addCriterion("PK_SIM <>", value, "pkSim");
            return (Criteria) this;
        }

        public Criteria andPkSimGreaterThan(String value) {
            addCriterion("PK_SIM >", value, "pkSim");
            return (Criteria) this;
        }

        public Criteria andPkSimGreaterThanOrEqualTo(String value) {
            addCriterion("PK_SIM >=", value, "pkSim");
            return (Criteria) this;
        }

        public Criteria andPkSimLessThan(String value) {
            addCriterion("PK_SIM <", value, "pkSim");
            return (Criteria) this;
        }

        public Criteria andPkSimLessThanOrEqualTo(String value) {
            addCriterion("PK_SIM <=", value, "pkSim");
            return (Criteria) this;
        }

        public Criteria andPkSimLike(String value) {
            addCriterion("PK_SIM like", value, "pkSim");
            return (Criteria) this;
        }

        public Criteria andPkSimNotLike(String value) {
            addCriterion("PK_SIM not like", value, "pkSim");
            return (Criteria) this;
        }

        public Criteria andPkSimIn(List<String> values) {
            addCriterion("PK_SIM in", values, "pkSim");
            return (Criteria) this;
        }

        public Criteria andPkSimNotIn(List<String> values) {
            addCriterion("PK_SIM not in", values, "pkSim");
            return (Criteria) this;
        }

        public Criteria andPkSimBetween(String value1, String value2) {
            addCriterion("PK_SIM between", value1, value2, "pkSim");
            return (Criteria) this;
        }

        public Criteria andPkSimNotBetween(String value1, String value2) {
            addCriterion("PK_SIM not between", value1, value2, "pkSim");
            return (Criteria) this;
        }

        public Criteria andSimidIsNull() {
            addCriterion("SIMID is null");
            return (Criteria) this;
        }

        public Criteria andSimidIsNotNull() {
            addCriterion("SIMID is not null");
            return (Criteria) this;
        }

        public Criteria andSimidEqualTo(String value) {
            addCriterion("SIMID =", value, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidNotEqualTo(String value) {
            addCriterion("SIMID <>", value, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidGreaterThan(String value) {
            addCriterion("SIMID >", value, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidGreaterThanOrEqualTo(String value) {
            addCriterion("SIMID >=", value, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidLessThan(String value) {
            addCriterion("SIMID <", value, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidLessThanOrEqualTo(String value) {
            addCriterion("SIMID <=", value, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidLike(String value) {
            addCriterion("SIMID like", value, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidNotLike(String value) {
            addCriterion("SIMID not like", value, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidIn(List<String> values) {
            addCriterion("SIMID in", values, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidNotIn(List<String> values) {
            addCriterion("SIMID not in", values, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidBetween(String value1, String value2) {
            addCriterion("SIMID between", value1, value2, "simid");
            return (Criteria) this;
        }

        public Criteria andSimidNotBetween(String value1, String value2) {
            addCriterion("SIMID not between", value1, value2, "simid");
            return (Criteria) this;
        }

        public Criteria andSerialIsNull() {
            addCriterion("SERIAL is null");
            return (Criteria) this;
        }

        public Criteria andSerialIsNotNull() {
            addCriterion("SERIAL is not null");
            return (Criteria) this;
        }

        public Criteria andSerialEqualTo(String value) {
            addCriterion("SERIAL =", value, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialNotEqualTo(String value) {
            addCriterion("SERIAL <>", value, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialGreaterThan(String value) {
            addCriterion("SERIAL >", value, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialGreaterThanOrEqualTo(String value) {
            addCriterion("SERIAL >=", value, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialLessThan(String value) {
            addCriterion("SERIAL <", value, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialLessThanOrEqualTo(String value) {
            addCriterion("SERIAL <=", value, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialLike(String value) {
            addCriterion("SERIAL like", value, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialNotLike(String value) {
            addCriterion("SERIAL not like", value, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialIn(List<String> values) {
            addCriterion("SERIAL in", values, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialNotIn(List<String> values) {
            addCriterion("SERIAL not in", values, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialBetween(String value1, String value2) {
            addCriterion("SERIAL between", value1, value2, "serial");
            return (Criteria) this;
        }

        public Criteria andSerialNotBetween(String value1, String value2) {
            addCriterion("SERIAL not between", value1, value2, "serial");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNull() {
            addCriterion("START_TIME is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNotNull() {
            addCriterion("START_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeEqualTo(String value) {
            addCriterion("START_TIME =", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotEqualTo(String value) {
            addCriterion("START_TIME <>", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThan(String value) {
            addCriterion("START_TIME >", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThanOrEqualTo(String value) {
            addCriterion("START_TIME >=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThan(String value) {
            addCriterion("START_TIME <", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThanOrEqualTo(String value) {
            addCriterion("START_TIME <=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLike(String value) {
            addCriterion("START_TIME like", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotLike(String value) {
            addCriterion("START_TIME not like", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeIn(List<String> values) {
            addCriterion("START_TIME in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotIn(List<String> values) {
            addCriterion("START_TIME not in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeBetween(String value1, String value2) {
            addCriterion("START_TIME between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotBetween(String value1, String value2) {
            addCriterion("START_TIME not between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andMessageMonthIsNull() {
            addCriterion("MESSAGE_MONTH is null");
            return (Criteria) this;
        }

        public Criteria andMessageMonthIsNotNull() {
            addCriterion("MESSAGE_MONTH is not null");
            return (Criteria) this;
        }

        public Criteria andMessageMonthEqualTo(String value) {
            addCriterion("MESSAGE_MONTH =", value, "messageMonth");
            return (Criteria) this;
        }

        public Criteria andMessageMonthNotEqualTo(String value) {
            addCriterion("MESSAGE_MONTH <>", value, "messageMonth");
            return (Criteria) this;
        }

        public Criteria andMessageMonthGreaterThan(String value) {
            addCriterion("MESSAGE_MONTH >", value, "messageMonth");
            return (Criteria) this;
        }

        public Criteria andMessageMonthGreaterThanOrEqualTo(String value) {
            addCriterion("MESSAGE_MONTH >=", value, "messageMonth");
            return (Criteria) this;
        }

        public Criteria andMessageMonthLessThan(String value) {
            addCriterion("MESSAGE_MONTH <", value, "messageMonth");
            return (Criteria) this;
        }

        public Criteria andMessageMonthLessThanOrEqualTo(String value) {
            addCriterion("MESSAGE_MONTH <=", value, "messageMonth");
            return (Criteria) this;
        }

        public Criteria andMessageMonthLike(String value) {
            addCriterion("MESSAGE_MONTH like", value, "messageMonth");
            return (Criteria) this;
        }

        public Criteria andMessageMonthNotLike(String value) {
            addCriterion("MESSAGE_MONTH not like", value, "messageMonth");
            return (Criteria) this;
        }

        public Criteria andMessageMonthIn(List<String> values) {
            addCriterion("MESSAGE_MONTH in", values, "messageMonth");
            return (Criteria) this;
        }

        public Criteria andMessageMonthNotIn(List<String> values) {
            addCriterion("MESSAGE_MONTH not in", values, "messageMonth");
            return (Criteria) this;
        }

        public Criteria andMessageMonthBetween(String value1, String value2) {
            addCriterion("MESSAGE_MONTH between", value1, value2, "messageMonth");
            return (Criteria) this;
        }

        public Criteria andMessageMonthNotBetween(String value1, String value2) {
            addCriterion("MESSAGE_MONTH not between", value1, value2, "messageMonth");
            return (Criteria) this;
        }

        public Criteria andFlowMonthIsNull() {
            addCriterion("FLOW_MONTH is null");
            return (Criteria) this;
        }

        public Criteria andFlowMonthIsNotNull() {
            addCriterion("FLOW_MONTH is not null");
            return (Criteria) this;
        }

        public Criteria andFlowMonthEqualTo(String value) {
            addCriterion("FLOW_MONTH =", value, "flowMonth");
            return (Criteria) this;
        }

        public Criteria andFlowMonthNotEqualTo(String value) {
            addCriterion("FLOW_MONTH <>", value, "flowMonth");
            return (Criteria) this;
        }

        public Criteria andFlowMonthGreaterThan(String value) {
            addCriterion("FLOW_MONTH >", value, "flowMonth");
            return (Criteria) this;
        }

        public Criteria andFlowMonthGreaterThanOrEqualTo(String value) {
            addCriterion("FLOW_MONTH >=", value, "flowMonth");
            return (Criteria) this;
        }

        public Criteria andFlowMonthLessThan(String value) {
            addCriterion("FLOW_MONTH <", value, "flowMonth");
            return (Criteria) this;
        }

        public Criteria andFlowMonthLessThanOrEqualTo(String value) {
            addCriterion("FLOW_MONTH <=", value, "flowMonth");
            return (Criteria) this;
        }

        public Criteria andFlowMonthLike(String value) {
            addCriterion("FLOW_MONTH like", value, "flowMonth");
            return (Criteria) this;
        }

        public Criteria andFlowMonthNotLike(String value) {
            addCriterion("FLOW_MONTH not like", value, "flowMonth");
            return (Criteria) this;
        }

        public Criteria andFlowMonthIn(List<String> values) {
            addCriterion("FLOW_MONTH in", values, "flowMonth");
            return (Criteria) this;
        }

        public Criteria andFlowMonthNotIn(List<String> values) {
            addCriterion("FLOW_MONTH not in", values, "flowMonth");
            return (Criteria) this;
        }

        public Criteria andFlowMonthBetween(String value1, String value2) {
            addCriterion("FLOW_MONTH between", value1, value2, "flowMonth");
            return (Criteria) this;
        }

        public Criteria andFlowMonthNotBetween(String value1, String value2) {
            addCriterion("FLOW_MONTH not between", value1, value2, "flowMonth");
            return (Criteria) this;
        }

        public Criteria andMessageTotalIsNull() {
            addCriterion("MESSAGE_TOTAL is null");
            return (Criteria) this;
        }

        public Criteria andMessageTotalIsNotNull() {
            addCriterion("MESSAGE_TOTAL is not null");
            return (Criteria) this;
        }

        public Criteria andMessageTotalEqualTo(String value) {
            addCriterion("MESSAGE_TOTAL =", value, "messageTotal");
            return (Criteria) this;
        }

        public Criteria andMessageTotalNotEqualTo(String value) {
            addCriterion("MESSAGE_TOTAL <>", value, "messageTotal");
            return (Criteria) this;
        }

        public Criteria andMessageTotalGreaterThan(String value) {
            addCriterion("MESSAGE_TOTAL >", value, "messageTotal");
            return (Criteria) this;
        }

        public Criteria andMessageTotalGreaterThanOrEqualTo(String value) {
            addCriterion("MESSAGE_TOTAL >=", value, "messageTotal");
            return (Criteria) this;
        }

        public Criteria andMessageTotalLessThan(String value) {
            addCriterion("MESSAGE_TOTAL <", value, "messageTotal");
            return (Criteria) this;
        }

        public Criteria andMessageTotalLessThanOrEqualTo(String value) {
            addCriterion("MESSAGE_TOTAL <=", value, "messageTotal");
            return (Criteria) this;
        }

        public Criteria andMessageTotalLike(String value) {
            addCriterion("MESSAGE_TOTAL like", value, "messageTotal");
            return (Criteria) this;
        }

        public Criteria andMessageTotalNotLike(String value) {
            addCriterion("MESSAGE_TOTAL not like", value, "messageTotal");
            return (Criteria) this;
        }

        public Criteria andMessageTotalIn(List<String> values) {
            addCriterion("MESSAGE_TOTAL in", values, "messageTotal");
            return (Criteria) this;
        }

        public Criteria andMessageTotalNotIn(List<String> values) {
            addCriterion("MESSAGE_TOTAL not in", values, "messageTotal");
            return (Criteria) this;
        }

        public Criteria andMessageTotalBetween(String value1, String value2) {
            addCriterion("MESSAGE_TOTAL between", value1, value2, "messageTotal");
            return (Criteria) this;
        }

        public Criteria andMessageTotalNotBetween(String value1, String value2) {
            addCriterion("MESSAGE_TOTAL not between", value1, value2, "messageTotal");
            return (Criteria) this;
        }

        public Criteria andFlowTotalIsNull() {
            addCriterion("FLOW_TOTAL is null");
            return (Criteria) this;
        }

        public Criteria andFlowTotalIsNotNull() {
            addCriterion("FLOW_TOTAL is not null");
            return (Criteria) this;
        }

        public Criteria andFlowTotalEqualTo(String value) {
            addCriterion("FLOW_TOTAL =", value, "flowTotal");
            return (Criteria) this;
        }

        public Criteria andFlowTotalNotEqualTo(String value) {
            addCriterion("FLOW_TOTAL <>", value, "flowTotal");
            return (Criteria) this;
        }

        public Criteria andFlowTotalGreaterThan(String value) {
            addCriterion("FLOW_TOTAL >", value, "flowTotal");
            return (Criteria) this;
        }

        public Criteria andFlowTotalGreaterThanOrEqualTo(String value) {
            addCriterion("FLOW_TOTAL >=", value, "flowTotal");
            return (Criteria) this;
        }

        public Criteria andFlowTotalLessThan(String value) {
            addCriterion("FLOW_TOTAL <", value, "flowTotal");
            return (Criteria) this;
        }

        public Criteria andFlowTotalLessThanOrEqualTo(String value) {
            addCriterion("FLOW_TOTAL <=", value, "flowTotal");
            return (Criteria) this;
        }

        public Criteria andFlowTotalLike(String value) {
            addCriterion("FLOW_TOTAL like", value, "flowTotal");
            return (Criteria) this;
        }

        public Criteria andFlowTotalNotLike(String value) {
            addCriterion("FLOW_TOTAL not like", value, "flowTotal");
            return (Criteria) this;
        }

        public Criteria andFlowTotalIn(List<String> values) {
            addCriterion("FLOW_TOTAL in", values, "flowTotal");
            return (Criteria) this;
        }

        public Criteria andFlowTotalNotIn(List<String> values) {
            addCriterion("FLOW_TOTAL not in", values, "flowTotal");
            return (Criteria) this;
        }

        public Criteria andFlowTotalBetween(String value1, String value2) {
            addCriterion("FLOW_TOTAL between", value1, value2, "flowTotal");
            return (Criteria) this;
        }

        public Criteria andFlowTotalNotBetween(String value1, String value2) {
            addCriterion("FLOW_TOTAL not between", value1, value2, "flowTotal");
            return (Criteria) this;
        }

        public Criteria andLastTimeIsNull() {
            addCriterion("LAST_TIME is null");
            return (Criteria) this;
        }

        public Criteria andLastTimeIsNotNull() {
            addCriterion("LAST_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andLastTimeEqualTo(String value) {
            addCriterion("LAST_TIME =", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeNotEqualTo(String value) {
            addCriterion("LAST_TIME <>", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeGreaterThan(String value) {
            addCriterion("LAST_TIME >", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeGreaterThanOrEqualTo(String value) {
            addCriterion("LAST_TIME >=", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeLessThan(String value) {
            addCriterion("LAST_TIME <", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeLessThanOrEqualTo(String value) {
            addCriterion("LAST_TIME <=", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeLike(String value) {
            addCriterion("LAST_TIME like", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeNotLike(String value) {
            addCriterion("LAST_TIME not like", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeIn(List<String> values) {
            addCriterion("LAST_TIME in", values, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeNotIn(List<String> values) {
            addCriterion("LAST_TIME not in", values, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeBetween(String value1, String value2) {
            addCriterion("LAST_TIME between", value1, value2, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeNotBetween(String value1, String value2) {
            addCriterion("LAST_TIME not between", value1, value2, "lastTime");
            return (Criteria) this;
        }

        public Criteria andCarnoIsNull() {
            addCriterion("CARNO is null");
            return (Criteria) this;
        }

        public Criteria andCarnoIsNotNull() {
            addCriterion("CARNO is not null");
            return (Criteria) this;
        }

        public Criteria andCarnoEqualTo(String value) {
            addCriterion("CARNO =", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotEqualTo(String value) {
            addCriterion("CARNO <>", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoGreaterThan(String value) {
            addCriterion("CARNO >", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoGreaterThanOrEqualTo(String value) {
            addCriterion("CARNO >=", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoLessThan(String value) {
            addCriterion("CARNO <", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoLessThanOrEqualTo(String value) {
            addCriterion("CARNO <=", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoLike(String value) {
            addCriterion("CARNO like", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotLike(String value) {
            addCriterion("CARNO not like", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoIn(List<String> values) {
            addCriterion("CARNO in", values, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotIn(List<String> values) {
            addCriterion("CARNO not in", values, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoBetween(String value1, String value2) {
            addCriterion("CARNO between", value1, value2, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotBetween(String value1, String value2) {
            addCriterion("CARNO not between", value1, value2, "carno");
            return (Criteria) this;
        }

        public Criteria andClientidIsNull() {
            addCriterion("CLIENTID is null");
            return (Criteria) this;
        }

        public Criteria andClientidIsNotNull() {
            addCriterion("CLIENTID is not null");
            return (Criteria) this;
        }

        public Criteria andClientidEqualTo(String value) {
            addCriterion("CLIENTID =", value, "clientid");
            return (Criteria) this;
        }

        public Criteria andClientidNotEqualTo(String value) {
            addCriterion("CLIENTID <>", value, "clientid");
            return (Criteria) this;
        }

        public Criteria andClientidGreaterThan(String value) {
            addCriterion("CLIENTID >", value, "clientid");
            return (Criteria) this;
        }

        public Criteria andClientidGreaterThanOrEqualTo(String value) {
            addCriterion("CLIENTID >=", value, "clientid");
            return (Criteria) this;
        }

        public Criteria andClientidLessThan(String value) {
            addCriterion("CLIENTID <", value, "clientid");
            return (Criteria) this;
        }

        public Criteria andClientidLessThanOrEqualTo(String value) {
            addCriterion("CLIENTID <=", value, "clientid");
            return (Criteria) this;
        }

        public Criteria andClientidLike(String value) {
            addCriterion("CLIENTID like", value, "clientid");
            return (Criteria) this;
        }

        public Criteria andClientidNotLike(String value) {
            addCriterion("CLIENTID not like", value, "clientid");
            return (Criteria) this;
        }

        public Criteria andClientidIn(List<String> values) {
            addCriterion("CLIENTID in", values, "clientid");
            return (Criteria) this;
        }

        public Criteria andClientidNotIn(List<String> values) {
            addCriterion("CLIENTID not in", values, "clientid");
            return (Criteria) this;
        }

        public Criteria andClientidBetween(String value1, String value2) {
            addCriterion("CLIENTID between", value1, value2, "clientid");
            return (Criteria) this;
        }

        public Criteria andClientidNotBetween(String value1, String value2) {
            addCriterion("CLIENTID not between", value1, value2, "clientid");
            return (Criteria) this;
        }

        public Criteria andBindTimeIsNull() {
            addCriterion("BIND_TIME is null");
            return (Criteria) this;
        }

        public Criteria andBindTimeIsNotNull() {
            addCriterion("BIND_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andBindTimeEqualTo(String value) {
            addCriterion("BIND_TIME =", value, "bindTime");
            return (Criteria) this;
        }

        public Criteria andBindTimeNotEqualTo(String value) {
            addCriterion("BIND_TIME <>", value, "bindTime");
            return (Criteria) this;
        }

        public Criteria andBindTimeGreaterThan(String value) {
            addCriterion("BIND_TIME >", value, "bindTime");
            return (Criteria) this;
        }

        public Criteria andBindTimeGreaterThanOrEqualTo(String value) {
            addCriterion("BIND_TIME >=", value, "bindTime");
            return (Criteria) this;
        }

        public Criteria andBindTimeLessThan(String value) {
            addCriterion("BIND_TIME <", value, "bindTime");
            return (Criteria) this;
        }

        public Criteria andBindTimeLessThanOrEqualTo(String value) {
            addCriterion("BIND_TIME <=", value, "bindTime");
            return (Criteria) this;
        }

        public Criteria andBindTimeLike(String value) {
            addCriterion("BIND_TIME like", value, "bindTime");
            return (Criteria) this;
        }

        public Criteria andBindTimeNotLike(String value) {
            addCriterion("BIND_TIME not like", value, "bindTime");
            return (Criteria) this;
        }

        public Criteria andBindTimeIn(List<String> values) {
            addCriterion("BIND_TIME in", values, "bindTime");
            return (Criteria) this;
        }

        public Criteria andBindTimeNotIn(List<String> values) {
            addCriterion("BIND_TIME not in", values, "bindTime");
            return (Criteria) this;
        }

        public Criteria andBindTimeBetween(String value1, String value2) {
            addCriterion("BIND_TIME between", value1, value2, "bindTime");
            return (Criteria) this;
        }

        public Criteria andBindTimeNotBetween(String value1, String value2) {
            addCriterion("BIND_TIME not between", value1, value2, "bindTime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeIsNull() {
            addCriterion("CREATIONTIME is null");
            return (Criteria) this;
        }

        public Criteria andCreationtimeIsNotNull() {
            addCriterion("CREATIONTIME is not null");
            return (Criteria) this;
        }

        public Criteria andCreationtimeEqualTo(String value) {
            addCriterion("CREATIONTIME =", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeNotEqualTo(String value) {
            addCriterion("CREATIONTIME <>", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeGreaterThan(String value) {
            addCriterion("CREATIONTIME >", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeGreaterThanOrEqualTo(String value) {
            addCriterion("CREATIONTIME >=", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeLessThan(String value) {
            addCriterion("CREATIONTIME <", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeLessThanOrEqualTo(String value) {
            addCriterion("CREATIONTIME <=", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeLike(String value) {
            addCriterion("CREATIONTIME like", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeNotLike(String value) {
            addCriterion("CREATIONTIME not like", value, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeIn(List<String> values) {
            addCriterion("CREATIONTIME in", values, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeNotIn(List<String> values) {
            addCriterion("CREATIONTIME not in", values, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeBetween(String value1, String value2) {
            addCriterion("CREATIONTIME between", value1, value2, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreationtimeNotBetween(String value1, String value2) {
            addCriterion("CREATIONTIME not between", value1, value2, "creationtime");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNull() {
            addCriterion("CREATOR is null");
            return (Criteria) this;
        }

        public Criteria andCreatorIsNotNull() {
            addCriterion("CREATOR is not null");
            return (Criteria) this;
        }

        public Criteria andCreatorEqualTo(String value) {
            addCriterion("CREATOR =", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotEqualTo(String value) {
            addCriterion("CREATOR <>", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThan(String value) {
            addCriterion("CREATOR >", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorGreaterThanOrEqualTo(String value) {
            addCriterion("CREATOR >=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThan(String value) {
            addCriterion("CREATOR <", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLessThanOrEqualTo(String value) {
            addCriterion("CREATOR <=", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorLike(String value) {
            addCriterion("CREATOR like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotLike(String value) {
            addCriterion("CREATOR not like", value, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorIn(List<String> values) {
            addCriterion("CREATOR in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotIn(List<String> values) {
            addCriterion("CREATOR not in", values, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorBetween(String value1, String value2) {
            addCriterion("CREATOR between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andCreatorNotBetween(String value1, String value2) {
            addCriterion("CREATOR not between", value1, value2, "creator");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeIsNull() {
            addCriterion("MODIFIEDTIME is null");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeIsNotNull() {
            addCriterion("MODIFIEDTIME is not null");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeEqualTo(String value) {
            addCriterion("MODIFIEDTIME =", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeNotEqualTo(String value) {
            addCriterion("MODIFIEDTIME <>", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeGreaterThan(String value) {
            addCriterion("MODIFIEDTIME >", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeGreaterThanOrEqualTo(String value) {
            addCriterion("MODIFIEDTIME >=", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeLessThan(String value) {
            addCriterion("MODIFIEDTIME <", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeLessThanOrEqualTo(String value) {
            addCriterion("MODIFIEDTIME <=", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeLike(String value) {
            addCriterion("MODIFIEDTIME like", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeNotLike(String value) {
            addCriterion("MODIFIEDTIME not like", value, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeIn(List<String> values) {
            addCriterion("MODIFIEDTIME in", values, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeNotIn(List<String> values) {
            addCriterion("MODIFIEDTIME not in", values, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeBetween(String value1, String value2) {
            addCriterion("MODIFIEDTIME between", value1, value2, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifiedtimeNotBetween(String value1, String value2) {
            addCriterion("MODIFIEDTIME not between", value1, value2, "modifiedtime");
            return (Criteria) this;
        }

        public Criteria andModifierIsNull() {
            addCriterion("MODIFIER is null");
            return (Criteria) this;
        }

        public Criteria andModifierIsNotNull() {
            addCriterion("MODIFIER is not null");
            return (Criteria) this;
        }

        public Criteria andModifierEqualTo(String value) {
            addCriterion("MODIFIER =", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierNotEqualTo(String value) {
            addCriterion("MODIFIER <>", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierGreaterThan(String value) {
            addCriterion("MODIFIER >", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierGreaterThanOrEqualTo(String value) {
            addCriterion("MODIFIER >=", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierLessThan(String value) {
            addCriterion("MODIFIER <", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierLessThanOrEqualTo(String value) {
            addCriterion("MODIFIER <=", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierLike(String value) {
            addCriterion("MODIFIER like", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierNotLike(String value) {
            addCriterion("MODIFIER not like", value, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierIn(List<String> values) {
            addCriterion("MODIFIER in", values, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierNotIn(List<String> values) {
            addCriterion("MODIFIER not in", values, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierBetween(String value1, String value2) {
            addCriterion("MODIFIER between", value1, value2, "modifier");
            return (Criteria) this;
        }

        public Criteria andModifierNotBetween(String value1, String value2) {
            addCriterion("MODIFIER not between", value1, value2, "modifier");
            return (Criteria) this;
        }

        public Criteria andDrIsNull() {
            addCriterion("DR is null");
            return (Criteria) this;
        }

        public Criteria andDrIsNotNull() {
            addCriterion("DR is not null");
            return (Criteria) this;
        }

        public Criteria andDrEqualTo(Long value) {
            addCriterion("DR =", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrNotEqualTo(Long value) {
            addCriterion("DR <>", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrGreaterThan(Long value) {
            addCriterion("DR >", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrGreaterThanOrEqualTo(Long value) {
            addCriterion("DR >=", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrLessThan(Long value) {
            addCriterion("DR <", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrLessThanOrEqualTo(Long value) {
            addCriterion("DR <=", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrIn(List<Long> values) {
            addCriterion("DR in", values, "dr");
            return (Criteria) this;
        }

        public Criteria andDrNotIn(List<Long> values) {
            addCriterion("DR not in", values, "dr");
            return (Criteria) this;
        }

        public Criteria andDrBetween(Long value1, Long value2) {
            addCriterion("DR between", value1, value2, "dr");
            return (Criteria) this;
        }

        public Criteria andDrNotBetween(Long value1, Long value2) {
            addCriterion("DR not between", value1, value2, "dr");
            return (Criteria) this;
        }

        public Criteria andVersionIsNull() {
            addCriterion("VERSION is null");
            return (Criteria) this;
        }

        public Criteria andVersionIsNotNull() {
            addCriterion("VERSION is not null");
            return (Criteria) this;
        }

        public Criteria andVersionEqualTo(Long value) {
            addCriterion("VERSION =", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotEqualTo(Long value) {
            addCriterion("VERSION <>", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionGreaterThan(Long value) {
            addCriterion("VERSION >", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionGreaterThanOrEqualTo(Long value) {
            addCriterion("VERSION >=", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionLessThan(Long value) {
            addCriterion("VERSION <", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionLessThanOrEqualTo(Long value) {
            addCriterion("VERSION <=", value, "version");
            return (Criteria) this;
        }

        public Criteria andVersionIn(List<Long> values) {
            addCriterion("VERSION in", values, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotIn(List<Long> values) {
            addCriterion("VERSION not in", values, "version");
            return (Criteria) this;
        }

        public Criteria andVersionBetween(Long value1, Long value2) {
            addCriterion("VERSION between", value1, value2, "version");
            return (Criteria) this;
        }

        public Criteria andVersionNotBetween(Long value1, Long value2) {
            addCriterion("VERSION not between", value1, value2, "version");
            return (Criteria) this;
        }

        public Criteria andPkOrgIsNull() {
            addCriterion("PK_ORG is null");
            return (Criteria) this;
        }

        public Criteria andPkOrgIsNotNull() {
            addCriterion("PK_ORG is not null");
            return (Criteria) this;
        }

        public Criteria andPkOrgEqualTo(String value) {
            addCriterion("PK_ORG =", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgNotEqualTo(String value) {
            addCriterion("PK_ORG <>", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgGreaterThan(String value) {
            addCriterion("PK_ORG >", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgGreaterThanOrEqualTo(String value) {
            addCriterion("PK_ORG >=", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgLessThan(String value) {
            addCriterion("PK_ORG <", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgLessThanOrEqualTo(String value) {
            addCriterion("PK_ORG <=", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgLike(String value) {
            addCriterion("PK_ORG like", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgNotLike(String value) {
            addCriterion("PK_ORG not like", value, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgIn(List<String> values) {
            addCriterion("PK_ORG in", values, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgNotIn(List<String> values) {
            addCriterion("PK_ORG not in", values, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgBetween(String value1, String value2) {
            addCriterion("PK_ORG between", value1, value2, "pkOrg");
            return (Criteria) this;
        }

        public Criteria andPkOrgNotBetween(String value1, String value2) {
            addCriterion("PK_ORG not between", value1, value2, "pkOrg");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }

        public Criteria andLogicalDeleted(boolean deleted) {
            return deleted ? andDrEqualTo(SimPojo.Dr.IS_DELETED.value()) : andDrNotEqualTo(SimPojo.Dr.IS_DELETED.value());
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}