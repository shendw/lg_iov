package com.yonyou.longgong.carsserver.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import lombok.Data;

@Data
public class CardocPojo implements Serializable {
    public static final Long IS_DELETED = Dr.IS_DELETED.value();

    public static final Long NOT_DELETED = Dr.NOT_DELETED.value();

    private String pkCardoc;

    @Excel(name = "车型型号",needMerge = true)
    private String carmodel;

    @Excel(name = "机型",needMerge = true)
    private String machinemodel;

    @Excel(name = "整车编号",needMerge = true)
    private String carno;

    @Excel(name = "服务代理商",needMerge = true)
    private String serviceAgent;

    @Excel(name = "销售代理商",needMerge = true)
    private String saleAgent;

    @Excel(name = "客户",needMerge = true)
    private String customer;

    @Excel(name = "流水号",needMerge = true)
    private String serial;

    @Excel(name = "流程状态",needMerge = true)
    private Long status;

    @Excel(name = "入库日期",needMerge = true)
    private String instocktime;

    @Excel(name = "出库日期",needMerge = true)
    private String outstocktime;

    @Excel(name = "销售日期",needMerge = true)
    private String saletime;

    @Excel(name = "销售公司",needMerge = true)
    private String salecompany;

    @Excel(name = "最终客户",needMerge = true)
    private String finalcustomer;

    @Excel(name = "最后通讯时间",needMerge = true)
    private String lasttime;

    @Excel(name = "工作时长",needMerge = true)
    private String worktime;

    @Excel(name = "位置信息",needMerge = true)
    private String location;

    private String creationtime;

    private String creator;

    private String modifiedtime;

    private String modifier;

    private Long dr;

    private Long version;

    private String pkOrg;

    @Excel(name = "备注",needMerge = true)
    private String remark;

    private String workstatus;

    private String lockstatus;

    @ExcelCollection(name = "车辆绑定信息", orderNum = "5")
    private List<TerminalCarPojo> terminalCarPojos;

    private String outlineer;

    private String outlinetime;

    private String debuger;

    private String debugtime;

    private String autobindtime;

    private static final long serialVersionUID = 1L;

    public void andLogicalDeleted(boolean deleted) {
        setDr(deleted ? Dr.IS_DELETED.value() : Dr.NOT_DELETED.value());
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        CardocPojo other = (CardocPojo) that;
        return (this.getPkCardoc() == null ? other.getPkCardoc() == null : this.getPkCardoc().equals(other.getPkCardoc()))
            && (this.getCarmodel() == null ? other.getCarmodel() == null : this.getCarmodel().equals(other.getCarmodel()))
            && (this.getMachinemodel() == null ? other.getMachinemodel() == null : this.getMachinemodel().equals(other.getMachinemodel()))
            && (this.getCarno() == null ? other.getCarno() == null : this.getCarno().equals(other.getCarno()))
            && (this.getServiceAgent() == null ? other.getServiceAgent() == null : this.getServiceAgent().equals(other.getServiceAgent()))
            && (this.getSaleAgent() == null ? other.getSaleAgent() == null : this.getSaleAgent().equals(other.getSaleAgent()))
            && (this.getCustomer() == null ? other.getCustomer() == null : this.getCustomer().equals(other.getCustomer()))
            && (this.getSerial() == null ? other.getSerial() == null : this.getSerial().equals(other.getSerial()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getInstocktime() == null ? other.getInstocktime() == null : this.getInstocktime().equals(other.getInstocktime()))
            && (this.getOutstocktime() == null ? other.getOutstocktime() == null : this.getOutstocktime().equals(other.getOutstocktime()))
            && (this.getSaletime() == null ? other.getSaletime() == null : this.getSaletime().equals(other.getSaletime()))
            && (this.getSalecompany() == null ? other.getSalecompany() == null : this.getSalecompany().equals(other.getSalecompany()))
            && (this.getFinalcustomer() == null ? other.getFinalcustomer() == null : this.getFinalcustomer().equals(other.getFinalcustomer()))
            && (this.getLasttime() == null ? other.getLasttime() == null : this.getLasttime().equals(other.getLasttime()))
            && (this.getWorktime() == null ? other.getWorktime() == null : this.getWorktime().equals(other.getWorktime()))
            && (this.getLocation() == null ? other.getLocation() == null : this.getLocation().equals(other.getLocation()))
            && (this.getCreationtime() == null ? other.getCreationtime() == null : this.getCreationtime().equals(other.getCreationtime()))
            && (this.getCreator() == null ? other.getCreator() == null : this.getCreator().equals(other.getCreator()))
            && (this.getModifiedtime() == null ? other.getModifiedtime() == null : this.getModifiedtime().equals(other.getModifiedtime()))
            && (this.getModifier() == null ? other.getModifier() == null : this.getModifier().equals(other.getModifier()))
            && (this.getDr() == null ? other.getDr() == null : this.getDr().equals(other.getDr()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()))
            && (this.getPkOrg() == null ? other.getPkOrg() == null : this.getPkOrg().equals(other.getPkOrg()))
            && (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()))
            && (this.getWorkstatus() == null ? other.getWorkstatus() == null : this.getWorkstatus().equals(other.getWorkstatus()))
            && (this.getLockstatus() == null ? other.getLockstatus() == null : this.getLockstatus().equals(other.getLockstatus()))
            && (this.getOutlineer() == null ? other.getOutlineer() == null : this.getOutlineer().equals(other.getOutlineer()))
            && (this.getOutlinetime() == null ? other.getOutlinetime() == null : this.getOutlinetime().equals(other.getOutlinetime()))
            && (this.getDebuger() == null ? other.getDebuger() == null : this.getDebuger().equals(other.getDebuger()))
            && (this.getDebugtime() == null ? other.getDebugtime() == null : this.getDebugtime().equals(other.getDebugtime()))
            && (this.getAutobindtime() == null ? other.getAutobindtime() == null : this.getAutobindtime().equals(other.getAutobindtime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getPkCardoc() == null) ? 0 : getPkCardoc().hashCode());
        result = prime * result + ((getCarmodel() == null) ? 0 : getCarmodel().hashCode());
        result = prime * result + ((getMachinemodel() == null) ? 0 : getMachinemodel().hashCode());
        result = prime * result + ((getCarno() == null) ? 0 : getCarno().hashCode());
        result = prime * result + ((getServiceAgent() == null) ? 0 : getServiceAgent().hashCode());
        result = prime * result + ((getSaleAgent() == null) ? 0 : getSaleAgent().hashCode());
        result = prime * result + ((getCustomer() == null) ? 0 : getCustomer().hashCode());
        result = prime * result + ((getSerial() == null) ? 0 : getSerial().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getInstocktime() == null) ? 0 : getInstocktime().hashCode());
        result = prime * result + ((getOutstocktime() == null) ? 0 : getOutstocktime().hashCode());
        result = prime * result + ((getSaletime() == null) ? 0 : getSaletime().hashCode());
        result = prime * result + ((getSalecompany() == null) ? 0 : getSalecompany().hashCode());
        result = prime * result + ((getFinalcustomer() == null) ? 0 : getFinalcustomer().hashCode());
        result = prime * result + ((getLasttime() == null) ? 0 : getLasttime().hashCode());
        result = prime * result + ((getWorktime() == null) ? 0 : getWorktime().hashCode());
        result = prime * result + ((getLocation() == null) ? 0 : getLocation().hashCode());
        result = prime * result + ((getCreationtime() == null) ? 0 : getCreationtime().hashCode());
        result = prime * result + ((getCreator() == null) ? 0 : getCreator().hashCode());
        result = prime * result + ((getModifiedtime() == null) ? 0 : getModifiedtime().hashCode());
        result = prime * result + ((getModifier() == null) ? 0 : getModifier().hashCode());
        result = prime * result + ((getDr() == null) ? 0 : getDr().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        result = prime * result + ((getPkOrg() == null) ? 0 : getPkOrg().hashCode());
        result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
        result = prime * result + ((getWorkstatus() == null) ? 0 : getWorkstatus().hashCode());
        result = prime * result + ((getLockstatus() == null) ? 0 : getLockstatus().hashCode());
        result = prime * result + ((getOutlineer() == null) ? 0 : getOutlineer().hashCode());
        result = prime * result + ((getOutlinetime() == null) ? 0 : getOutlinetime().hashCode());
        result = prime * result + ((getDebuger() == null) ? 0 : getDebuger().hashCode());
        result = prime * result + ((getDebugtime() == null) ? 0 : getDebugtime().hashCode());
        result = prime * result + ((getAutobindtime() == null) ? 0 : getAutobindtime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", pkCardoc=").append(pkCardoc);
        sb.append(", carmodel=").append(carmodel);
        sb.append(", machinemodel=").append(machinemodel);
        sb.append(", carno=").append(carno);
        sb.append(", serviceAgent=").append(serviceAgent);
        sb.append(", saleAgent=").append(saleAgent);
        sb.append(", customer=").append(customer);
        sb.append(", serial=").append(serial);
        sb.append(", status=").append(status);
        sb.append(", instocktime=").append(instocktime);
        sb.append(", outstocktime=").append(outstocktime);
        sb.append(", saletime=").append(saletime);
        sb.append(", salecompany=").append(salecompany);
        sb.append(", finalcustomer=").append(finalcustomer);
        sb.append(", lasttime=").append(lasttime);
        sb.append(", worktime=").append(worktime);
        sb.append(", location=").append(location);
        sb.append(", creationtime=").append(creationtime);
        sb.append(", creator=").append(creator);
        sb.append(", modifiedtime=").append(modifiedtime);
        sb.append(", modifier=").append(modifier);
        sb.append(", dr=").append(dr);
        sb.append(", version=").append(version);
        sb.append(", pkOrg=").append(pkOrg);
        sb.append(", remark=").append(remark);
        sb.append(", workstatus=").append(workstatus);
        sb.append(", lockstatus=").append(lockstatus);
        sb.append(", outlineer=").append(outlineer);
        sb.append(", outlinetime=").append(outlinetime);
        sb.append(", debuger=").append(debuger);
        sb.append(", debugtime=").append(debugtime);
        sb.append(", autobindtime=").append(autobindtime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    public enum Dr {
        NOT_DELETED(new Long("0"), "未删除"),
        IS_DELETED(new Long("1"), "已删除");

        private final Long value;

        private final String name;

        Dr(Long value, String name) {
            this.value = value;
            this.name = name;
        }

        public Long getValue() {
            return this.value;
        }

        public Long value() {
            return this.value;
        }

        public String getName() {
            return this.name;
        }
    }

    public enum Column {
        pkCardoc("PK_CARDOC", "pkCardoc", "VARCHAR", false),
        carmodel("CARMODEL", "carmodel", "VARCHAR", false),
        machinemodel("MACHINEMODEL", "machinemodel", "VARCHAR", false),
        carno("CARNO", "carno", "VARCHAR", false),
        serviceAgent("SERVICE_AGENT", "serviceAgent", "VARCHAR", false),
        saleAgent("SALE_AGENT", "saleAgent", "VARCHAR", false),
        customer("CUSTOMER", "customer", "VARCHAR", false),
        serial("SERIAL", "serial", "VARCHAR", false),
        status("STATUS", "status", "DECIMAL", false),
        instocktime("INSTOCKTIME", "instocktime", "CHAR", false),
        outstocktime("OUTSTOCKTIME", "outstocktime", "CHAR", false),
        saletime("SALETIME", "saletime", "CHAR", false),
        salecompany("SALECOMPANY", "salecompany", "VARCHAR", false),
        finalcustomer("FINALCUSTOMER", "finalcustomer", "VARCHAR", false),
        lasttime("LASTTIME", "lasttime", "CHAR", false),
        worktime("WORKTIME", "worktime", "VARCHAR", false),
        location("LOCATION", "location", "VARCHAR", false),
        creationtime("CREATIONTIME", "creationtime", "CHAR", false),
        creator("CREATOR", "creator", "VARCHAR", false),
        modifiedtime("MODIFIEDTIME", "modifiedtime", "CHAR", false),
        modifier("MODIFIER", "modifier", "VARCHAR", false),
        dr("DR", "dr", "DECIMAL", false),
        version("VERSION", "version", "DECIMAL", false),
        pkOrg("PK_ORG", "pkOrg", "VARCHAR", false),
        remark("REMARK", "remark", "VARCHAR", false),
        workstatus("WORKSTATUS", "workstatus", "VARCHAR", false),
        lockstatus("LOCKSTATUS", "lockstatus", "VARCHAR", false),
        outlineer("OUTLINEER", "outlineer", "VARCHAR", false),
        outlinetime("OUTLINETIME", "outlinetime", "CHAR", false),
        debuger("DEBUGER", "debuger", "VARCHAR", false),
        debugtime("DEBUGTIME", "debugtime", "CHAR", false),
        autobindtime("AUTOBINDTIME", "autobindtime", "CHAR", false);

        private static final String BEGINNING_DELIMITER = "\"";

        private static final String ENDING_DELIMITER = "\"";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}