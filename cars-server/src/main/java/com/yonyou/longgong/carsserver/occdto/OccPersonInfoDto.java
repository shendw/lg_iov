package com.yonyou.longgong.carsserver.occdto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-11-20
 * @描述 渠道云人员信息对象
 **/
@Data
public class OccPersonInfoDto {
    private String id;
    private String creationTime;
    private String creator;
    private String departmentCode;
    private String departmentId;
    private String departmentName;
    private Integer dr;
    private Integer isMain;
    private String modifiedTime;
    private String modifier;
    private String organizationCode;
    private String organizationId;
    private String organizationName;
    private String persistStatus;
    private String personCode;
    private String personId;
    private String personName;
    private String postCode;
    private String postEndDate;
    private String postId;
    private String postName;
    private String postStartDate;
    private String promptMessage;
    private String ts;
}
