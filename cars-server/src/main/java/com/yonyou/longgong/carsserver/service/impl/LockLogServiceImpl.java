package com.yonyou.longgong.carsserver.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dao.LockLogPojoMapper;
import com.yonyou.longgong.carsserver.dto.LockLogDto;
import com.yonyou.longgong.carsserver.enums.TerminalCarStatus;
import com.yonyou.longgong.carsserver.example.LockLogPojoExample;
import com.yonyou.longgong.carsserver.exception.BusinessException;
import com.yonyou.longgong.carsserver.pojo.LockLogPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalCarPojo;
import com.yonyou.longgong.carsserver.service.ICardocService;
import com.yonyou.longgong.carsserver.service.ILockLogService;
import com.yonyou.longgong.carsserver.service.ITerminalCarService;
import com.yonyou.longgong.carsserver.utils.DataTimeUtil;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.UUIDUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;

/**
 * @author shendawei
 * @创建时间 2020/4/26
 * @描述
 **/
@Service("iLockLogService")
@Slf4j
public class LockLogServiceImpl implements ILockLogService {

    @Autowired
    private LockLogPojoMapper lockLogPojoMapper;

    @Autowired
    private ITerminalCarService iTerminalCarService;

    @Autowired
    private ICardocService iCardocService;

    @Override
    public PageInfo<LockLogPojo> selectLocklog(LockLogDto lockLogDto) {
        String terminalid = lockLogDto.getTerminalid();
        if (StringUtils.isBlank(terminalid)){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        LockLogPojoExample lockLogPojoExample = new LockLogPojoExample();
        LockLogPojoExample.Criteria criteria = lockLogPojoExample.createCriteria();
        criteria.andTerminalidEqualTo(terminalid);
        if (StringUtils.isNotEmpty(lockLogDto.getBegintime())){
            criteria.andCreatetimeGreaterThanOrEqualTo(lockLogDto.getBegintime());
        }
        if (StringUtils.isNotEmpty(lockLogDto.getEndtime())){
            criteria.andCreatetimeLessThanOrEqualTo(lockLogDto.getEndtime());
        }
        criteria.andLogicalDeleted(false);
        lockLogPojoExample.setOrderByClause(" createtime desc ");
        PageHelper.startPage(Integer.parseInt(lockLogDto.getPageNum()),Integer.parseInt(lockLogDto.getPageSize()));
        List<LockLogPojo> lockLogPojos = lockLogPojoMapper.selectByExample(lockLogPojoExample);
        return new PageInfo<>(lockLogPojos);
    }

    @Override
    public Integer insertLocklog(String terminalid, String userid,String code) {
        LockLogPojo lockLogPojo = new LockLogPojo();
        lockLogPojo.setPkLocklog(UUIDUtils.getUniqueIdByUUId());
        lockLogPojo.setChannel("GPRS");
        lockLogPojo.setOperator(userid);
        lockLogPojo.setTerminalid(terminalid);
        lockLogPojo.setRecord(code);
        lockLogPojo.setCreatetime(DataTimeUtil.dateToStr(new Date()));
        lockLogPojo.setDr(0L);
        lockLogPojo.setVersion(1L);
        lockLogPojoMapper.insertSelective(lockLogPojo);
        TerminalCarPojo terminalCarPojo = new TerminalCarPojo();
        terminalCarPojo.setTerminalid(terminalid);
        terminalCarPojo.setEnablestate(TerminalCarStatus.EFFECTIVE.getCode());
        List<TerminalCarPojo> terminalCarPojos = iTerminalCarService.selectTerminalCar(terminalCarPojo);
        if (!CollectionUtils.isEmpty(terminalCarPojos)){
            String carno = terminalCarPojos.get(0).getCarno();
            iCardocService.updateLockStatus(carno,code);
        }
        return null;
    }


    @Override
    public void addLocklog(String terminalid, String userid,String code) {
        LockLogPojo lockLogPojo = new LockLogPojo();
        lockLogPojo.setPkLocklog(UUIDUtils.getUniqueIdByUUId());
        lockLogPojo.setChannel("GPRS");
        lockLogPojo.setOperator(userid);
        lockLogPojo.setTerminalid(terminalid);
        lockLogPojo.setRecord(code);
        lockLogPojo.setCreatetime(DataTimeUtil.dateToStr(new Date()));
        lockLogPojo.setDr(0L);
        lockLogPojo.setVersion(1L);
        lockLogPojoMapper.insertSelective(lockLogPojo);

    }
}
