package com.yonyou.longgong.carsserver.dao;

import com.yonyou.longgong.carsserver.example.LockLogPojoExample;
import com.yonyou.longgong.carsserver.pojo.LockLogPojo;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LockLogPojoMapper {
    long countByExample(LockLogPojoExample example);

    int deleteByExample(LockLogPojoExample example);

    int insert(LockLogPojo record);

    int insertSelective(@Param("record") LockLogPojo record, @Param("selective") LockLogPojo.Column ... selective);

    List<LockLogPojo> selectByExample(LockLogPojoExample example);

    int updateByExampleSelective(@Param("record") LockLogPojo record, @Param("example") LockLogPojoExample example, @Param("selective") LockLogPojo.Column ... selective);

    int updateByExample(@Param("record") LockLogPojo record, @Param("example") LockLogPojoExample example);

    int logicalDeleteByExample(@Param("example") LockLogPojoExample example);
}