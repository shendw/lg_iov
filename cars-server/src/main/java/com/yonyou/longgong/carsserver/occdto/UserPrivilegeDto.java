package com.yonyou.longgong.carsserver.occdto;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户权限视图的数据传输对象。
 *
 * @author wangruiv
 * @date 2018-05-25 09:09:55
 */
@Getter
@Setter
public class UserPrivilegeDto {
    private String id;

    private String userId;

    private String loginName;

    private String userName;

    private String roleId;

    private String roleCode;

    private String roleName;

    private String labelId;

    private String labelCode;

    private String labelName;

    private Integer labelLevel;
}
