package com.yonyou.longgong.carsserver.occdto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-11-18
 * @描述
 **/
@Data
public class OccSupplierAccountDto {
    /**
     * 户名
     */
    private String accountName;
    /**
     * 账户性质编码
     */
    private String accountNatureCode;
    /**
     * 账户性质id
     */
    private String accountNatureId;
    /**
     * 账户性质名称
     */
    private String accountNatureName;
    /**
     * 银行账号
     */
    private String accountNumber;
    /**
     * 银行类别编码
     */
    private String bankCategoryCode;
    /**
     * 银行类别名称
     */
    private String bankCategoryName;
    /**
     * 联系人
     */
    private String contact;
    /**
     * 创建时间
     */
    private String creationTime;
    /**
     * 创建人
     */
    private String creator;
    /**
     * 币种编码
     */
    private String currencyCode;
    /**
     * 币种id
     */
    private String currencyId;
    /**
     * 币种名称
     */
    private String currencyName;
    /**
     * 开户行编码
     */
    private String depositBankCode;
    /**
     * 开户银行id
     */
    private String depositBankId;
    /**
     * 开户行名字
     */
    private String depositBankName;
    /**
     * 删除标识
     */
    private Integer dr;
    /**
     * 扩展字段01
     */
    private String ext01;
    /**
     * 扩展字段02
     */
    private String ext02;
    /**
     * 扩展字段03
     */
    private String ext03;
    /**
     * 扩展字段04
     */
    private String ext04;
    /**
     * 扩展字段05
     */
    private String ext05;
    /**
     * 扩展字段06
     */
    private String ext06;
    /**
     * 扩展字段07
     */
    private String ext07;
    /**
     * 扩展字段08
     */
    private String ext08;
    /**
     * 扩展字段09
     */
    private String ext09;
    /**
     * 扩展字段10
     */
    private String ext10;
    /**
     * 主键
     */
    private String id;
    /**
     * 是否默认
     */
    private Integer isDefault;
    /**
     * 修改时间
     */
    private String modifiedTime;
    /**
     * 修改人
     */
    private String modifier;
    /**
     * 持久化状态
     */
    private String persistStatus;
    /**
     * 供应商编码
     */
    private String supplierCode;
    /**
     * 供应商id
     */
    private String supplierId;
    /**
     * 供应商名字
     */
    private String supplierName;
    /**
     * 联系电话
     */
    private String tel;
    /**
     * 时间戳
     */
    private String ts;
}
