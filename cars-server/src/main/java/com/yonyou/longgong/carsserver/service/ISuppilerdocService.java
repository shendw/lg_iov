package com.yonyou.longgong.carsserver.service;

import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dto.SuppilerdocSelectDto;
import com.yonyou.longgong.carsserver.pojo.SuppilerdocPojo;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-11-13
 * @描述
 **/
public interface ISuppilerdocService {
    Integer insertSuppilerdoc(SuppilerdocPojo suppilerdocPojo);
    Integer delSuppilerdoc(List<SuppilerdocPojo> suppilerdocPojos);
    Integer updateSuppilerdoc(SuppilerdocPojo suppilerdocPojo);
    PageInfo<SuppilerdocPojo> selectSuppilerdoc(SuppilerdocSelectDto suppilerdocSelectDto);
    List<SuppilerdocPojo> selectSuppilerdocByExcel(SuppilerdocSelectDto suppilerdocSelectDto);
    SuppilerdocPojo selectSuppilerByKeyword(String keyword);
}
