package com.yonyou.longgong.carsserver.dto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-11-04
 * @描述
 **/
@Data
public class SimSelectDto {
    /**
     * sim卡号
     */
    private String simid;
    /**
     * sim卡串口号
     */
    private String serial;
    /**
     * 最少短信条数
     */
    private String messageLow;
    /**
     * 最多短信条数
     */
    private String messageHigh;
    /**
     * 最少流量
     */
    private String flowLow;
    /**
     * 最多流量
     */
    private String flowHigh;

    private Integer pageSize = 10;

    private Integer pageNum = 1;
}
