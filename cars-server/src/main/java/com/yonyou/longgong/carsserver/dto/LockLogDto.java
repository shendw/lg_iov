package com.yonyou.longgong.carsserver.dto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2020/4/26
 * @描述
 **/
@Data
public class LockLogDto {

    private String terminalid;

    private String begintime;

    private String endtime;

    private String pageSize = "10";

    private String pageNum = "1";
}
