package com.yonyou.longgong.carsserver.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.client.EquipmentPoolApi;
import com.yonyou.longgong.carsserver.client.U9dataClient;
import com.yonyou.longgong.carsserver.dao.CardocPojoMapper;
import com.yonyou.longgong.carsserver.dao.TerminalCarPojoMapper;
import com.yonyou.longgong.carsserver.dto.CardocSelectDto;
import com.yonyou.longgong.carsserver.entity.UserIdentity;
import com.yonyou.longgong.carsserver.enums.CarProcessStatus;
import com.yonyou.longgong.carsserver.enums.CarWorkStatus;
import com.yonyou.longgong.carsserver.enums.TerminalCarStatus;
import com.yonyou.longgong.carsserver.enums.TerminalStatus;
import com.yonyou.longgong.carsserver.example.CardocPojoExample;
import com.yonyou.longgong.carsserver.example.TerminalCarPojoExample;
import com.yonyou.longgong.carsserver.excel.ExcelVerifyCardocHandlerImpl;
import com.yonyou.longgong.carsserver.excel.ExcelVerifyCardocPojoOfMode;
import com.yonyou.longgong.carsserver.exception.BusinessException;
import com.yonyou.longgong.carsserver.pojo.CardocPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalCarPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalPojo;
import com.yonyou.longgong.carsserver.service.*;
import com.yonyou.longgong.carsserver.u9dto.U9CardocPojo;
import com.yonyou.longgong.carsserver.utils.*;
import com.yonyou.longgong.iotserver.service.IDeviceService;
import com.yonyou.longgong.iotserver.vo.DeviceDataVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author shendawei
 * @创建时间 2019-11-07
 * @描述
 **/
@Service("iCardocService")
@Slf4j
public class CardocServiceImpl implements ICardocService {

    @Autowired
    private CardocPojoMapper cardocPojoMapper;

    @Autowired
    private TerminalCarPojoMapper terminalCarPojoMapper;

    @Autowired
    private IDeviceService deviceService;

    @Autowired
    private U9dataClient u9dataClient;

    @Autowired
    private UserIdentityUtil userIdentityUtil;

    @Autowired
    private ISimService iSimService;

    @Autowired
    private ITerminalService iTerminalService;

    @Autowired
    private ITerminalCarService iTerminalCarService;

    @Autowired
    private IBindErrorService iBindErrorService;

    @Autowired
    private EquipmentPoolApi equipmentPoolApi;

    @Override
    public Integer insertCardoc(CardocPojo cardocPojo) {
        if (StringUtils.isBlank(cardocPojo.getCarno())){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        cardocPojoExample.createCriteria().andCarnoEqualTo(cardocPojo.getCarno()).andLogicalDeleted(false);
        long count = cardocPojoMapper.countByExample(cardocPojoExample);
        if (count > 0 ){
            throw new BusinessException(ResponseCode.CARNO_IS_EXIST);
        }
        //去同步u9数据，u9中有数据在插入
        U9CardocPojo u9CardocPojo = new U9CardocPojo();
        u9CardocPojo.setCarno(cardocPojo.getCarno());
        List<U9CardocPojo> u9data = u9dataClient.getU9data(Arrays.asList(u9CardocPojo));
        if (CollectionUtils.isEmpty(u9data)){
            throw new BusinessException(ResponseCode.CARDOC_U9DATA_IS_NULL);
        }
        BeanUtils.copyProperties(u9data.get(0),cardocPojo);
        //日期格式化
        if (StringUtils.isNotEmpty(cardocPojo.getInstocktime())){
            cardocPojo.setInstocktime(cardocPojo.getInstocktime().substring(0,19));
        }
        if (StringUtils.isNotEmpty(cardocPojo.getOutstocktime())){
            cardocPojo.setOutstocktime(cardocPojo.getOutstocktime().substring(0,19));
        }
        if (StringUtils.isNotEmpty(cardocPojo.getSaletime())){
            cardocPojo.setSaletime(cardocPojo.getSaletime().substring(0,19));
        }
        cardocPojo.setPkCardoc(UUIDUtils.getUniqueIdByUUId());
        cardocPojo.setDr(0L);
        cardocPojo.setVersion(1L);
        return cardocPojoMapper.insertSelective(cardocPojo);
    }

    @Override
    public Integer iotInsertCardoc(CardocPojo cardocPojo,String terminalid) {
        String carno = cardocPojo.getCarno();
        if (StringUtils.isBlank(carno)){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        cardocPojoExample.createCriteria().andCarnoEqualTo(carno).andLogicalDeleted(false);
        long count = cardocPojoMapper.countByExample(cardocPojoExample);
        if (count > 0 ){
            iBindErrorService.insertBindErrorInfo(carno,terminalid,ResponseCode.CARNO_IS_EXIST.getDesc());
            throw new BusinessException(ResponseCode.CARNO_IS_EXIST);
        }
        carno = StringUtil.formatCarno(carno);
        String regex = "^>?[A-Za-z]{3,}\\w+<?$";
        if (!carno.matches(regex)){
            iBindErrorService.insertBindErrorInfo(carno,terminalid,ResponseCode.CARNO_IS_NOTFORMAT.getDesc());
            throw new BusinessException(ResponseCode.CARNO_IS_NOTFORMAT);
        }
        carno = carno.replaceAll(">", "").trim();
        carno = carno.replaceAll("<", "").trim();
        // 根据整车编号判断车辆属于哪个组织
        String orgFlag = carno.substring(1, 2);
        if (orgFlag.equalsIgnoreCase("S")){
            cardocPojo.setPkOrg("505");
        }else if (orgFlag.equalsIgnoreCase("F")){
            cardocPojo.setPkOrg("526");
        }else {
            iBindErrorService.insertBindErrorInfo(carno,terminalid,ResponseCode.CARNO_IS_NOTHAVEORG.getDesc());
            throw new BusinessException(ResponseCode.CARNO_IS_NOTHAVEORG);
        }
        cardocPojo.setPkCardoc(UUIDUtils.getUniqueIdByUUId());
        cardocPojo.setDr(0L);
        cardocPojo.setVersion(1L);
        //补一下><
        cardocPojo.setCarno(cardocPojo.getCarno());
        cardocPojo.setWorkstatus(CarWorkStatus.STOP.getCode());
        //给设备池插入一条数据
        equipmentPoolApi.init(cardocPojo.getCarno(),terminalid,cardocPojo.getStatus().toString());
        return cardocPojoMapper.insertSelective(cardocPojo);
    }

    @Override
    public Integer delCardoc(List<CardocPojo> cardocPojos) {
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        cardocPojoExample.createCriteria().andLogicalDeleted(false).andPkCardocIn(cardocPojos.stream().map(CardocPojo::getPkCardoc).collect(Collectors.toList()));
        List<CardocPojo> existCardocs = cardocPojoMapper.selectByExample(cardocPojoExample);
        if (!CollectionUtils.isEmpty(existCardocs)){
            TerminalCarPojoExample terminalCarPojoExample = new TerminalCarPojoExample();
            for (CardocPojo cardocPojo:
                    existCardocs) {
                terminalCarPojoExample.clear();
                terminalCarPojoExample.createCriteria().andLogicalDeleted(false).andCarnoEqualTo(cardocPojo.getCarno());
                List<TerminalCarPojo> carNoTies = terminalCarPojoMapper.selectByExample(terminalCarPojoExample);
                if (!CollectionUtils.isEmpty(carNoTies)){
                    for (TerminalCarPojo carNoTie:
                            carNoTies) {
                        if (carNoTie.getEnablestate() == 0){
                            throw new BusinessException(ResponseCode.TERMINAL_CAR_CARNO_IS_BIND);
                        }
                    }
                }
            }
        }
        return cardocPojoMapper.logicalDeleteByExample(cardocPojoExample);
    }

    @Override
    public Integer updateCardoc(CardocPojo cardocPojo) {
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        cardocPojoExample.createCriteria().andCarnoEqualTo(cardocPojo.getCarno()).andLogicalDeleted(false);
        List<CardocPojo> cardocPojos = cardocPojoMapper.selectByExample(cardocPojoExample);
        if (!CollectionUtils.isEmpty(cardocPojos) && !cardocPojos.get(0).getPkCardoc().equals(cardocPojo.getPkCardoc())){
            throw new BusinessException(ResponseCode.CARNO_IS_EXIST);
        }
        CardocPojo oldCardocPojo = cardocPojoMapper.selectByPrimaryKey(cardocPojo.getPkCardoc());
        if (oldCardocPojo == null){
            throw new BusinessException(ResponseCode.CARDOC_IS_NULL);
        }
        String oldCarNo = oldCardocPojo.getCarno();
        int row = cardocPojoMapper.updateWithVersionByPrimaryKeySelective(cardocPojo.getVersion(), cardocPojo);
        if (row >0){
            TerminalCarPojo carPojo = new TerminalCarPojo();
            carPojo.setCarno(cardocPojo.getCarno());
            TerminalCarPojoExample terminalCarPojoExample = new TerminalCarPojoExample();
            terminalCarPojoExample.createCriteria().andLogicalDeleted(false).andCarnoEqualTo(oldCarNo);
            row = terminalCarPojoMapper.updateByExampleSelective(carPojo,terminalCarPojoExample,TerminalCarPojo.Column.carno);
        }
        return row;
    }

    @Override
    public PageInfo<CardocPojo> selectCarDocByCarNo(String carno){
        CardocSelectDto cardocSelectDto = new CardocSelectDto();
        cardocSelectDto.setCarno(carno);
        CardocPojoExample cardocPojoExample = selectRule(cardocSelectDto);
        return selectByCarNo(cardocPojoExample,cardocSelectDto);
    }

    private PageInfo<CardocPojo> selectByCarNo(CardocPojoExample cardocPojoExample,CardocSelectDto cardocSelectDto){
        PageHelper.startPage(cardocSelectDto.getPageNum(),cardocSelectDto.getPageSize());
        List<CardocPojo> cardocPojos = cardocPojoMapper.selectByExample(cardocPojoExample);
        PageInfo<CardocPojo> cardocPojoPageInfo = new PageInfo<>(cardocPojos);
        if (!CollectionUtils.isEmpty(cardocPojos)){
            TerminalCarPojoExample terminalCarPojoExample = new TerminalCarPojoExample();
            for (CardocPojo cardocPojo:
                    cardocPojos) {
                terminalCarPojoExample.clear();
                terminalCarPojoExample.createCriteria().andLogicalDeleted(false).andCarnoEqualTo(cardocPojo.getCarno());
                List<TerminalCarPojo> terminalCarPojos = terminalCarPojoMapper.selectByExample(terminalCarPojoExample);
                cardocPojo.setTerminalCarPojos(terminalCarPojos);
            }
        }
        return cardocPojoPageInfo;
    }

    @Override
    public PageInfo<CardocPojo> selectCardoc(CardocSelectDto cardocSelectDto) {
        CardocPojoExample cardocPojoExample = selectRule(cardocSelectDto);
        return selectByPageInfo(cardocPojoExample,cardocSelectDto);
    }

    @Override
    public PageInfo<CardocPojo> selectCardoc(CardocSelectDto cardocSelectDto,String userId,String loginName) {
        CardocPojoExample cardocPojoExample = selectRule(cardocSelectDto,userId,loginName);
        return selectByPageInfo(cardocPojoExample,cardocSelectDto);
    }

    private PageInfo<CardocPojo> selectByPageInfo(CardocPojoExample cardocPojoExample,CardocSelectDto cardocSelectDto){
        PageHelper.startPage(cardocSelectDto.getPageNum(),cardocSelectDto.getPageSize());
        List<CardocPojo> cardocPojos = cardocPojoMapper.selectByExample(cardocPojoExample);
        PageInfo<CardocPojo> cardocPojoPageInfo = new PageInfo<>(cardocPojos);
        if (!CollectionUtils.isEmpty(cardocPojos)){
            TerminalCarPojoExample terminalCarPojoExample = new TerminalCarPojoExample();
            for (CardocPojo cardocPojo:
                    cardocPojos) {
                terminalCarPojoExample.clear();
                terminalCarPojoExample.createCriteria().andLogicalDeleted(false).andCarnoEqualTo(cardocPojo.getCarno());
                List<TerminalCarPojo> terminalCarPojos = terminalCarPojoMapper.selectByExample(terminalCarPojoExample);
                cardocPojo.setTerminalCarPojos(terminalCarPojos);
                Optional<TerminalCarPojo> terminalCarPojo = terminalCarPojos.stream().filter(item -> TerminalCarStatus.DEFEAT.getCode().equals(item.getEnablestate())).findFirst();
                if (terminalCarPojo.isPresent()){
                    //绑定iot数据
                    DeviceDataVO deviceData = deviceService.getDeviceData(terminalCarPojo.get().getTerminalid());
                    if (deviceData != null){
                        cardocPojo.setLasttime(deviceData.getLast_time());
                        cardocPojo.setLocation(deviceData.getCoordinates());
                        cardocPojo.setWorktime(deviceData.getEngine_running_duration());
                    }
                }
            }
        }
        return cardocPojoPageInfo;
    }

    @Override
    public List<CardocPojo> selectCardocByExcel(CardocSelectDto cardocSelectDto,String userId,String loginName) {
        CardocPojoExample cardocPojoExample = selectRule(cardocSelectDto,userId,loginName);
        List<CardocPojo> cardocPojos = cardocPojoMapper.selectByExample(cardocPojoExample);
        if (!CollectionUtils.isEmpty(cardocPojos)){
            TerminalCarPojoExample terminalCarPojoExample = new TerminalCarPojoExample();
            for (CardocPojo cardocPojo:
                    cardocPojos) {
                terminalCarPojoExample.clear();
                terminalCarPojoExample.createCriteria().andLogicalDeleted(false).andCarnoEqualTo(cardocPojo.getCarno());
                List<TerminalCarPojo> terminalCarPojos = terminalCarPojoMapper.selectByExample(terminalCarPojoExample);
                cardocPojo.setTerminalCarPojos(terminalCarPojos);
            }
        }
        return cardocPojos;
    }

    private CardocPojoExample selectRule(CardocSelectDto cardocSelectDto){
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        CardocPojoExample.Criteria criteria = cardocPojoExample.createCriteria();
        if (StringUtils.isNotBlank(cardocSelectDto.getCarmodel())){
            criteria.andCarmodelEqualTo(cardocSelectDto.getCarmodel());
        }
        if (StringUtils.isNotBlank(cardocSelectDto.getCarno())){
            criteria.andCarnoLike("%"+cardocSelectDto.getCarno()+"%");
        }
        if (StringUtils.isNotBlank(cardocSelectDto.getMachinemodel())){
            criteria.andMachinemodelEqualTo(cardocSelectDto.getMachinemodel());
        }
        criteria.andLogicalDeleted(false);
        return cardocPojoExample;
    }

    private CardocPojoExample selectRule(CardocSelectDto cardocSelectDto, String userId, String loginName){
        UserIdentity userIdentity = userIdentityUtil.getUserIdentity(userId, loginName);
        List<String> orgs = userIdentity.getOrgs();
        String agentName = userIdentity.getAgentName();
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        CardocPojoExample.Criteria criteria = cardocPojoExample.createCriteria();
        if (!CollectionUtils.isEmpty(orgs)){
            criteria.andPkOrgIn(orgs);
            if (StringUtils.isNotBlank(cardocSelectDto.getCarmodel())){
                criteria.andCarmodelEqualTo(cardocSelectDto.getCarmodel());
            }
            if (StringUtils.isNotBlank(cardocSelectDto.getCarno())){
                criteria.andCarnoLike("%"+cardocSelectDto.getCarno()+"%");
            }
            if (StringUtils.isNotBlank(cardocSelectDto.getMachinemodel())){
                criteria.andMachinemodelEqualTo(cardocSelectDto.getMachinemodel());
            }
            criteria.andLogicalDeleted(false);
        }
        if (StringUtils.isNotBlank(agentName)){
            CardocPojoExample.Criteria criteria1 = cardocPojoExample.or();
            criteria1.andServiceAgentEqualTo(agentName);
            if (StringUtils.isNotBlank(cardocSelectDto.getCarmodel())){
                criteria1.andCarmodelEqualTo(cardocSelectDto.getCarmodel());
            }
            if (StringUtils.isNotBlank(cardocSelectDto.getCarno())){
                criteria1.andCarnoLike("%"+cardocSelectDto.getCarno()+"%");
            }
            if (StringUtils.isNotBlank(cardocSelectDto.getMachinemodel())){
                criteria1.andMachinemodelEqualTo(cardocSelectDto.getMachinemodel());
            }
            criteria1.andLogicalDeleted(false);
        }
        if (StringUtils.isNotBlank(agentName)){
            CardocPojoExample.Criteria criteria2 = cardocPojoExample.or();
            criteria2.andSaleAgentEqualTo(agentName);
            if (StringUtils.isNotBlank(cardocSelectDto.getCarmodel())){
                criteria2.andCarmodelEqualTo(cardocSelectDto.getCarmodel());
            }
            if (StringUtils.isNotBlank(cardocSelectDto.getCarno())){
                criteria2.andCarnoLike("%"+cardocSelectDto.getCarno()+"%");
            }
            if (StringUtils.isNotBlank(cardocSelectDto.getMachinemodel())){
                criteria2.andMachinemodelEqualTo(cardocSelectDto.getMachinemodel());
            }
            criteria2.andLogicalDeleted(false);
        }
        return cardocPojoExample;
    }

    @Override
    public List<String> selectCarno() {
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        cardocPojoExample.createCriteria().andLogicalDeleted(false);
        List<CardocPojo> cardocPojos = cardocPojoMapper.selectByExample(cardocPojoExample);
        if (CollectionUtils.isEmpty(cardocPojos)){
            throw new BusinessException(ResponseCode.CARDOC_IS_NULL);
        }
        return cardocPojos.stream().map(CardocPojo::getCarno).collect(Collectors.toList());
    }

    @Override
    public Integer syncCardocFromU9(List<CardocPojo> cardocPojos) {
        if (CollectionUtils.isEmpty(cardocPojos)){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        /*List<U9CardocPojo> u9CardocPojos = new ArrayList<>(cardocPojos.size());
        for (CardocPojo cardocPojo: cardocPojos) {
            U9CardocPojo u9CardocPojo = new U9CardocPojo();
            u9CardocPojo.setCarno(cardocPojo.getCarno());
            u9CardocPojos.add(u9CardocPojo);
        }
        List<U9CardocPojo> u9datas = u9dataClient.getU9data(u9CardocPojos);*/
        ArrayList<String> arrayList = new ArrayList<>(cardocPojos.size());
        cardocPojos.forEach(s ->{
            arrayList.add(s.getCarno());
        });
        List<U9CardocPojo> u9datas = cardocPojoMapper.selectByCarNo(arrayList);
        int num = 0;
        if (CollectionUtils.isEmpty(u9datas)){
            return -1;
        }
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        for (U9CardocPojo u9data:
             u9datas) {
            CardocPojo cardocPojo = new CardocPojo();
            BeanUtils.copyProperties(u9data,cardocPojo);
            //日期格式化
            if (StringUtils.isNotEmpty(cardocPojo.getInstocktime())){
                cardocPojo.setInstocktime(cardocPojo.getInstocktime().substring(0,19));
            }
            if (StringUtils.isNotEmpty(cardocPojo.getOutstocktime())){
                cardocPojo.setOutstocktime(cardocPojo.getOutstocktime().substring(0,19));
            }
            if (StringUtils.isNotEmpty(cardocPojo.getSaletime())){
                cardocPojo.setSaletime(cardocPojo.getSaletime().substring(0,19));
            }
            cardocPojo.setStatus(CarProcessStatus.SOLD.getCode());
            cardocPojoExample.clear();
            cardocPojoExample.createCriteria().andLogicalDeleted(false).andCarnoEqualTo(cardocPojo.getCarno());
            int row = cardocPojoMapper.updateByExampleSelective(cardocPojo, cardocPojoExample);
            log.info(cardocPojo.getCarno());
            num = num+row;
        }
        return num;
    }

    @Override
    public Integer syncCardocStockFromU9(List<CardocPojo> cardocPojos) {
        if (CollectionUtils.isEmpty(cardocPojos)){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        List<U9CardocPojo> u9CardocPojos = new ArrayList<>(cardocPojos.size());
        for (CardocPojo cardocPojo: cardocPojos) {
            U9CardocPojo u9CardocPojo = new U9CardocPojo();
            u9CardocPojo.setCarno(cardocPojo.getCarno());
            u9CardocPojos.add(u9CardocPojo);
        }
        List<U9CardocPojo> u9datas = u9dataClient.getCardocStock(u9CardocPojos);
        int num = 0;
        if (CollectionUtils.isEmpty(u9datas)){
            return -1;
        }
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        for (U9CardocPojo u9data:
                u9datas) {
            CardocPojo cardocPojo = new CardocPojo();
            BeanUtils.copyProperties(u9data,cardocPojo);
            //日期格式化
            if (StringUtils.isNotEmpty(cardocPojo.getInstocktime())){
                cardocPojo.setInstocktime(cardocPojo.getInstocktime().substring(0,19));
            }
            cardocPojoExample.clear();
            cardocPojoExample.createCriteria().andLogicalDeleted(false).andCarnoEqualTo(cardocPojo.getCarno());
            String saleAgent = cardocPojo.getSaleAgent();
            if (StringUtils.isNotBlank(saleAgent) && !"龙工".equals(saleAgent)){
                cardocPojo.setStatus(CarProcessStatus.STOCK.getCode());
            }
            int row = cardocPojoMapper.updateByExampleSelective(cardocPojo, cardocPojoExample);
            log.info(cardocPojo.getCarno());
            num = num+row;
        }
        return num;
    }

    @Override
    @Transactional
    public String insertCardocByExcel(ExcelVerifyCardocPojoOfMode excelVerifyCardocPojoOfMode){
        String simid = excelVerifyCardocPojoOfMode.getSimid();
        String carno = excelVerifyCardocPojoOfMode.getCarno();
        String terminalid = excelVerifyCardocPojoOfMode.getTerminalid();
        String status = excelVerifyCardocPojoOfMode.getStatus();
        String terminalVersion = excelVerifyCardocPojoOfMode.getTerminalVersion();

        Integer row = iSimService.updateSim(simid, terminalid);
        if (row == 0) {
            return "该sim卡信息没有维护，请去sim卡档案维护！";
        }
        //先判断有没有该终端id，如果有判断该终端id是否绑定车辆
        TerminalPojo existTerminalPojo = iTerminalService.selectTerminalByTerminalid(terminalid);
        if (existTerminalPojo != null) {
            TerminalCarPojo terminalCarPojo = new TerminalCarPojo();
            terminalCarPojo.setTerminalid(terminalid);
            List<TerminalCarPojo> terminalCarPojos = iTerminalCarService.selectTerminalCar(terminalCarPojo);
            //如果绑定了车辆，判断是否当前车辆
            if (!CollectionUtils.isEmpty(terminalCarPojos)) {
                if (!terminalCarPojos.get(0).getCarno().equals(carno)) {
                    return "该终端已经绑定非当前车辆！";
                }else{
                    //如果绑定的是当前车辆，说明绑过了
                    return "该终端已经绑定当前车辆！";
                }
            }
        } else {
            TerminalPojo terminalPojo = new TerminalPojo();
            terminalPojo.setTerminalid(terminalid);
            terminalPojo.setTerminalVersion(terminalVersion);
            terminalPojo.setSimid(simid);
            terminalPojo.setStatus(TerminalStatus.WORK.getCode());
            Integer insertTerminalRow = iTerminalService.insertTerminal(terminalPojo);
            if (insertTerminalRow == 0) {
                return "终端信息插入失败！";
            }
        }
        //直接去档案中找，如果存在档案，就return
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        cardocPojoExample.createCriteria().andLogicalDeleted(false).andCarnoEqualTo(carno);
        long count = cardocPojoMapper.countByExample(cardocPojoExample);
        if (count > 0){
            return "该车辆档案已存在";
        }
//        TerminalPojoExample terminalPojoExample = new TerminalPojoExample();
//        terminalPojoExample.createCriteria().andLogicalDeleted(false).andTerminalidEqualTo(terminalid);
//        count = terminalPojoMapper.countByExample(terminalPojoExample);
//        if (count > 0){
//            return "该终端ID已存在";
//        }
        //车辆流程状态以及车辆组织
        CardocPojo cardocPojo = new CardocPojo();
        cardocPojo.setCarno(carno);
        CarProcessStatus[] carProcessStatus = CarProcessStatus.values();
        for (CarProcessStatus processStatus:
        carProcessStatus) {
            if(processStatus.getDesc().equals(status)){
                cardocPojo.setStatus(processStatus.getCode());
                break;
            }
        }
        String regex = "^>?[A-Za-z]{3,}\\w+<?$";
        if (!carno.matches(regex)){
            return ResponseCode.CARNO_IS_NOTFORMAT.getDesc();
        }
        String carno4Org = carno.replaceAll(">", "").trim();
        // 根据整车编号判断车辆属于哪个组织
        String orgFlag = carno4Org.substring(1, 2);
        if (orgFlag.equalsIgnoreCase("S")){
            cardocPojo.setPkOrg("505");
        }else if (orgFlag.equalsIgnoreCase("F")){
            cardocPojo.setPkOrg("526");
        }
        cardocPojo.setPkCardoc(UUIDUtils.getUniqueIdByUUId());
        cardocPojo.setDr(0L);
        cardocPojo.setVersion(1L);
        cardocPojo.setCreationtime(DataTimeUtil.dateToStr(new Date()));
        cardocPojoMapper.insertSelective(cardocPojo);
//        TerminalPojo terminalPojo = new TerminalPojo();
//        terminalPojo.setPkTerminal(UUIDUtils.getUniqueIdByUUId());
//        terminalPojo.setTerminalid(terminalid);
//        terminalPojo.setStatus(TerminalStatus.WORK.getCode());
//        terminalPojoMapper.insertSelective(terminalPojo);
        TerminalCarPojo terminalCarPojo = new TerminalCarPojo();
        terminalCarPojo.setCarno(carno);
        terminalCarPojo.setTerminalid(terminalid);
        terminalCarPojo.setPkTerminalCar(UUIDUtils.getUniqueIdByUUId());
        terminalCarPojo.setDr(0L);
        terminalCarPojo.setVersion(1L);
        terminalCarPojo.setBindTime(DataTimeUtil.dateToStr(new Date()));
        terminalCarPojo.setEnablestate(TerminalCarStatus.DEFEAT.getCode());
        terminalCarPojoMapper.insertSelective(terminalCarPojo);
        return null;
    }

    @Override
    public ExcelImportResult<ExcelVerifyCardocPojoOfMode> checkExcel(MultipartFile carExcel) {
        try {
            if (carExcel == null) {
                throw new BusinessException(ResponseCode.SIMEXCEL_IS_NULL);
            }
            String fileName = carExcel.getOriginalFilename();
            if (!fileName.matches("^.+\\.(?i)(xls)$") && !fileName.matches("^.+\\.(?i)(xlsx)$")) {
                // 文件格式不正确
                throw new BusinessException(ResponseCode.SIMEXCEL_IS_ILLEGAL);
            }
            ImportParams params = new ImportParams();
            //params.setTitleRows(1);
            params.setNeedVerify(true);
            params.setVerifyHandler(new ExcelVerifyCardocHandlerImpl());
            params.setImportFields(new String[] { "终端", "版本","SIM卡号","流程状态","整机编号"});
            ExcelImportResult<ExcelVerifyCardocPojoOfMode> result = ExcelImportUtil.importExcelMore(carExcel.getInputStream(), ExcelVerifyCardocPojoOfMode.class, params);
            return result;
        }catch (Exception e){
            throw new BusinessException(ResponseCode.SIMEXCEL_IS_ERROR);
        }
    }

    @Override
    public Integer changeCardoc(CardocPojo cardocPojo) {
        String carno = cardocPojo.getCarno();
        if (StringUtils.isBlank(cardocPojo.getPkCardoc()) || cardocPojo.getStatus() == null || StringUtils.isBlank(carno)){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        CardocPojo oldCardoc = cardocPojoMapper.selectByPrimaryKey(cardocPojo.getPkCardoc());
        String oldCarno = oldCardoc.getCarno();
        //如果整机编号一致，更改流程状态，不一致，认为人为修改流程整机编号
        if (oldCarno.equals(carno)){
            return cardocPojoMapper.updateByPrimaryKeySelective(cardocPojo,CardocPojo.Column.status);
        }else {
            //校验下修改的整机编号有没有
            CardocPojoExample cardocPojoExample = new CardocPojoExample();
            cardocPojoExample.createCriteria().andLogicalDeleted(false).andCarnoEqualTo(carno);
            List<CardocPojo> cardocPojos = cardocPojoMapper.selectByExample(cardocPojoExample);
            if (!CollectionUtils.isEmpty(cardocPojos)){
                throw new BusinessException(ResponseCode.CARNO_IS_EXIST);
            }
            //先查找原来的绑定关系，并解绑，再绑定修改的车
            TerminalCarPojoExample terminalCarPojoExample = new TerminalCarPojoExample();
            terminalCarPojoExample.createCriteria().andLogicalDeleted(false).andCarnoEqualTo(oldCarno).andEnablestateEqualTo(TerminalCarStatus.EFFECTIVE.getCode());
            List<TerminalCarPojo> terminalCarPojos = terminalCarPojoMapper.selectByExample(terminalCarPojoExample);
            if (!CollectionUtils.isEmpty(terminalCarPojos)){
                if (terminalCarPojos.size() > 1){
                    throw new BusinessException(ResponseCode.TERMINAL_CAR_IS_TOOMANY);
                }
                TerminalCarPojo terminalCarPojo = terminalCarPojos.get(0);
                String nowTime = DataTimeUtil.dateToStr(new Date());
                terminalCarPojo.setUnbundtime(nowTime);
                terminalCarPojo.setEnablestate(TerminalCarStatus.INVALID.getCode());
                int row = terminalCarPojoMapper.updateByPrimaryKeySelective(terminalCarPojo, TerminalCarPojo.Column.unbundtime, TerminalCarPojo.Column.enablestate);
                //解绑成功后再绑定
                if (row > 0){
                    TerminalCarPojo newTerminalCar = new TerminalCarPojo();
                    newTerminalCar.setPkTerminalCar(UUIDUtils.getUniqueIdByUUId());
                    newTerminalCar.setDr(0L);
                    newTerminalCar.setVersion(1L);
                    newTerminalCar.setBindTime(nowTime);
                    newTerminalCar.setEnablestate(TerminalCarStatus.DEFEAT.getCode());
                    newTerminalCar.setCarno(carno);
                    newTerminalCar.setTerminalid(terminalCarPojo.getTerminalid());
                    row = terminalCarPojoMapper.insertSelective(newTerminalCar);
                    if (row >0){
                        return cardocPojoMapper.updateByPrimaryKeySelective(cardocPojo,CardocPojo.Column.status,CardocPojo.Column.carno);
                    }
                }
            }else {
                throw new BusinessException(ResponseCode.TERMINAL_CAR_IS_NONE);
            }
        }
        return 0;
    }

    @Override
    public Integer updateLockStatus(String carno, String lockcode) {
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        cardocPojoExample.createCriteria().andLogicalDeleted(false).andCarnoEqualTo(carno);
        CardocPojo cardocPojo = new CardocPojo();
        cardocPojo.setLockstatus(lockcode);
        return cardocPojoMapper.updateByExampleSelective(cardocPojo,cardocPojoExample,CardocPojo.Column.lockstatus);
    }
}
