package com.yonyou.longgong.carsserver.service;

import com.alibaba.fastjson.JSONObject;

/**
 * @author shendawei
 * @创建时间 2020/4/28
 * @描述
 **/
public interface ISyncTaskService {
    /**
     * 库存车转换到已售
     * @return
     */
    JSONObject getU9InfoNotSaleToSale();

    /**
     * 库存车
     * @return
     */
    JSONObject getU9InfoNotSale();

    /**
     * 已售车
     * @return
     */
    JSONObject getU9Info();
}
