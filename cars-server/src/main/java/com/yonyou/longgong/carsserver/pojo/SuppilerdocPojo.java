package com.yonyou.longgong.carsserver.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

@Data
public class SuppilerdocPojo implements Serializable {
    public static final Long IS_DELETED = Dr.IS_DELETED.value();

    public static final Long NOT_DELETED = Dr.NOT_DELETED.value();

    private String pkSupplier;

    @Excel(name = "供应商名称")
    private String name;

    @Excel(name = "供应商编码")
    private String code;

    @Excel(name = "编号")
    private String no;

    @Excel(name = "供应商关键字")
    private String keyword;

    private String creationtime;

    private String creator;

    private String modifiedtime;

    private String modifier;

    private Long dr;

    private Long version;

    private String pkOrg;

    private static final long serialVersionUID = 1L;

    public void andLogicalDeleted(boolean deleted) {
        setDr(deleted ? Dr.IS_DELETED.value() : Dr.NOT_DELETED.value());
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        SuppilerdocPojo other = (SuppilerdocPojo) that;
        return (this.getPkSupplier() == null ? other.getPkSupplier() == null : this.getPkSupplier().equals(other.getPkSupplier()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getCode() == null ? other.getCode() == null : this.getCode().equals(other.getCode()))
            && (this.getNo() == null ? other.getNo() == null : this.getNo().equals(other.getNo()))
            && (this.getKeyword() == null ? other.getKeyword() == null : this.getKeyword().equals(other.getKeyword()))
            && (this.getCreationtime() == null ? other.getCreationtime() == null : this.getCreationtime().equals(other.getCreationtime()))
            && (this.getCreator() == null ? other.getCreator() == null : this.getCreator().equals(other.getCreator()))
            && (this.getModifiedtime() == null ? other.getModifiedtime() == null : this.getModifiedtime().equals(other.getModifiedtime()))
            && (this.getModifier() == null ? other.getModifier() == null : this.getModifier().equals(other.getModifier()))
            && (this.getDr() == null ? other.getDr() == null : this.getDr().equals(other.getDr()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()))
            && (this.getPkOrg() == null ? other.getPkOrg() == null : this.getPkOrg().equals(other.getPkOrg()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getPkSupplier() == null) ? 0 : getPkSupplier().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
        result = prime * result + ((getNo() == null) ? 0 : getNo().hashCode());
        result = prime * result + ((getKeyword() == null) ? 0 : getKeyword().hashCode());
        result = prime * result + ((getCreationtime() == null) ? 0 : getCreationtime().hashCode());
        result = prime * result + ((getCreator() == null) ? 0 : getCreator().hashCode());
        result = prime * result + ((getModifiedtime() == null) ? 0 : getModifiedtime().hashCode());
        result = prime * result + ((getModifier() == null) ? 0 : getModifier().hashCode());
        result = prime * result + ((getDr() == null) ? 0 : getDr().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        result = prime * result + ((getPkOrg() == null) ? 0 : getPkOrg().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", pkSupplier=").append(pkSupplier);
        sb.append(", name=").append(name);
        sb.append(", code=").append(code);
        sb.append(", no=").append(no);
        sb.append(", keyword=").append(keyword);
        sb.append(", creationtime=").append(creationtime);
        sb.append(", creator=").append(creator);
        sb.append(", modifiedtime=").append(modifiedtime);
        sb.append(", modifier=").append(modifier);
        sb.append(", dr=").append(dr);
        sb.append(", version=").append(version);
        sb.append(", pkOrg=").append(pkOrg);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    public enum Dr {
        NOT_DELETED(new Long("0"), "未删除"),
        IS_DELETED(new Long("1"), "已删除");

        private final Long value;

        private final String name;

        Dr(Long value, String name) {
            this.value = value;
            this.name = name;
        }

        public Long getValue() {
            return this.value;
        }

        public Long value() {
            return this.value;
        }

        public String getName() {
            return this.name;
        }
    }

    public enum Column {
        pkSupplier("PK_SUPPLIER", "pkSupplier", "VARCHAR", false),
        name("NAME", "name", "VARCHAR", false),
        code("CODE", "code", "VARCHAR", false),
        no("NO", "no", "VARCHAR", false),
        keyword("KEYWORD", "keyword", "VARCHAR", false),
        creationtime("CREATIONTIME", "creationtime", "CHAR", false),
        creator("CREATOR", "creator", "VARCHAR", false),
        modifiedtime("MODIFIEDTIME", "modifiedtime", "CHAR", false),
        modifier("MODIFIER", "modifier", "VARCHAR", false),
        dr("DR", "dr", "DECIMAL", false),
        version("VERSION", "version", "DECIMAL", false),
        pkOrg("PK_ORG", "pkOrg", "VARCHAR", false);

        private static final String BEGINNING_DELIMITER = "\"";

        private static final String ENDING_DELIMITER = "\"";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}