package com.yonyou.longgong.carsserver.web;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dto.OplogSelectDto;
import com.yonyou.longgong.carsserver.pojo.OplogPojo;
import com.yonyou.longgong.carsserver.service.IOplogService;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-11-23
 * @描述
 **/
@RestController
@RequestMapping(value = "/oplog")
@Deprecated
public class OplogConntroller {

    @Autowired
    private IOplogService iOplogService;

    @PostMapping(value = "/getOplog")
    public ResponseData getOplog(OplogSelectDto oplogSelectDto){
        PageInfo<OplogPojo> oplogPojoPageInfo = iOplogService.selectOplogByPage(oplogSelectDto);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),oplogPojoPageInfo);
    }

    @RequestMapping(value = "/excelExport")
    public void export(OplogSelectDto oplogSelectDto, HttpServletResponse response){
        try{
            List<OplogPojo> oplogPojos = iOplogService.selectOplog(oplogSelectDto);
            Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams("操作日志", "操作日志"), OplogPojo.class, oplogPojos);
            // 告诉浏览器用什么软件可以打开此文件
            response.setHeader("content-Type", "application/vnd.ms-excel");
            // 下载文件的默认名称
            response.setHeader("Content-Disposition", "attachment;filename=oplog.xls");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            sheets.write(response.getOutputStream());
            return;
        }catch (IOException e){

        }
    }
}
