package com.yonyou.longgong.carsserver.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.handler.inter.IExcelModel;
import com.yonyou.longgong.carsserver.pojo.SimPojo;


public class ExcelVerifySimPojoOfMode extends SimPojo implements IExcelModel {

    @Excel(name = "错误信息")
    private String errorMsg;

    @Override
    public String getErrorMsg() {
        return errorMsg;
    }

    @Override
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

}
