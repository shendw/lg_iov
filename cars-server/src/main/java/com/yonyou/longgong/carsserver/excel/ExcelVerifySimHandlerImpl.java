/**
 * Copyright 2013-2015 JueYue (qrb.jueyue@gmail.com)
 *   
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.yonyou.longgong.carsserver.excel;

import cn.afterturn.easypoi.excel.entity.result.ExcelVerifyHandlerResult;
import cn.afterturn.easypoi.handler.inter.IExcelVerifyHandler;
import com.yonyou.longgong.carsserver.pojo.SimPojo;
import com.yonyou.longgong.carsserver.service.ISimService;
import com.yonyou.longgong.carsserver.utils.SpringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;

/**
 * sim卡excel导入校验
 * @author shendawei
 * @date
 */
@DependsOn("springUtils")
public class ExcelVerifySimHandlerImpl implements IExcelVerifyHandler<SimPojo> {

    @Autowired
    private ISimService iSimService = SpringUtils.getBean("iSimService");

    @Override
    public ExcelVerifyHandlerResult verifyHandler(SimPojo obj) {
        if (StringUtils.isNotBlank(obj.getSimid()) && StringUtils.isNotBlank(obj.getSerial())){
            long count = iSimService.countSim(obj.getSimid(), obj.getSerial());
            if (count > 0){
                StringBuilder builder = new StringBuilder();
                builder.append("已存在sim卡号或串号！");
                return new ExcelVerifyHandlerResult(false, builder.toString());
            }
        }
        return new ExcelVerifyHandlerResult(true);
    }

}
