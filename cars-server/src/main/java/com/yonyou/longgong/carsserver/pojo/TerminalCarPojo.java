package com.yonyou.longgong.carsserver.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.*;

@Data
public class TerminalCarPojo implements Serializable {
    public static final Long IS_DELETED = Dr.IS_DELETED.value();

    public static final Long NOT_DELETED = Dr.NOT_DELETED.value();

    private String pkTerminalCar;

    @Excel(name = "终端id")
    private String terminalid;

    @Excel(name = "整车编号")
    private String carno;

    @Excel(name = "绑定时间")
    private String bindTime;

    @Excel(name = "状态",replace = {"启用_0","未启用_1"})
    private Long enablestate;

    private Long dr;

    private String creationtime;

    private String creator;

    private String modifiedtime;

    private String modifier;

    private Long version;

    private String pkOrg;

    @Excel(name = "解绑时间")
    private String unbundtime;

    private static final long serialVersionUID = 1L;

    public void andLogicalDeleted(boolean deleted) {
        setDr(deleted ? Dr.IS_DELETED.value() : Dr.NOT_DELETED.value());
    }

    public enum Dr {
        NOT_DELETED(new Long("0"), "未删除"),
        IS_DELETED(new Long("1"), "已删除");

        private final Long value;

        private final String name;

        Dr(Long value, String name) {
            this.value = value;
            this.name = name;
        }

        public Long getValue() {
            return this.value;
        }

        public Long value() {
            return this.value;
        }

        public String getName() {
            return this.name;
        }
    }

    public enum Column {
        pkTerminalCar("PK_TERMINAL_CAR", "pkTerminalCar", "VARCHAR", false),
        terminalid("TERMINALID", "terminalid", "VARCHAR", false),
        carno("CARNO", "carno", "VARCHAR", false),
        bindTime("BIND_TIME", "bindTime", "CHAR", false),
        enablestate("ENABLESTATE", "enablestate", "DECIMAL", false),
        dr("DR", "dr", "DECIMAL", false),
        creationtime("CREATIONTIME", "creationtime", "CHAR", false),
        creator("CREATOR", "creator", "VARCHAR", false),
        modifiedtime("MODIFIEDTIME", "modifiedtime", "CHAR", false),
        modifier("MODIFIER", "modifier", "VARCHAR", false),
        version("VERSION", "version", "DECIMAL", false),
        pkOrg("PK_ORG", "pkOrg", "VARCHAR", false),
        unbundtime("UNBUNDTIME", "unbundtime", "CHAR", false);

        private static final String BEGINNING_DELIMITER = "\"";

        private static final String ENDING_DELIMITER = "\"";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}