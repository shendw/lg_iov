package com.yonyou.longgong.carsserver.web;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dto.CarInfoSelectDto;
import com.yonyou.longgong.carsserver.pojo.CardocPojo;
import com.yonyou.longgong.carsserver.service.ICarInfoService;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import com.yonyou.longgong.carsserver.vo.CarInfoVo;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-11-12
 * @描述
 **/
@RestController
@RequestMapping(value = "/carinfo")
public class CarInfoController {

    @Autowired
    private ICarInfoService iCarInfoService;

    @PostMapping(value = "/getCarInfo")
    public ResponseData getCarInfo(CarInfoSelectDto carInfoSelectDto,
                                   @CookieValue(value = "userId") String userId,
                                   @CookieValue(value = "_A_P_userLoginName") String loginName){
        PageInfo<CarInfoVo> carInfoVoPageInfo = iCarInfoService.selectCarInfo(carInfoSelectDto,userId,loginName);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),carInfoVoPageInfo);
    }

    @PostMapping(value = "/updateRemark")
    public ResponseData updateRemark(CardocPojo cardocPojo){
        Integer row = iCarInfoService.updateRemark(cardocPojo);
        if (row>0){
            return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(),ResponseCode.SUCCESS_UPDATE.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @RequestMapping(value = "/excelExport")
    public void export(CarInfoSelectDto carInfoSelectDto, HttpServletResponse response,
                       @CookieValue(value = "userId") String userId,
                       @CookieValue(value = "_A_P_userLoginName") String loginName){
        try{
            List<CarInfoVo> carInfoVos = iCarInfoService.selectCarInfoByExcel(carInfoSelectDto,userId,loginName);
            Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams("车辆信息查询", "车辆信息查询"), CarInfoVo.class, carInfoVos);
            // 告诉浏览器用什么软件可以打开此文件
            response.setHeader("content-Type", "application/vnd.ms-excel");
            // 下载文件的默认名称
            response.setHeader("Content-Disposition", "attachment;filename=simdoc.xls");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            sheets.write(response.getOutputStream());
            return;
        }catch (IOException e){

        }
    }
}
