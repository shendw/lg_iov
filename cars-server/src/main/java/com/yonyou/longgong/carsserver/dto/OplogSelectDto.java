package com.yonyou.longgong.carsserver.dto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-11-23
 * @描述
 **/
@Data
@Deprecated
public class OplogSelectDto {
    /**
     * 终端id
     */
    private String terminalid;
    /**
     * 通讯方式
     */
    private String channel;
    /**
     * 开始时间
     */
    private String starttime;
    /**
     * 结束时间
     */
    private String endtime;

    private Integer pageSize = 10;

    private Integer pageNum = 1;
}
