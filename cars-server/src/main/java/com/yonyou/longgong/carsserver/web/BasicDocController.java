package com.yonyou.longgong.carsserver.web;

import com.yonyou.longgong.carsserver.occdto.OccOrgDto;
import com.yonyou.longgong.carsserver.service.IBasicDocService;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-12-05
 * @描述
 **/
@RestController
@RequestMapping("/basicdoc")
public class BasicDocController {

    @Autowired
    private IBasicDocService iBasicDocService;

    @RequestMapping("/getOrgs")
    public ResponseData getOrgs(HttpServletRequest request, @CookieValue(value = "userId") String userId){
        List<OccOrgDto> orgs = iBasicDocService.getOrgs();
        String host = request.getRemoteHost();
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),orgs);
    }

    @RequestMapping("/getOrgsByUser")
    public ResponseData getOrgsByUser(@CookieValue(value = "userId") String userId,
                                      @CookieValue(value = "_A_P_userLoginName") String loginName){
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),iBasicDocService.getOrgsByUser(userId,loginName));
    }
}
