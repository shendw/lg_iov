package com.yonyou.longgong.carsserver.exception;

import com.yonyou.longgong.carsserver.utils.ResponseCode;
import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-11-04
 * @描述
 **/
@Data
public class BusinessException extends RuntimeException {

    private ResponseCode responseCode;

    public BusinessException(Throwable cause) {
        super(cause);
    }

    public BusinessException(ResponseCode responseCode) {
        this.responseCode = responseCode;
    }
}