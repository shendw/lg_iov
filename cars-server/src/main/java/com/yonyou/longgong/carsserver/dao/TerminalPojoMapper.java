package com.yonyou.longgong.carsserver.dao;

import com.yonyou.longgong.carsserver.example.TerminalPojoExample;
import com.yonyou.longgong.carsserver.pojo.TerminalPojo;
import java.util.List;
import java.util.Map;

import com.yonyou.longgong.carsserver.vo.TerminalFinishedVO;
import org.apache.ibatis.annotations.Param;

public interface TerminalPojoMapper {
    long countByExample(TerminalPojoExample example);

    int deleteWithVersionByExample(@Param("version") Long version, @Param("example") TerminalPojoExample example);

    int deleteByExample(TerminalPojoExample example);

    int deleteWithVersionByPrimaryKey(@Param("version") Long version, @Param("key") String pkTerminal);

    int deleteByPrimaryKey(String pkTerminal);

    int insert(TerminalPojo record);

    int insertSelective(@Param("record") TerminalPojo record, @Param("selective") TerminalPojo.Column ... selective);

    List<TerminalPojo> selectByExample(TerminalPojoExample example);

    TerminalPojo selectByPrimaryKey(String pkTerminal);

    TerminalPojo selectByPrimaryKeyWithLogicalDelete(@Param("pkTerminal") String pkTerminal, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    int updateWithVersionByExample(@Param("version") Long version, @Param("record") TerminalPojo record, @Param("example") TerminalPojoExample example);

    int updateWithVersionByExampleSelective(@Param("version") Long version, @Param("record") TerminalPojo record, @Param("example") TerminalPojoExample example, @Param("selective") TerminalPojo.Column ... selective);

    int updateByExampleSelective(@Param("record") TerminalPojo record, @Param("example") TerminalPojoExample example, @Param("selective") TerminalPojo.Column ... selective);

    int updateByExample(@Param("record") TerminalPojo record, @Param("example") TerminalPojoExample example);

    int updateWithVersionByPrimaryKey(@Param("version") Long version, @Param("record") TerminalPojo record);

    int updateWithVersionByPrimaryKeySelective(@Param("version") Long version, @Param("record") TerminalPojo record, @Param("selective") TerminalPojo.Column ... selective);

    int updateByPrimaryKeySelective(@Param("record") TerminalPojo record, @Param("selective") TerminalPojo.Column ... selective);

    int updateByPrimaryKey(TerminalPojo record);

    int logicalDeleteByExample(@Param("example") TerminalPojoExample example);

    int logicalDeleteWithVersionByExample(@Param("version") Long version, @Param("nextVersion") Long nextVersion, @Param("example") TerminalPojoExample example);

    int logicalDeleteByPrimaryKey(String pkTerminal);

    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Long version, @Param("nextVersion") Long nextVersion, @Param("key") String pkTerminal);

    int updateLastTime(@Param("lasttime") String lasttime,@Param("terminalid") String terminalid);

    int updateLastLoginTime(@Param("lastlogintime") String lastlogintime,@Param("terminalid") String terminalid);

    List<TerminalFinishedVO> select4Finished(Map<String,Object> param);
}