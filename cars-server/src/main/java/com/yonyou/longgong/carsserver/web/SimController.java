package com.yonyou.longgong.carsserver.web;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dto.SimSelectDto;
import com.yonyou.longgong.carsserver.excel.ExcelVerifySimPojoOfMode;
import com.yonyou.longgong.carsserver.pojo.SimPojo;
import com.yonyou.longgong.carsserver.service.ISimService;
import com.yonyou.longgong.carsserver.service.ITerminalService;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author shendawei
 * @创建时间 2019-11-05
 * @描述
 **/
@RestController
@RequestMapping(value = "/sim")
@Slf4j
public class SimController {

    @Autowired
    private ISimService iSimService;

    @Autowired
    private ITerminalService iTerminalService;

    @PostMapping(value = "/getSimList")
    public ResponseData getSimList(SimSelectDto simSelectDto){
        PageInfo<SimPojo> simPojoPageInfo = iSimService.selectSim(simSelectDto);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),simPojoPageInfo);
    }

    @PostMapping(value = "/addSimList")
    public ResponseData addSim(SimPojo simPojo){
        Integer row = iSimService.insertSim(Arrays.asList(simPojo));
        if (row>0){
            return ResponseData.result(ResponseCode.SUCCESS_INSERT.getCode(),ResponseCode.SUCCESS_INSERT.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @PostMapping(value = "/updateSim")
    public ResponseData updateSim(SimPojo simPojo){
        Integer row = iSimService.updateSim(simPojo);
        if (row>0){
            return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(),ResponseCode.SUCCESS_UPDATE.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @PostMapping(value = "/delSim")
    public ResponseData delSim(@RequestBody List<SimPojo> simPojos){
        Integer row = iSimService.delSim(simPojos);
        if (row>0){
            return ResponseData.result(ResponseCode.SUCCESS_DELETE.getCode(),ResponseCode.SUCCESS_DELETE.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @PostMapping(value = "/excelInsert")
    public void batchInsertSim(@RequestParam("simExcel") MultipartFile simExcel, HttpServletResponse response){
        try {
            ExcelImportResult<ExcelVerifySimPojoOfMode> result = iSimService.checkExcel(simExcel);
            if (!CollectionUtils.isEmpty(result.getFailList())){
                // 告诉浏览器用什么软件可以打开此文件
                response.setHeader("content-Type", "application/vnd.ms-excel");
                // 下载文件的默认名称
                response.setHeader("Content-Disposition", "attachment;filename=simdoc.xls");
                response.setHeader("Status","50001");
                response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
                response.setHeader("Access-Control-Expose-Headers", "Status");
                List<ExcelVerifySimPojoOfMode> failList = result.getFailList();
                Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams("sim卡档案错误信息", "sim卡档案错误信息"), ExcelVerifySimPojoOfMode.class, failList);
                sheets.write(response.getOutputStream());
            }else {
                List<ExcelVerifySimPojoOfMode> resultList = result.getList();
                List<SimPojo> simPojos = resultList.stream().map(item -> new SimPojo(item.getSimid(),item.getSerial(),item.getStartTime())).collect(Collectors.toList());
                //为了防止SQL语句超出长度出错，分成几次插入
                Integer row = 0;
                if(simPojos.size()<=500){
                    row = iSimService.insertSimByExcel(simPojos);
                }else{
                    int times = (int)Math.ceil( simPojos.size()/500.0 );
                    for(int i=0; i<times; i++ ){
                        row = iSimService.insertSimByExcel(simPojos.subList (i*500,Math.min((i+1)*500, simPojos.size())));
                    }
                }
                if (row>0){
                    response.setHeader("Status","20000");
                    response.setHeader("Access-Control-Expose-Headers", "Status");
                }else {
                    response.setHeader("Status","50000");
                    response.setHeader("Access-Control-Expose-Headers", "Status");
                }
            }
        }catch (IOException e){
            response.setHeader("Status","50000");
            response.setHeader("Access-Control-Expose-Headers", "Status");
        }
    }

    @PostMapping(value = "/matchSim")
    public ResponseData matchSim(String imsi,String originTerminalid,String terminalVersion){
        log.info("收到绑定终端请求,imsi:{},originTerminalid:{},terminalVersion:{}",imsi,originTerminalid,terminalVersion);
        SimPojo simPojo = iSimService.matchSim(imsi);
        if (simPojo == null){
            return ResponseData.result(ResponseCode.SIMMATCH_IS_ERROR.getCode(),ResponseCode.SIMMATCH_IS_ERROR.getDesc());
        }else {
            HashMap<String, String> resultMap = new HashMap<>(2);
            @NotNull String simid = simPojo.getSimid();
            resultMap.put("simid",simid);
            if (originTerminalid.endsWith("00000000")){
                String terminalid = iSimService.bindSim(imsi, originTerminalid, terminalVersion, simPojo);
                resultMap.put("terminalid", terminalid);
                return ResponseData.result(ResponseCode.SUCCESS_MATCH.getCode(),resultMap,ResponseCode.SUCCESS_MATCH.getDesc());
            }else {
                resultMap.put("terminalid", originTerminalid);
                return ResponseData.result(ResponseCode.SIMMATCH_IS_EXIST.getCode(),resultMap,ResponseCode.SIMMATCH_IS_EXIST.getDesc());
            }
        }
    }

    @RequestMapping(value = "/excelExport")
    public ResponseData export(SimSelectDto simSelectDto, HttpServletResponse response){
        try{
            List<SimPojo> simPojos = iSimService.selectSimByExcel(simSelectDto);
            Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams("sim卡档案", "sim卡档案"), SimPojo.class, simPojos);
            // 告诉浏览器用什么软件可以打开此文件
            response.setHeader("content-Type", "application/vnd.ms-excel");
            // 下载文件的默认名称
            response.setHeader("Content-Disposition", "attachment;filename=simdoc.xls");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            sheets.write(response.getOutputStream());
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }catch (IOException e){
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }
}
