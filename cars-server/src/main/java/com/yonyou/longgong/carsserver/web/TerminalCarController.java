package com.yonyou.longgong.carsserver.web;

import com.yonyou.longgong.carsserver.pojo.TerminalCarPojo;
import com.yonyou.longgong.carsserver.service.ITerminalCarService;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-11-07
 * @描述
 **/
@RestController
@RequestMapping(value = "/terminalcar")
public class TerminalCarController {

    @Autowired
    private ITerminalCarService iTerminalCarService;

    @PostMapping(value = "/getTerminalCar")
    public ResponseData getTerminalCar(TerminalCarPojo terminalCarPojo){
        List<TerminalCarPojo> terminalCarPojos = iTerminalCarService.selectTerminalCar(terminalCarPojo);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),terminalCarPojos,ResponseCode.SUCCESS.getDesc());
    }

    @PostMapping(value = "/addTerminalCar")
    public ResponseData addTerminalCar(TerminalCarPojo terminalCarPojo){
        Integer row = iTerminalCarService.insertTerminalCar(terminalCarPojo);
        if (row>0){
            return ResponseData.result(ResponseCode.SUCCESS_INSERT.getCode(),ResponseCode.SUCCESS_INSERT.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @PostMapping(value = "/updateTerminalCar")
    public ResponseData updateTerminalCar(TerminalCarPojo terminalCarPojo){
        Integer row = iTerminalCarService.unbundTerminalCar(terminalCarPojo.getPkTerminalCar());
        if (row>0){
            return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(),ResponseCode.SUCCESS_UPDATE.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

}
