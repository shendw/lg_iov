package com.yonyou.longgong.carsserver.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import lombok.Data;

@Data
public class LockLogPojo implements Serializable {
    public static final Long IS_DELETED = Dr.IS_DELETED.value();

    public static final Long NOT_DELETED = Dr.NOT_DELETED.value();

    private String pkLocklog;

    private String terminalid;

    private String channel;

    private String createtime;

    private String record;

    private String operator;

    private Long dr;

    private Long version;

    private static final long serialVersionUID = 1L;

    public void andLogicalDeleted(boolean deleted) {
        setDr(deleted ? Dr.IS_DELETED.value() : Dr.NOT_DELETED.value());
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        LockLogPojo other = (LockLogPojo) that;
        return (this.getPkLocklog() == null ? other.getPkLocklog() == null : this.getPkLocklog().equals(other.getPkLocklog()))
            && (this.getTerminalid() == null ? other.getTerminalid() == null : this.getTerminalid().equals(other.getTerminalid()))
            && (this.getChannel() == null ? other.getChannel() == null : this.getChannel().equals(other.getChannel()))
            && (this.getCreatetime() == null ? other.getCreatetime() == null : this.getCreatetime().equals(other.getCreatetime()))
            && (this.getRecord() == null ? other.getRecord() == null : this.getRecord().equals(other.getRecord()))
            && (this.getOperator() == null ? other.getOperator() == null : this.getOperator().equals(other.getOperator()))
            && (this.getDr() == null ? other.getDr() == null : this.getDr().equals(other.getDr()))
            && (this.getVersion() == null ? other.getVersion() == null : this.getVersion().equals(other.getVersion()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getPkLocklog() == null) ? 0 : getPkLocklog().hashCode());
        result = prime * result + ((getTerminalid() == null) ? 0 : getTerminalid().hashCode());
        result = prime * result + ((getChannel() == null) ? 0 : getChannel().hashCode());
        result = prime * result + ((getCreatetime() == null) ? 0 : getCreatetime().hashCode());
        result = prime * result + ((getRecord() == null) ? 0 : getRecord().hashCode());
        result = prime * result + ((getOperator() == null) ? 0 : getOperator().hashCode());
        result = prime * result + ((getDr() == null) ? 0 : getDr().hashCode());
        result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", IS_DELETED=").append(IS_DELETED);
        sb.append(", NOT_DELETED=").append(NOT_DELETED);
        sb.append(", pkLocklog=").append(pkLocklog);
        sb.append(", terminalid=").append(terminalid);
        sb.append(", channel=").append(channel);
        sb.append(", createtime=").append(createtime);
        sb.append(", record=").append(record);
        sb.append(", operator=").append(operator);
        sb.append(", dr=").append(dr);
        sb.append(", version=").append(version);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    public enum Dr {
        NOT_DELETED(new Long("0"), "未删除"),
        IS_DELETED(new Long("1"), "已删除");

        private final Long value;

        private final String name;

        Dr(Long value, String name) {
            this.value = value;
            this.name = name;
        }

        public Long getValue() {
            return this.value;
        }

        public Long value() {
            return this.value;
        }

        public String getName() {
            return this.name;
        }
    }

    public enum Column {
        pkLocklog("PK_LOCKLOG", "pkLocklog", "VARCHAR", false),
        terminalid("TERMINALID", "terminalid", "VARCHAR", false),
        channel("CHANNEL", "channel", "VARCHAR", false),
        createtime("CREATETIME", "createtime", "CHAR", false),
        record("RECORD", "record", "VARCHAR", false),
        operator("OPERATOR", "operator", "VARCHAR", false),
        dr("DR", "dr", "DECIMAL", false),
        version("VERSION", "version", "DECIMAL", false);

        private static final String BEGINNING_DELIMITER = "\"";

        private static final String ENDING_DELIMITER = "\"";

        private final String column;

        private final boolean isColumnNameDelimited;

        private final String javaProperty;

        private final String jdbcType;

        public String value() {
            return this.column;
        }

        public String getValue() {
            return this.column;
        }

        public String getJavaProperty() {
            return this.javaProperty;
        }

        public String getJdbcType() {
            return this.jdbcType;
        }

        Column(String column, String javaProperty, String jdbcType, boolean isColumnNameDelimited) {
            this.column = column;
            this.javaProperty = javaProperty;
            this.jdbcType = jdbcType;
            this.isColumnNameDelimited = isColumnNameDelimited;
        }

        public String desc() {
            return this.getEscapedColumnName() + " DESC";
        }

        public String asc() {
            return this.getEscapedColumnName() + " ASC";
        }

        public static Column[] excludes(Column ... excludes) {
            ArrayList<Column> columns = new ArrayList<>(Arrays.asList(Column.values()));
            if (excludes != null && excludes.length > 0) {
                columns.removeAll(new ArrayList<>(Arrays.asList(excludes)));
            }
            return columns.toArray(new Column[]{});
        }

        public String getEscapedColumnName() {
            if (this.isColumnNameDelimited) {
                return new StringBuilder().append(BEGINNING_DELIMITER).append(this.column).append(ENDING_DELIMITER).toString();
            } else {
                return this.column;
            }
        }

        public String getAliasedEscapedColumnName() {
            return this.getEscapedColumnName();
        }
    }
}