package com.yonyou.longgong.carsserver.web;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dto.CardocSelectDto;
import com.yonyou.longgong.carsserver.dto.TerminalSelectDto;
import com.yonyou.longgong.carsserver.excel.ExcelVerifyCardocPojoOfMode;
import com.yonyou.longgong.carsserver.excel.ExcelVerifyTerminalPojoOfMode;
import com.yonyou.longgong.carsserver.pojo.CardocPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalPojo;
import com.yonyou.longgong.carsserver.service.ITerminalService;
import com.yonyou.longgong.carsserver.utils.DataTimeUtil;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import com.yonyou.longgong.carsserver.vo.TerminalFinishedVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-11-06
 * @描述
 **/
@RestController
@RequestMapping(value = "/terminal")
@Slf4j
public class TerminalController {

    @Autowired
    private ITerminalService iTerminalService;


    @PostMapping(value = "/getTerminal")
    public ResponseData getTerminal(TerminalSelectDto terminalSelectDto,
                                    @CookieValue(value = "userId") String userId,
                                    @CookieValue(value = "_A_P_userLoginName") String loginName){
        PageInfo<TerminalPojo> terminalPojoPageInfo = iTerminalService.selectTerminalsTest(terminalSelectDto,userId,loginName);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),terminalPojoPageInfo);
    }

    @PostMapping(value = "/getTerminalFinished")
    public ResponseData getTerminalFinished(TerminalSelectDto terminalSelectDto,
                                           @CookieValue(value = "userId") String userId,
                                           @CookieValue(value = "_A_P_userLoginName") String loginName){
        PageInfo<TerminalFinishedVO> terminalFinishedVOPageInfo = iTerminalService.selectTerminalsFinished(terminalSelectDto, userId, loginName);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),terminalFinishedVOPageInfo);
    }

    @PostMapping(value = "/getTerminalProcess")
    public ResponseData getTerminalProcess(TerminalSelectDto terminalSelectDto,
                                           @CookieValue(value = "userId") String userId,
                                           @CookieValue(value = "_A_P_userLoginName") String loginName){
        PageInfo<TerminalPojo> terminalPojoPageInfo = iTerminalService.selectTerminalsProcess(terminalSelectDto,userId,loginName);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),terminalPojoPageInfo);
    }

    @PostMapping(value = "/updateTerminal")
    public ResponseData updateTerminal(TerminalPojo terminalPojo){
        Integer row = iTerminalService.updateTermianals(terminalPojo);
        if (row > 0 ){
            return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(),ResponseCode.SUCCESS_UPDATE.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR_UPDATE.getCode(),ResponseCode.ERROR_UPDATE.getDesc());
        }
    }

    @PostMapping(value = "/terminalFinish")
    public ResponseData terminalFinish(TerminalPojo terminalPojo){
        terminalPojo.setFinishedtime(DataTimeUtil.dateToStr(new Date()));
        Integer row = iTerminalService.changeStatus(terminalPojo);
        if (row > 0 ){
            return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(),ResponseCode.SUCCESS_UPDATE.getDesc());
        }else {
            return ResponseData.result(ResponseCode.ERROR_UPDATE.getCode(),ResponseCode.ERROR_UPDATE.getDesc());
        }
    }

    @RequestMapping(value = "/excelExport")
    public void export(TerminalSelectDto terminalSelectDto, HttpServletResponse response,
                       @CookieValue(value = "userId") String userId,
                       @CookieValue(value = "_A_P_userLoginName") String loginName){
        try{
            List<TerminalPojo> terminalPojos = iTerminalService.selectTerminalsByExcel(terminalSelectDto,userId,loginName);
            Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams("终端档案", "终端档案"), TerminalPojo.class, terminalPojos);
            // 告诉浏览器用什么软件可以打开此文件
            response.setHeader("content-Type", "application/vnd.ms-excel");
            // 下载文件的默认名称
            response.setHeader("Content-Disposition", "attachment;filename=simdoc.xls");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            sheets.write(response.getOutputStream());
            return;
        }catch (IOException e){

        }
    }

    @PostMapping(value = "/excelInsert")
    public void batchInsertCar(@RequestParam("terminalExcel") MultipartFile terminalExcel, HttpServletResponse response){
        try {
            log.info("begin terminalExcel insert!");
            long startTime = System.currentTimeMillis();
            ExcelImportResult<ExcelVerifyTerminalPojoOfMode> excelImportResult = iTerminalService.checkExcel(terminalExcel);
            long endTime = System.currentTimeMillis();
            log.info("terminalExcel check end fail.size {},success.size {},time {}"
                    ,excelImportResult.getFailList().size()
                    ,excelImportResult.getList().size()
                    ,endTime-startTime);
            if (!CollectionUtils.isEmpty(excelImportResult.getFailList())){
                // 告诉浏览器用什么软件可以打开此文件
                response.setHeader("content-Type", "application/vnd.ms-excel");
                // 下载文件的默认名称
                response.setHeader("Content-Disposition", "attachment;filename=simdoc.xls");
                response.setHeader("Status","50001");
                response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
                response.setHeader("Access-Control-Expose-Headers", "Status");
                List<ExcelVerifyTerminalPojoOfMode> failList = excelImportResult.getFailList();
                Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams(null, "终端错误信息"), ExcelVerifyCardocPojoOfMode.class, failList);
                sheets.write(response.getOutputStream());
            }else {
                response.setHeader("Status","20000");
                response.setHeader("Access-Control-Expose-Headers", "Status");
            }
        }catch (IOException e){
            response.setHeader("Status","50000");
            response.setHeader("Access-Control-Expose-Headers", "Status");
        }
    }
}
