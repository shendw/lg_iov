package com.yonyou.longgong.carsserver.service;

import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dto.CarInfoSelectDto;
import com.yonyou.longgong.carsserver.pojo.CardocPojo;
import com.yonyou.longgong.carsserver.vo.CarInfoVo;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-11-11
 * @描述
 **/
public interface ICarInfoService {
    /**
     *@描述   查询车辆信息，分页
     *@参数 车辆信息查询条件
     *@返回值 分页信息
     *@创建人 sdw
     *@创建时间 2019-12-04
     *@修改人和其它信息
     */
    PageInfo<CarInfoVo> selectCarInfo(CarInfoSelectDto carInfoSelectDto,String userId,String loginName);

    /**
     * 查询车辆信息，不分页，给导出使用，查询条件和selectCarInfo()一致
     * @param carInfoSelectDto
     * @return
     */
    List<CarInfoVo> selectCarInfoByExcel(CarInfoSelectDto carInfoSelectDto,String userId,String loginName);

    /**
     * 编辑备注接口
     * @param cardocPojo
     * @return
     */
    Integer updateRemark(CardocPojo cardocPojo);
}
