package com.yonyou.longgong.carsserver.service.impl;

import com.yonyou.longgong.carsserver.client.OccBaseClient;
import com.yonyou.longgong.carsserver.client.OccCmptClient;
import com.yonyou.longgong.carsserver.dto.OrgDto;
import com.yonyou.longgong.carsserver.entity.UserIdentity;
import com.yonyou.longgong.carsserver.exception.BusinessException;
import com.yonyou.longgong.carsserver.occdto.FeignPage;
import com.yonyou.longgong.carsserver.occdto.OccOrgDto;
import com.yonyou.longgong.carsserver.occdto.OccPersonInfoDto;
import com.yonyou.longgong.carsserver.occdto.OccUserDto;
import com.yonyou.longgong.carsserver.service.IBasicDocService;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.UserIdentityUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-12-05
 * @描述
 **/
@Service("iBasicDocService")
public class BasicDocServiceImpl implements IBasicDocService {

    @Autowired
    private OccBaseClient occBaseClient;

    @Autowired
    private OccCmptClient occCmptClient;

    @Override
    public List<OccOrgDto> getOrgs() {
        FeignPage<OccOrgDto> organization = occBaseClient.getOrgList("10000", "0", "organization");
        if (organization != null){
            List<OccOrgDto> orgDtoList = organization.getContent();
            if (CollectionUtils.isEmpty(orgDtoList)){
                return null;
            }

        }
        return null;
    }

    @Override
    public List<OrgDto> getOrgsByUser(String userId,String loginName) {
        //根据组织过滤车辆 1.先判断是渠道商还是人员
        if (StringUtils.isBlank(userId) || StringUtils.isBlank(loginName)){
            throw new BusinessException(ResponseCode.USERID_IS_NULL);
        }
        OccUserDto userInfoByloginName = occCmptClient.getUserInfoByloginName(loginName);
        if (userInfoByloginName == null){
            throw new BusinessException(ResponseCode.USERINFO_IS_NULL);
        }
        String personId = userInfoByloginName.getBusinessPersonId();
        String agentName = userInfoByloginName.getChannelCustomerName();
        String agentCode = userInfoByloginName.getChannelCustomerCode();
        String agentId = userInfoByloginName.getChannelCustomerId();
        //如果有渠道商信息，直接返回
        if (StringUtils.isNotBlank(agentId)){
            return Arrays.asList(new OrgDto(agentName,agentCode,agentId));
        }
        if (StringUtils.isBlank(personId) && StringUtils.isBlank(agentName)){
            throw new BusinessException(ResponseCode.IDENTITY_IS_NULL);
        }
        // 天呐，根据渠道商和人员信息分别添加过滤条件
        //存在人员信息 1.获取人员组织 2.获取该组织下的所有组织
        List<OrgDto> orgs =null;
        if (StringUtils.isNotEmpty(personId)){
            OccPersonInfoDto[] personInfo = occBaseClient.getPersonInfo(personId, "person");
            String organizationName = personInfo[0].getOrganizationName();
            String organizationCode = personInfo[0].getOrganizationCode();
            String organizationId = personInfo[0].getOrganizationId();
            FeignPage<OccOrgDto> organizationPage = occBaseClient.getOrgList("10000", "0", "organization");
            List<OccOrgDto> occOrgDtoList = organizationPage.getContent();
            orgs = new ArrayList<>();
            orgs.add(new OrgDto(organizationName,organizationCode,organizationId));
            List<String> tempOrgs = new ArrayList<>();
            tempOrgs.add(organizationCode);
            while (true){
                ArrayList<String> temp = new ArrayList<>();
                for (OccOrgDto occOrgDto:
                        occOrgDtoList) {
                    if (tempOrgs.contains(occOrgDto.getParentCode()) && occOrgDto.getIsEnable().equals("1")){
                        String name = occOrgDto.getName();
                        String code = occOrgDto.getCode();
                        String id = occOrgDto.getId();
                        temp.add(code);
                        orgs.add(new OrgDto(name,code,id));
                    }
                }
                if (CollectionUtils.isEmpty(temp)){
                    break;
                }else {
                    tempOrgs = temp;
                }
            }
        }
        return orgs;
    }
}
