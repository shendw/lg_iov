package com.yonyou.longgong.carsserver.excel;

import cn.afterturn.easypoi.excel.entity.result.ExcelVerifyHandlerResult;
import cn.afterturn.easypoi.handler.inter.IExcelVerifyHandler;
import com.yonyou.longgong.carsserver.enums.TerminalStatus;
import com.yonyou.longgong.carsserver.pojo.CardocPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalCarPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalPojo;
import com.yonyou.longgong.carsserver.service.ICardocService;
import com.yonyou.longgong.carsserver.service.ISimService;
import com.yonyou.longgong.carsserver.service.ITerminalCarService;
import com.yonyou.longgong.carsserver.service.ITerminalService;
import com.yonyou.longgong.carsserver.utils.SpringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-12-02
 * @描述
 **/
@Slf4j
public class ExcelVerifyCardocHandlerImpl implements IExcelVerifyHandler<ExcelVerifyCardocPojoOfMode> {

    @Autowired
    private ICardocService iCardocService = SpringUtils.getBean("iCardocService");

    @Override
    public ExcelVerifyHandlerResult verifyHandler(ExcelVerifyCardocPojoOfMode excelVerifyCardocPojoOfMode) {
        String msg = iCardocService.insertCardocByExcel(excelVerifyCardocPojoOfMode);
        if (msg != null) {
            StringBuilder builder = new StringBuilder();
            builder.append(msg);
            return new ExcelVerifyHandlerResult(false, builder.toString());
        }
        CardocPojo cardocPojo = new CardocPojo();
        cardocPojo.setCarno(excelVerifyCardocPojoOfMode.getCarno());
        try{
            Integer row = iCardocService.syncCardocFromU9(Arrays.asList(cardocPojo));
        }catch (Exception ex){
            log.warn("syncCardocFromU9 fail, carno : {}",excelVerifyCardocPojoOfMode.getCarno());
        }
//            if (row == 0){
//                StringBuilder builder = new StringBuilder();
//                builder.append("同步数据时出错！请手动同步该车数据！");
//                return new ExcelVerifyHandlerResult(false, builder.toString());
//            }
        return new ExcelVerifyHandlerResult(true);
    }
}
