package com.yonyou.longgong.carsserver.dao;

import com.yonyou.longgong.carsserver.example.CardocPojoExample;
import com.yonyou.longgong.carsserver.pojo.CardocPojo;
import java.util.List;
import java.util.Map;

import com.yonyou.longgong.carsserver.u9dto.U9CardocPojo;
import com.yonyou.longgong.carsserver.vo.CarInfoVo;
import com.yonyou.longgong.carsserver.vo.CarProcessVO;
import com.yonyou.longgong.iotserver.vo.DeviceDetailVO;
import org.apache.ibatis.annotations.Param;

public interface CardocPojoMapper {
    long countByExample(CardocPojoExample example);

    int deleteWithVersionByExample(@Param("version") Long version, @Param("example") CardocPojoExample example);

    int deleteByExample(CardocPojoExample example);

    int deleteWithVersionByPrimaryKey(@Param("version") Long version, @Param("key") String pkCardoc);

    int deleteByPrimaryKey(String pkCardoc);

    int insert(CardocPojo record);

    int insertSelective(@Param("record") CardocPojo record, @Param("selective") CardocPojo.Column ... selective);

    List<CardocPojo> selectByExample(CardocPojoExample example);

    CardocPojo selectByPrimaryKey(String pkCardoc);

    CardocPojo selectByPrimaryKeyWithLogicalDelete(@Param("pkCardoc") String pkCardoc, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    int updateWithVersionByExample(@Param("version") Long version, @Param("record") CardocPojo record, @Param("example") CardocPojoExample example);

    int updateWithVersionByExampleSelective(@Param("version") Long version, @Param("record") CardocPojo record, @Param("example") CardocPojoExample example, @Param("selective") CardocPojo.Column ... selective);

    int updateByExampleSelective(@Param("record") CardocPojo record, @Param("example") CardocPojoExample example, @Param("selective") CardocPojo.Column ... selective);

    int updateByExample(@Param("record") CardocPojo record, @Param("example") CardocPojoExample example);

    int updateWithVersionByPrimaryKey(@Param("version") Long version, @Param("record") CardocPojo record);

    int updateWithVersionByPrimaryKeySelective(@Param("version") Long version, @Param("record") CardocPojo record, @Param("selective") CardocPojo.Column ... selective);

    int updateByPrimaryKeySelective(@Param("record") CardocPojo record, @Param("selective") CardocPojo.Column ... selective);

    int updateByPrimaryKey(CardocPojo record);

    int logicalDeleteByExample(@Param("example") CardocPojoExample example);

    int logicalDeleteWithVersionByExample(@Param("version") Long version, @Param("nextVersion") Long nextVersion, @Param("example") CardocPojoExample example);

    int logicalDeleteByPrimaryKey(String pkCardoc);

    int logicalDeleteWithVersionByPrimaryKey(@Param("version") Long version, @Param("nextVersion") Long nextVersion, @Param("key") String pkCardoc);

    List<CarInfoVo> select4CarInfo(Map<String,Object> param);

    List<CarProcessVO> select4CarProcess(Map<String,Object> param);

    DeviceDetailVO selectterminalInfoById(@Param("id")String id);

    List<U9CardocPojo> selectByCarNo(List<String> carNo);
}