package com.yonyou.longgong.carsserver.example;

import com.yonyou.longgong.carsserver.pojo.BindErrorInfoPojo;
import java.util.ArrayList;
import java.util.List;

public class BindErrorInfoPojoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BindErrorInfoPojoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPkBinderrorIsNull() {
            addCriterion("PK_BINDERROR is null");
            return (Criteria) this;
        }

        public Criteria andPkBinderrorIsNotNull() {
            addCriterion("PK_BINDERROR is not null");
            return (Criteria) this;
        }

        public Criteria andPkBinderrorEqualTo(String value) {
            addCriterion("PK_BINDERROR =", value, "pkBinderror");
            return (Criteria) this;
        }

        public Criteria andPkBinderrorNotEqualTo(String value) {
            addCriterion("PK_BINDERROR <>", value, "pkBinderror");
            return (Criteria) this;
        }

        public Criteria andPkBinderrorGreaterThan(String value) {
            addCriterion("PK_BINDERROR >", value, "pkBinderror");
            return (Criteria) this;
        }

        public Criteria andPkBinderrorGreaterThanOrEqualTo(String value) {
            addCriterion("PK_BINDERROR >=", value, "pkBinderror");
            return (Criteria) this;
        }

        public Criteria andPkBinderrorLessThan(String value) {
            addCriterion("PK_BINDERROR <", value, "pkBinderror");
            return (Criteria) this;
        }

        public Criteria andPkBinderrorLessThanOrEqualTo(String value) {
            addCriterion("PK_BINDERROR <=", value, "pkBinderror");
            return (Criteria) this;
        }

        public Criteria andPkBinderrorLike(String value) {
            addCriterion("PK_BINDERROR like", value, "pkBinderror");
            return (Criteria) this;
        }

        public Criteria andPkBinderrorNotLike(String value) {
            addCriterion("PK_BINDERROR not like", value, "pkBinderror");
            return (Criteria) this;
        }

        public Criteria andPkBinderrorIn(List<String> values) {
            addCriterion("PK_BINDERROR in", values, "pkBinderror");
            return (Criteria) this;
        }

        public Criteria andPkBinderrorNotIn(List<String> values) {
            addCriterion("PK_BINDERROR not in", values, "pkBinderror");
            return (Criteria) this;
        }

        public Criteria andPkBinderrorBetween(String value1, String value2) {
            addCriterion("PK_BINDERROR between", value1, value2, "pkBinderror");
            return (Criteria) this;
        }

        public Criteria andPkBinderrorNotBetween(String value1, String value2) {
            addCriterion("PK_BINDERROR not between", value1, value2, "pkBinderror");
            return (Criteria) this;
        }

        public Criteria andTerminalidIsNull() {
            addCriterion("TERMINALID is null");
            return (Criteria) this;
        }

        public Criteria andTerminalidIsNotNull() {
            addCriterion("TERMINALID is not null");
            return (Criteria) this;
        }

        public Criteria andTerminalidEqualTo(String value) {
            addCriterion("TERMINALID =", value, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidNotEqualTo(String value) {
            addCriterion("TERMINALID <>", value, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidGreaterThan(String value) {
            addCriterion("TERMINALID >", value, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidGreaterThanOrEqualTo(String value) {
            addCriterion("TERMINALID >=", value, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidLessThan(String value) {
            addCriterion("TERMINALID <", value, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidLessThanOrEqualTo(String value) {
            addCriterion("TERMINALID <=", value, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidLike(String value) {
            addCriterion("TERMINALID like", value, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidNotLike(String value) {
            addCriterion("TERMINALID not like", value, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidIn(List<String> values) {
            addCriterion("TERMINALID in", values, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidNotIn(List<String> values) {
            addCriterion("TERMINALID not in", values, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidBetween(String value1, String value2) {
            addCriterion("TERMINALID between", value1, value2, "terminalid");
            return (Criteria) this;
        }

        public Criteria andTerminalidNotBetween(String value1, String value2) {
            addCriterion("TERMINALID not between", value1, value2, "terminalid");
            return (Criteria) this;
        }

        public Criteria andCarnoIsNull() {
            addCriterion("CARNO is null");
            return (Criteria) this;
        }

        public Criteria andCarnoIsNotNull() {
            addCriterion("CARNO is not null");
            return (Criteria) this;
        }

        public Criteria andCarnoEqualTo(String value) {
            addCriterion("CARNO =", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotEqualTo(String value) {
            addCriterion("CARNO <>", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoGreaterThan(String value) {
            addCriterion("CARNO >", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoGreaterThanOrEqualTo(String value) {
            addCriterion("CARNO >=", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoLessThan(String value) {
            addCriterion("CARNO <", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoLessThanOrEqualTo(String value) {
            addCriterion("CARNO <=", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoLike(String value) {
            addCriterion("CARNO like", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotLike(String value) {
            addCriterion("CARNO not like", value, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoIn(List<String> values) {
            addCriterion("CARNO in", values, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotIn(List<String> values) {
            addCriterion("CARNO not in", values, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoBetween(String value1, String value2) {
            addCriterion("CARNO between", value1, value2, "carno");
            return (Criteria) this;
        }

        public Criteria andCarnoNotBetween(String value1, String value2) {
            addCriterion("CARNO not between", value1, value2, "carno");
            return (Criteria) this;
        }

        public Criteria andErrorinfoIsNull() {
            addCriterion("ERRORINFO is null");
            return (Criteria) this;
        }

        public Criteria andErrorinfoIsNotNull() {
            addCriterion("ERRORINFO is not null");
            return (Criteria) this;
        }

        public Criteria andErrorinfoEqualTo(String value) {
            addCriterion("ERRORINFO =", value, "errorinfo");
            return (Criteria) this;
        }

        public Criteria andErrorinfoNotEqualTo(String value) {
            addCriterion("ERRORINFO <>", value, "errorinfo");
            return (Criteria) this;
        }

        public Criteria andErrorinfoGreaterThan(String value) {
            addCriterion("ERRORINFO >", value, "errorinfo");
            return (Criteria) this;
        }

        public Criteria andErrorinfoGreaterThanOrEqualTo(String value) {
            addCriterion("ERRORINFO >=", value, "errorinfo");
            return (Criteria) this;
        }

        public Criteria andErrorinfoLessThan(String value) {
            addCriterion("ERRORINFO <", value, "errorinfo");
            return (Criteria) this;
        }

        public Criteria andErrorinfoLessThanOrEqualTo(String value) {
            addCriterion("ERRORINFO <=", value, "errorinfo");
            return (Criteria) this;
        }

        public Criteria andErrorinfoLike(String value) {
            addCriterion("ERRORINFO like", value, "errorinfo");
            return (Criteria) this;
        }

        public Criteria andErrorinfoNotLike(String value) {
            addCriterion("ERRORINFO not like", value, "errorinfo");
            return (Criteria) this;
        }

        public Criteria andErrorinfoIn(List<String> values) {
            addCriterion("ERRORINFO in", values, "errorinfo");
            return (Criteria) this;
        }

        public Criteria andErrorinfoNotIn(List<String> values) {
            addCriterion("ERRORINFO not in", values, "errorinfo");
            return (Criteria) this;
        }

        public Criteria andErrorinfoBetween(String value1, String value2) {
            addCriterion("ERRORINFO between", value1, value2, "errorinfo");
            return (Criteria) this;
        }

        public Criteria andErrorinfoNotBetween(String value1, String value2) {
            addCriterion("ERRORINFO not between", value1, value2, "errorinfo");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("REMARK is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("REMARK =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("REMARK <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("REMARK >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("REMARK >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("REMARK <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("REMARK <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("REMARK like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("REMARK not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("REMARK in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("REMARK not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("REMARK between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("REMARK not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andIshandleIsNull() {
            addCriterion("ISHANDLE is null");
            return (Criteria) this;
        }

        public Criteria andIshandleIsNotNull() {
            addCriterion("ISHANDLE is not null");
            return (Criteria) this;
        }

        public Criteria andIshandleEqualTo(String value) {
            addCriterion("ISHANDLE =", value, "ishandle");
            return (Criteria) this;
        }

        public Criteria andIshandleNotEqualTo(String value) {
            addCriterion("ISHANDLE <>", value, "ishandle");
            return (Criteria) this;
        }

        public Criteria andIshandleGreaterThan(String value) {
            addCriterion("ISHANDLE >", value, "ishandle");
            return (Criteria) this;
        }

        public Criteria andIshandleGreaterThanOrEqualTo(String value) {
            addCriterion("ISHANDLE >=", value, "ishandle");
            return (Criteria) this;
        }

        public Criteria andIshandleLessThan(String value) {
            addCriterion("ISHANDLE <", value, "ishandle");
            return (Criteria) this;
        }

        public Criteria andIshandleLessThanOrEqualTo(String value) {
            addCriterion("ISHANDLE <=", value, "ishandle");
            return (Criteria) this;
        }

        public Criteria andIshandleLike(String value) {
            addCriterion("ISHANDLE like", value, "ishandle");
            return (Criteria) this;
        }

        public Criteria andIshandleNotLike(String value) {
            addCriterion("ISHANDLE not like", value, "ishandle");
            return (Criteria) this;
        }

        public Criteria andIshandleIn(List<String> values) {
            addCriterion("ISHANDLE in", values, "ishandle");
            return (Criteria) this;
        }

        public Criteria andIshandleNotIn(List<String> values) {
            addCriterion("ISHANDLE not in", values, "ishandle");
            return (Criteria) this;
        }

        public Criteria andIshandleBetween(String value1, String value2) {
            addCriterion("ISHANDLE between", value1, value2, "ishandle");
            return (Criteria) this;
        }

        public Criteria andIshandleNotBetween(String value1, String value2) {
            addCriterion("ISHANDLE not between", value1, value2, "ishandle");
            return (Criteria) this;
        }

        public Criteria andDrIsNull() {
            addCriterion("DR is null");
            return (Criteria) this;
        }

        public Criteria andDrIsNotNull() {
            addCriterion("DR is not null");
            return (Criteria) this;
        }

        public Criteria andDrEqualTo(Long value) {
            addCriterion("DR =", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrNotEqualTo(Long value) {
            addCriterion("DR <>", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrGreaterThan(Long value) {
            addCriterion("DR >", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrGreaterThanOrEqualTo(Long value) {
            addCriterion("DR >=", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrLessThan(Long value) {
            addCriterion("DR <", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrLessThanOrEqualTo(Long value) {
            addCriterion("DR <=", value, "dr");
            return (Criteria) this;
        }

        public Criteria andDrIn(List<Long> values) {
            addCriterion("DR in", values, "dr");
            return (Criteria) this;
        }

        public Criteria andDrNotIn(List<Long> values) {
            addCriterion("DR not in", values, "dr");
            return (Criteria) this;
        }

        public Criteria andDrBetween(Long value1, Long value2) {
            addCriterion("DR between", value1, value2, "dr");
            return (Criteria) this;
        }

        public Criteria andDrNotBetween(Long value1, Long value2) {
            addCriterion("DR not between", value1, value2, "dr");
            return (Criteria) this;
        }

        public Criteria andHandlerIsNull() {
            addCriterion("HANDLER is null");
            return (Criteria) this;
        }

        public Criteria andHandlerIsNotNull() {
            addCriterion("HANDLER is not null");
            return (Criteria) this;
        }

        public Criteria andHandlerEqualTo(String value) {
            addCriterion("HANDLER =", value, "handler");
            return (Criteria) this;
        }

        public Criteria andHandlerNotEqualTo(String value) {
            addCriterion("HANDLER <>", value, "handler");
            return (Criteria) this;
        }

        public Criteria andHandlerGreaterThan(String value) {
            addCriterion("HANDLER >", value, "handler");
            return (Criteria) this;
        }

        public Criteria andHandlerGreaterThanOrEqualTo(String value) {
            addCriterion("HANDLER >=", value, "handler");
            return (Criteria) this;
        }

        public Criteria andHandlerLessThan(String value) {
            addCriterion("HANDLER <", value, "handler");
            return (Criteria) this;
        }

        public Criteria andHandlerLessThanOrEqualTo(String value) {
            addCriterion("HANDLER <=", value, "handler");
            return (Criteria) this;
        }

        public Criteria andHandlerLike(String value) {
            addCriterion("HANDLER like", value, "handler");
            return (Criteria) this;
        }

        public Criteria andHandlerNotLike(String value) {
            addCriterion("HANDLER not like", value, "handler");
            return (Criteria) this;
        }

        public Criteria andHandlerIn(List<String> values) {
            addCriterion("HANDLER in", values, "handler");
            return (Criteria) this;
        }

        public Criteria andHandlerNotIn(List<String> values) {
            addCriterion("HANDLER not in", values, "handler");
            return (Criteria) this;
        }

        public Criteria andHandlerBetween(String value1, String value2) {
            addCriterion("HANDLER between", value1, value2, "handler");
            return (Criteria) this;
        }

        public Criteria andHandlerNotBetween(String value1, String value2) {
            addCriterion("HANDLER not between", value1, value2, "handler");
            return (Criteria) this;
        }

        public Criteria andHsndletimeIsNull() {
            addCriterion("HSNDLETIME is null");
            return (Criteria) this;
        }

        public Criteria andHsndletimeIsNotNull() {
            addCriterion("HSNDLETIME is not null");
            return (Criteria) this;
        }

        public Criteria andHsndletimeEqualTo(String value) {
            addCriterion("HSNDLETIME =", value, "hsndletime");
            return (Criteria) this;
        }

        public Criteria andHsndletimeNotEqualTo(String value) {
            addCriterion("HSNDLETIME <>", value, "hsndletime");
            return (Criteria) this;
        }

        public Criteria andHsndletimeGreaterThan(String value) {
            addCriterion("HSNDLETIME >", value, "hsndletime");
            return (Criteria) this;
        }

        public Criteria andHsndletimeGreaterThanOrEqualTo(String value) {
            addCriterion("HSNDLETIME >=", value, "hsndletime");
            return (Criteria) this;
        }

        public Criteria andHsndletimeLessThan(String value) {
            addCriterion("HSNDLETIME <", value, "hsndletime");
            return (Criteria) this;
        }

        public Criteria andHsndletimeLessThanOrEqualTo(String value) {
            addCriterion("HSNDLETIME <=", value, "hsndletime");
            return (Criteria) this;
        }

        public Criteria andHsndletimeLike(String value) {
            addCriterion("HSNDLETIME like", value, "hsndletime");
            return (Criteria) this;
        }

        public Criteria andHsndletimeNotLike(String value) {
            addCriterion("HSNDLETIME not like", value, "hsndletime");
            return (Criteria) this;
        }

        public Criteria andHsndletimeIn(List<String> values) {
            addCriterion("HSNDLETIME in", values, "hsndletime");
            return (Criteria) this;
        }

        public Criteria andHsndletimeNotIn(List<String> values) {
            addCriterion("HSNDLETIME not in", values, "hsndletime");
            return (Criteria) this;
        }

        public Criteria andHsndletimeBetween(String value1, String value2) {
            addCriterion("HSNDLETIME between", value1, value2, "hsndletime");
            return (Criteria) this;
        }

        public Criteria andHsndletimeNotBetween(String value1, String value2) {
            addCriterion("HSNDLETIME not between", value1, value2, "hsndletime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("CREATETIME is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("CREATETIME is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(String value) {
            addCriterion("CREATETIME =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(String value) {
            addCriterion("CREATETIME <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(String value) {
            addCriterion("CREATETIME >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(String value) {
            addCriterion("CREATETIME >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(String value) {
            addCriterion("CREATETIME <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(String value) {
            addCriterion("CREATETIME <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLike(String value) {
            addCriterion("CREATETIME like", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotLike(String value) {
            addCriterion("CREATETIME not like", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<String> values) {
            addCriterion("CREATETIME in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<String> values) {
            addCriterion("CREATETIME not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(String value1, String value2) {
            addCriterion("CREATETIME between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(String value1, String value2) {
            addCriterion("CREATETIME not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andTsIsNull() {
            addCriterion("TS is null");
            return (Criteria) this;
        }

        public Criteria andTsIsNotNull() {
            addCriterion("TS is not null");
            return (Criteria) this;
        }

        public Criteria andTsEqualTo(String value) {
            addCriterion("TS =", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsNotEqualTo(String value) {
            addCriterion("TS <>", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsGreaterThan(String value) {
            addCriterion("TS >", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsGreaterThanOrEqualTo(String value) {
            addCriterion("TS >=", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsLessThan(String value) {
            addCriterion("TS <", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsLessThanOrEqualTo(String value) {
            addCriterion("TS <=", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsLike(String value) {
            addCriterion("TS like", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsNotLike(String value) {
            addCriterion("TS not like", value, "ts");
            return (Criteria) this;
        }

        public Criteria andTsIn(List<String> values) {
            addCriterion("TS in", values, "ts");
            return (Criteria) this;
        }

        public Criteria andTsNotIn(List<String> values) {
            addCriterion("TS not in", values, "ts");
            return (Criteria) this;
        }

        public Criteria andTsBetween(String value1, String value2) {
            addCriterion("TS between", value1, value2, "ts");
            return (Criteria) this;
        }

        public Criteria andTsNotBetween(String value1, String value2) {
            addCriterion("TS not between", value1, value2, "ts");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }

        public Criteria andLogicalDeleted(boolean deleted) {
            return deleted ? andDrEqualTo(BindErrorInfoPojo.Dr.IS_DELETED.value()) : andDrNotEqualTo(BindErrorInfoPojo.Dr.IS_DELETED.value());
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}