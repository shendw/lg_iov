package com.yonyou.longgong.carsserver.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author yushilin
 * @version 1.0
 * @description: 基础设备池
 * @date 2021/5/7 14:59
 */
@Data
public class BaseEquipmentPool implements Serializable {


    /**
     * 整机编码
     */
    private String  code;


    /**
     * 整机名称
     */
    private String  name;

    /**
     * 产品线
     */
    private String  productLine;


    //产品分类
    private String  productCategory;

    /**
     * 商品编码
     */
    private String  goods;




    //产品名称
    private String  product;


    /**
     * 规格型号
     */
    private String  specificationsModel;

    /**
     * 库存状态
     */
    private String  storageStatus;


    /**
     * 生命周期状态
     */
    private String  lifeCycleSataus;



    /**
     * 生产组织
     */
    private String  productionOrganization;


    /**
     * 入库日期
     */
    private Date  wareDate;


    /**
     * 调拨日期
     */
    private Date transferDate;


    /**
     * 销售组织
     */
    private String  salesOrganization;

    /**
     * 样机收货时间
     */
    private Date  receiveDate;


    /**
     * 工单号
     */
    private String  workNum;


    /**
     * 订单类型
     */
    private String  orderType;

    /**
     * 生产日期
     */
    private Date  productDate;

    /**
     * 存储地点（调入仓库)
     */
    private String  storagePlace;

    /**
     * 存储地点（调入仓库编码)
     */
    private String storagePlaceCode;


    /**
     * 是否到货
     */
    private String  isArrival;

    /**
     * 样机采购Ids
     */
    private String  specimentids;

    //是否冬储
    private String  isStorage;

    //调拨申请单单号
    private String  speNum;

    /**
     * 是否生成返利
     */
    private String  isGenerateRebate;



    //    *********************车联网信息***************************
//    @JsonManagedReference
//    @Where(clause = "DR = 0")
//    @OneToOne(cascade = CascadeType.ALL, mappedBy = "equipmentPool")
//    private  EquipmentPoolCarInternet eqCarInternet;



    /**
     * 终端
     */
    private String  terminal;



    /**
     * 终端Id
     */
    private String  terminalId;

    /**
     * 终端状态
     */
    private String terminalStatus;

    /**
     * 终端最近更新时间
     */
    private Date terminalLastupdateTime;

    /**
     * SIM卡号
     */
    private String SIM;


    /**
     * 是否有GPS
     */
    private String isGPS;



    //    *********************销售信息***************************

//    @JsonManagedReference
//    @Where(clause = "DR = 0")
//    @OneToOne(cascade = CascadeType.ALL, mappedBy = "equipmentPool")
//    private  EquipmentPoolSale eqSale;


    /**
     * 所属代理商
     */
    private String agent;


    /**
     * 销售日期
     */
    private Date saleDate;

    /**
     * 销售方式
     */
    private String  saleMode;

    /**
     * 销售考核卡编号
     */
    private String  saleAssessmentNum;

    /**
     * 销售金额
     */
    private BigDecimal saleMoney;

    /**
     * 首付款金额
     */
    private BigDecimal  downPayment;

    /**
     * 已还款金额
     */
    private BigDecimal  rePayment;

    /**
     * 逾期金额
     */
    private BigDecimal  overdueAmount;

    /**
     * 客户类型
     */
    private String  customerType;

    /**
     * 客户编码
     */
    private String  customerNum;


    /**
     * 客户名称
     */
    private String  customerName;

    /**
     * 所属行业
     */
    private String  industry;

    /**
     * 联系人
     */
    private String  contacts;

    /**
     * 联系电话
     */
    private String  contactsTel;

    /**
     * 客户地址
     */
    private String  customerAddress;


    //    *********************售后信息***************************
//    @JsonManagedReference
//    @Where(clause = "DR = 0")
//    @OneToOne(cascade = CascadeType.ALL, mappedBy = "equipmentPool")
//    private  EquipmentPoolAfterSale eqAfterSaleter;

    /**
     * 保修开始时间
     */
    private Date wsarrantyStartTime;

    /**
     * 保修结束时间
     */
    private Date  wsarrantyEndTime;

    /**
     * 所属维修中心
     */
    private String  maintenanceCenter;

    /**
     * 保修卡编号
     */
    private String  warrantyNum;

    /**
     * 二级网点
     */
    private String  secondBranch;

    /**
     * 维修状态
     */
    private String  maintenanceStatus;


    /**
     * 保养状态
     */
    private String  maintainStatus;



    private String  saleAgentCode;

    private String  saleAgentName;

    private String  saleCustomerCode;

    private String  saleCustomerName;

    //机主
    private String  machineOwner;


    //证件号码
    private String  credentialsNo;


    //    *********************装配清单***************************



    private String  ext01;

    private String  ext02;

    private String  ext03;

    private Date  ext04;

    private String  ext05;

    private String  ext06;

    private String  ext07;

    private String  ext08;

    private String  ext09;

    private String  ext10;

    private String  ext11;

    private String  ext12;

    private String  ext13;

    private String  ext14;

    private String  ext15;

    private String  ext16;

    private String  ext17;

    private String  ext18;

    private String  ext19;

    private String  ext20;


}
