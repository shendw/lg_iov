package com.yonyou.longgong.carsserver.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.client.OccCmptClient;
import com.yonyou.longgong.carsserver.dao.CardocPojoMapper;
import com.yonyou.longgong.carsserver.dto.CardocSelectDto;
import com.yonyou.longgong.carsserver.entity.UserIdentity;
import com.yonyou.longgong.carsserver.enums.CarProcessStatus;
import com.yonyou.longgong.carsserver.enums.TerminalCarStatus;
import com.yonyou.longgong.carsserver.example.CardocPojoExample;
import com.yonyou.longgong.carsserver.exception.BusinessException;
import com.yonyou.longgong.carsserver.occdto.UserPrivilegeDto;
import com.yonyou.longgong.carsserver.pojo.CardocPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalCarPojo;
import com.yonyou.longgong.carsserver.service.ICarProcessService;
import com.yonyou.longgong.carsserver.service.ITerminalCarService;
import com.yonyou.longgong.carsserver.utils.DataTimeUtil;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import com.yonyou.longgong.carsserver.utils.UserIdentityUtil;
import com.yonyou.longgong.carsserver.vo.CarProcessVO;
import com.yonyou.longgong.iotserver.service.IOrderCodeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * @author shendawei
 * @创建时间 2019-12-18
 * @描述
 **/
@Service("iCarProcessService")
public class CarProcessServiceImpl implements ICarProcessService {

    @Autowired
    private CardocPojoMapper cardocPojoMapper;

    @Autowired
    private ITerminalCarService iTerminalCarService;

    @Autowired
    private IOrderCodeService orderCodeService;

    @Autowired
    private UserIdentityUtil userIdentityUtil;

    @Autowired
    private OccCmptClient occCmptClient;

    @Override
    public Integer carOutline(String carno,String loginName) {
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        cardocPojoExample.createCriteria()
                .andLogicalDeleted(false)
                .andStatusEqualTo(CarProcessStatus.AUTOBIND.getCode())
                .andCarnoEqualTo(carno);
        List<CardocPojo> cardocPojos = cardocPojoMapper.selectByExample(cardocPojoExample);
        if (CollectionUtils.isEmpty(cardocPojos)){
            throw new BusinessException(ResponseCode.CARDOCSTATUS_ERROR);
        }
        CardocPojo cardocPojo = cardocPojos.get(0);
        cardocPojo.setStatus(CarProcessStatus.OUTLINE.getCode());
        cardocPojo.setOutlineer(loginName);
        cardocPojo.setOutlinetime(DataTimeUtil.dateToStr(new Date()));
        return cardocPojoMapper.updateWithVersionByPrimaryKeySelective(cardocPojo.getVersion(),cardocPojo);
    }

    @Override
    public Integer carTest(String carno,String loginName) {
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        cardocPojoExample.createCriteria()
                .andLogicalDeleted(false)
                .andStatusIn(Arrays.asList(CarProcessStatus.OUTLINE.getCode(),CarProcessStatus.TEST.getCode()))
                .andCarnoEqualTo(carno);
        List<CardocPojo> cardocPojos = cardocPojoMapper.selectByExample(cardocPojoExample);
        if (CollectionUtils.isEmpty(cardocPojos)){
            throw new BusinessException(ResponseCode.CARDOCSTATUS_ERROR);
        }
        TerminalCarPojo terminalCarPojo = new TerminalCarPojo();
        terminalCarPojo.setCarno(carno);
        terminalCarPojo.setEnablestate(TerminalCarStatus.EFFECTIVE.getCode());
        List<TerminalCarPojo> terminalCarPojos = iTerminalCarService.selectTerminalCar(terminalCarPojo);
        if (CollectionUtils.isEmpty(terminalCarPojos)){
            throw new BusinessException(ResponseCode.TERMINAL_CAR_CARNO_IS_NULL);
        }
        ResponseData responseData = orderCodeService.execGet47H(terminalCarPojos.get(0).getTerminalid(),false);
        CardocPojo cardocPojo = cardocPojos.get(0);
        cardocPojo.setDebuger(loginName);
        cardocPojo.setDebugtime(DataTimeUtil.dateToStr(new Date()));
        if (responseData.getStatus().equals(ResponseCode.SUCCESS.getCode())){
            cardocPojo.setStatus(CarProcessStatus.INLINE.getCode());
            cardocPojo.setInstocktime(DataTimeUtil.dateToStr(new Date()));
            return cardocPojoMapper.updateWithVersionByPrimaryKeySelective(cardocPojo.getVersion(), cardocPojo);
        }else {
            if (cardocPojo.getStatus().equals(CarProcessStatus.OUTLINE.getCode())){
                cardocPojo.setStatus(CarProcessStatus.TEST.getCode());
                cardocPojoMapper.updateWithVersionByPrimaryKeySelective(cardocPojo.getVersion(), cardocPojo);
            }
            return 0;
        }
    }

    @Override
    public ResponseData selectCardocOutline(CardocSelectDto cardocSelectDto, String userId, String loginName) {
        ArrayList<Long> status = new ArrayList<>();
        status.add(CarProcessStatus.AUTOBIND.getCode());
        return selectCardoc(cardocSelectDto,userId,loginName,status);
    }

    @Override
    public ResponseData selectCardocDebug(CardocSelectDto cardocSelectDto, String userId, String loginName) {
        ArrayList<Long> status = new ArrayList<>();
        status.add(CarProcessStatus.OUTLINE.getCode());
        status.add(CarProcessStatus.TEST.getCode());
        return selectCardoc(cardocSelectDto,userId,loginName,status);
    }

    private ResponseData selectCardoc(CardocSelectDto cardocSelectDto, String userId, String loginName,ArrayList<Long> status) {
        UserIdentity userIdentity = userIdentityUtil.getUserIdentity(userId, loginName);
        List<UserPrivilegeDto> userPrivilegesByUserId = occCmptClient.getUserPrivilegesByUserId(userId);
        if (CollectionUtils.isEmpty(userPrivilegesByUserId)){
            throw new BusinessException(ResponseCode.ROLE_IS_NOT_IOV);
        }
        List<String> orgs = userIdentity.getOrgs();
        if (CollectionUtils.isEmpty(orgs)){
            throw new BusinessException(ResponseCode.ROLE_IS_NOT_IOV);
        }
        HashMap<String, Object> paramMap = new HashMap<>(16);
        paramMap.put("orgs",orgs);
        String carNo = cardocSelectDto.getCarno();
        paramMap.put("carno",carNo);
        paramMap.put("status",status);
        PageHelper.startPage(cardocSelectDto.getPageNum(),cardocSelectDto.getPageSize());
        List<CarProcessVO> cardocPojos = cardocPojoMapper.select4CarProcess(paramMap);
        PageInfo<CarProcessVO> cardocPojoPageInfo = new PageInfo<>(cardocPojos);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),cardocPojoPageInfo,ResponseCode.SUCCESS.getDesc());
    }
}
