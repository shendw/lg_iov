package com.yonyou.longgong.carsserver.occdto;



import lombok.Getter;
import lombok.Setter;
import javax.annotation.Generated;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;


@Getter
@Setter
@Generated(value = "com.yonyou.ocm.util.codegenerator.CodeGenerator")
public class EquipmentPoolDto{



    @Size(max = 50)
    private String  code;

    @Size(max = 50)
    private String  name;

    /**
     * 产品线
     */
    private String  productLineId;
    private String  productLineCode;
    private String  productLineName;



    /**
     * 产品分类(其实是商品分类)
     */
    private String  productCategoryId;
    private String  productCategoryCode;
    private String  productCategoryName;


    /**
     * 商品
     */
    private String  goodsId;
    private String  goodsCode;
    private String  goodsName;



    /**
     * 产品名称
     */
    private String  productId;
    private String  productCode;
    private String  productName;
    private String  productEntrustPrice;



    /**
     * 规格型号
     */
    @Size(max = 50)
    private String  specificationsModel;

    /**
     * 库存状态
     */
    @Size(max = 50)
    private String  storageStatus;

    /**
     * 生命周期状态
     */
    @Size(max = 50)
    private String  lifeCycleSataus;

    /**
     * 生产组织
     */
    @Size(max = 50)
    private String  productionOrganizationName;
    private String  productionOrganizationCode;
    private String  productionOrganizationId;


    /**
     * 入库日期
     */
    private Date wareDate;

    /**
     * 调拨日期
     */
    private Date  transferDate;

    /**
     * 销售组织
     */
    @Size(max = 50)
    private String  salesOrganizationName;
    private String  salesOrganizationCode;
    private String  salesOrganizationId;

    /**
     * 样机收货时间
     */
    private Date  receiveDate;

    /**
     * 工单号
     */
    @Size(max = 50)
    private String  workNum;

    /**
     * 订单类型
     */
    @Size(max = 50)
    private String  orderType;


    /**
     * 生产日期
     */
    private Date  productDate;

    /**
     * 存储地点（调入仓库)
     */
    @Size(max = 50)
    private String  storagePlaceId;
    private String  storagePlaceCode;
    private String  storagePlaceName;



//    *********************车联网信息***************************

    /**
     * 终端
     */
    @Size(max = 50)
    private String  terminal;

    /**
     * 终端Id
     */
    @Size(max = 50)
    private String  terminalId;

    /**
     * 终端状态
     */
    @Size(max = 50)
    private String  terminalStatus;

    /**
     * 终端最近更新时间
     */
    private Date  terminalLastupdateTime;

    /**
     * SIM卡号
     */
    private String  SIM;


    /**
     * 是否有GPS
     */
    private String  isGPS;

    //    *********************销售信息***************************

    /**
     * 所属代理商
     */
    private String  agentName;
    private String  agentCode;
    private String  agentId;

    /**
     * 销售日期
     */
    private Date saleDate;

    /**
     * 销售方式
     */
    private String  saleModeId;
    private String  saleModeCode;
    private String  saleModeName;

    /**
     * 销售考核卡编号
     */
    private String  saleAssessmentNum;

    /**
     * 销售金额
     */
    private BigDecimal saleMoney;

    /**
     * 首付款金额
     */
    private BigDecimal  downPayment;

    /**
     * 已还款金额
     */
    private BigDecimal  rePayment;

    /**
     * 逾期金额
     */
    private BigDecimal  overdueAmount;

    /**
     * 客户类型
     */
    private String  customerTypeId;
    private String  customerTypeCode;
    private String  customerTypeName;

    /**
     * 客户编码
     */

    private String  customerNumId;
    private String  customerNumCode;
    private String  customerNumName;
    
    /**
     * 客户名称
     */
    private String  customerName;

    /**
     * 所属行业
     */
    private String  industryId;
    private String  industryCode;
    private String  industryName;

    /**
     * 联系人
     */
    private String  contacts;

    /**
     * 联系电话
     */
    private String  contactsTel;

    /**
     * 客户地址
     */
    private String  customerAddress;



    //    *********************售后信息***************************

    /**
     * 保修开始时间
     */
    private Date  wsarrantyStartTime;

    /**
     * 保修结束时间
     */
    private Date  wsarrantyEndTime;

    /**
     * 维修中心
     */
    @Size(max = 50)
    private String  maintenanceCenterId;
    private String  maintenanceCenterName;
    private String  maintenanceCenterCode;

    /**
     * 保修卡编号
     */
    @Size(max = 50)
    private String  warrantyNum;


    /**
     * 二级网点
     */
    @Size(max = 50)
    private String  secondBranchId;
    private String  secondBranchName;
    private String  secondBranchCode;


    /**
     * 维修状态
     */
    private String  maintenanceStatus;

    private String maintenanceStatusDesc;


    /**
     * 保养状态
     */
    private String  maintainStatus;

    private String maintainStatusDesc;


    private String  specimentids;

    private String  isStorage;
    private String  speNum;

    private String  ext01;

    private String  ext02;

    private String  ext03;

    private Date  ext04;

    private String  ext05;

    private String  ext06;

    private String  ext07;

    private String  ext08;

    private String  ext09;

    private String  ext10;

    private String  ext11;

    private String  ext12;

    private String  ext13;

    private String  ext14;

    private String  ext15;

    private String  ext16;

    private String  ext17;

    private String  ext18;

    private String  ext19;

    private String  ext20;

    //服务经销商编码
    private String  saleAgentCode;
    //服务经销商名称
    private String  saleAgentName;
    //代理商编码
    private String  saleCustomerCode;
    //代理商名称
    private String  saleCustomerName;
    //机主
    private String  machineOwner;
    //证件号码
    private String  credentialsNo;
    //服务经销商信息
    private CustomerDto customerDto;

}
