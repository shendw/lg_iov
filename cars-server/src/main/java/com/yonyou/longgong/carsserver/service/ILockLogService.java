package com.yonyou.longgong.carsserver.service;

import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dto.LockLogDto;
import com.yonyou.longgong.carsserver.pojo.LockLogPojo;

/**
 * @author shendawei
 * @创建时间 2020/4/26
 * @描述
 **/
public interface ILockLogService {
    /**
     * 查询锁车记录
     * @param lockLogDto
     * @return
     */
    PageInfo<LockLogPojo> selectLocklog(LockLogDto lockLogDto);

    /**
     * 插入一条解锁车记录并更新车辆锁车状态
     * @param terminalid
     * @param userid
     * @return
     */
    Integer insertLocklog(String terminalid,String userid,String code);

    void addLocklog(String terminalid, String userid,String code);
}
