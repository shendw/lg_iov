package com.yonyou.longgong.afterservice.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yonyou.longgong.afterservice.service.IAfterService;
import com.yonyou.longgong.afterservice.vo.DeviceWorkSituationVO;
import com.yonyou.longgong.carsserver.enums.TerminalCarStatus;
import com.yonyou.longgong.carsserver.exception.BusinessException;
import com.yonyou.longgong.carsserver.pojo.TerminalCarPojo;
import com.yonyou.longgong.carsserver.service.ITerminalCarService;
import com.yonyou.longgong.carsserver.utils.DataTimeUtil;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.iotserver.common.Const;
import com.yonyou.longgong.iotserver.dto.HistoryDataParamDto;
import com.yonyou.longgong.iotserver.enums.TagsEnums;
import com.yonyou.longgong.iotserver.service.IDeviceService;
import com.yonyou.longgong.iotserver.service.ISiteService;
import com.yonyou.longgong.iotserver.utils.LocateByAmapUtils;
import com.yonyou.longgong.iotserver.utils.RedisUtils;
import com.yonyou.longgong.iotserver.utils.TransLocationsUtil;
import com.yonyou.longgong.iotserver.vo.DeviceDataVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service(value = "iAfterService")
@Slf4j
public class AfterServiceImpl implements IAfterService {

    @Autowired
    private RedisUtils redis;

    @Autowired
    private IDeviceService deviceService;

    @Autowired
    private ITerminalCarService iTerminalCarService;
    /**
     * 设置销售服务需要的常规数据
     *
     * @param terminal              终端ID
     * @param deviceDataVO          设备对象
     * @param deviceWorkSituationVO 需要被填充的返回给售后服务的对象
     */
    private void setRoutineData(String terminal, DeviceDataVO deviceDataVO, DeviceWorkSituationVO deviceWorkSituationVO) {
        if (deviceDataVO == null) {
            Map<Object, Object> dataMap = redis.hmget(terminal);
            if (dataMap != null && !dataMap.isEmpty()){
                // 如果获取的发动机运行时间为空，那么认为没有更新获取Redis缓存当中的更新时间
                Object redisUpdateTime = dataMap.get(Const.UPDATETIME);
                String updateTime = redisUpdateTime == null ? null : redisUpdateTime.toString();
                deviceWorkSituationVO.setUpdateTime(updateTime);

                // 发动机运行时间为空的情况下从缓存当中拿总工时
                Object redisWorkHours = dataMap.get(Const.WORKTIME);
                String totalWorkingHours = redisWorkHours == null ? null : redisWorkHours.toString();
                deviceWorkSituationVO.setTotalWorkingHours(totalWorkingHours);

                // 如果获取的经纬度为空，那么获取Redis缓存当中的经纬度
                Object redislocation = dataMap.get(Const.LOCATION);
                // 由于Redis当中的经纬度没有校准，执行一次校准
                String location = redislocation == null ? null : TransLocationsUtil.transLocation(redislocation.toString());
                if (!StringUtils.isBlank(location)) {
                    String currLocation = LocateByAmapUtils.getAddress(location);
                    deviceWorkSituationVO.setCurrLocation(currLocation);
                    deviceWorkSituationVO.setLogAndLat(location);
                }
            }
        } else {
            BeanUtils.copyProperties(deviceDataVO,deviceWorkSituationVO);
            deviceWorkSituationVO.setUpdateTime(deviceDataVO.getTimestamp());
            deviceWorkSituationVO.setTotalWorkingHours(deviceDataVO.getEngine_running_duration());
            //定位
            String currLocation = deviceDataVO.getCoordinates();
            if (StringUtils.isNotBlank(currLocation)) {
                deviceWorkSituationVO.setLogAndLat(currLocation);
                currLocation = LocateByAmapUtils.getAddress(currLocation);
                deviceWorkSituationVO.setCurrLocation(currLocation);
            }
            deviceWorkSituationVO.setEngineWaterTemperature(deviceDataVO.getEngine_water_temperature());
            deviceWorkSituationVO.setEngineSpeed(deviceDataVO.getEngine_speed());
            deviceWorkSituationVO.setThrottleGear(deviceDataVO.getThrottle_gear());
            deviceWorkSituationVO.setFuelLevel(deviceDataVO.getFuel_level());
//            deviceWorkSituationVO.setDeviceStatus(deviceDataVO.getCommunication_state());
        }
    }

    @Override
    public DeviceWorkSituationVO getWorkSituation(String carno, String date) {
        TerminalCarPojo terminalCarPojo = new TerminalCarPojo();
        terminalCarPojo.setCarno(carno);
        terminalCarPojo.setEnablestate(TerminalCarStatus.EFFECTIVE.getCode());
        List<TerminalCarPojo> terminalCarPojos = iTerminalCarService.selectTerminalCar(terminalCarPojo);
        if (CollectionUtils.isEmpty(terminalCarPojos)) {
            return null;
        }
        String terminal = terminalCarPojos.get(0).getTerminalid();
        DeviceDataVO deviceDataVO = deviceService.getDeviceData(terminal);
        DeviceWorkSituationVO deviceWorkSituationVO = new DeviceWorkSituationVO();
        deviceWorkSituationVO.setCarNO(carno);
        this.setRoutineData(terminal, deviceDataVO, deviceWorkSituationVO);
        String today = DataTimeUtil.dateToStr(new Date(), "yyyy-MM-dd");
        String[] todayWorkingHours = deviceService.getTodayWorkingHours(terminal, today);
        if (todayWorkingHours != null){
            deviceWorkSituationVO.setTodayWorkingHours(todayWorkingHours[0]);
            deviceWorkSituationVO.setWorkStartTime(todayWorkingHours[1]);
        }
        return deviceWorkSituationVO;
    }

    @Override
    public List<DeviceWorkSituationVO> getWorkTime(String[] carnos) {
        if (carnos == null || carnos.length == 0){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        DeviceWorkSituationVO[] deviceWorkSituationVOs = new DeviceWorkSituationVO[carnos.length];
        for (int i = 0;i < carnos.length;i++) {
            DeviceWorkSituationVO deviceWorkSituationVO = new DeviceWorkSituationVO();
            TerminalCarPojo terminalCarPojo = new TerminalCarPojo();
            terminalCarPojo.setEnablestate(TerminalCarStatus.EFFECTIVE.getCode());
            terminalCarPojo.setCarno(carnos[i]);
            List<TerminalCarPojo> terminalCarPojos = iTerminalCarService.selectTerminalCar(terminalCarPojo);
            if (CollectionUtils.isEmpty(terminalCarPojos)){
                deviceWorkSituationVO.setCarNO(carnos[i]);
                deviceWorkSituationVO.setEngine_running_duration("0");
                deviceWorkSituationVOs[i] = deviceWorkSituationVO;
                continue;
            }
            Object worktime = redis.hget(Const.PREFIX_TERMINAL + terminalCarPojos.get(0).getTerminalid(), Const.WORKTIME);
            if (worktime == null){
                deviceWorkSituationVO.setCarNO(carnos[i]);
                deviceWorkSituationVO.setEngine_running_duration("0");
            }else {
                deviceWorkSituationVO.setCarNO(carnos[i]);
                deviceWorkSituationVO.setEngine_running_duration(worktime.toString());
            }
            deviceWorkSituationVOs[i] = deviceWorkSituationVO;
        }
        return Arrays.asList(deviceWorkSituationVOs);
    }
}
