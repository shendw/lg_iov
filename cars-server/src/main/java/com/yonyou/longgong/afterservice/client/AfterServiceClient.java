package com.yonyou.longgong.afterservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author shendawei
 * @创建时间 2020/4/4
 * @描述
 **/
@FeignClient(name = "after-sales")
public interface AfterServiceClient {

    @PostMapping(value = "/api/v1/lonking/vehicle/status")
    String updateStatus(@RequestParam("code") String code, @RequestParam("status") boolean status);
}
