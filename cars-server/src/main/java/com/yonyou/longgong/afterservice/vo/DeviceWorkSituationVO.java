package com.yonyou.longgong.afterservice.vo;

import lombok.Data;

@Data
public class DeviceWorkSituationVO {
    /**
     * 在线离线标识
     */
//    private String deviceStatus = "false";
    /**
     * 整机编号
     */
    private String carNO;
    /**
     * 更新时间
     */
    private String updateTime;
    /**
     * 总工时
     */
    private String totalWorkingHours;
    /**
     * 当前位置
     */
    private String currLocation;
    /**
     * 经纬度
     */
    private String logAndLat;
    /**
     * 发动机水温
     */
    private String engineWaterTemperature;
    /**
     * 发动机转速
     */
    private String engineSpeed;
    /**
     * 油门档位
     */
    private String throttleGear;
    /**
     * 燃油油位
     */
    private String fuelLevel;
    /**
     * 当日工作时长
     */
    private String todayWorkingHours;
    /**
     * 工作小时
      */
    private String engine_running_duration;
    /**
     * 机器电压
     */
    private String machine_voltage;
    /**
     *发动机水温
     */
    private String engine_water_temperature;
    /**
     * 液压油温
     */
    private String hydraulic_oil_temperature;
    /**
     * 机油压力
     */
    private String oil_pressure;
    /**
     * 燃油油位
     */
    private String fuel_level;
    /**
     * 发动机转速
     */
    private String engine_speed;
    /**
     * 油门档位
     */
    private String throttle_gear;
    /**
     * 电池电压
     */
    private String rcm_battery_voltage;
    /**
     * 最新锁车状态
     */
    private String locked_state;
    /**
     * 工作时间段
     */
    private Object workingTimeSlot;
    /**
     * 开工时间点
     */
    private String workStartTime;

}
