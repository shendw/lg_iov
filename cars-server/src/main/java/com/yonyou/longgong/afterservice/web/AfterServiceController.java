package com.yonyou.longgong.afterservice.web;

import com.yonyou.longgong.afterservice.service.IAfterService;
import com.yonyou.longgong.afterservice.vo.DeviceWorkSituationVO;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/afterservice")
public class AfterServiceController {

    @Autowired
    private IAfterService iAfterService;

    @RequestMapping(value = "/getWorkSituation")
    public ResponseData getWorkSituation(String carno, String date) {
        DeviceWorkSituationVO deviceWorkSituationVO = iAfterService.getWorkSituation(carno, date);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(), deviceWorkSituationVO);
    }

    @RequestMapping(value = "/getWorkTime")
    public ResponseData getWorkTime(String[] carno){
        List<DeviceWorkSituationVO> deviceWorkSituationVO = iAfterService.getWorkTime(carno);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(), deviceWorkSituationVO);
    }
}
