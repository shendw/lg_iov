package com.yonyou.longgong.afterservice.service;

import com.yonyou.longgong.afterservice.vo.DeviceWorkSituationVO;

import java.util.List;

public interface IAfterService {
    DeviceWorkSituationVO getWorkSituation(String carno, String date);

    List<DeviceWorkSituationVO> getWorkTime(String[] carnos);
}
