package com.yonyou.longgong.iotserver.mq;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.yonyou.longgong.afterservice.client.AfterServiceClient;
import com.yonyou.longgong.carsserver.dao.CardocPojoMapper;
import com.yonyou.longgong.carsserver.dao.TerminalPojoMapper;
import com.yonyou.longgong.carsserver.enums.CarWorkStatus;
import com.yonyou.longgong.carsserver.enums.TerminalCarStatus;
import com.yonyou.longgong.carsserver.example.CardocPojoExample;
import com.yonyou.longgong.carsserver.example.TerminalPojoExample;
import com.yonyou.longgong.carsserver.pojo.CardocPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalCarPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalPojo;
import com.yonyou.longgong.carsserver.service.ITerminalCarService;
import com.yonyou.longgong.carsserver.utils.DataTimeUtil;
import com.yonyou.longgong.iotserver.common.Const;
import com.yonyou.longgong.iotserver.enums.TagsEnums;
import com.yonyou.longgong.iotserver.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.zeromq.ZMQ;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * @author shendawei
 * @创建时间 2020/3/19
 * @描述
 **/
@Component
@Slf4j
@Order(1)
public class receiveTerminalInfo implements ApplicationRunner {

    @Autowired
    private TerminalPojoMapper terminalPojoMapper;

    @Autowired
    private AfterServiceClient afterServiceClient;

    @Autowired
    private ITerminalCarService iTerminalCarService;

    @Autowired
    private CardocPojoMapper cardocPojoMapper;

    @Autowired
    private RedisUtils redis;

    @Value("${iot.zeromq}")
    private String zeromq;

    @Value("${iot.zeromq-test}")
    private String zeromqTest;

    @Value("${iot.zeromq-old}")
    private String zeromqOld;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        connectpool();
    }

    private void connect(String url,int ioThreads){
        ZMQ.Context context = ZMQ.context(ioThreads);
        ZMQ.Socket socket = context.socket(ZMQ.SUB);
        socket.connect(url);
        log.info("connect iot zeromq success!!! url:"+url);
        socket.subscribe("".getBytes());
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        CardocPojo cardocPojo = new CardocPojo();
        TerminalCarPojo terminalCarPojo = new TerminalCarPojo();
        while (true){
                String reply = socket.recvStr();
//                log.info(reply);
                JSONObject data = JSON.parseObject(reply);
                JSONArray readings = data.getJSONArray("readings");
                String device = data.getString("device");
                String throttleGear = null;
                String machineStart = null;
                String machineShutdown = null;
                String machineElectrify = null;
                String communicationState = null;
                String enginRunningDuration = null;
                String location = null;
                String locationStatus = null;
                String fuelLevel = null;
                String communicationMode = null;
                String latestLandingTime = null;
                for (int i = 0;i<readings.size();i++){
                    String name = readings.getJSONObject(i).getString("name");
                    String value = readings.getJSONObject(i).getString("value");
                    //通讯方式有值，跳出循环
                    if (TagsEnums.COMMUNICATION_MODE.getChinese().equals(name)){
                        communicationMode = value;
                        break;
                    }
                    //通讯状态，如果通讯状态有值，那么直接跳出循环
                    if (TagsEnums.COMMUNICATION_STATE.getChinese().equals(name)){
                        communicationState = value;
                        break;
                    }
                    //最新登陆时间
                    if (TagsEnums.LATEST_LANDING_TIME.getChinese().equals(name)){
                        latestLandingTime = value;
                        break;
                    }
                    //用来判断设备是否在线
                    if (TagsEnums.THROTTLE_GEAR.getChinese().equals(name)){
                        throttleGear= value;
                        continue;
                    }
                    if (TagsEnums.MACHINE_START.getChinese().equals(name)){
                        machineStart= value;
                        continue;
                    }
                    if (TagsEnums.MACHINE_SHUTDOWN.getChinese().equals(name)){
                        machineShutdown= value;
                        continue;
                    }
                    if (TagsEnums.MACHINE_ELECTRIFY.getChinese().equals(name)){
                        machineElectrify= value;
                        continue;
                    }
                    //发动机运行时间 == 工作时间
                    if (TagsEnums.ENGINE_RUNNING_DURATION.getChinese().equals(name)){
                        enginRunningDuration = value;
                        continue;
                    }
                    //定位
                    if (TagsEnums.COORDINATES.getChinese().equals(name)){
                        location = value;
                        continue;
                    }
                    //定位状态
                    if (TagsEnums.GPS_POSITION_STATUS.getChinese().equals(name)){
                        locationStatus = value;
                        continue;
                    }
                    //燃油油位
                    if (TagsEnums.FUEL_LEVEL.getChinese().equals(name)){
                        fuelLevel = value;
                        continue;
                    }
                }
                //通讯方式不处理
                if (StringUtils.isNotBlank(communicationMode)){
                    continue;
                }
                if (StringUtils.isNotBlank(communicationState)){
                    String lastTime = DataTimeUtil.dateToStr(new Date());
                    terminalPojoMapper.updateLastTime(lastTime,device);
                    log.info("终端号:{},更新最后时间:{}", device, lastTime);
                }
                if (StringUtils.isNotBlank(latestLandingTime)){
                    String lastlogintime = DataTimeUtil.stampToDate(latestLandingTime);
                    terminalPojoMapper.updateLastLoginTime(lastlogintime,device);
                    log.info("终端号:{},更新登陆时间:{}", device, lastlogintime);
                }
                Object carNoCache = redis.hget(Const.PREFIX_TERMINAL + device, Const.CARNO);
                String carno = null;
                if (carNoCache == null){
                    terminalCarPojo.setTerminalid(device);
                    terminalCarPojo.setEnablestate(TerminalCarStatus.EFFECTIVE.getCode());
                    List<TerminalCarPojo> terminalCarPojos = iTerminalCarService.selectTerminalCar(terminalCarPojo);
                    if (CollectionUtils.isEmpty(terminalCarPojos)){
                        continue;
                    }
                    carno = terminalCarPojos.get(0).getCarno();
                    redis.hset(Const.PREFIX_TERMINAL + device, Const.CARNO,carno);
                }else {
                    carno = carNoCache.toString();
                }
                //根据通讯状态
                if (StringUtils.isNotBlank(communicationState)){
                    if ("false".equals(communicationState)){
                        cardocPojoExample.clear();
                        cardocPojoExample.createCriteria().andCarnoEqualTo(carno);
                        cardocPojo.setWorkstatus("1");
                        cardocPojoMapper.updateByExampleSelective(cardocPojo,cardocPojoExample,CardocPojo.Column.workstatus);
                        log.info("整机编号:{} 更改工作状态：停机，通讯状态false",carno);
                    }
                    try{
                        afterServiceClient.updateStatus(carno,"true".equals(communicationState));
                        log.info("售后：终端号:{},整机编号:{},更改在线状态:{}",device, carno,communicationState);
                    }catch (Exception ex){
                        log.info("售后服务出错");
                    }
                    continue;
                }
                //缓存定位，燃油油位，工作小时
                if (StringUtils.isNotBlank(enginRunningDuration)){
                    Map<String, Object> historyMap = new HashMap<>(3);
                    boolean flag = false;
                    //工作小时
                    if (!"0".equals(enginRunningDuration) && !"0.0".equals(enginRunningDuration)){
                        historyMap.put(Const.WORKTIME,enginRunningDuration);
                        flag = true;
                    }
                    //定位
                    if ("有效定位".equals(locationStatus)){
                        historyMap.put(Const.LOCATION,location);
                        flag = true;
                    }
                    //燃油油位
                    if(!"0".equals(fuelLevel) && !"0.0".equals(fuelLevel)){
                        historyMap.put(Const.FUEL_LEVEL,fuelLevel);
                        flag = true;
                    }
                    if (flag){
                        historyMap.put(Const.UPDATETIME,DataTimeUtil.dateToStr(new Date()));
                        redis.hmset(Const.PREFIX_TERMINAL + device,historyMap);
                        log.info("更新 {} 有效数据",device);
                    }
                }

                if (StringUtils.isNotBlank(throttleGear) && StringUtils.isNotBlank(machineStart) && StringUtils.isNotBlank(machineElectrify)){
                    String workStatus = this.getWorkStatus(machineElectrify, machineStart, machineShutdown, throttleGear);
                    if (StringUtils.isNotBlank(workStatus)){
                        try{
                            afterServiceClient.updateStatus(carno,!"1".equals(workStatus));
                            log.info("售后：终端号:{},整机编号:{},更改在线状态:{}",device, carno,!"1".equals(workStatus));
                        }catch (Exception ex){
                            log.info("售后服务出错");
                        }
                        cardocPojo.setWorkstatus(workStatus);
                        cardocPojoExample.clear();
                        cardocPojoExample.createCriteria().andCarnoEqualTo(carno);
                        cardocPojoMapper.updateByExampleSelective(cardocPojo,cardocPojoExample,CardocPojo.Column.workstatus);
                        log.info("整机编号:{} 更改工作状态：{}",carno,workStatus);
                    }
                }
            }
    }

    private void connectpool(){
        ThreadFactory namedThreadFactory = new ThreadFactoryBuilder()
                .setNameFormat("zeromq-pool-%d").build();
        ExecutorService singleThreadPool = new ThreadPoolExecutor(4, 10,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(1024), namedThreadFactory, new ThreadPoolExecutor.AbortPolicy());
        singleThreadPool.execute(() ->{
            this.connect(zeromq,1);
        });
        singleThreadPool.execute(() ->{
            this.connect(zeromqOld,1);
        });
        singleThreadPool.execute(() ->{
            this.connect(zeromqTest,1);
        });
    }

    private String getWorkStatus(String mElectrify,String mStart,String mShutdown,String throttleGear){
        String trueStr = "true";
        String falseStr = "false";
        if (trueStr.equals(mElectrify) && falseStr.equals(mStart)){
            return CarWorkStatus.ELECTRIFY.getCode();
        }else if (falseStr.equals(mElectrify) && falseStr.equals(mStart)){
            return CarWorkStatus.STOP.getCode();
        }else if (trueStr.equals(mElectrify) && trueStr.equals(mStart) && "1".equals(throttleGear)){
            return CarWorkStatus.SLOWSPEED.getCode();
        }else if (trueStr.equals(mElectrify) && trueStr.equals(mStart)){
            return CarWorkStatus.START.getCode();
        }
        return null;
    }
}
