package com.yonyou.longgong.iotserver.common;

import com.yonyou.longgong.carsserver.exception.BusinessException;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.iotserver.utils.RedisLock;
import com.yonyou.longgong.iotserver.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author yushilin
 * @version 1.0
 * @description: 切面，加入redis锁
 * @date 2021/4/27 16:49
 */

@Component
@Aspect
@Slf4j
public class RedisLockAspect {
    // 把切面的连接点放在了我们的注解上
    @Pointcut("@annotation(com.yonyou.longgong.iotserver.common.RedisLockAn)")
    public void redisLockAspect(){
    }

    @Autowired
    private RedisUtils redis;

    private RedisLock redisLock;

    /**
     * 前置通知
     */
    @Before("redisLockAspect()")
    public void beforeMethod(JoinPoint joinPoint){
        String terminalid = getMethodTerminalid(joinPoint);
        try {
            if(StringUtils.isBlank(terminalid)){
                throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
            }
            redisLock = new RedisLock(redis, Const.PREFIX_EXEC + terminalid, 0, 300000);
            if(redisLock.acquire()){

            }else {
                throw new BusinessException(ResponseCode.ERROR_LOCKED);
            }
        } catch (InterruptedException e) {
            throw new BusinessException(ResponseCode.ERROR_LOCKED);
        }
    }

    /**
     * 后置通知
     *
     */
    @After("redisLockAspect()")
    public void methodAfterReturning(JoinPoint joinPoint){
        if(redisLock == null){
            return;
        }
        redisLock.release();
    }

    private static String getMethodTerminalid(JoinPoint joinPoint){
        // 下面两个数组中，参数值和参数名的个数和位置是一一对应的。
        Object[] args = joinPoint.getArgs();
        String[] argNames = ((MethodSignature)joinPoint.getSignature()).getParameterNames();
        for (int i = 0; i < argNames.length ; i ++ ) {
            String argName = argNames[i];
            if("terminalid".equals(argName)){
                return args[i].toString();
            }
        }
        return null;
    }

}
