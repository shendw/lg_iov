package com.yonyou.longgong.iotserver.vo;

import lombok.Data;

import java.util.List;

@Data
public class PageVO {
    /**
     * 总记录行数
     */
    private int totalRecord;
    /**
     * 返回对象
     */
    private List resultData;
}
