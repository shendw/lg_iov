package com.yonyou.longgong.iotserver.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yonyou.longgong.carsserver.dao.CardocPojoMapper;
import com.yonyou.longgong.carsserver.dto.CardocSelectDto;
import com.yonyou.longgong.carsserver.enums.CarWorkStatus;
import com.yonyou.longgong.carsserver.example.CardocPojoExample;
import com.yonyou.longgong.carsserver.exception.BusinessException;
import com.yonyou.longgong.carsserver.pojo.CardocPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalPojo;
import com.yonyou.longgong.carsserver.service.ICardocService;
import com.yonyou.longgong.carsserver.service.ITerminalService;
import com.yonyou.longgong.carsserver.utils.DataTimeUtil;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.iotserver.dto.HistoryDataParamDto;
import com.yonyou.longgong.iotserver.enums.TagsEnums;
import com.yonyou.longgong.iotserver.service.IDeviceService;
import com.yonyou.longgong.iotserver.service.ISiteService;
import com.yonyou.longgong.iotserver.utils.*;
import com.yonyou.longgong.iotserver.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

@Service(value = "deviceService")
@Slf4j
public class DeviceServiceImpl implements IDeviceService {

    @Value("${iot.site}")
    private String site;
    @Value("${iot.domain}")
    private String domain;
    @Value("${iot.domain-test}")
    private String domainTest;
    @Autowired
    private ICardocService iCardocService;
    @Autowired
    private ITerminalService iTerminalService;
    @Autowired
    private TransformUtils transformUtils;
    @Autowired
    private ISiteService iSiteService;
    @Autowired(required = false)
    private CardocPojoMapper cardocPojoMapper;

    /**
     * 根据终端和标签获取IOT参数
     *
     * @param terminals 终端
     * @param tags      标签
     * @return IOT参数
     */
    public String getTagName(String[] terminals, TagsEnums[] tags,Boolean isTest) {
        String tagName = "";
        for (String terminal : terminals) {
            //如果该终端站点为空，跳过本次循环
            String site = iSiteService.getSite(terminal, isTest);
            if (StringUtils.isEmpty(site)){
                continue;
            }
            for (TagsEnums tag : tags) {
                tagName += site + "." + terminal + "." + tag.getChinese() + ",";
            }
        }
        if (!StringUtils.isBlank(tagName)) {
            tagName = tagName.substring(0, tagName.length() - 1);
        }
        return tagName;
    }

    @Override
    public DeviceDataVO getDeviceData(String terminal) {
        return this.getDeviceData(terminal,false);
    }

    @Override
    public DeviceDataVO getDeviceData(String terminal,Boolean isTest) {
        if (StringUtils.isBlank(terminal)) {
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        // 根据终端ID获取设备数据
        String pre = isTest ? domainTest : domain;
        String url = pre + "/core-data/api/v1/device/" + terminal + "/allRtData";
        String result = HttpUtils.sendGet(url, null);
        JSONObject resultJson;
        try{
            resultJson = (JSONObject) JSON.parse(result);
        }catch (Exception ex){
            return null;
        }
        //FIXME 错误的终端id resultJson.get("data") == null 会抛出 nullpointexception
        if (resultJson == null || resultJson.get("data") == null) {
            return null;
        }
        DeviceDataVO deviceDataVO = transformUtils.transformCurrData(resultJson, DeviceDataVO.class);
        if (deviceDataVO != null) {
            deviceDataVO.setCoordinates(TransLocationsUtil.transLocation(deviceDataVO.getCoordinates()));
            deviceDataVO.setGps_version("V4.1");
            if (StringUtils.isEmpty(deviceDataVO.getCarNo())){
                deviceDataVO.setHydraulic_oil_temperature("0");
                deviceDataVO.setEngine_water_temperature("0");
                deviceDataVO.setOil_pressure("0");
                deviceDataVO.setEngine_speed("0");
                deviceDataVO.setThrottle_gear("0");
            }else {
                CardocPojoExample cardocPojoExample = new CardocPojoExample();
                cardocPojoExample.createCriteria().andLogicalDeleted(false).andCarnoEqualTo(deviceDataVO.getCarNo());
                List<CardocPojo> cardocPojos = cardocPojoMapper.selectByExample(cardocPojoExample);
                if (!CollectionUtils.isEmpty(cardocPojos)) {
                    String workstatus = cardocPojos.get(0).getWorkstatus();
                    if (CarWorkStatus.STOP.getCode().equals(workstatus)) {
                        deviceDataVO.setHydraulic_oil_temperature("0");
                        deviceDataVO.setEngine_water_temperature("0");
                        deviceDataVO.setOil_pressure("0");
                        deviceDataVO.setEngine_speed("0");
                        deviceDataVO.setThrottle_gear("0");
                    }
                }
            }
        }
        return deviceDataVO;
    }

    @SuppressWarnings("all")
    public DeviceDetailVO getDeviceDetailVO(String carno, String terminalid){
        DeviceDetailVO deviceDetailVO = cardocPojoMapper.selectterminalInfoById(terminalid);
        if(deviceDetailVO == null){
            return null;
        }
        //格式化gps时间
        String gps_time = deviceDetailVO.getGps_time();
        if (StringUtils.isNotBlank(gps_time)){
            deviceDetailVO.setGps_time(DataTimeUtil.stampToDate(gps_time));
        }
        CardocPojo cardocPojo = iCardocService.selectCarDocByCarNo(carno).getList().get(0);
        deviceDetailVO.setCarno(cardocPojo.getCarno());
        TerminalPojo terminalPojo = iTerminalService.selectTerminalByTerminalid(terminalid);
        if (terminalPojo != null) {
            deviceDetailVO.setSimid(terminalPojo.getSimid());
        }
        deviceDetailVO.setMachinemodel(cardocPojo.getMachinemodel());
        deviceDetailVO.setCarmodel(cardocPojo.getCarmodel());
        deviceDetailVO.setInstocktime(cardocPojo.getInstocktime());
        deviceDetailVO.setOutstocktime(cardocPojo.getOutstocktime());
        deviceDetailVO.setSaletime(cardocPojo.getSaletime());
        deviceDetailVO.setSalecompany(cardocPojo.getSalecompany());
        deviceDetailVO.setFinalcustomer(cardocPojo.getCustomer());

        //工作时间，最新报警信息，得问问，娶那个值
        String[] todayWorkingHours = this.getTodayWorkingHours(terminalid, DataTimeUtil.dateToStr(new Date(), "yyyy-MM-dd"));
        if (todayWorkingHours != null){
            deviceDetailVO.setTodayWorkingHours(todayWorkingHours[0]);
        }

        String coordinates = deviceDetailVO.getCoordinates();
        coordinates = TransLocationsUtil.transLocation(coordinates);
        if (StringUtils.isNotEmpty(coordinates)){
            deviceDetailVO.setLocation(LocateByAmapUtils.getAddress(coordinates));
        }
        if (StringUtils.isNotBlank(deviceDetailVO.getLocked_state())) {
            deviceDetailVO.setLocked_state(deviceDetailVO.getLocked_state());
            deviceDetailVO.setLocked_date(deviceDetailVO.getLocked_date());
        }
        RatioVO ratioVO = this.getAlarmData(terminalid,false);
        if (ratioVO != null && !ratioVO.getData().isEmpty()) {
            List<AlarmVO> alarmVOS = ratioVO.getData();
            deviceDetailVO.setAlarm_message(alarmVOS.get(0).getAlarm_message());
        }
        return deviceDetailVO;
    }

    @SuppressWarnings("all")
    @Override
    public DeviceDetailVO getDeviceDetailData(String carno, String terminalid,Boolean isTest) {
        if(true){
            return getDeviceDetailVO(carno,terminalid);
        }
        CardocSelectDto cardocSelectDto = new CardocSelectDto();
        cardocSelectDto.setCarno(carno);
        List<CardocPojo> cardocPojos = iCardocService.selectCardoc(cardocSelectDto).getList();
        if (cardocPojos == null || cardocPojos.isEmpty()) {
            return null;
        }
        // iot没数据的时候把自己系统的数据返回给前端
        CardocPojo cardocPojo = cardocPojos.get(0);
        DeviceDetailVO deviceDetailVO = new DeviceDetailVO();
        deviceDetailVO.setCarno(cardocPojo.getCarno());
        TerminalPojo terminalPojo = iTerminalService.selectTerminalByTerminalid(terminalid);
        if (terminalPojo != null) {
            deviceDetailVO.setSimid(terminalPojo.getSimid());
        }
        deviceDetailVO.setMachinemodel(cardocPojo.getMachinemodel());
        deviceDetailVO.setCarmodel(cardocPojo.getCarmodel());
        deviceDetailVO.setInstocktime(cardocPojo.getInstocktime());
        deviceDetailVO.setOutstocktime(cardocPojo.getOutstocktime());
        deviceDetailVO.setSaletime(cardocPojo.getSaletime());
        deviceDetailVO.setSalecompany(cardocPojo.getSalecompany());
        deviceDetailVO.setFinalcustomer(cardocPojo.getCustomer());
        String[] todayWorkingHours = this.getTodayWorkingHours(terminalid, DataTimeUtil.dateToStr(new Date(), "yyyy-MM-dd"));
        if (todayWorkingHours != null){
            deviceDetailVO.setTodayWorkingHours(todayWorkingHours[0]);
        }
        DeviceDataVO deviceDataVO = this.getDeviceData(terminalid,isTest);
        if (deviceDataVO == null) {
            return deviceDetailVO;
        }
        deviceDetailVO.setDevice(deviceDataVO.getDevice());
        deviceDetailVO.setGps_manufacturer(deviceDataVO.getGps_manufacturer());
        deviceDetailVO.setCoordinates(deviceDataVO.getCoordinates());
        deviceDetailVO.setLocation(LocateByAmapUtils.getAddress(deviceDataVO.getCoordinates()));
        if (StringUtils.isNotBlank(deviceDataVO.getLocked_state())) {
            deviceDetailVO.setLocked_state(deviceDataVO.getLocked_state());
            deviceDetailVO.setLocked_date(deviceDataVO.getTimestamp());
        }
        deviceDetailVO.setGps_version(deviceDataVO.getGps_version());
        deviceDetailVO.setGround_speed(deviceDataVO.getGround_speed());
        deviceDetailVO.setGround_heading(deviceDataVO.getGround_heading());
        deviceDetailVO.setGsm_signal_strength(deviceDataVO.getGsm_signal_strength());
        deviceDetailVO.setEngine_running_duration(deviceDataVO.getEngine_running_duration());
        deviceDetailVO.setMachine_voltage(deviceDataVO.getMachine_voltage());
        deviceDetailVO.setEngine_water_temperature(deviceDataVO.getEngine_water_temperature());
        deviceDetailVO.setHydraulic_oil_temperature(deviceDataVO.getHydraulic_oil_temperature());
        deviceDetailVO.setOil_pressure(deviceDataVO.getOil_pressure());
        deviceDetailVO.setFuel_level(deviceDataVO.getFuel_level());
        deviceDetailVO.setEngine_speed(deviceDataVO.getEngine_speed());
        deviceDetailVO.setThrottle_gear(deviceDataVO.getThrottle_gear());
        deviceDetailVO.setRcm_battery_voltage(deviceDataVO.getRcm_battery_voltage());
        deviceDetailVO.setMachine_clock_time(deviceDataVO.getMachine_clock_time());
        deviceDetailVO.setGps_time(deviceDataVO.getGps_time());
        RatioVO ratioVO = this.getAlarmData(terminalid,isTest);
        if (ratioVO != null && !ratioVO.getData().isEmpty()) {
            List<AlarmVO> alarmVOS = ratioVO.getData();
            deviceDetailVO.setAlarm_message(alarmVOS.get(0).getAlarm_message());
        }
        return deviceDetailVO;
    }

    @Override
    public String getJsonByTag(String tagNames) {
        return this.getJsonByTag(tagNames,false);
    }

    @Override
    public String getJsonByTag(String tagNames,Boolean isTest) {
        if (StringUtils.isBlank(tagNames)) {
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        // 根据终端ID获取设备数据
        String pre = isTest ? domainTest : domain;
        String url = pre + "/core-data/api/v1/data/GetRtDataByTag";
        Map<String, String> parameters = new HashMap<>();
        parameters.put("tags", tagNames);
        return HttpUtils.sendGet(url, parameters);
    }

    @Override
    public List<DeviceDataVO> getDeviceDataByTag(String tagNames) {
        return this.getDeviceDataByTag(tagNames,false);
    }

    @Override
    public List<DeviceDataVO> getDeviceDataByTag(String tagNames,Boolean isTest) {
        if (StringUtils.isBlank(tagNames)) {
            throw new BusinessException(ResponseCode.IOV_SITE_IS_NULL);
        }
        // 根据终端ID获取设备数据
        String pre = isTest ? domainTest : domain;
        String url = pre + "/core-data/api/v1/data/GetRtDataByTag";
        Map<String, String> parameters = new HashMap<>();
        parameters.put("tags", tagNames);
        String result = HttpUtils.sendGet(url, parameters);
        JSONObject resultJson = (JSONObject) JSON.parse(result);
        if (resultJson.get("data") == null) {
            return null;
        }
        List<DeviceDataVO> deviceDataVOS = transformUtils.transformCurrDataArray(resultJson, DeviceDataVO.class);
        List<DeviceDataVO> dataVOS = null;
        if (!CollectionUtils.isEmpty(deviceDataVOS)) {
            dataVOS = deviceDataVOS.stream().filter(item -> item.getTimestamp() != null).collect(Collectors.toList());
            dataVOS.sort((Comparator.comparing(DeviceDataVO::getTimestamp).reversed()));
            dataVOS.stream().forEach(item -> item.setCoordinates(TransLocationsUtil.transLocation(item.getCoordinates())));
        }
        return dataVOS;
    }

    @Override
    public List<DeviceDataVO> getDeviceHistoryData(HistoryDataParamDto historyDataParamDto,Boolean isTest) {
        String terminalid = historyDataParamDto.getTerminalid();
        if (StringUtils.isBlank(terminalid)) {
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        String tagNames = "";
        // 设备所有数据
        TagsEnums[] enums = TagsEnums.values();
        String site = iSiteService.getSite(terminalid, isTest);
        if (StringUtils.isEmpty(site)){
            return null;
        }
        for (TagsEnums tagName : enums) {
            tagNames += site + "." + terminalid + "." + tagName.getChinese() + ",";
        }
        tagNames = tagNames.substring(0, tagNames.length() - 1);
        String result = this.getDeviceHistoryData(historyDataParamDto, tagNames,isTest);
        JSONObject resultJson = (JSONObject) JSON.parse(result);
        if (resultJson.get("data") == null) {
            return null;
        }
        List<DeviceDataVO> deviceDataVOS = transformUtils.transformHistoryData(resultJson, DeviceDataVO.class);
        deviceDataVOS.sort((Comparator.comparing(DeviceDataVO::getTimestamp).reversed()));
        if (!CollectionUtils.isEmpty(deviceDataVOS)) {
            deviceDataVOS.stream().forEach(item -> item.setCoordinates(TransLocationsUtil.transLocation(item.getCoordinates())));
        }
        return deviceDataVOS;
    }

    @Override
    public List<DeviceDataVO> getDeviceRoutineData(HistoryDataParamDto historyDataParamDto,Boolean isTest) {
        List<DeviceDataVO> routineData = new ArrayList<>();
        List<DeviceDataVO> deviceDataVOS = this.getDeviceHistoryData(historyDataParamDto,isTest);
        try {
            routineData = CheckDataUtils.checkFieldIsBlank(DeviceDataVO.class, deviceDataVOS, CheckFieldUtils.getDeviceRoutineField());
        } catch (Exception e) {
            // FIXME 异常处理待修改
            e.printStackTrace();
        }
        if (routineData.isEmpty()) {
            return null;
        }
        return routineData;
    }

    @Override
    public List<DeviceDataVO> getDeviceGPSData(HistoryDataParamDto historyDataParamDto,Boolean isTest) {
        List<DeviceDataVO> routineData = new ArrayList<>();
        List<DeviceDataVO> deviceDataVOS = this.getDeviceHistoryData(historyDataParamDto,isTest);
        try {
            routineData = CheckDataUtils.checkFieldIsBlank(DeviceDataVO.class, deviceDataVOS, CheckFieldUtils.getDeviceGPSField());
        } catch (Exception e) {
            // FIXME 异常处理待修改
            e.printStackTrace();
        }
        if (routineData.isEmpty()) {
            return null;
        }
        return routineData;
    }

    @Override
    public List<DeviceDataVO> getMachineState(HistoryDataParamDto historyDataParamDto,Boolean isTest) {
        List<DeviceDataVO> routineData = new ArrayList<>();
        List<DeviceDataVO> deviceDataVOS = this.getDeviceHistoryData(historyDataParamDto,isTest);
        try {
            routineData = CheckDataUtils.checkFieldIsBlank(DeviceDataVO.class, deviceDataVOS, CheckFieldUtils.getMachineStateField());
        } catch (Exception e) {
            // FIXME 异常处理待修改
            e.printStackTrace();
        }
        if (routineData.isEmpty()) {
            return null;
        }
        return routineData;
    }

    @Override
    public String getDeviceHistoryData(HistoryDataParamDto historyDataParamDto, String tagNames) {
        return this.getDeviceHistoryData(historyDataParamDto,tagNames,false);
    }

    @Override
    public String getDeviceHistoryData(HistoryDataParamDto historyDataParamDto, String tagNames,Boolean isTest) {
        if (StringUtils.isBlank(tagNames)) {
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        String pre = isTest ? domainTest : domain;
        String url = pre + "/core-data/api/v1/getHistoryDataByTag";
        Map<String, String> parameters = new HashMap<>();
        try {
            // 将传入的时间转换为时间戳格式
            String startTime = DataTimeUtil.dateToStamp(historyDataParamDto.getBeginTime());
            String endTime = DataTimeUtil.dateToStamp(historyDataParamDto.getEndTime());
            if (Long.parseLong(endTime)-Long.parseLong(startTime) > 2592000){
                throw new BusinessException(ResponseCode.ERROR_TIMEOUTRANGE);
            }
            parameters.put("startTime", startTime);
            parameters.put("endTime", endTime);
        } catch (ParseException e) {
            throw new BusinessException(ResponseCode.ERROR_TIMEFORMAT);
        }
        parameters.put("tags", tagNames);
        parameters.put("intervalType", historyDataParamDto.getIntervalType());
        parameters.put("interval", historyDataParamDto.getInterval());
        // 远程调用IOT的服务获取历史轨迹
        return HttpUtils.sendGet(url, parameters);
    }

    @Override
    public DeviceOverView getDeviceOverView(String terminal) {
        if (StringUtils.isBlank(terminal)) {
            return null;
        }
        DeviceOverView deviceOverView = new DeviceOverView();
        RatioVO ratioVO = this.getAlarmData(terminal,false);
        deviceOverView.setRatioVO(ratioVO);
        List<DeviceLocationVO> deviceLocationVOS = this.getDeviceLocation(terminal);
        deviceOverView.setDeviceLocationVO(deviceLocationVOS);
        String[] terminalArray = terminal.split(",");
        String tagNames = this.getTagName(terminalArray, new TagsEnums[]{TagsEnums.COMMUNICATION_STATE},false);
        List<DeviceDataVO> deviceDataVOS = this.getDeviceDataByTag(tagNames);
        //FIXME 车辆总揽数量是车辆档案数量还是绑定终端的车辆数量？
        deviceOverView.setTotalnum(terminalArray.length);
        for (DeviceDataVO deviceDataVO : deviceDataVOS) {
            String communication_state = deviceDataVO.getCommunication_state();
            if (Boolean.valueOf(communication_state).booleanValue()) {
                deviceOverView.setOnlinenum(deviceOverView.getOnlinenum() + 1);
            }
        }
        return deviceOverView;
    }

    @Override
    public List<DeviceLocationVO> getDeviceLocation(String terminal) {
        // 初始化中国所有省市数据
        List<DeviceLocationVO> deviceLocationVOS = new ArrayList<>(DeviceLocationVO.initChinaCity().values());
        if (StringUtils.isBlank(terminal)){ return deviceLocationVOS;}
        String tagNames = "";
        String[] terminalArray = terminal.split(",");
        for (String key : terminalArray){
            String site = iSiteService.getSite(key, false);
            if (StringUtils.isEmpty(site)){
                continue;
            }
            tagNames += site + "." + key + "." + TagsEnums.COORDINATES.getChinese() + ",";
        }
        tagNames = tagNames.substring(0, tagNames.length() - 1);
        String result = this.getJsonByTag(tagNames);
        JSONObject resultJson = (JSONObject) JSON.parse(result);
        if (resultJson.get("data") == null) {
            return deviceLocationVOS;
        }
        JSONArray locationArray = new JSONArray();
        JSONArray jsonArray = resultJson.getJSONArray("data");
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String device = jsonObject.getString("device");
            if (device == null || device.length() == 0) {
                continue;
            }
            String coordinates = jsonObject.getString("value");
            if (StringUtils.isNotBlank(coordinates)) {
                coordinates = coordinates.replace("E", "");
                coordinates = coordinates.replace("N", "");
                locationArray.add(coordinates);
            }
        }
        return LocateByAmapUtils.getProvinceDistribution(locationArray);
    }

    @Override
    public RatioVO getAlarmData(String terminal,Boolean isTest) {
        if (StringUtils.isBlank(terminal)) {return null;}
        StringBuilder tagNames = new StringBuilder();
        String[] terminalArray = terminal.split(",");
        for (String key : terminalArray) {
            String site = iSiteService.getSite(key, isTest);
            if (StringUtils.isEmpty(site)){
                    continue;
            }
            for (TagsEnums tagName : AlarmVO.alarmArray) {
                tagNames.append(site).append(".").append(key).append(".").append(tagName.getChinese()).append(",");
            }
        }
        tagNames = new StringBuilder(tagNames.substring(0, tagNames.length() - 1));
        String result = this.getJsonByTag(tagNames.toString(),isTest);
        JSONObject resultJson = (JSONObject) JSON.parse(result);
        if (resultJson.get("data") == null) {
            return null;
        }
        Map<String, AlarmVO> alarmVOMap = new HashMap<>();
        Map<String, AlarmVO> resultAlarmData = new HashMap<>();
        List<AlarmVO> alarmVOList = transformUtils.transformCurrDataArray(resultJson, AlarmVO.class);
        if(!CollectionUtils.isEmpty(alarmVOList)){
            for (AlarmVO alarmVO : alarmVOList) {
                alarmVOMap.put(alarmVO.getDevice(), alarmVO);
            }
        }
        JSONArray jsonArray = resultJson.getJSONArray("data");
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String name = jsonObject.getString("name");
            // 通过Lambda表达式来判断标签是否存在
            TagsEnums tag = Arrays.asList(AlarmVO.alarmArray).stream().filter(alarmEnum -> alarmEnum.getChinese().equals(name))
                    .findFirst().orElse(null);
            if (tag != null && tag.getEnglish().indexOf("alarm") != -1) {
                String value = jsonObject.getString("value");
                if (StringUtils.isBlank(value) || "false".equals(value)) {
                    continue;
                }
                AlarmVO newAlarmVO = new AlarmVO();
                AlarmVO alarmVO = alarmVOMap.get(jsonObject.getString(AlarmVO.DEVICE));
                if (alarmVO == null) {
                    continue;
                }
                BeanUtils.copyProperties(alarmVO, newAlarmVO);
                newAlarmVO.setAlarm_message(tag.getChinese());
                String key = newAlarmVO.getDevice() + "," + newAlarmVO.getTimestamp() + "," + newAlarmVO.getAlarm_message();
                if (!resultAlarmData.containsKey(key)) {
                    resultAlarmData.put(key, newAlarmVO);
                }
            }
        }
        if (resultAlarmData.isEmpty()) {
            RatioVO ratioVO = new RatioVO();
            ratioVO.setData(new ArrayList());
            // 异常设备数量
            ratioVO.setQuantity(alarmVOMap.keySet().size());
            // 设备总数量
            ratioVO.setTotal_quantity(terminalArray.length);
        }
        RatioVO ratioVO = new RatioVO();
        List<AlarmVO> alarmVOS = new ArrayList<>(resultAlarmData.values());
        // 根据设备排序后设置数据
        alarmVOS.sort((Comparator.comparing(AlarmVO::getDevice)));
        ratioVO.setData(alarmVOS);
        // 异常设备数量
        ratioVO.setQuantity(resultAlarmData.keySet().size());
        // 设备总数量
        ratioVO.setTotal_quantity(terminalArray.length);
        return ratioVO;
    }

    @Override
    public List<AlarmVO> getAlarmData(HistoryDataParamDto historyDataParamDto,Boolean isTest) {
        String tagNames = "";
        String[] terminals = historyDataParamDto.getTerminalid().split(",");
        for (String key : terminals) {
            String site = iSiteService.getSite(key, isTest);
            if (StringUtils.isEmpty(site)){
                continue;
            }
            for (TagsEnums tagName : AlarmVO.alarmArray) {
                tagNames += site + "." + key + "." + tagName.getChinese() + ",";
            }
        }
        tagNames = tagNames.substring(0, tagNames.length() - 1);
        String result = this.getDeviceHistoryData(historyDataParamDto, tagNames,isTest);
        JSONObject resultJson = (JSONObject) JSON.parse(result);
        if (resultJson.get("data") == null) {
            return null;
        }
        JSONObject clz = new JSONObject();
        JSONObject jsonObject = resultJson.getJSONObject("data");
        for (String key : jsonObject.keySet()) {
            String name = key.substring(key.lastIndexOf(".") + 1, key.length());
            // 通过Lambda表达式来判断标签是否存在
            TagsEnums tag = Arrays.stream(AlarmVO.alarmArray).filter(alarmEnum -> alarmEnum.getChinese().equals(name))
                    .findFirst().orElse(null);
            JSONArray jsonArray = jsonObject.getJSONArray(key);
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject json = jsonArray.getJSONObject(i);
                // 因为返回的历史数据以标签当作Key，所以先使用缓存Json存放相同时间的数据
                JSONObject temp = new JSONObject();
                String timestamp = json.getString(AlarmVO.TIMESTAMP);
                if (clz.containsKey(timestamp)) {
                    temp = clz.getJSONObject(timestamp);
                }
                // 由于IOT没有终端ID标签，单独设置
                if (!temp.containsKey(AlarmVO.DEVICE)) {
                    temp.put(AlarmVO.DEVICE, json.getString(AlarmVO.DEVICE));
                }
                // 由于IOT没有保存时间标签，单独设置
                if (!temp.containsKey(AlarmVO.TIMESTAMP)) {
                    if (timestamp != null && timestamp.length() >= 10) {
                        temp.put(AlarmVO.TIMESTAMP, DataTimeUtil.stampToDate(timestamp));
                    }
                }
                // 标签不为空的情况下，将标签对应的english信息当作Key存入Json方便转换对象
                if (tag != null) {
                    String value = json.getString("value");
                    if (StringUtils.isBlank(value) || "false".equals(value)) {
                        continue;
                    }
                    if (tag.getEnglish().contains("alarm")) {
                        temp.put(AlarmVO.ALARM_MESSAGE, tag.getChinese());
                    } else {
                        if (tag.getEnglish().contains("time")) {
                            value = DataTimeUtil.stampToDate(value);
                        }
                        temp.put(tag.getEnglish(), value);
                    }
                }
                clz.put(timestamp, temp);
            }
        }
        if (clz == null || clz.isEmpty()) {
            return null;
        }
        // 去除没有报警信息的数据
        JSONArray alarmArray = new JSONArray();
        for (String timestamp : clz.keySet()) {
            JSONObject temp = clz.getJSONObject(timestamp);
            if (StringUtils.isNotBlank(temp.getString(AlarmVO.ALARM_MESSAGE))) {
                alarmArray.add(temp);
            }
        }
        List<AlarmVO> alarmVOS = JSON.parseArray(alarmArray.toJSONString(), AlarmVO.class);
        alarmVOS.sort((Comparator.comparing(AlarmVO::getDevice)).thenComparing(AlarmVO::getTimestamp).reversed());
        return alarmVOS;
    }

    @Override
    public String[] getTodayWorkingHours(String terminal, String date) {
        HistoryDataParamDto historyDataParamDto = new HistoryDataParamDto();
        historyDataParamDto.setTerminalid(terminal);
        historyDataParamDto.setBeginTime(date + DataTimeUtil.DEFAULTSTARTTIME);
        historyDataParamDto.setEndTime(date + DataTimeUtil.DEFAULTENDTIME);
        String site = iSiteService.getSite(terminal, false);
        if (StringUtils.isEmpty(site)){
            log.error("终端 {} 没站点",terminal);
            return null;
        }
        String  tagName = site + "." + terminal + "." + TagsEnums.ENGINE_RUNNING_DURATION.getChinese();
        String result = this.getDeviceHistoryData(historyDataParamDto, tagName, false);
        JSONObject resultJson = (JSONObject) JSON.parse(result);
        if (resultJson.get("data") == null) {
            return null;
        }
        JSONObject jsonObject = new JSONObject();
        if (resultJson.get("data") instanceof JSONObject) {
            jsonObject = resultJson.getJSONObject("data");
        }
        String lastworktime = null;
        String firstworktime = null;
        String starttime = null;
        for (String key : jsonObject.keySet()) {
            String name = key.substring(key.lastIndexOf(".") + 1);
            if (TagsEnums.ENGINE_RUNNING_DURATION.getChinese().equals(name)){
                JSONArray jsonArray = jsonObject.getJSONArray(key);
                int size = jsonArray.size();
                if (size > 0){
                    for (int i = size;i>0;i--){
                        JSONObject js = jsonArray.getJSONObject(i-1);
                        String value = js.getString("value");
                        if (!"0".equals(value)){
                            lastworktime = value;
                            break;
                        }
                    }
                    for (int i = 0;i<size;i++){
                        JSONObject js = jsonArray.getJSONObject(i);
                        String value = js.getString("value");
                        if (!"0".equals(value)){
                            firstworktime = value;
                            starttime = DataTimeUtil.stampToDate(js.getString("timestamp"));
                            break;
                        }
                    }
                }
            }
        }
        if (StringUtils.isEmpty(lastworktime) || StringUtils.isEmpty(firstworktime)){
            return null;
        }
        double todayworktime = Double.parseDouble(lastworktime)- Double.parseDouble(firstworktime);
        DecimalFormat df = new DecimalFormat("0.0");
        String format = df.format(todayworktime);
        return new String[]{format,starttime};
    }
}
