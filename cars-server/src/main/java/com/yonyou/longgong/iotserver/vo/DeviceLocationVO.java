package com.yonyou.longgong.iotserver.vo;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 设备位置
 *
 * @author fengsheng
 * @创建时间 2019-11-11
 **/
@Data
public class DeviceLocationVO {

    public DeviceLocationVO() {

    }

    public DeviceLocationVO(String name) {
        this.name = name;
    }

    /**
     * 地区名称
     */
    private String name;
    /**
     * 地区分布数量
     */
    private int value = 0;

    /**
     * 初始化中国所有省市
     */
    public static Map<String, DeviceLocationVO> initChinaCity() {
        Map<String, DeviceLocationVO> deviceLocationVOs = new HashMap<>();
        deviceLocationVOs.put("北京市", new DeviceLocationVO("北京"));
        deviceLocationVOs.put("天津市", new DeviceLocationVO("天津"));
        deviceLocationVOs.put("上海市", new DeviceLocationVO("上海"));
        deviceLocationVOs.put("重庆市", new DeviceLocationVO("重庆"));
        deviceLocationVOs.put("河北省", new DeviceLocationVO("河北"));
        deviceLocationVOs.put("山西省", new DeviceLocationVO("山西"));
        deviceLocationVOs.put("辽宁省", new DeviceLocationVO("辽宁"));
        deviceLocationVOs.put("吉林省", new DeviceLocationVO("吉林"));
        deviceLocationVOs.put("黑龙江省", new DeviceLocationVO("黑龙江"));
        deviceLocationVOs.put("江苏省", new DeviceLocationVO("江苏"));
        deviceLocationVOs.put("浙江省", new DeviceLocationVO("浙江"));
        deviceLocationVOs.put("安徽省", new DeviceLocationVO("安徽"));
        deviceLocationVOs.put("福建省", new DeviceLocationVO("福建"));
        deviceLocationVOs.put("江西省", new DeviceLocationVO("江西"));
        deviceLocationVOs.put("山东省", new DeviceLocationVO("山东"));
        deviceLocationVOs.put("河南省", new DeviceLocationVO("河南"));
        deviceLocationVOs.put("湖北省", new DeviceLocationVO("湖北"));
        deviceLocationVOs.put("湖南省", new DeviceLocationVO("湖南"));
        deviceLocationVOs.put("广东省", new DeviceLocationVO("广东"));
        deviceLocationVOs.put("海南省", new DeviceLocationVO("海南"));
        deviceLocationVOs.put("四川省", new DeviceLocationVO("四川"));
        deviceLocationVOs.put("贵州省", new DeviceLocationVO("贵州"));
        deviceLocationVOs.put("云南省", new DeviceLocationVO("云南"));
        deviceLocationVOs.put("陕西省", new DeviceLocationVO("陕西"));
        deviceLocationVOs.put("甘肃省", new DeviceLocationVO("甘肃"));
        deviceLocationVOs.put("青海省", new DeviceLocationVO("青海"));
        deviceLocationVOs.put("台湾省", new DeviceLocationVO("台湾"));
        deviceLocationVOs.put("内蒙古自治区", new DeviceLocationVO("内蒙古"));
        deviceLocationVOs.put("广西壮族自治区", new DeviceLocationVO("广西"));
        deviceLocationVOs.put("西藏自治区", new DeviceLocationVO("西藏"));
        deviceLocationVOs.put("宁夏回族自治区", new DeviceLocationVO("宁夏"));
        deviceLocationVOs.put("新疆维吾尔自治区", new DeviceLocationVO("新疆"));
        deviceLocationVOs.put("香港特别行政区", new DeviceLocationVO("香港"));
        deviceLocationVOs.put("澳门特别行政区", new DeviceLocationVO("澳门"));
        return deviceLocationVOs;
    }
}
