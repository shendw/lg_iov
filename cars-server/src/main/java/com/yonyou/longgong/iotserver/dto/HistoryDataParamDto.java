package com.yonyou.longgong.iotserver.dto;

import lombok.Data;

/**
 * @author fengsheng
 * @创建时间 2019-11-11
 * @描述
 **/
@Data
public class HistoryDataParamDto {

    /**
     * 终端ID
     */
    private String terminalid;
    /**
     * 开始时间
     */
    private String beginTime;
    /**
     * 结束时间
     */
    private String endTime;
    /**
     * 时间单位：-1代表原始数据/0代表秒钟/1代表分钟
     */
    private String intervalType = "-1";
    /**
     * 步长（代表间隔时间）
     */
    private String interval = "1";
    /**
     * 页码
     */
    private int pageNum = 1;
    /**
     * 页数
     */
    private int pageSize = 10;
}
