package com.yonyou.longgong.iotserver.service;

import com.yonyou.longgong.carsserver.utils.ResponseData;
import org.springframework.web.bind.annotation.CookieValue;

/**
 * 命令字接口
 */
public interface IOrderCodeService {

    /**
     * 设置IP端口号
     *
     * @param terminalid 终端ID
     * @param ipAndPort  IP端口号
     * @return 执行结果
     */
    ResponseData execSet50H(String terminalid, String ipAndPort,Boolean isTest);

    /**
     * 设置锁车/解锁
     *
     * @param terminalid 终端ID
     * @param lock       是否锁车：true/false，字符类型
     * @return 执行结果
     */
    ResponseData execSet5AH(String terminalid, String lock,Boolean isTest,String loginName);

    /**
     * 预约解锁机
     *
     * @param terminalid 终端ID
     * @return 执行结果
     */
    ResponseData execSet5BH(String terminalid, Boolean isTest,String loginName);

    /**
     * 设置数据采集时间间隔
     *
     * @param terminalid 终端ID
     * @param paramJson  参数：{"energized_interval":"","not_energized_interval":""}，通电采集间隔/未通电采集间隔
     * @return 执行结果
     */
    ResponseData execSet51H(String terminalid, String paramJson,Boolean isTest);

    /**
     * 获取预约解锁采集时间
     * @param terminalid
     * @param isTest
     * @return
     */
    ResponseData execGet4BH(String terminalid,Boolean isTest);

    /**
     * 获取IP端口号
     *
     * @param terminalid 终端ID
     * @return 执行结果
     */
    ResponseData execGet40H(String terminalid,Boolean isTest);

    /**
     * 获取数据采集时间间隔
     *
     * @param terminalid 终端ID
     * @return 执行结果
     */
    ResponseData execGet41H(String terminalid,Boolean isTest);

    /**
     * 获取实时位置信息
     *
     * @param terminalid 终端ID
     * @return 执行结果
     */
    ResponseData execGet43H(String terminalid,Boolean isTest);

    /**
     * 获取实时时钟
     *
     * @param terminalid 终端ID
     * @return 执行结果
     */
    ResponseData execGet44H(String terminalid,Boolean isTest);

    /**
     * 获取锁车状态
     *
     * @param terminalid 终端ID
     * @return 执行结果
     */
    ResponseData execGet4AH(String terminalid,Boolean isTest);

    /**
     * 采集心跳指令
     *
     * @param terminalid
     * @return
     */
    ResponseData execGet47H(String terminalid,Boolean isTest);

    /**
     * 下发测试端口转正式端口命令
     *
     * @param terminalid
     * @return
     */
    ResponseData execSet50H(String terminalid,Boolean isTest);
}
