package com.yonyou.longgong.iotserver.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.exception.BusinessException;
import com.yonyou.longgong.carsserver.utils.DataTimeUtil;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.iotserver.dto.TerminalLogDto;
import com.yonyou.longgong.iotserver.service.ITerminalLogService;
import com.yonyou.longgong.iotserver.utils.HttpUtils;
import com.yonyou.longgong.iotserver.vo.TerminalLogVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.*;

/**
 * @author shendawei
 * @创建时间 2020/1/6
 * @描述
 **/
@Service("iTerminalLogService")
public class TerminalLogServiceImpl implements ITerminalLogService {

    @Value("${iot.domain-test}")
    private String domainTest;

    @Value("${iot.domain}")
    private String domain;

    @Override
    public PageInfo<TerminalLogVO> selectTerminalLogByPage(TerminalLogDto terminalLogDto, Boolean isTest) {
        StringBuilder builder = new StringBuilder();
        if (isTest){
            builder.append(domainTest);
        }else{
            builder.append(domain);
        }
        builder.append(this.selectRule(terminalLogDto,isTest));
        Map<String, String> param = new HashMap<>(1);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("pageNum",terminalLogDto.getPageNum()-1);
        jsonObject.put("pagesize",terminalLogDto.getPageSize());
        try {
            param.put("pageInfo",URLEncoder.encode(jsonObject.toString(),"utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String count = HttpUtils.sendGet(builder.toString()+"/count", param);
        Integer total = parseCount(count);
        PageInfo<TerminalLogVO> terminalLogVOPageInfo = new PageInfo<>();
        if (total > 0 ){
            String result = HttpUtils.sendGet(builder.toString(), param);
            List<TerminalLogVO> terminalLogVOS = parseResult(result);
            terminalLogVOPageInfo.setList(terminalLogVOS);
            terminalLogVOPageInfo.setPageNum(terminalLogDto.getPageNum());
            terminalLogVOPageInfo.setPageSize(terminalLogDto.getPageSize());
            terminalLogVOPageInfo.setTotal(total);
        }
        return terminalLogVOPageInfo;
    }

    @Override
    public List<TerminalLogVO> selectTerminalLog(TerminalLogDto terminalLogDto, Boolean isTest) {
        StringBuilder builder = new StringBuilder();
        if (isTest){
            builder.append(domainTest);
        }else{
            builder.append(domain);
        }
        builder.append(this.selectRule(terminalLogDto,isTest));
        String result = HttpUtils.sendGet(builder.toString(), null);
        return parseResult(result);
    }

    private Integer parseCount(String result){
        JSONObject resultJson = (JSONObject) JSON.parse(result);
        String count = resultJson.getString("data");
        if (StringUtils.isNotBlank(count)){
            return Integer.parseInt(count);
        }else {
            return 0;
        }
    }

    private List<TerminalLogVO> parseResult(String result){
        JSONObject resultJson = (JSONObject) JSON.parse(result);
        Object data = resultJson.get("data");
        List<TerminalLogVO> terminalLogVOS = new ArrayList<>();
        if (data instanceof JSONArray && ((JSONArray) data).size() > 0){
            JSONArray resultDatas = (JSONArray) data;
            if (resultDatas.size() > 0 ){
                for (int i = 0;i<resultDatas.size();i++){
                    JSONObject resultData = resultDatas.getJSONObject(i);
                    TerminalLogVO terminalLogVO = new TerminalLogVO();
                    terminalLogVO.setChannel(resultData.getString("keywords"));
                    terminalLogVO.setDetail(resultData.getString("message"));
                    terminalLogVO.setOpkeyword(resultData.getString("cmd"));
                    terminalLogVO.setTerminalid(resultData.getString("originDevice"));
                    String timestamp = StringUtils.isBlank(resultData.getString("timestamp"))?"0":resultData.getString("timestamp");
                    terminalLogVO.setOptime(DataTimeUtil.stampToDate(timestamp));
                    String direction = resultData.getString("direction");
                    terminalLogVO.setDirection(StringUtils.isBlank(direction)? null:
                            direction.equalsIgnoreCase("UpMessage")?"上行":
                                    direction.equalsIgnoreCase("DownMessage")?"下行":null);
                    terminalLogVOS.add(terminalLogVO);
                }
            }
        }
        terminalLogVOS.sort(Comparator.comparing(TerminalLogVO::getOptime).reversed());
        return terminalLogVOS;
    }

    private String selectRule(TerminalLogDto terminalLogDto,Boolean isTest){
        String terminalid = terminalLogDto.getTerminalid();
        String cmd = terminalLogDto.getCmd();
        String keywords = terminalLogDto.getKeywords();
        String startTime = terminalLogDto.getStartTime();
        String endTime = terminalLogDto.getEndTime();
        if (StringUtils.isBlank(terminalid) || StringUtils.isBlank(startTime) || StringUtils.isBlank(endTime)){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        StringBuilder builder = new StringBuilder();
        builder.append("/support-commview/api/v1/message");
        builder.append("/service");
        if (isTest){
            builder.append("/device-rcms-6210-6270");
        }else {
            builder.append("/device-rcms-6210");
        }
        builder.append("/OriginDevice/");
        builder.append(terminalid);
        if (StringUtils.isNotBlank(keywords)){
            builder.append("/keywords/");
            builder.append(keywords);
        }
        if (StringUtils.isNotBlank(cmd)){
            builder.append("/cmd/");
            builder.append(cmd);
        }
        try {
            // 将传入的时间转换为时间戳格式
            startTime = DataTimeUtil.dateToStamp(startTime);
            endTime = DataTimeUtil.dateToStamp(endTime);
        } catch (ParseException e) {
            throw new BusinessException(ResponseCode.ERROR_TIMEFORMAT);
        }
        builder.append("/"+startTime+"000");
        builder.append("/"+endTime+"000");
        return builder.toString();
    }
}
