package com.yonyou.longgong.iotserver.utils;

/**
 * @author shendawei
 * @创建时间 2019-11-26
 * @描述
 **/
public class RedisLock {
    private RedisUtils redis;

    String lockKey;
    int expireMsecs = 60000;
    int timeoutMsecs = 10000;
    boolean locked;

    public RedisLock(RedisUtils redis,String lockKey) {
        this.redis = redis;
        this.expireMsecs = 60000;
        this.timeoutMsecs = 10000;
        this.locked = false;
        this.lockKey = lockKey;
    }

    public RedisLock(RedisUtils redis,String lockKey, int timeoutMsecs) {
        this(redis,lockKey);
        this.timeoutMsecs = timeoutMsecs;
    }

    public RedisLock(RedisUtils redis,String lockKey, int timeoutMsecs,
                     int expireMsecs) {
        this(redis,lockKey, timeoutMsecs);
        this.expireMsecs = expireMsecs;
    }

    public String getLockKey() {
        return this.lockKey;
    }

    /**
     * 获得 lock. 实现思路: 主要是使用了redis 的setnx命令,缓存了锁. reids缓存的key是锁的key,所有的共享,
     * value是锁的到期时间(注意:这里把过期时间放在value了,没有时间上设置其超时时间) 执行过程:
     * 1.通过setnx尝试设置某个key的值,成功(当前没有这个锁)则返回,成功获得锁
     * 2.锁已经存在则获取锁的到期时间,和当前时间比较,超时的话,则设置新的值
     *
     */

    public synchronized boolean acquire()
            throws InterruptedException {

        long expires = System.currentTimeMillis() + (long) this.expireMsecs
                + 1L;
        // 锁到期时间
        String expiresStr = String.valueOf(expires);
        if (redis.setnx(this.lockKey, expiresStr)) {
            this.locked = true;
            return true;
        }
        // 判断是否为空，不为空的情况下，如果被其他线程设置了值，则第二个条件判断是过不去的
        // 获取上一个锁到期时间，并设置现在的锁到期时间，
        // 只有一个线程才能获取上一个线上的设置时间，因为jedis.getSet是同步的
        String currentValueStr = String.valueOf(redis.get(this.lockKey));
        if (currentValueStr != null
                && Long.parseLong(currentValueStr) < System.currentTimeMillis()) {
            String oldValueStr = String.valueOf(redis.getSet(this.lockKey, expiresStr));
            if (oldValueStr != null && oldValueStr.equals(currentValueStr)) {
                this.locked = true;
                return true;
            }
        }
        return false;
    }

    public synchronized void release() {
        if (this.locked) {
            redis.del(new String[] { this.lockKey });
            this.locked = false;
        }

    }
}
