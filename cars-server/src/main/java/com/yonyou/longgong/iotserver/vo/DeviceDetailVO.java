package com.yonyou.longgong.iotserver.vo;

import lombok.Data;

@Data
public class DeviceDetailVO {
    // 整机编号
    private String carno;
    // 终端ID
    private String device;
    // SIM卡
    private String simid;
    // 机型
    private String machinemodel;
    // 型号
    private String carmodel;
    // 入库日期
    private String instocktime;
    // 出厂日期
    private String outstocktime;
    // 销售日期
    private String saletime;
    // 终端提供商
    private String gps_manufacturer;
    // 销售公司
    private String salecompany;
    // 最终客户
    private String finalcustomer;
    // 位置
    private String coordinates;
    // 中文位置
    private String location;
    // 最新锁车状态
    private String locked_state;
    // 最新锁车时间
    private String locked_date;
    // 终端版本
    private String gps_version;
    // 速度
    private String ground_speed;
    // 方向
    private String ground_heading;
    // GPS信号
    private String gsm_signal_strength;
    // 工作小时
    private String engine_running_duration;
    // 机器电压
    private String machine_voltage;
    // 发动机水温
    private String engine_water_temperature;
    // 液压油温
    private String hydraulic_oil_temperature;
    // 机油压力
    private String oil_pressure;
    // 燃油油位
    private String fuel_level;
    // 发动机转速
    private String engine_speed;
    // 油门档位
    private String throttle_gear;
    // 终端电池电压
    private String rcm_battery_voltage;
    // 机器时间
    private String machine_clock_time;
    // GPS时间
    private String gps_time;
    // 最新报警信息
    private String alarm_message;
    /**
     * 当日工作时长
     */
    private String todayWorkingHours;
}
