package com.yonyou.longgong.iotserver.web;

import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import com.yonyou.longgong.iotserver.dto.HistoryDataParamDto;
import com.yonyou.longgong.iotserver.service.IHistoricalTrackService;
import com.yonyou.longgong.iotserver.vo.HistoricalTrackVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 历史轨迹
 *
 * @author fengsheng
 * @创建时间 2019-11-11
 **/
@RestController
@RequestMapping(value = "/iot")
public class HistoricalTrackController {

    @Autowired
    private IHistoricalTrackService historicalTrackService;

    @RequestMapping(value = "/getHistoricalTrack")
    public ResponseData getHistoricalTrack(HistoryDataParamDto historyDataParamDto) {
        List<HistoricalTrackVO> historicalTrackVOS = historicalTrackService.getHistoricalTrack(historyDataParamDto);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(), historicalTrackVOS);
    }
}
