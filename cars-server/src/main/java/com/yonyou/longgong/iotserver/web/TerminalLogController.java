package com.yonyou.longgong.iotserver.web;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import com.yonyou.longgong.iotserver.dto.TerminalLogDto;
import com.yonyou.longgong.iotserver.service.ITerminalLogService;
import com.yonyou.longgong.iotserver.vo.TerminalLogVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @author shendawei
 * @创建时间 2020/1/6
 * @描述
 **/
@RestController
@RequestMapping("/iotlog")
@Slf4j
public class TerminalLogController {

    @Autowired
    private ITerminalLogService iTerminalLogService;

    @PostMapping(value = "/getOplog")
    public ResponseData getOplog(TerminalLogDto terminalLogDto){
        PageInfo<TerminalLogVO> terminalLogVOPageInfo = iTerminalLogService.selectTerminalLogByPage(terminalLogDto, false);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),terminalLogVOPageInfo);
    }

    @PostMapping(value = "/getOplog/test")
    public ResponseData getOplogTest(TerminalLogDto terminalLogDto){
        PageInfo<TerminalLogVO> terminalLogVOPageInfo = iTerminalLogService.selectTerminalLogByPage(terminalLogDto, true);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),terminalLogVOPageInfo);
    }

    @RequestMapping(value = "/excelExport")
    public void export(TerminalLogDto terminalLogDto, HttpServletResponse response){
        try{
            List<TerminalLogVO> terminalLogVOS = iTerminalLogService.selectTerminalLog(terminalLogDto, false);
            Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams("操作日志", "操作日志"), TerminalLogVO.class, terminalLogVOS);
            // 告诉浏览器用什么软件可以打开此文件
            response.setHeader("content-Type", "application/vnd.ms-excel");
            // 下载文件的默认名称
            response.setHeader("Content-Disposition", "attachment;filename=oplog.xls");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            sheets.write(response.getOutputStream());
            return;
        }catch (IOException e){
            log.error("测试操作日志导出失败",e);
        }
    }

    @RequestMapping(value = "/excelExport/test")
    public void exportTest(TerminalLogDto terminalLogDto, HttpServletResponse response){
        try{
            List<TerminalLogVO> terminalLogVOS = iTerminalLogService.selectTerminalLog(terminalLogDto, true);
            Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams("操作日志", "操作日志"), TerminalLogVO.class, terminalLogVOS);
            // 告诉浏览器用什么软件可以打开此文件
            response.setHeader("content-Type", "application/vnd.ms-excel");
            // 下载文件的默认名称
            response.setHeader("Content-Disposition", "attachment;filename=oplog.xls");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            sheets.write(response.getOutputStream());
            return;
        }catch (IOException e){
            log.error("操作日志导出失败",e);
        }
    }
}
