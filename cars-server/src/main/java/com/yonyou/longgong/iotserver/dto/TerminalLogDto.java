package com.yonyou.longgong.iotserver.dto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2020/1/6
 * @描述
 **/
@Data
public class TerminalLogDto {

    /**
     * 终端id
     */
    private String terminalid;
    /**
     * 指令
     */
    private String cmd;
    /**
     * 开始时间
     */
    private String startTime;
    /**
     * 结束时间
     */
    private String endTime;
    /**
     * 关键字
     */
    private String keywords;
    /**
     * 页码
     */
    private int pageNum = 1;
    /**
     * 页数
     */
    private int pageSize = 10;

}
