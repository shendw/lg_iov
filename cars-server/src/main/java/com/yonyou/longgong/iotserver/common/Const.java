package com.yonyou.longgong.iotserver.common;

/**
 * @author shendawei
 * @创建时间 2019-11-26
 * @描述
 **/
public class Const {
    /**
     * terminal cache
     */
    public static final String PREFIX_TERMINAL = "history:terminalid:";
    /**
     * location cache
     */
    public static final String  CARNO = "carno";
    /**
     * 定位 cache
     */
    public static final String  LOCATION = "location";
    /**
     * 工作小时 cache
     */
    public static final String  WORKTIME = "worktime";
    /**
     * 燃油油位 cache
     */
    public static final String  FUEL_LEVEL = "fuellevel";
    /**
     * updatetime cache
     */
    public static final String  UPDATETIME = "updatetime";
    /**
     * lock cache
     */
    public static final String  LOCKSTATUS = "lockstatus";
    /**
     * location cache
     */
    public static final String PREFIX_EXEC = "exec:terminalid:";
}
