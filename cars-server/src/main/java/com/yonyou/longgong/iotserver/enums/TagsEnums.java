package com.yonyou.longgong.iotserver.enums;

import lombok.Getter;


/**
 * IOT仪表位号信息
 *
 * @author fengsheng
 * @创建时间 2019-11-11
 **/
@Getter
public enum TagsEnums {
    CAR_INFORMATION("car_information", "整车信息"),
    CAR_TYPE("car_type", "整车型号"),
    CAR_MANUFACTURER("car_manufacturer", "整车制造商"),
    GPS_VERSION("gps_version", "GPS终端版本"),
    GPS_MANUFACTURER("gps_manufacturer", "GPS终端制造商"),
    // IPPROT("ipprot", "IP端口号"),
    ACQUISITION_INTERVAL("acquisition_interval", "数采时钟间隔"),
    REAL_TIME("real_time", "实时时钟"),
    GPS_MONITOR_STATUS("gps_monitor_status", "GPS监控状态"),
    GPS_ANTENNA_FAILURE_ALARM("gps_antenna_failure_alarm", "GPS天线故障"),
    GPRS_ANTENNA_FAILURE_ALARM("gprs_antenna_failure_alarm", "GPRS天线故障"),
    MACHINE_VOLTAGE_FAULT_ALARM("machine_voltage_fault_alarm", "机器电压报警"),
    RCM_BATTERY_VOLTAGE_FAULT_ALARM("rcm_battery_voltage_fault_alarm", "RCM电池电压报警"),
    COMMUNICATION_ALARM("communication_alarm", "CAN通讯中断报警"),
    MACHINE_OUT_OF_RANGE_ALARM("machine_out_of_range_alarm", "机器越界报警"),
    MACHINE_START("machine_start", "机器启动"),
    MACHINE_SHUTDOWN("machine_shutdown", "机器停机"),
    MACHINE_ELECTRIFY("machine_electrify", "机器通电"),
    SMS_ENABLE("sms_enable", "SMS启用"),
    GPS_TIME("gps_time", "GPS时间"),
    GPS_POSITION_STATUS("gps_position_status", "GPS定位状态"),
    COORDINATES("coordinates", "经纬度"),
    GROUND_SPEED("ground_speed", "地面速度"),
    GROUND_HEADING("ground_heading", "地面航向"),
    MAGNETIC_DECLINATION("magnetic_declination", "磁偏角"),
    DECLINATION_DIRECTION("declination_direction", "磁偏角方向"),
    NUMBER_OF_SATELLITES("number_of_satellites", "卫星个数"),
    ALTITUDE("altitude", "海拔高度"),
    GSM_SIGNAL_STRENGTH("gsm_signal_strength", "GSM信号强度"),
    MACHINE_VOLTAGE("machine_voltage", "机器电压"),
    ENGINE_RUNNING_DURATION("engine_running_duration", "发动机运行时间"),
    ENGINE_WATER_TEMPERATURE("engine_water_temperature", "发动机水温"),
    HYDRAULIC_OIL_TEMPERATURE("hydraulic_oil_temperature", "液压油温"),
    OIL_PRESSURE("oil_pressure", "机油压力"),
    FUEL_LEVEL("fuel_level", "燃油油位"),
    ENGINE_SPEED("engine_speed", "发动机转速"),
    THROTTLE_GEAR("throttle_gear", "油门档位"),
    MACHINE_OPERATION_MODE("machine_operation_mode", "机器工作模式"),
    MACHINE_INFORMATION("machine_information", "机器信息"),
    MACHINE_CLOCK_TIME("machine_clock_time", "机器时钟时间"),
    RCM_BATTERY_VOLTAGE("rcm_battery_voltage", "RCM电池电压"),
    RCM_CLOCK_TIME("rcm_clock_time", "RCM时钟时间"),
    DAILY_DISCHARGE("daily_discharge", "日流量"),
    CENTER_LONGITUDE("center_longitude", "左上角经度或圆心经度"),
    CENTER_LATITUDE("center_latitude", "左上角纬度或圆心纬度"),
    RADIUS("radius", "右下角经度或半径"),
    RECOGNITION_CONSTANT("recognition_constant", "右下角纬度或识别常数"),
    LATEST_LANDING_TIME("latest_landing_time", "最新登录时间"),
    LAST_TIME("last_time", "最后时间"),
    COMMUNICATION_MODE("communication_mode", "通讯方式"),
    COMMUNICATION_STATE("communication_state", "通讯状态"),
    LOCKED_STATE("locked_state", "锁车状态"),
    ;

    private final String english;
    private final String chinese;

    TagsEnums(String english, String chinese) {
        this.english = english;
        this.chinese = chinese;
    }

    //根据枚举类的 code 值，获取枚举类型；当枚举类与switch结合使用时，此方法是必须的
    public static TagsEnums getEnumByKey(String key) {
        for (TagsEnums deviceType : TagsEnums.values()) {
            if (deviceType.getChinese().equals(key)) {
                return deviceType;
            }
        }
        return null;
    }
}
