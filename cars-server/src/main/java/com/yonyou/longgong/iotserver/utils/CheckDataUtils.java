package com.yonyou.longgong.iotserver.utils;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CheckDataUtils {

    public static <T> List<T> checkFieldIsBlank(Class<T> clazz, List<T> voList, Map<String, String> checkFields) throws Exception {
        List<T> resultData = new ArrayList<>();
        Class clz = Class.forName(clazz.getName());
        for (T vo : voList) {
            int blankLength = 0;
            Field[] fields = clz.getDeclaredFields();
            for (Field field : fields) {
                if (checkFields.containsKey(field.getName())) {
                    Method method = clz.getMethod(checkFields.get(field.getName()));
                    Object value = method.invoke(vo);
                    if (value == null || StringUtils.isBlank(value.toString())) {
                        blankLength++;
                    }
                }
            }
            if (blankLength != checkFields.keySet().size()) {
                resultData.add(vo);
            }
        }
        return resultData;
    }
}
