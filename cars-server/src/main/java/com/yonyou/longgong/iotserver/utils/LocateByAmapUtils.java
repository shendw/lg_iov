package com.yonyou.longgong.iotserver.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yonyou.longgong.iotserver.vo.DeviceLocationVO;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 根据高德进行定位的工具类
 */
public class LocateByAmapUtils {

    private static final int MAXSIZE = 20;
    private static final String SUCCESS = "1";

    public static List<DeviceLocationVO> getProvinceDistribution(JSONArray locationArray) {
        Map<String, DeviceLocationVO> deviceLocationVOMap = DeviceLocationVO.initChinaCity();
        if (locationArray == null || locationArray.size() == 0) {
            return new ArrayList<>(deviceLocationVOMap.values());
        }
        String url = "https://restapi.amap.com/v3/geocode/regeo";
        Map<String, String> parameters = new HashMap<>();
        parameters.put("key", "0482dc6466cce6bfc90555f404852d43");
        parameters.put("location", "");
        parameters.put("batch", "true");
        for (int i = 0; i < locationArray.size(); i++) {
            StringBuilder locations = new StringBuilder();
            for (int j = 0; j < MAXSIZE && i + j < locationArray.size(); j++) {
                locations.append(locationArray.get(i + j)).append("%7C");
            }
            locations = new StringBuilder(locations.substring(0, locations.lastIndexOf("%7C")));
            parameters.put("location", locations.toString());
            String resultStr = HttpUtils.sendGet(url, parameters);
            JSONObject resultJson = (JSONObject) JSON.parse(resultStr);
            if (SUCCESS.equals(resultJson.get("status"))) {
                JSONArray regeocodes = resultJson.getJSONArray("regeocodes");
                for (int j = 0; j < regeocodes.size(); j++) {
                    String province = regeocodes.getJSONObject(j).getJSONObject("addressComponent").getString("province");

                    if (deviceLocationVOMap.containsKey(province)) {
                        DeviceLocationVO deviceLocationVO = deviceLocationVOMap.get(province);
                        deviceLocationVO.setValue(deviceLocationVO.getValue() + 1);
                        deviceLocationVOMap.put(province, deviceLocationVO);
                    }
                }
            }
            i = i + (MAXSIZE - 1);
        }
        return new ArrayList<>(deviceLocationVOMap.values());
    }

    public static String getAddress(String location){
        String url = "https://restapi.amap.com/v3/geocode/regeo";
        Map<String, String> parameters = new HashMap<>();
        parameters.put("key", "0482dc6466cce6bfc90555f404852d43");
        parameters.put("location", location);
        parameters.put("radius", "1000");
        parameters.put("extensions", "base");
        parameters.put("output", "json");
        String resultStr = HttpUtils.sendGet(url, parameters);
        JSONObject resultJson = (JSONObject) JSON.parse(resultStr);
        if (SUCCESS.equals(resultJson.get("status"))) {
            JSONObject regeocode = resultJson.getJSONObject("regeocode");
            return regeocode.getString("formatted_address");
        }
        return null;
    }

    public static String getLocation(String address){
        String url = "https://restapi.amap.com/v3/geocode/geo";
        Map<String, String> parameters = new HashMap<>();
        parameters.put("key", "0482dc6466cce6bfc90555f404852d43");
        parameters.put("address", address);
        parameters.put("output", "json");
        String resultStr = HttpUtils.sendGet(url, parameters);
        JSONObject resultJson = (JSONObject) JSON.parse(resultStr);
        if (SUCCESS.equals(resultJson.get("status"))) {
            JSONArray jsonArray = JSON.parseArray(resultJson.getString("geocodes"));
            JSONObject jsonObject = (JSONObject) jsonArray.get(0);
            return jsonObject.getString("location");
        }
        return null;
    }

}
