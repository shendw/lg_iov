package com.yonyou.longgong.iotserver.vo;

import lombok.Data;

import java.util.List;

@Data
public class DeviceOverView {
    /**
     * 设备总数
     */
    private int totalnum;
    /**
     * 在线数量
     */
    private int onlinenum;
    /**
     * 异常情况
     */
    private RatioVO ratioVO;
    /**
     * 车辆分布情况
     */
    private List<DeviceLocationVO> deviceLocationVO;
}
