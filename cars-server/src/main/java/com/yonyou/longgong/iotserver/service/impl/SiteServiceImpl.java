package com.yonyou.longgong.iotserver.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yonyou.longgong.carsserver.exception.BusinessException;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.iotserver.service.ISiteService;
import com.yonyou.longgong.iotserver.utils.HttpUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * @author shendawei
 * @创建时间 2020/4/7
 * @描述
 **/
@Service("iSiteService")
public class SiteServiceImpl implements ISiteService {

    @Value("${iot.domain}")
    private String domain;
    @Value("${iot.domain-test}")
    private String domainTest;

    @Override
    public String getSite(String terminalid, Boolean isTest) {
        String pre = isTest ? domainTest : domain;
        String url = pre + "/core-metadata/api/v1/device/simple";
        Map<String, String> parameters = new HashMap<>();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("key",terminalid);
        try {
            parameters.put("query",URLEncoder.encode(jsonObject.toString(),"utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        jsonObject.clear();
        jsonObject.put("pageNum",0);
        jsonObject.put("pagesize",1);
        try {
            parameters.put("pageInfo", URLEncoder.encode(jsonObject.toString(),"utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String result = HttpUtils.sendGet(url, parameters);
        JSONObject resultJson = (JSONObject) JSON.parse(result);
        if (resultJson == null || resultJson.get("data") == null) {
            throw new BusinessException(ResponseCode.IOV_SITE_IS_NULL);
        }
        JSONArray data = resultJson.getJSONArray("data");
        if (data == null || data.size() == 0){
            throw new BusinessException(ResponseCode.IOV_SITE_IS_NULL);
        }
        String site = data.getJSONObject(0).getString("site");
        return site;
//        return "site1";
    }
}
