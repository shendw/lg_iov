package com.yonyou.longgong.iotserver.vo;

import lombok.Data;

import java.util.List;

@Data
public class RatioVO {
    /**
     * 数量
     */
    private int quantity;
    /**
     * 总数
     */
    private int total_quantity;
    /**
     * 返回数据
     */
    private List data;
}
