package com.yonyou.longgong.iotserver.service;

/**
 * @author shendawei
 * @创建时间 2020/4/7
 * @描述
 **/
public interface ISiteService {
    /**
     * iot多站点配置，根据终端号获取站点
     * @param terminalid
     * @param isTest
     * @return
     */
    String getSite(String terminalid,Boolean isTest);
}
