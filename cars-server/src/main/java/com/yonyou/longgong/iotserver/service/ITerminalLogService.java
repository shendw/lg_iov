package com.yonyou.longgong.iotserver.service;

import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.iotserver.dto.TerminalLogDto;
import com.yonyou.longgong.iotserver.vo.TerminalLogVO;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2020/1/6
 * @描述
 **/
public interface ITerminalLogService {
    PageInfo<TerminalLogVO> selectTerminalLogByPage(TerminalLogDto terminalLogDto,Boolean isTest);
    List<TerminalLogVO> selectTerminalLog(TerminalLogDto terminalLogDto,Boolean isTest);
}
