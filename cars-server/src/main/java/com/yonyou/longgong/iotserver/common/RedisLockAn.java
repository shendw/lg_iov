package com.yonyou.longgong.iotserver.common;

import java.lang.annotation.*;

/**
 * @author yushilin
 * @version 1.0
 * @description: redis锁
 * @date 2021/4/27 16:46
 */

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface RedisLockAn {

}
