package com.yonyou.longgong.iotserver.dto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-11-21
 * @描述
 **/
@Data
public class AmapTransLocationDto {

    private String status;
    private String info;
    private String infocode;
    private String locations;
}
