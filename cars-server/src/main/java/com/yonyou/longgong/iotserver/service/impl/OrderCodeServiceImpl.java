package com.yonyou.longgong.iotserver.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yonyou.longgong.carsserver.enums.LockCarEnum;
import com.yonyou.longgong.carsserver.pojo.OplogPojo;
import com.yonyou.longgong.carsserver.service.ILockLogService;
import com.yonyou.longgong.carsserver.service.IOplogService;
import com.yonyou.longgong.carsserver.utils.DataTimeUtil;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import com.yonyou.longgong.iotserver.common.Const;
import com.yonyou.longgong.iotserver.common.RedisLockAn;
import com.yonyou.longgong.iotserver.enums.IOTEnums;
import com.yonyou.longgong.iotserver.service.IOrderCodeService;
import com.yonyou.longgong.iotserver.utils.HttpUtils;
import com.yonyou.longgong.iotserver.utils.RedisLock;
import com.yonyou.longgong.iotserver.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;

@Service(value = "orderCodeService")
@Slf4j
public class OrderCodeServiceImpl implements IOrderCodeService {

    @Value("${iot.domain-test}")
    private String domainTest;

    @Value("${iot.domain}")
    private String domain;

    @Autowired
    private IOplogService iOplogService;

    @Autowired
    private RedisUtils redis;

    @Autowired
    private ILockLogService iLockLogService;

    private ResponseData execGetOrderCode(String url, String terminalid, String ordercode) {
        String content = "报文内容";
        String currDate = DataTimeUtil.dateToStr(new Date());
        log.info("命令字采集请求路径:{},参数:{}",url,null);
        String result = HttpUtils.sendGet(url, null);
        log.info("iot返回数据:{}",result);
        JSONObject resultJson = (JSONObject) JSON.parse(result);
        Object resultData = resultJson.get("data");
        if (resultData instanceof JSONObject) {
            OplogPojo oplogPojo = new OplogPojo(terminalid, "GPRS", currDate, "采集", content, ordercode, "是");
            iOplogService.insertOplog(oplogPojo);
            return ResponseData.result(ResponseCode.SUCCESS.getCode(), resultData);
        } else {
            OplogPojo oplogPojo = new OplogPojo(terminalid, "GPRS", currDate, "采集", content, ordercode, "否");
            iOplogService.insertOplog(oplogPojo);
            if (resultData != null) {
                String resultStatus = resultData.toString();
                IOTEnums errorEnums = Arrays.stream(IOTEnums.values()).filter(iotEnums -> iotEnums.getCode().equals(resultStatus)).findFirst().orElse(null);
                return ResponseData.result(ResponseCode.ERROR.getCode(), ResponseCode.ERROR.getDesc() + errorEnums.getDesc());
            } else {
                return ResponseData.result(ResponseCode.ERROR.getCode(), ResponseCode.ERROR.getDesc());
            }
        }
    }

    private ResponseData execSettingOrderCode(String url, String jsonStr, String terminalid, String ordercode) {
        String content = "报文内容";
        String currDate = DataTimeUtil.dateToStr(new Date());
        log.info("命令字设置请求路径:{},参数:{}",url,jsonStr);
        String result = HttpUtils.sendPut(url, jsonStr);
        log.info("iot返回数据:{}",result);
        JSONObject resultJson = (JSONObject) JSON.parse(result);
        String resultStatus = resultJson.getString("data");
        if (IOTEnums.SUCCESS.getCode().equals(resultStatus)) {
            OplogPojo oplogPojo = new OplogPojo(terminalid, "GPRS", currDate, "设置", result, ordercode, "是");
            iOplogService.insertOplog(oplogPojo);
            JSONObject resultData = new JSONObject();
            resultData.put("operatetime", currDate);
            resultData.put("operatedesc", IOTEnums.SUCCESS.getDesc());
            return ResponseData.result(ResponseCode.SUCCESS.getCode(), resultData);
        } else {
            OplogPojo oplogPojo = new OplogPojo(terminalid, "GPRS", currDate, "设置", result, ordercode, "否");
            iOplogService.insertOplog(oplogPojo);
            IOTEnums errorEnums = Arrays.stream(IOTEnums.values()).filter(iotEnums -> iotEnums.getCode().equals(resultStatus)).findFirst().orElse(null);
            JSONObject resultData = new JSONObject();
            resultData.put("operatetime", currDate);
            if (errorEnums != null) {
                resultData.put("operatedesc", ResponseCode.ERROR.getDesc() + errorEnums.getDesc());
                return ResponseData.result(ResponseCode.ERROR.getCode(), resultData);
            }
            resultData.put("operatedesc", ResponseCode.ERROR.getDesc());
            return ResponseData.result(ResponseCode.ERROR.getCode(), resultData);
        }
    }

    @Override
    public ResponseData execSet50H(String terminalid, String ipAndPort,Boolean isTest) {
        if (StringUtils.isBlank(terminalid) || StringUtils.isBlank(ipAndPort)) {
            return ResponseData.result(ResponseCode.ERROR_PARAM_IS_NULL.getCode(), ResponseCode.ERROR_PARAM_IS_NULL.getDesc());
        }
        RedisLock redisLock = new RedisLock(redis, Const.PREFIX_EXEC + terminalid, 0, 300000);
        try {
            if (redisLock.acquire()) {
                String url = domain + "/core-command/api/v1/device/" + terminalid + "/Set50H";
                if (isTest){
                    url = domainTest + "/core-command/api/v1/device/" + terminalid + "/Set50H";
                }
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("IP端口号", ipAndPort);
                JSONArray jsonArray = new JSONArray();
                jsonArray.add(jsonObject);
                return this.execSettingOrderCode(url, jsonArray.toJSONString(), terminalid, "50H");
            } else {
                return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
            }
        } catch (Exception e) {
            return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
        } finally {
            redisLock.release();
        }
    }

    @Override
    public ResponseData execSet5AH(String terminalid, String lock,Boolean isTest,String loginName) {
        if (StringUtils.isBlank(terminalid) || StringUtils.isBlank(lock)) {
            return ResponseData.result(ResponseCode.ERROR_PARAM_IS_NULL.getCode(), ResponseCode.ERROR_PARAM_IS_NULL.getDesc());
        }
        RedisLock redisLock = new RedisLock(redis, Const.PREFIX_EXEC + terminalid, 0, 300000);
        try {
            if (redisLock.acquire()) {
                String url = domain + "/core-command/api/v1/device/" + terminalid + "/Set5AH";
                if (isTest){
                    url = domainTest + "/core-command/api/v1/device/" + terminalid + "/Set5AH";
                }
                JSONObject jsonObject = new JSONObject();
                if ("false".equals(lock)){
                    lock = "0";
                }else if ("true".equals(lock)){
                    lock = "1";
                }
                jsonObject.put("锁车状态", lock);
                JSONArray jsonArray = new JSONArray();
                jsonArray.add(jsonObject);
                ResponseData responseData = this.execSettingOrderCode(url, jsonArray.toJSONString(), terminalid, "5AH");
                //锁车状态记录以及保存
                if (responseData.getStatus().equals(ResponseCode.SUCCESS.getCode())) {
                    iLockLogService.insertLocklog(terminalid,loginName,lock);
                }
                return responseData;
            } else {
                return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
            }
        } catch (InterruptedException e) {
            return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
        } finally {
            redisLock.release();
        }
    }

    @Override
    @RedisLockAn
    public ResponseData execSet5BH(String terminalid, Boolean isTest, String loginName) {
        if (StringUtils.isBlank(terminalid) ) {
            return ResponseData.result(ResponseCode.ERROR_PARAM_IS_NULL.getCode(), ResponseCode.ERROR_PARAM_IS_NULL.getDesc());
        }
        /*RedisLock redisLock = new RedisLock(redis, Const.PREFIX_EXEC + terminalid, 0, 300000);
        try {
            if (redisLock.acquire()) {
                String url = domain + "/core-command/api/v1/device/" + terminalid + "/Set5BH";
                if (isTest){
                    url = domainTest + "/core-command/api/v1/device/" + terminalid + "/Set5BH";
                }
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("锁车日期","");
                jsonObject.put("剩余锁车时间","");
                JSONArray jsonArray = new JSONArray();
                jsonArray.add(jsonObject);
                //ResponseData responseData = this.execSettingOrderCode(url, jsonArray.toJSONString(), terminalid, "5BH");
                ResponseData responseData = new ResponseData(ResponseCode.SUCCESS.getCode(),"模拟发送命令成功！");
                //锁车状态记录以及保存
                if (responseData.getStatus().equals(ResponseCode.SUCCESS.getCode())) {
                    //iLockLogService.addLocklog(terminalid,loginName, LockCarEnum.RESERVATIONUNLOCK.getCode());
                    System.out.println("aaaa");
                }
                return responseData;
            } else {
                return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
            }
        } catch (InterruptedException e) {
            return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
        } finally {
            redisLock.release();
        }*/
        String url = domain + "/core-command/api/v1/device/" + terminalid + "/Set5BH";
        if (isTest){
            url = domainTest + "/core-command/api/v1/device/" + terminalid + "/Set5BH";
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("锁车日期","");
        jsonObject.put("剩余锁车时间","");
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(jsonObject);
        ResponseData responseData = this.execSettingOrderCode(url, jsonArray.toJSONString(), terminalid, "5BH");
        //ResponseData responseData = new ResponseData(ResponseCode.SUCCESS.getCode(),"模拟发送命令成功！");
        //锁车状态记录以及保存
        if (responseData.getStatus().equals(ResponseCode.SUCCESS.getCode())) {
            iLockLogService.addLocklog(terminalid,loginName, LockCarEnum.RESERVATIONUNLOCK.getCode());
            //System.out.println("aaaa");
        }
        return responseData;
    }

    @Override
    public ResponseData execSet51H(String terminalid, String paramJson,Boolean isTest) {
        if (StringUtils.isBlank(terminalid) || StringUtils.isBlank(paramJson)) {
            return ResponseData.result(ResponseCode.ERROR_PARAM_IS_NULL.getCode(), ResponseCode.ERROR_PARAM_IS_NULL.getDesc());
        }
        RedisLock redisLock = new RedisLock(redis, Const.PREFIX_EXEC + terminalid, 0, 300000);
        try {
            if (redisLock.acquire()) {
                JSONObject param = (JSONObject) JSON.parse(paramJson);
                String url = domain + "/core-command/api/v1/device/" + terminalid + "/Set51H";
                if (isTest){
                    url = domainTest + "/core-command/api/v1/device/" + terminalid + "/Set51H";
                }
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("数采时钟间隔(通电)", param.getString("energized_interval"));
                JSONObject jsonObject2 = new JSONObject();
                jsonObject2.put("数采时钟间隔(未通电)", param.getString("not_energized_interval"));
                JSONArray jsonArray = new JSONArray();
                jsonArray.add(jsonObject1);
                jsonArray.add(jsonObject2);
                return this.execSettingOrderCode(url, jsonArray.toJSONString(), terminalid, "51H");
            } else {
                return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
            }
        } catch (InterruptedException e) {
            return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
        } finally {
            redisLock.release();
        }
    }

    @Override
    @RedisLockAn
    public ResponseData execGet4BH(String terminalid,Boolean isTest) {
        if (StringUtils.isBlank(terminalid)) {
            return ResponseData.result(ResponseCode.ERROR_PARAM_IS_NULL.getCode(), ResponseCode.ERROR_PARAM_IS_NULL.getDesc());
        }
        String url = domain + "/core-command/api/v1/device/" + terminalid + "/Get4BH";
        if (isTest){
            url = domainTest + "/core-command/api/v1/device/" + terminalid + "/Get4BH";
        }
        return this.execGetOrderCode(url, terminalid, "40H");
    }

    @Override
    public ResponseData execGet40H(String terminalid,Boolean isTest) {
        if (StringUtils.isBlank(terminalid)) {
            return ResponseData.result(ResponseCode.ERROR_PARAM_IS_NULL.getCode(), ResponseCode.ERROR_PARAM_IS_NULL.getDesc());
        }
        RedisLock redisLock = new RedisLock(redis, Const.PREFIX_EXEC + terminalid, 0, 300000);
        try {
            if (redisLock.acquire()) {
                String url = domain + "/core-command/api/v1/device/" + terminalid + "/Get40H";
                if (isTest){
                    url = domainTest + "/core-command/api/v1/device/" + terminalid + "/Get40H";
                }
                return this.execGetOrderCode(url, terminalid, "40H");
            } else {
                return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
            }
        } catch (InterruptedException e) {
            return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
        } finally {
            redisLock.release();
        }
    }

    @Override
    public ResponseData execGet41H(String terminalid,Boolean isTest) {
        if (StringUtils.isBlank(terminalid)) {
            return ResponseData.result(ResponseCode.ERROR_PARAM_IS_NULL.getCode(), ResponseCode.ERROR_PARAM_IS_NULL.getDesc());
        }
        RedisLock redisLock = new RedisLock(redis, Const.PREFIX_EXEC + terminalid, 0, 300000);
        try {
            if (redisLock.acquire()) {
                String url = domain + "/core-command/api/v1/device/" + terminalid + "/Get41H";
                if (isTest){
                    url = domainTest + "/core-command/api/v1/device/" + terminalid + "/Get41H";
                }
                return this.execGetOrderCode(url, terminalid, "41H");
            } else {
                return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
            }
        } catch (InterruptedException e) {
            return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
        } finally {
            redisLock.release();
        }
    }

    @Override
    public ResponseData execGet43H(String terminalid,Boolean isTest) {
        if (StringUtils.isBlank(terminalid)) {
            return ResponseData.result(ResponseCode.ERROR_PARAM_IS_NULL.getCode(), ResponseCode.ERROR_PARAM_IS_NULL.getDesc());
        }
        RedisLock redisLock = new RedisLock(redis, Const.PREFIX_EXEC + terminalid, 0, 300000);
        try {
            if (redisLock.acquire()) {
                String url = domain + "/core-command/api/v1/device/" + terminalid + "/Get43H";
                if (isTest){
                    url = domainTest + "/core-command/api/v1/device/" + terminalid + "/Get43H";
                }
                return this.execGetOrderCode(url, terminalid, "43H");
            } else {
                return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
            }
        } catch (InterruptedException e) {
            return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
        } finally {
            redisLock.release();
        }
    }

    @Override
    public ResponseData execGet44H(String terminalid,Boolean isTest) {
        if (StringUtils.isBlank(terminalid)) {
            return ResponseData.result(ResponseCode.ERROR_PARAM_IS_NULL.getCode(), ResponseCode.ERROR_PARAM_IS_NULL.getDesc());
        }
        RedisLock redisLock = new RedisLock(redis, Const.PREFIX_EXEC + terminalid, 0, 300000);
        try {
            if (redisLock.acquire()) {
                String url = domain + "/core-command/api/v1/device/" + terminalid + "/Get44H";
                if (isTest){
                    url = domainTest + "/core-command/api/v1/device/" + terminalid + "/Get44H";
                }
                return this.execGetOrderCode(url, terminalid, "44H");
            } else {
                return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
            }
        } catch (InterruptedException e) {
            return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
        } finally {
            redisLock.release();
        }
    }

    @Override
    public ResponseData execGet4AH(String terminalid,Boolean isTest) {
        if (StringUtils.isBlank(terminalid)) {
            return ResponseData.result(ResponseCode.ERROR_PARAM_IS_NULL.getCode(), ResponseCode.ERROR_PARAM_IS_NULL.getDesc());
        }
        RedisLock redisLock = new RedisLock(redis, Const.PREFIX_EXEC + terminalid, 0, 300000);
        try {
            if (redisLock.acquire()) {
                String url = domain + "/core-command/api/v1/device/" + terminalid + "/Get4AH";
                if (isTest){
                    url = domainTest + "/core-command/api/v1/device/" + terminalid + "/Get4AH";
                }
                return this.execGetOrderCode(url, terminalid, "4AH");
            } else {
                return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
            }
        } catch (InterruptedException e) {
            return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
        } finally {
            redisLock.release();
        }
    }


    @Override
    public ResponseData execGet47H(String terminalid,Boolean isTest) {
        if (StringUtils.isBlank(terminalid)) {
            return ResponseData.result(ResponseCode.ERROR_PARAM_IS_NULL.getCode(), ResponseCode.ERROR_PARAM_IS_NULL.getDesc());
        }
        RedisLock redisLock = new RedisLock(redis, Const.PREFIX_EXEC + terminalid, 0, 300000);
        try {
            if (redisLock.acquire()) {
                String url = domain + "/core-command/api/v1/device/" + terminalid + "/Get47H";
                if (isTest){
                    url = domainTest + "/core-command/api/v1/device/" + terminalid + "/Get47H";
                }
                return this.execGetOrderCode(url, terminalid, "47H");
            } else {
                return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
            }
        } catch (InterruptedException e) {
            return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
        } finally {
            redisLock.release();
        }
    }

    @Override
    public ResponseData execSet50H(String terminalid,Boolean isTest) {
        if (StringUtils.isBlank(terminalid)) {
            return ResponseData.result(ResponseCode.ERROR_PARAM_IS_NULL.getCode(), ResponseCode.ERROR_PARAM_IS_NULL.getDesc());
        }
        RedisLock redisLock = new RedisLock(redis, Const.PREFIX_EXEC + terminalid, 0, 300000);
        try {
            if (redisLock.acquire()) {
                String url = domain + "/core-command/api/v1/device/" + terminalid + "/Set50H";
                if (isTest){
                    url = domainTest + "/core-command/api/v1/device/" + terminalid + "/Set50H";
                }
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("IP端口号", "000.000.000.000:00000");
                JSONArray jsonArray = new JSONArray();
                jsonArray.add(jsonObject);
                ResponseData responseData = this.execSettingOrderCode(url, jsonArray.toString(), terminalid, "50H");
                return responseData;
            } else {
                return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
            }
        } catch (Exception e) {
            return ResponseData.result(ResponseCode.ERROR_LOCKED.getCode(), ResponseCode.ERROR_LOCKED.getDesc());
        } finally {
            redisLock.release();
        }
    }
}
