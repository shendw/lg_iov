package com.yonyou.longgong.iotserver.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/**
 * 设备数据
 *
 * @author fengsheng
 * @创建时间 2019-11-11
 **/
@Data
public class DeviceDataVO {
    private String carNo;
    private String device;
    private String car_information;
    private String car_type;
    private String car_manufacturer;
    private String gps_version;
    private String gps_manufacturer;
    // private String ip_prot;
    private String acquisition_interval;
    private String real_time;
    private String gps_monitor_status;
    private String machine_start;
    private String machine_shutdown;
    private String machine_electrify;
    private String sms_enable;
    @Excel(name = "定位时间")
    private String gps_time;
    @Excel(name = "定位状态", orderNum = "10")
    private String gps_position_status;
    @Excel(name = "经纬度", orderNum = "11")
    private String coordinates;
    private String ground_speed;
    private String ground_heading;
    @Excel(name = "磁偏角", orderNum = "16")
    private String magnetic_declination;
    @Excel(name = "磁偏角方向", orderNum = "17")
    private String declination_direction;
    @Excel(name = "卫星数量", orderNum = "12")
    private String number_of_satellites;
    @Excel(name = "海拔高度", orderNum = "15")
    private String altitude;
    @Excel(name = "信号强度", orderNum = "14")
    private String gsm_signal_strength;
    @Excel(name = "机器电压", orderNum = "7")
    private String machine_voltage;
    @Excel(name = "工作小时", orderNum = "1")
    private String engine_running_duration = "0";
    @Excel(name = "发动机水温", orderNum = "2")
    private String engine_water_temperature;
    @Excel(name = "液压油温", orderNum = "3")
    private String hydraulic_oil_temperature;
    @Excel(name = "机油压力", orderNum = "4")
    private String oil_pressure;
    private String fuel_level;
    @Excel(name = "发动机转速", orderNum = "5")
    private String engine_speed;
    @Excel(name = "油门档位", orderNum = "6")
    private String throttle_gear;
    private String machine_operation_mode;
    private String machine_information;
    @Excel(name = "机器时间", orderNum = "9")
    private String machine_clock_time;
    @Excel(name = "RCM电池", orderNum = "8")
    private String rcm_battery_voltage;
    private String rcm_clock_time;
    private String daily_discharge;
    private String center_longitude;
    private String center_latitude;
    private String radius;
    private String recognition_constant;
    private String latest_landing_time;
    private String last_time;
    @Excel(name = "通讯方式", orderNum = "13")
    private String communication_mode;
    private String communication_state;
    private String timestamp;
    private String locked_state = "false";
}
