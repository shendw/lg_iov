package com.yonyou.longgong.iotserver.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2020/1/6
 * @描述
 **/
@Data
public class TerminalLogVO {

    @Excel(name = "终端ID")
    private String terminalid;

    @Excel(name = "通讯通道")
    private String channel;

    @Excel(name = "操作时间")
    private String optime;

    @Excel(name = "方向")
    private String direction;

    @Excel(name = "报文内容")
    private String detail;

    @Excel(name = "命令关键字")
    private String opkeyword;

    @Excel(name = "是否操作成功")
    private String issuccess;

}
