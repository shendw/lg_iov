package com.yonyou.longgong.iotserver.service;

import com.yonyou.longgong.iotserver.dto.HistoryDataParamDto;
import com.yonyou.longgong.iotserver.vo.HistoricalTrackVO;

import java.util.List;

/**
 * 历史轨迹接口
 */
public interface IHistoricalTrackService {

    /**
     * 获取车辆历史轨迹
     *
     * @param historyDataParamDto 终端及时间参数
     * @return 历史轨迹对象
     */
    List<HistoricalTrackVO> getHistoricalTrack(HistoryDataParamDto historyDataParamDto);
}