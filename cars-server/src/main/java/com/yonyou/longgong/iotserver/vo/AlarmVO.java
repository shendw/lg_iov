package com.yonyou.longgong.iotserver.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.yonyou.longgong.iotserver.enums.TagsEnums;
import lombok.Data;

/**
 * 报警数据
 *
 * @author fengsheng
 * @创建时间 2019-11-11
 **/
@Data
public class AlarmVO {

    // 报警数组
    public static final TagsEnums[] alarmArray = {TagsEnums.COMMUNICATION_MODE,
            TagsEnums.GPS_ANTENNA_FAILURE_ALARM, TagsEnums.GPRS_ANTENNA_FAILURE_ALARM,
            TagsEnums.MACHINE_VOLTAGE_FAULT_ALARM, TagsEnums.RCM_BATTERY_VOLTAGE_FAULT_ALARM,
            TagsEnums.MACHINE_OUT_OF_RANGE_ALARM, TagsEnums.MACHINE_CLOCK_TIME, TagsEnums.RCM_CLOCK_TIME,TagsEnums.COMMUNICATION_ALARM};

    public static final String DEVICE = "device";
    public static final String TIMESTAMP = "timestamp";
    public static final String COMMUNICATION_MODE = "communication_mode";
    public static final String ALARM_MESSAGE = "alarm_message";
    public static final String MACHINE_CLOCK_TIME = "machine_clock_time";
    public static final String RCM_CLOCK_TIME = "rcm_clock_time";

    /**
     * 终端ID
     */
    @Excel(name = "终端ID")
    private String device;

    /**
     * 保存时间
     */
    @Excel(name = "保存时间")
    private String timestamp;
    /**
     * 通讯方式
     */
    @Excel(name = "通讯方式")
    private String communication_mode;
    /**
     * 报警信息
     */
    @Excel(name = "报警信息")
    private String alarm_message;
    /**
     * 机器时间
     */
    @Excel(name = "机器时间")
    private String machine_clock_time;
    /**
     * 终端时间
     */
    @Excel(name = "终端时间")
    private String rcm_clock_time;

}
