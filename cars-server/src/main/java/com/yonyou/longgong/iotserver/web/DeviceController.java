package com.yonyou.longgong.iotserver.web;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.yonyou.longgong.carsserver.enums.TerminalCarStatus;
import com.yonyou.longgong.carsserver.service.ITerminalCarService;
import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import com.yonyou.longgong.iotserver.dto.HistoryDataParamDto;
import com.yonyou.longgong.iotserver.service.IDeviceService;
import com.yonyou.longgong.iotserver.vo.*;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 设备数据
 *
 * @author fengsheng
 * @创建时间 2019-11-11
 **/
@RestController
@RequestMapping(value = "/iot")
public class DeviceController {

    @Autowired
    private IDeviceService deviceService;
    @Autowired
    private ITerminalCarService iTerminalCarService;

    /**
     * 分页方法
     *
     * @param pageNum  页码
     * @param pageSize 页数
     * @param voList   需要分页的集合
     * @return 分页后的数据对象
     */
    private PageVO pagingMethod(int pageNum, int pageSize, List voList) {
        if (voList == null || voList.isEmpty()) {
            return null;
        }
        int beginIndex = (pageNum - 1) * pageSize;
        List dataVOList = new ArrayList();
        for (int i = beginIndex; i < (beginIndex + pageSize); i++) {
            if (voList.size() > i) {
                dataVOList.add(voList.get(i));
            }
        }
        PageVO resultData = new PageVO();
        resultData.setTotalRecord(voList.size());
        resultData.setResultData(dataVOList);
        return resultData;
    }

    /**
     * 获取正在使用的终端
     *
     * @return 终端信息
     */
    private String getTerminal() {
        StringBuilder terminal = new StringBuilder();
        List<String> terminals = iTerminalCarService.selectTerminalid(TerminalCarStatus.EFFECTIVE.getCode());
        if (terminals == null || terminals.isEmpty()) {
            return null;
        }
        for (String str : terminals){
            terminal.append(str).append(",");
        }
        return terminal.substring(0, terminal.length() - 1);
    }

    @RequestMapping(value = "/getDeviceOverView")
    public ResponseData getDeviceOverView() {
        String terminal = this.getTerminal();
        DeviceOverView deviceOverView = deviceService.getDeviceOverView(terminal);
        return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(), deviceOverView);
    }

    @RequestMapping(value = "/getDeviceLocation")
    public ResponseData getDeviceLocation() {
        String terminal = this.getTerminal();
        List<DeviceLocationVO> locationVOS = deviceService.getDeviceLocation(terminal);
        return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(), locationVOS);
    }

    @RequestMapping(value = "/getCurrDeviceData")
    public ResponseData getCurrDeviceData(String terminalid) {
        DeviceDataVO deviceData = deviceService.getDeviceData(terminalid);
        return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(), deviceData);
    }

    @RequestMapping(value = "/getDeviceDetailData")
    public ResponseData getDeviceDetailData(String carno, String terminalid) {
        DeviceDetailVO deviceDetailVO = deviceService.getDeviceDetailData(carno, terminalid,false);
        return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(), deviceDetailVO);
    }

    @RequestMapping(value = "/getDeviceHistoryData")
    public ResponseData getDeviceHistoryData(HistoryDataParamDto historyDataParamDto) {
        List<DeviceDataVO> deviceDataVOS = deviceService.getDeviceHistoryData(historyDataParamDto,false);
        PageVO resultData = this.pagingMethod(historyDataParamDto.getPageNum(), historyDataParamDto.getPageSize(), deviceDataVOS);
        return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(), resultData);
    }

    @RequestMapping(value = "/getDeviceRoutineData")
    public ResponseData getDeviceRoutineData(HistoryDataParamDto historyDataParamDto) {
        List<DeviceDataVO> deviceDataVOS = deviceService.getDeviceRoutineData(historyDataParamDto,false);
        PageVO resultData = this.pagingMethod(historyDataParamDto.getPageNum(), historyDataParamDto.getPageSize(), deviceDataVOS);
        return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(), resultData);
    }

    @RequestMapping(value = "/getDeviceRoutineData/test")
    public ResponseData getTestDeviceRoutineData(HistoryDataParamDto historyDataParamDto) {
        List<DeviceDataVO> deviceDataVOS = deviceService.getDeviceRoutineData(historyDataParamDto,true);
        PageVO resultData = this.pagingMethod(historyDataParamDto.getPageNum(), historyDataParamDto.getPageSize(), deviceDataVOS);
        return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(), resultData);
    }

    @RequestMapping(value = "/getDeviceGPSData")
    public ResponseData getDeviceGPSData(HistoryDataParamDto historyDataParamDto) {
        List<DeviceDataVO> deviceDataVOS = deviceService.getDeviceGPSData(historyDataParamDto,false);
        PageVO resultData = this.pagingMethod(historyDataParamDto.getPageNum(), historyDataParamDto.getPageSize(), deviceDataVOS);
        return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(), resultData);
    }

    @RequestMapping(value = "/getDeviceGPSData/test")
    public ResponseData getTestDeviceGPSData(HistoryDataParamDto historyDataParamDto) {
        List<DeviceDataVO> deviceDataVOS = deviceService.getDeviceGPSData(historyDataParamDto,true);
        PageVO resultData = this.pagingMethod(historyDataParamDto.getPageNum(), historyDataParamDto.getPageSize(), deviceDataVOS);
        return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(), resultData);
    }

    @RequestMapping(value = "/getMachineState")
    public ResponseData getMachineState(HistoryDataParamDto historyDataParamDto) {
        List<DeviceDataVO> deviceDataVOS = deviceService.getMachineState(historyDataParamDto,false);
        PageVO resultData = this.pagingMethod(historyDataParamDto.getPageNum(), historyDataParamDto.getPageSize(), deviceDataVOS);
        return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(), resultData);
    }

    @RequestMapping(value = "/getCurrAlarmData")
    public ResponseData getCurrAlarmData() {
        String terminal = this.getTerminal();
        RatioVO alarmVO = deviceService.getAlarmData(terminal,false);
        return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(), alarmVO);
    }

    @RequestMapping(value = "/getCurrAlarmDataByPage")
    public ResponseData getCurrAlarmDataByPage(int pageNum, int pageSize) {
        String terminal = this.getTerminal();
        RatioVO alarmVO = deviceService.getAlarmData(terminal,false);
        if (alarmVO == null) {
            return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(), null);
        }
        PageVO resultData = this.pagingMethod(pageNum, pageSize, alarmVO.getData());
        return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(), resultData);
    }

    @RequestMapping(value = "/getHistoryAlarmData")
    public ResponseData getHistoryAlarmData(HistoryDataParamDto historyDataParamDto) {
        List<AlarmVO> alarmVO = deviceService.getAlarmData(historyDataParamDto,false);
        PageVO resultData = this.pagingMethod(historyDataParamDto.getPageNum(), historyDataParamDto.getPageSize(), alarmVO);
        return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(), resultData);
    }

    @RequestMapping(value = "/getHistoryAlarmData/test")
    public ResponseData getTestHistoryAlarmData(HistoryDataParamDto historyDataParamDto) {
        List<AlarmVO> alarmVO = deviceService.getAlarmData(historyDataParamDto,true);
        PageVO resultData = this.pagingMethod(historyDataParamDto.getPageNum(), historyDataParamDto.getPageSize(), alarmVO);
        return ResponseData.result(ResponseCode.SUCCESS_UPDATE.getCode(), resultData);
    }

    @RequestMapping(value = "/historyAlarmExport")
    public void historyAlarmExport(HistoryDataParamDto historyDataParamDto, HttpServletResponse response) {
        try {
            List<AlarmVO> alarmVO = deviceService.getAlarmData(historyDataParamDto,false);
            Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams("报警信息", "报警信息"), AlarmVO.class, alarmVO);
            // 告诉浏览器用什么软件可以打开此文件
            response.setHeader("content-Type", "application/vnd.ms-excel");
            // 下载文件的默认名称
            response.setHeader("Content-Disposition", "attachment;filename=historyAlarm.xls");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            sheets.write(response.getOutputStream());
            return;
        } catch (IOException e) {

        }
    }


    @RequestMapping(value = "/deviceHistoryExport")
    public void deviceHistoryExport(HistoryDataParamDto historyDataParamDto, HttpServletResponse response) {
        try {
            List<DeviceDataVO> deviceDataVOS = deviceService.getDeviceHistoryData(historyDataParamDto,false);
            Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams("设备数据", "设备数据"), DeviceDataVO.class, deviceDataVOS);
            // 告诉浏览器用什么软件可以打开此文件
            response.setHeader("content-Type", "application/vnd.ms-excel");
            // 下载文件的默认名称
            response.setHeader("Content-Disposition", "attachment;filename=deviceData.xls");
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            sheets.write(response.getOutputStream());
            return;
        } catch (IOException e) {

        }
    }
}
