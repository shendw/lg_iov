package com.yonyou.longgong.iotserver.service;

import com.yonyou.longgong.iotserver.dto.HistoryDataParamDto;
import com.yonyou.longgong.iotserver.vo.*;

import java.util.List;

/**
 * 设备服务
 */
public interface IDeviceService {

    /**
     * 根据终端ID获取设备数据
     *
     * @param terminal 终端ID
     * @return 设备对象
     */
    DeviceDataVO getDeviceData(String terminal);
    DeviceDataVO getDeviceData(String terminal,Boolean isTest);

    /**
     * 根据终端ID获取设备明细数据
     *
     * @param terminal 终端ID
     * @return 设备对象
     */
    DeviceDetailVO getDeviceDetailData(String carno, String terminal,Boolean isTest);

    /**
     * 根据标签获取IOT数据
     *
     * @param tagNames 需要被查询的标签（支持多个终端，以","分隔）
     * @return IOT返回的数据
     */
    String getJsonByTag(String tagNames);
    String getJsonByTag(String tagNames,Boolean isTest);

    /**
     * 根据标签获取设备数据
     *
     * @param tagNames 需要被查询的标签（支持多个终端，以","分隔）
     * @return 封装后的设备数据
     */
    List<DeviceDataVO> getDeviceDataByTag(String tagNames);
    List<DeviceDataVO> getDeviceDataByTag(String tagNames,Boolean isTest);

    /**
     * 获取设备历史数据
     *
     * @param historyDataParamDto 终端及时间参数
     * @return 设备对象
     */
    List<DeviceDataVO> getDeviceHistoryData(HistoryDataParamDto historyDataParamDto,Boolean isTest);

    /**
     * 获取设备常规数据
     *
     * @param historyDataParamDto 终端及时间参数
     * @return 设备对象
     */
    List<DeviceDataVO> getDeviceRoutineData(HistoryDataParamDto historyDataParamDto,Boolean isTest);

    /**
     * 获取设备GPS数据
     *
     * @param historyDataParamDto 终端及时间参数
     * @return 设备对象
     */
    List<DeviceDataVO> getDeviceGPSData(HistoryDataParamDto historyDataParamDto,Boolean isTest);

    /**
     * 获取机器状态信息
     *
     * @param historyDataParamDto 终端及时间参数
     * @return 设备对象
     */
    List<DeviceDataVO> getMachineState(HistoryDataParamDto historyDataParamDto,Boolean isTest);

    /**
     * 根据标签获取设备历史数据
     *
     * @param historyDataParamDto 终端及时间参数
     * @param tagNames            需要被查询的标签
     * @return IOT返回的数据
     */
    String getDeviceHistoryData(HistoryDataParamDto historyDataParamDto, String tagNames);
    String getDeviceHistoryData(HistoryDataParamDto historyDataParamDto, String tagNames,Boolean isTest);

    /**
     * 获取车辆总览数据
     *
     * @param terminal 终端信息
     * @return 车辆总览对象
     */
    DeviceOverView getDeviceOverView(String terminal);

    /**
     * 获取设备当前位置
     *
     * @return 设备位置
     */
    List<DeviceLocationVO> getDeviceLocation(String terminal);

    /**
     * 根据终端ID获取设备报警信息
     *
     * @param terminal 终端ID
     * @return 报警对象
     */
    RatioVO getAlarmData(String terminal,Boolean isTest);

    /**
     * 获取设备历史报警信息
     *
     * @param historyDataParamDto 终端及时间参数
     * @return 报警对象
     */
    List<AlarmVO> getAlarmData(HistoryDataParamDto historyDataParamDto,Boolean isTest);

    /**
     * [当日工作时间长度，开工时间点]
     * @param terminal
     * @param date
     * @return
     */
    String[] getTodayWorkingHours(String terminal, String date);
}
