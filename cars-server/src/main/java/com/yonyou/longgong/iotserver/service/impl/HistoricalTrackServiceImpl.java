package com.yonyou.longgong.iotserver.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yonyou.longgong.carsserver.utils.DataTimeUtil;
import com.yonyou.longgong.iotserver.dto.HistoryDataParamDto;
import com.yonyou.longgong.iotserver.enums.TagsEnums;
import com.yonyou.longgong.iotserver.service.IDeviceService;
import com.yonyou.longgong.iotserver.service.IHistoricalTrackService;
import com.yonyou.longgong.iotserver.service.ISiteService;
import com.yonyou.longgong.iotserver.utils.TransLocationsUtil;
import com.yonyou.longgong.iotserver.vo.HistoricalTrackVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

@Service(value = "historicalTrackService")
public class HistoricalTrackServiceImpl implements IHistoricalTrackService {

    @Value("${iot.site}")
    private String site;
    @Value("${iot.domain}")
    private String domain;
    @Autowired
    private IDeviceService deviceService;
    @Autowired
    private ISiteService iSiteService;

    @Override
    public List<HistoricalTrackVO> getHistoricalTrack(HistoryDataParamDto historyDataParamDto) {
        Map<String, HistoricalTrackVO> historicalTrackVOMap = this.getHistoricalTrackMap(historyDataParamDto);
        this.checkEffectiveness(historyDataParamDto, historicalTrackVOMap);
        if (historicalTrackVOMap == null || historicalTrackVOMap.isEmpty()) {
            return null;
        }
        List<HistoricalTrackVO> historicalTrackVOS = new ArrayList<>(historicalTrackVOMap.values());
        historicalTrackVOS.stream().forEach(item -> item.setCoordinates(TransLocationsUtil.transLocation(item.getCoordinates())));
        historicalTrackVOS.sort((Comparator.comparing(HistoricalTrackVO::getTimestamp)));
        return historicalTrackVOS;
    }

    private Map<String, HistoricalTrackVO> getHistoricalTrackMap(HistoryDataParamDto historyDataParamDto) {
        String terminalid = historyDataParamDto.getTerminalid();
        String site = iSiteService.getSite(terminalid, false);
        if (StringUtils.isEmpty(site)){
            return null;
        }
        String tag = site + "." + terminalid + "." + TagsEnums.COORDINATES.getChinese();
        String result = deviceService.getDeviceHistoryData(historyDataParamDto, tag);
        JSONObject resultJson = (JSONObject) JSON.parse(result);
        if (resultJson.get("data") == null) {
            return null;
        }
        JSONArray jsonArray = new JSONArray();
        if (resultJson.get("data") instanceof JSONObject) {
            JSONObject jsonObject = resultJson.getJSONObject("data");
            if (jsonObject.size() != 0 && jsonObject.containsKey(tag)) {
                if (jsonObject.get(tag) instanceof JSONArray) {
                    jsonArray = jsonObject.getJSONArray(tag);
                }
            }
        }
        Map<String, HistoricalTrackVO> historicalTrackVOMap = new HashMap<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            HistoricalTrackVO vo = new HistoricalTrackVO();
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String value = jsonObject.getString("value");
            if (StringUtils.isBlank(value)) {
                continue;
            }
            String timestamp = jsonObject.getString("timestamp");
            vo.setTimestamp(DataTimeUtil.stampToDate(timestamp));
            vo.setCoordinates(jsonObject.getString("value"));
            historicalTrackVOMap.put(timestamp, vo);
        }
        return historicalTrackVOMap;
    }

    private void checkEffectiveness(HistoryDataParamDto historyDataParamDto, Map<String, HistoricalTrackVO> historicalTrackVOMap) {
        String terminalid = historyDataParamDto.getTerminalid();
        if (historicalTrackVOMap == null || historicalTrackVOMap.isEmpty()) {
            return;
        }
        String site = iSiteService.getSite(terminalid, false);
        if (StringUtils.isEmpty(this.site)){
            return;
        }
        String tag = site + "." + terminalid + "." + TagsEnums.GPS_POSITION_STATUS.getChinese();
        String result = deviceService.getDeviceHistoryData(historyDataParamDto, tag);
        JSONObject resultJson = (JSONObject) JSON.parse(result);
        JSONArray jsonArray = new JSONArray();
        if (resultJson.get("data") instanceof JSONObject) {
            JSONObject jsonObject = resultJson.getJSONObject("data");
            if (jsonObject.size() != 0 && jsonObject.containsKey(tag)) {
                if (jsonObject.get(tag) instanceof JSONArray) {
                    jsonArray = jsonObject.getJSONArray(tag);
                }
            }
        }
        for (int i = 0; i < jsonArray.size(); i++) {
            HistoricalTrackVO vo = new HistoricalTrackVO();
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String value = jsonObject.getString("value");
            String timestamp = jsonObject.getString("timestamp");
            if (StringUtils.isBlank(value) || "无效定位".equals(value)) {
                if (historicalTrackVOMap.containsKey(timestamp)) {
                    historicalTrackVOMap.remove(timestamp);
                }
            }
        }
    }
}
