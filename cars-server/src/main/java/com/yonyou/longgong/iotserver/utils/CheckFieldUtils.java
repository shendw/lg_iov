package com.yonyou.longgong.iotserver.utils;

import com.yonyou.longgong.iotserver.enums.TagsEnums;

import java.util.HashMap;
import java.util.Map;

public class CheckFieldUtils {

    public static Map<String, String> getDeviceRoutineField() {
        Map<String, String> checkFields = new HashMap<>();
        checkFields.put(TagsEnums.ENGINE_WATER_TEMPERATURE.getEnglish(), "getEngine_water_temperature");
        checkFields.put(TagsEnums.HYDRAULIC_OIL_TEMPERATURE.getEnglish(), "getHydraulic_oil_temperature");
        checkFields.put(TagsEnums.OIL_PRESSURE.getEnglish(), "getOil_pressure");
        checkFields.put(TagsEnums.ENGINE_SPEED.getEnglish(), "getEngine_speed");
        checkFields.put(TagsEnums.THROTTLE_GEAR.getEnglish(), "getThrottle_gear");
        checkFields.put(TagsEnums.MACHINE_VOLTAGE.getEnglish(), "getMachine_voltage");
        checkFields.put(TagsEnums.RCM_BATTERY_VOLTAGE.getEnglish(), "getRcm_battery_voltage");
        //44H会出现只有时间的情况，需要过滤掉
        //checkFields.put(TagsEnums.MACHINE_CLOCK_TIME.getEnglish(), "getMachine_clock_time");
        return checkFields;
    }

    public static Map<String, String> getDeviceGPSField() {
        Map<String, String> checkFields = new HashMap<>();
        checkFields.put(TagsEnums.GPS_POSITION_STATUS.getEnglish(), "getGps_position_status");
        checkFields.put(TagsEnums.GPS_TIME.getEnglish(), "getGps_time");
        checkFields.put(TagsEnums.COORDINATES.getEnglish(), "getCoordinates");
        checkFields.put(TagsEnums.OIL_PRESSURE.getEnglish(), "getOil_pressure");
        checkFields.put(TagsEnums.NUMBER_OF_SATELLITES.getEnglish(), "getNumber_of_satellites");
        checkFields.put(TagsEnums.GSM_SIGNAL_STRENGTH.getEnglish(), "getGsm_signal_strength");
        checkFields.put(TagsEnums.ALTITUDE.getEnglish(), "getAltitude");
        checkFields.put(TagsEnums.MAGNETIC_DECLINATION.getEnglish(), "getMagnetic_declination");
        checkFields.put(TagsEnums.DECLINATION_DIRECTION.getEnglish(), "getDeclination_direction");
        return checkFields;
    }

    public static Map<String, String> getMachineStateField() {
        Map<String, String> checkFields = new HashMap<>();
        checkFields.put(TagsEnums.MACHINE_START.getEnglish(), "getMachine_start");
        checkFields.put(TagsEnums.MACHINE_SHUTDOWN.getEnglish(), "getMachine_shutdown");
        checkFields.put(TagsEnums.MACHINE_ELECTRIFY.getEnglish(), "getMachine_electrify");
        checkFields.put(TagsEnums.MACHINE_ELECTRIFY.getEnglish(), "getMachine_electrify");
        return checkFields;
    }
}
