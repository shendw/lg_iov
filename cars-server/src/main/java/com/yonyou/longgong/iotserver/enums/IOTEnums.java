package com.yonyou.longgong.iotserver.enums;

import lombok.Getter;

@Getter
public enum IOTEnums {
    SUCCESS("1000", "设置成功"),
    ERROR_TIME_OUT("408", "请求超时"),
    ERROR_POSITION("0", "位置错误"),
    ERROR_NOT_EXIST("1", "当前执行终端已离线"),
    ERROR_EXEC_FAIL("2", "执行终端存在,但是命令下传失败"),
    ERROR_EXEC_TIME_OUT("3", "执行终端存在,命令下传成功,但是执行终端回应超时"),
    ERROR_CHECK("4", "校验错误"),
    ERROR_SMS_NOT_SEND("5", "SMS传输,多帧小时无法发送"),
    ERROR_LOGIN_FAIL("6", "自动绑定或登录失败"),
    ERROR_NO_RESPONSE("7", "仪表无回复"),
    ERROR_ONLY_GPRS_BUT_FAIL("8", "收到唤醒命令后只能使用GPRS方式,但是当前GSM信号强度不够(SMS方式已经被禁用)"),
    ;
    private String code;
    private String desc;

    IOTEnums(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
