package com.yonyou.longgong.iotserver.web;


import com.yonyou.longgong.carsserver.utils.ResponseCode;
import com.yonyou.longgong.carsserver.utils.ResponseData;
import com.yonyou.longgong.iotserver.service.IOrderCodeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 命令字
 *
 * @author fengsheng
 * @创建时间 2019-11-23
 **/
@RestController
@RequestMapping(value = "/order")
public class OrderCodeController {

    @Autowired
    private IOrderCodeService orderCodeService;

    @RequestMapping(value = "/execSet50H")
    public ResponseData execSet50H(String terminalid, String ipAndPort) {
        return orderCodeService.execSet50H(terminalid, ipAndPort,false);
    }

    @RequestMapping(value = "/execSet5AH")
    public ResponseData execSet5AH(String terminalid, String lock,@CookieValue(value = "_A_P_userLoginName") String loginName) {
        return orderCodeService.execSet5AH(terminalid, lock,false,loginName);
    }

    @RequestMapping(value = "/execSet5BH")
    public ResponseData execSet5BH(String terminalid, @CookieValue(value = "_A_P_userLoginName") String loginName) {
        return orderCodeService.execSet5BH(terminalid, false,loginName);
    }

    @RequestMapping(value = "/execSet51H")
    public ResponseData execSet51H(String terminalid, String paramJson) {
        return orderCodeService.execSet51H(terminalid, paramJson,false);
    }

    @RequestMapping(value = "/execGet4BH")
    public ResponseData execGet4BH(String terminalid) {
        return orderCodeService.execGet4BH(terminalid,false);
    }

    @RequestMapping(value = "/execGet40H")
    public ResponseData execSet51H(String terminalid) {
        return orderCodeService.execGet40H(terminalid,false);
    }

    @RequestMapping(value = "/execGet41H")
    public ResponseData execGet41H(String terminalid) {
        return orderCodeService.execGet41H(terminalid,false);
    }

    @RequestMapping(value = "/execGet43H")
    public ResponseData execGet43H(String terminalid) {
        return orderCodeService.execGet43H(terminalid,false);
    }

    @RequestMapping(value = "/execGet44H")
    public ResponseData execGet44H(String terminalid) {
        return orderCodeService.execGet44H(terminalid,false);
    }

    @RequestMapping(value = "/execGet4AH")
    public ResponseData execGet4AH(String terminalid) {
        return orderCodeService.execGet4AH(terminalid,false);
    }

    @RequestMapping(value = "/execGet47H")
    public ResponseData execGet47H(String terminalid) {
        return orderCodeService.execGet47H(terminalid,false);
    }

    @RequestMapping(value = "/testExecSet50H")
    public ResponseData testExecSet50H(String terminalid, String ipAndPort) {
        if (StringUtils.isNotBlank(ipAndPort)){
            return orderCodeService.execSet50H(terminalid,ipAndPort,true);
        }else {
            //没有ip端口号，默认为改成品步骤
            //再转成成品的时候先修改下采集时间
            String param = "{energized_interval:4,not_energized_interval:480}";
            ResponseData data = orderCodeService.execSet51H(terminalid, param, true);
            if (ResponseCode.SUCCESS.getCode().equals(data.getStatus())){
                return orderCodeService.execSet50H(terminalid,true);
            }
            return data;
        }
    }

    @RequestMapping(value = "/testExecSet5AH")
    public ResponseData testExecSet5AH(String terminalid, String lock,@CookieValue(value = "_A_P_userLoginName") String loginName) {
        return orderCodeService.execSet5AH(terminalid, lock,true,loginName);
    }

    @RequestMapping(value = "/testExecSet51H")
    public ResponseData testExecSet51H(String terminalid, String paramJson) {
        return orderCodeService.execSet51H(terminalid, paramJson,true);
    }

    @RequestMapping(value = "/testExecGet40H")
    public ResponseData testExecSet51H(String terminalid) {
        return orderCodeService.execGet40H(terminalid,true);
    }

    @RequestMapping(value = "/testExecGet41H")
    public ResponseData testExecGet41H(String terminalid) {
        return orderCodeService.execGet41H(terminalid,true);
    }

    @RequestMapping(value = "/testExecGet43H")
    public ResponseData testExecGet43H(String terminalid) {
        return orderCodeService.execGet43H(terminalid,true);
    }

    @RequestMapping(value = "/testExecGet44H")
    public ResponseData testExecGet44H(String terminalid) {
        return orderCodeService.execGet44H(terminalid,true);
    }

    @RequestMapping(value = "/testExecGet4AH")
    public ResponseData testExecGet4AH(String terminalid) {
        return orderCodeService.execGet4AH(terminalid,true);
    }

    @RequestMapping(value = "/testExecGet47H")
    public ResponseData testExecGet47H(String terminalid) {
        return orderCodeService.execGet47H(terminalid,true);
    }
}
