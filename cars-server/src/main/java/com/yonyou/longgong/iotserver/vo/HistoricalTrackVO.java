package com.yonyou.longgong.iotserver.vo;

import lombok.Data;

/**
 * 历史轨迹
 *
 * @author fengsheng
 * @创建时间 2019-11-11
 **/
@Data
public class HistoricalTrackVO {

    public static final String TIMESTAMP = "timestamp";
    public static final String COORDINATES = "coordinates";

    /**
     * 时间戳
     */
    private String timestamp;
    /**
     * 经纬度
     */
    private String coordinates;
}
