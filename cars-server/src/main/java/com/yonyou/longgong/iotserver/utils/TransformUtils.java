package com.yonyou.longgong.iotserver.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.yonyou.longgong.carsserver.utils.DataTimeUtil;
import com.yonyou.longgong.iotserver.common.Const;
import com.yonyou.longgong.iotserver.enums.TagsEnums;
import com.yonyou.longgong.iotserver.vo.DeviceDataVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.*;

@Component
public class TransformUtils {

    @Autowired
    private RedisUtils redis;

    private void fillValue(JSONObject clz, JSONObject jsonObject) {
        String name = jsonObject.getString("name");
        // 由于没有保存时间的标签，单独设置
        if (!clz.containsKey("timestamp")) {
            String timestamp = jsonObject.getString("timestamp");
            if (timestamp != null && timestamp.length() >= 10) {
                clz.put("timestamp", DataTimeUtil.stampToDate(timestamp));
            }
        }
        TagsEnums[] tagsEnums = TagsEnums.values();
        // 通过Lambda表达式来判断标签是否存在
        TagsEnums tag = Arrays.stream(tagsEnums).filter(tagsEnum -> tagsEnum.getChinese().equals(name))
                .findFirst().orElse(null);
        // 标签不为空的情况下，将标签对应的english信息当作Key存入Json方便转换对象
        if (tag != null) {
            String value = jsonObject.getString("value");
            if (tag.getEnglish().contains("time")) {
                if (value != null && value.length() >= 10) {
                    clz.put(tag.getEnglish(), DataTimeUtil.stampToDate(value));
                }
            } else {
                clz.put(tag.getEnglish(), value);
            }
        }
    }

    public <T> T transformCurrData(JSONObject resultJson, Class<T> clazz) {
        JSONArray jsonArray = new JSONArray();
        if (resultJson.get("data") instanceof JSONArray) {
            jsonArray = resultJson.getJSONArray("data");
        }
        if (jsonArray.isEmpty()) {
            return null;
        }
        // 缓存Json，为了方便将IOT的数据封装成VO对象返回
        JSONObject clz = new JSONObject();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            // 由于没有终端ID的标签，单独设置
            if (!clz.containsKey("device")) {
                String device = jsonObject.getString("device");
                if (device != null && device.length() != 0) {
                    clz.put("device", device);
                }
            }
            // 填充JSON数据
            fillValue(clz, jsonObject);
        }
        T o = JSONObject.parseObject(clz.toJSONString(), clazz);
        //判断是否设备数据，把位置信息缓存到redis
        if (DeviceDataVO.class.isAssignableFrom(clazz)){
            DeviceDataVO deviceDataVO = (DeviceDataVO) o;
            return (T) getCacheData(deviceDataVO);
        }
        return o;
    }

    public <T> List<T> transformCurrDataArray(JSONObject resultJson, Class<T> clazz) {
        JSONArray jsonArray = new JSONArray();
        if (resultJson.get("data") instanceof JSONArray) {
            jsonArray = resultJson.getJSONArray("data");
        }
        if (jsonArray.isEmpty()) {
            return null;
        }
        // 缓存Json，为了方便将IOT的数据封装成VO对象返回
        JSONObject voList = new JSONObject();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            String device = jsonObject.getString("device");
            if (StringUtils.isBlank(device)) {
                continue;
            }
            // 使用缓存Json存放设备的数据
            JSONObject clz = new JSONObject();
            if (!voList.containsKey(device)) {
                clz.put("device", device);
            } else {
                clz = voList.getJSONObject(device);
            }
            // 填充JSON数据
            fillValue(clz, jsonObject);
            voList.put(device, clz);
        }
        List<T> os = JSON.parseArray(JSON.toJSONString(voList.values()), clazz);
        if (!CollectionUtils.isEmpty(os)){
            for (int i =0;i<os.size();i++) {
                //判断是否设备数据，把位置信息缓存到redis
                if (DeviceDataVO.class.isAssignableFrom(clazz)){
                    T o = os.get(i);
                    DeviceDataVO deviceDataVO = (DeviceDataVO) o;
                    os.set(i,(T) getCacheData(deviceDataVO));
                }
            }
        }
        return os;
    }

    public <T> List<T> transformHistoryData(JSONObject resultJson, Class<T> clazz) {
        JSONObject jsonObject = new JSONObject();
        if (resultJson.get("data") instanceof JSONObject) {
            jsonObject = resultJson.getJSONObject("data");
        }
        // 缓存Json，为了方便将IOT的数据封装成VO对象返回
        JSONObject voList = new JSONObject();
        TagsEnums[] tagsEnums = TagsEnums.values();
        for (String key : jsonObject.keySet()) {
            String name = key.substring(key.lastIndexOf(".") + 1);
            // 通过Lambda表达式来判断标签是否存在
            TagsEnums tag = Arrays.stream(tagsEnums).filter(tagsEnum -> tagsEnum.getChinese().equals(name))
                    .findFirst().orElse(null);
            // 标签不为空的情况下，将标签对应的english信息当作Key存入Json方便转换对象
            if (tag != null) {
                JSONArray jsonArray = jsonObject.getJSONArray(key);
                for (int i = 0; i < jsonArray.size(); i++) {
                    JSONObject json = jsonArray.getJSONObject(i);
                    // 因为返回的历史数据以标签当作Key，所以先使用缓存Json存放相同时间的数据
                    JSONObject clz = new JSONObject();
                    String device = json.getString("device");
                    String timestamp = json.getString("timestamp");
                    if (voList.containsKey(device + timestamp)) {
                        clz = voList.getJSONObject(device + timestamp);
                    }
                    // 如果终端ID为空或者时间戳格式不正确认为数据不正确，进入下一个数据
                    if (StringUtils.isBlank(device) && StringUtils.isBlank(timestamp) && timestamp.length() < 10) {
                        continue;
                    }
                    if (!clz.containsKey("device")) {
                        // 由于IOT没有终端ID标签，单独设置
                        clz.put("device", device);
                    }
                    if (!clz.containsKey("timestamp")) {
                        // 由于没有保存时间的标签，单独设置
                        clz.put("timestamp", DataTimeUtil.stampToDate(timestamp));
                    }
                    String value = json.getString("value");
                    if (tag.getEnglish().contains("time")) {
                        if (!StringUtils.isBlank(value) && value.length() >= 10) {
                            clz.put(tag.getEnglish(), DataTimeUtil.stampToDate(value));
                        }
                    } else {
                        clz.put(tag.getEnglish(), value);
                    }
                    voList.put(device + timestamp, clz);
                }
            }
        }
        return JSON.parseArray(JSON.toJSONString(voList.values()), clazz);
    }

    private DeviceDataVO getCacheData(DeviceDataVO deviceDataVO){
        String terminalid = deviceDataVO.getDevice();
        if (StringUtils.isNotBlank(terminalid)){
            Map<Object, Object> dataMap = redis.hmget(Const.PREFIX_TERMINAL+terminalid);
            if (dataMap != null && !dataMap.isEmpty()){
                Object location = dataMap.get(Const.LOCATION);
                if (location != null){
                    deviceDataVO.setCoordinates(location.toString());
                }
                Object worktime = dataMap.get(Const.WORKTIME);
                if (worktime != null){
                    deviceDataVO.setEngine_running_duration(worktime.toString());
                }
//                Object lockstatus = dataMap.get(Const.LOCKSTATUS);
//                if (lockstatus != null){
//                    deviceDataVO.setLocked_state(lockstatus.toString());
//                }
                Object fuelLevel = dataMap.get(Const.FUEL_LEVEL);
                if (fuelLevel != null){
                    deviceDataVO.setFuel_level(fuelLevel.toString());
                }
                Object carNo = dataMap.get(Const.CARNO);
                if (carNo != null){
                    deviceDataVO.setCarNo(carNo.toString());
                }
            }
        }
        return deviceDataVO;
    }
}
