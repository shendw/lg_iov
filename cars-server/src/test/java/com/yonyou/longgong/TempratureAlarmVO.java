package com.yonyou.longgong;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2020/5/21
 * @描述
 **/
@Data
@AllArgsConstructor
public class TempratureAlarmVO {

    @Excel(name = "终端号")
    private String device;

    @Excel(name = "整机编号")
    private String carno;

    @Excel(name = "报警类型")
    private String name;

    @Excel(name = "报警值")
    private String value;

    @Excel(name = "时间")
    private String timestamp;
}
