package com.yonyou.longgong;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.github.pagehelper.PageInfo;
import com.yonyou.longgong.carsserver.dao.CardocPojoMapper;
import com.yonyou.longgong.carsserver.dto.CarInfoSelectDto;
import com.yonyou.longgong.carsserver.enums.CarProcessStatus;
import com.yonyou.longgong.carsserver.example.CardocPojoExample;
import com.yonyou.longgong.carsserver.excel.ExcelVerifyCardocHandlerImpl;
import com.yonyou.longgong.carsserver.excel.ExcelVerifyCardocPojoOfMode;
import com.yonyou.longgong.carsserver.excel.ExcelVerifyTerminalHandlerImpl;
import com.yonyou.longgong.carsserver.excel.ExcelVerifyTerminalPojoOfMode;
import com.yonyou.longgong.carsserver.pojo.CardocPojo;
import com.yonyou.longgong.carsserver.service.ICarInfoService;
import com.yonyou.longgong.carsserver.service.ICardocService;
import com.yonyou.longgong.carsserver.vo.CarInfoVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author shendawei
 * @创建时间 2020/4/11
 * @描述
 **/
@SpringBootTest(classes = CarsServerApplication.class)
@RunWith(SpringRunner.class)
@Slf4j
public class CarInfoServiceTest {

    @Autowired
    private ICarInfoService iCarInfoService;

    @Autowired
    private ICardocService iCardocService;

    @Autowired
    private CardocPojoMapper cardocPojoMapper;

    @Test
    public void carInfoService(){
        CarInfoSelectDto carInfoSelectDto = new CarInfoSelectDto();
//        carInfoSelectDto.setCarNo("66");
//        PageInfo<CarInfoVo> carInfoVoPageInfo = iCarInfoService.selectCarInfo(carInfoSelectDto, "ebe1f6ed8ade4e52a02cbc88363fb3d9", "sdw");
//        PageInfo<CarInfoVo> carInfoVoPageInfo = iCarInfoService.selectCarInfo(carInfoSelectDto, "a1cffec832d54d2283a0f742c8c91a03", "00111");
        PageInfo<CarInfoVo> carInfoVoPageInfo = iCarInfoService.selectCarInfo(carInfoSelectDto, "c4d9c43cd1934686a0045d57b2f34d8d", "1425318");
        return;
    }

    @Test
    public void carInfoInsert() throws IOException {
        log.info("begin carExcel insert!");
        long startTime = System.currentTimeMillis();
        File file = new File("C:\\Users\\user\\Desktop\\1111\\3台大挖.xlsx");
        ImportParams params = new ImportParams();
        //params.setTitleRows(1);
        params.setNeedVerify(true);
        params.setVerifyHandler(new ExcelVerifyCardocHandlerImpl());
        params.setImportFields(new String[] { "终端", "版本","SIM卡号","流程状态","整机编号"});
        ExcelImportResult<ExcelVerifyCardocPojoOfMode> excelImportResult = ExcelImportUtil.importExcelMore(file, ExcelVerifyCardocPojoOfMode.class, params);
        long endTime = System.currentTimeMillis();
        log.info("carExcel check end fail.size {},success.size {},time {}"
                ,excelImportResult.getFailList().size()
                ,excelImportResult.getList().size()
                ,endTime-startTime);
        List<ExcelVerifyCardocPojoOfMode> failList = excelImportResult.getFailList();
        Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams(null, "车辆档案错误信息"), ExcelVerifyCardocPojoOfMode.class, failList);
        FileOutputStream fos = new FileOutputStream("C:\\Users\\user\\Desktop\\1111\\3台大挖错误信息.xlsx");
        sheets.write(fos);
    }

    @Test
    public void terminalInsert() throws IOException {
        File file = new File("/Users/shendawei/Desktop/龙工0411文件/成品终端导入.xlsx");
        ImportParams params = new ImportParams();
        params.setNeedVerify(true);
        params.setVerifyHandler(new ExcelVerifyTerminalHandlerImpl());
        params.setImportFields(new String[] { "终端", "版本","SIM卡号"});
        ExcelImportResult<ExcelVerifyTerminalPojoOfMode> excelImportResult = ExcelImportUtil.importExcelMore(file, ExcelVerifyTerminalPojoOfMode.class, params);
        log.info("carExcel check end fail.size {},success.size {}"
                ,excelImportResult.getFailList().size()
                ,excelImportResult.getList().size());
        List<ExcelVerifyTerminalPojoOfMode> failList = excelImportResult.getFailList();
        Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams(null, "成品终端导入错误信息"), ExcelVerifyTerminalPojoOfMode.class, failList);
        FileOutputStream fos = new FileOutputStream("/Users/shendawei/Desktop/龙工0411文件/成品终端导入错误信息.xlsx");
        sheets.write(fos);
    }

    @Test
    public void getU9Info(){
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        cardocPojoExample.createCriteria().andLogicalDeleted(false).andStatusEqualTo(CarProcessStatus.SOLD.getCode());
        List<CardocPojo> cardocPojos = cardocPojoMapper.selectByExample(cardocPojoExample);
        int times = (int)Math.ceil( cardocPojos.size()/50.0 );
        for(int i=0; i<times; i++ ){
            Integer integer = iCardocService.syncCardocFromU9(cardocPojos.subList(i * 50, Math.min((i + 1) * 50, cardocPojos.size())));
            System.out.println("第"+(i+1)+"次");
            System.out.println(integer);
        }
    }

    @Test
    public void getU9InfoNotSale(){
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
//        cardocPojoExample.createCriteria().andLogicalDeleted(false).andStatusIn(Arrays.asList(CarProcessStatus.OUTLINE.getCode(),CarProcessStatus.TEST.getCode(),CarProcessStatus.INLINE.getCode(),CarProcessStatus.STOCK.getCode(),CarProcessStatus.AUTOBIND.getCode()));
        cardocPojoExample.createCriteria().andLogicalDeleted(false).andStatusIn(Arrays.asList(CarProcessStatus.OUTLINE.getCode()));
        List<CardocPojo> cardocPojos = cardocPojoMapper.selectByExample(cardocPojoExample);
        cardocPojos.forEach(x -> {
            System.out.println(x.getCarno());
        });
        int times = (int)Math.ceil( cardocPojos.size()/50.0 );
        for(int i=0; i<times; i++ ){
            Integer integer = iCardocService.syncCardocStockFromU9(cardocPojos.subList(i * 50, Math.min((i + 1) * 50, cardocPojos.size())));
            System.out.println("第"+(i+1)+"次");
            System.out.println(integer);
        }
    }

    @Test
    public void getU9InfoNotSaleToSale(){
        CardocPojoExample cardocPojoExample = new CardocPojoExample();
        cardocPojoExample.createCriteria().andLogicalDeleted(false)
                .andStatusIn(Arrays.asList(CarProcessStatus.OUTLINE.getCode(),CarProcessStatus.INLINE.getCode(),CarProcessStatus.STOCK.getCode(),CarProcessStatus.OUTLINE.getCode(),CarProcessStatus.TEST.getCode()));
        List<CardocPojo> cardocPojos = cardocPojoMapper.selectByExample(cardocPojoExample);
        cardocPojos.forEach(x -> {
            System.out.println(x.getCarno());
        });
        int times = (int)Math.ceil( cardocPojos.size()/50.0 );
        for(int i=0; i<times; i++ ){
            Integer integer = iCardocService.syncCardocFromU9(cardocPojos.subList(i * 50, Math.min((i + 1) * 50, cardocPojos.size())));
//            System.out.println("第"+(i+1)+"次");
            log.info("第"+(i+1)+"次");
            System.out.println(integer);
        }
    }
}
