package com.yonyou.longgong;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ctc.wstx.util.DataUtil;
import com.yonyou.longgong.carsserver.dao.TerminalCarPojoMapper;
import com.yonyou.longgong.carsserver.dao.TerminalPojoMapper;
import com.yonyou.longgong.carsserver.enums.TerminalCarStatus;
import com.yonyou.longgong.carsserver.example.TerminalCarPojoExample;
import com.yonyou.longgong.carsserver.example.TerminalPojoExample;
import com.yonyou.longgong.carsserver.pojo.SimPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalCarPojo;
import com.yonyou.longgong.carsserver.pojo.TerminalPojo;
import com.yonyou.longgong.carsserver.service.ITerminalService;
import com.yonyou.longgong.carsserver.utils.DataTimeUtil;
import com.yonyou.longgong.iotserver.common.Const;
import com.yonyou.longgong.iotserver.dto.HistoryDataParamDto;
import com.yonyou.longgong.iotserver.dto.TerminalLogDto;
import com.yonyou.longgong.iotserver.enums.TagsEnums;
import com.yonyou.longgong.iotserver.service.IDeviceService;
import com.yonyou.longgong.iotserver.service.ISiteService;
import com.yonyou.longgong.iotserver.service.ITerminalLogService;
import com.yonyou.longgong.iotserver.utils.RedisUtils;
import com.yonyou.longgong.iotserver.vo.DeviceDetailVO;
import com.yonyou.longgong.iotserver.vo.TerminalLogVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * @author shendawei
 * @创建时间 2020/4/14
 * @描述
 **/
@SpringBootTest(classes = CarsServerApplication.class)
@RunWith(SpringRunner.class)
@Slf4j
public class DeviceServiceTest {

    @Autowired
    private IDeviceService deviceService;

    @Autowired
    private ISiteService iSiteService;

    @Autowired
    private ITerminalLogService iTerminalLogService;

    @Autowired
    private TerminalCarPojoMapper terminalCarPojoMapper;

    @Autowired
    private TerminalPojoMapper terminalPojoMapper;

    @Autowired
    private RedisUtils redis;

    @Test
    public void testDevice(){
        DeviceDetailVO hlkm10DB7E166E00 = deviceService.getDeviceDetailData(">LFW0220ECH0001658<", "HLKM10DB7E166E00", false);
        return;
    }

    @Test
    public void fixDevice(){
            String[] cars = new String[]{
                    ">LSW0365EAL0020828<",">LSW0490FKL0020188<",">LSW0490FEL0020198<",">LSW0285ETL0021368<",">LSW0365EPL0021518<"
            };
        TerminalCarPojoExample terminalCarPojoExample = new TerminalCarPojoExample();
        terminalCarPojoExample.createCriteria().andLogicalDeleted(false).andCarnoIn(Arrays.asList(cars));
        List<TerminalCarPojo> terminalCarPojos = terminalCarPojoMapper.selectByExample(terminalCarPojoExample);
        int num = 0;
        for (TerminalCarPojo terminalCarPojo: terminalCarPojos) {
            String terminalid = terminalCarPojo.getTerminalid();
            HistoryDataParamDto historyDataParamDto = new HistoryDataParamDto();
            historyDataParamDto.setTerminalid(terminalid);
            historyDataParamDto.setBeginTime("2020-04-15 00:00:00");
            historyDataParamDto.setEndTime("2020-04-20 19:00:00");
            String site = iSiteService.getSite(terminalid, false);
            if (StringUtils.isEmpty(site)){
                log.error("终端 {} 没站点",terminalid);
                continue;
            }
            String[] tags = new String[]{TagsEnums.ENGINE_RUNNING_DURATION.getChinese(),
                    TagsEnums.COORDINATES.getChinese(),TagsEnums.FUEL_LEVEL.getChinese()};
            String tagNames = "";
            for (String tagName : tags) {
                tagNames += site + "." + terminalid + "." + tagName + ",";
            }
            tagNames = tagNames.substring(0, tagNames.length() - 1);
            String result = deviceService.getDeviceHistoryData(historyDataParamDto, tagNames, false);
            JSONObject resultJson = (JSONObject) JSON.parse(result);
            if (resultJson.get("data") == null) {
                log.error("终端 {} 没数据",terminalid);
                continue;
            }
            JSONObject jsonObject = new JSONObject();
            if (resultJson.get("data") instanceof JSONObject) {
                jsonObject = resultJson.getJSONObject("data");
            }
            String worktime = null;
            String location = null;
            String fuelLevel = null;
            for (String key : jsonObject.keySet()) {
                String name = key.substring(key.lastIndexOf(".") + 1);
                if (TagsEnums.ENGINE_RUNNING_DURATION.getChinese().equals(name)){
                    JSONArray jsonArray = jsonObject.getJSONArray(key);
                    int size = jsonArray.size();
                    if (size > 0){
                        for (int i = size;i>0;i--){
                            JSONObject js = jsonArray.getJSONObject(i-1);
                            String value = js.getString("value");
                            if (!"0".equals(value)){
                                worktime = value;
                                break;
                            }
                        }

                    }
                    continue;
                }
                if (TagsEnums.COORDINATES.getChinese().equals(name)){
                    JSONArray jsonArray = jsonObject.getJSONArray(key);
                    int size = jsonArray.size();
                    if (size > 0){
                        for (int i = size ;i>0;i--){
                            JSONObject js = jsonArray.getJSONObject(i-1);
                            String value = js.getString("value");
                            if (!"0.000000E,0.000000N".equals(value)){
                                location = value;
                                break;
                            }
                        }

                    }
                    continue;
                }
                if (TagsEnums.FUEL_LEVEL.getChinese().equals(name)){
                    JSONArray jsonArray = jsonObject.getJSONArray(key);
                    int size = jsonArray.size();
                    if (size > 0){
                        for (int i = size ;i>0;i--){
                            JSONObject js = jsonArray.getJSONObject(i-1);
                            String value = js.getString("value");
                            if (!"0".equals(value)){
                                fuelLevel = value;
                                break;
                            }
                        }

                    }
                    continue;
                }
            }
            HashMap<String, Object> datamap = new HashMap<>(3);
            if (StringUtils.isNotEmpty(worktime)){
                datamap.put(Const.WORKTIME,worktime);
            }else {
                datamap.put(Const.WORKTIME,"0");
                log.error("终端号 {} 没工作小时",terminalid);
            }
            if (StringUtils.isNotEmpty(location)){
                datamap.put(Const.LOCATION,location);
            }else{
                redis.hdel(Const.PREFIX_TERMINAL + terminalid,Const.LOCATION);
                log.error("终端号 {} 没定位",terminalid);
            }
            if (StringUtils.isNotEmpty(fuelLevel)){
                datamap.put(Const.FUEL_LEVEL,fuelLevel);
            }else {
                datamap.put(Const.FUEL_LEVEL,"0");
                log.error("终端号 {} 燃油油位没有",terminalid);
            }
            redis.hmset(Const.PREFIX_TERMINAL + terminalid, datamap);
            System.out.println(num++);
        }
    }

    @Test
    public void recache(){
        String[] terminals = new String[]{
                "HLKM118B41DF9A90",
                "HLKM11AFD1FE09F0",
                "HLKM11388326FE50",
                "HLKM10BC8BAAC2F0"
        };
        int num = 0;
        for (String terminal:
             terminals) {
            redis.hdel(Const.PREFIX_TERMINAL+terminal,Const.CARNO);
            System.out.println(num++);
        }
    }

    /**
     * 统计水温高于90度或者油温高于65度的车辆
     */
    @Test
    public void findtemp() throws IOException {
        TerminalCarPojoExample terminalCarPojoExample = new TerminalCarPojoExample();
        terminalCarPojoExample.createCriteria().andLogicalDeleted(false).andEnablestateEqualTo(TerminalCarStatus.DEFEAT.getCode());
        List<TerminalCarPojo> terminalCarPojos = terminalCarPojoMapper.selectByExample(terminalCarPojoExample);
        ArrayList<TempratureAlarmVO> tempAlarmList = new ArrayList<>();
        int index = 0;
        for (TerminalCarPojo terminalCarPojo: terminalCarPojos) {
            System.out.println(index);
            String terminalid = terminalCarPojo.getTerminalid();
//            Object hget = redis.hget(Const.PREFIX_TERMINAL + terminalid, Const.CARNO);
//            String carno = hget == null?null:hget.toString();
            String carno = terminalCarPojo.getCarno();
            HistoryDataParamDto historyDataParamDto = new HistoryDataParamDto();
            historyDataParamDto.setTerminalid(terminalid);
            historyDataParamDto.setBeginTime("2020-07-15 00:00:00");
            historyDataParamDto.setEndTime("2020-05-22 00:00:00");
            String site = iSiteService.getSite(terminalid, false);
            if (StringUtils.isEmpty(site)){
                log.error("终端 {} 没站点",terminalid);
                continue;
            }
            String[] tags = new String[]{TagsEnums.ENGINE_WATER_TEMPERATURE.getChinese(),
                    TagsEnums.HYDRAULIC_OIL_TEMPERATURE.getChinese()};
            String tagNames = "";
            for (String tagName : tags) {
                tagNames += site + "." + terminalid + "." + tagName + ",";
            }
            tagNames = tagNames.substring(0, tagNames.length() - 1);
            String result = deviceService.getDeviceHistoryData(historyDataParamDto, tagNames, false);
            JSONObject resultJson = (JSONObject) JSON.parse(result);
            if (resultJson.get("data") == null) {
                log.error("终端 {} 没数据",terminalid);
                continue;
            }
            JSONObject jsonObject = new JSONObject();
            if (resultJson.get("data") instanceof JSONObject) {
                jsonObject = resultJson.getJSONObject("data");
            }
            for (String key : jsonObject.keySet()) {
                String name = key.substring(key.lastIndexOf(".") + 1);
                if (TagsEnums.ENGINE_WATER_TEMPERATURE.getChinese().equals(name)){
                    JSONArray jsonArray = jsonObject.getJSONArray(key);
                    int size = jsonArray.size();
                    if (size > 0){
                        List<JSONObject> jsonObjects = jsonArray.toJavaList(JSONObject.class);
                        Collections.sort(jsonObjects, Comparator.comparing(o -> o.getString("value")));
                        JSONObject js = jsonObjects.get(0);
                        String value = js.getString("value");
                        if ("90".compareTo(value) < 0){
                            String timestamp = js.getString("timestamp");
                            tempAlarmList.add(new TempratureAlarmVO(terminalid,carno,name,value,DataTimeUtil.stampToDate(timestamp)));
                        }
                    }else{
                        continue;
                    }
                }
                if (TagsEnums.HYDRAULIC_OIL_TEMPERATURE.getChinese().equals(name)){
                    JSONArray jsonArray = jsonObject.getJSONArray(key);
                    int size = jsonArray.size();
                    if (size > 0){
                        List<JSONObject> jsonObjects = jsonArray.toJavaList(JSONObject.class);
                        Collections.sort(jsonObjects, Comparator.comparing(o -> o.getString("value")));
                        JSONObject js = jsonObjects.get(0);
                        String value = js.getString("value");
                        if ("65".compareTo(value) < 0){
                            String timestamp = js.getString("timestamp");
                            tempAlarmList.add(new TempratureAlarmVO(terminalid,carno,name,value,DataTimeUtil.stampToDate(timestamp)));
                        }
                    }else{
                        continue;
                    }
                }
            }
        }
        Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams("油温水温报警统计", "油温水温报警统计"), TempratureAlarmVO.class, tempAlarmList);
        FileOutputStream fos = new FileOutputStream("C:\\Users\\user\\Desktop\\1111\\5月份油温水温报警统计.xlsx");
        sheets.write(fos);
    }

    @Test
    public void getWorkTime() throws IOException {
        String carno = ">LSW0365EAL0016018<";
        String terminalid = "HLKM11137E497BA0";
        HistoryDataParamDto historyDataParamDto = new HistoryDataParamDto();
        historyDataParamDto.setTerminalid(terminalid);
        historyDataParamDto.setBeginTime("2020-04-00 00:00:00");
        historyDataParamDto.setEndTime("2020-05-01 00:00:00");
        String site = iSiteService.getSite(terminalid, false);
        if (StringUtils.isEmpty(site)){
            log.error("终端 {} 没站点",terminalid);
            return;
        }
        String[] tags = new String[]{TagsEnums.ENGINE_RUNNING_DURATION.getChinese()};
        String tagNames = "";
        for (String tagName : tags) {
            tagNames += site + "." + terminalid + "." + tagName + ",";
        }
        tagNames = tagNames.substring(0, tagNames.length() - 1);
        String result = deviceService.getDeviceHistoryData(historyDataParamDto, tagNames, false);
        JSONObject resultJson = (JSONObject) JSON.parse(result);
        if (resultJson.get("data") == null) {
            log.error("终端 {} 没数据",terminalid);
            return;
        }
        JSONObject jsonObject = new JSONObject();
        if (resultJson.get("data") instanceof JSONObject) {
            jsonObject = resultJson.getJSONObject("data");
        }
        ArrayList<WorkTimeVO> tempAlarmList = new ArrayList<>();
        for (String key : jsonObject.keySet()) {
            String name = key.substring(key.lastIndexOf(".") + 1);
                JSONArray jsonArray = jsonObject.getJSONArray(key);
                int size = jsonArray.size();
                if (size > 0){
                    List<JSONObject> jsonObjects = jsonArray.toJavaList(JSONObject.class);
                    if (jsonObjects.size() > 0){
                        jsonObjects.forEach(x -> {
                            String value = x.getString("value");
                            String timestamp = x.getString("timestamp");
                            tempAlarmList.add(new WorkTimeVO(terminalid,carno,name,value,DataTimeUtil.stampToDate(timestamp)));
                        });
                    }
                }else{
                    continue;
                }
        }
        Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams("工作小时", "工作小时"), WorkTimeVO.class, tempAlarmList);
        FileOutputStream fos = new FileOutputStream("/Users/shendawei/Desktop/工作小时.xlsx");
        sheets.write(fos);
    }


    @Test
    public void cmd() throws IOException {
//        TerminalPojoExample terminalPojoExample = new TerminalPojoExample();
//        terminalPojoExample.createCriteria().andIsnewIsNull().andStatusEqualTo(5L);
//        List<TerminalPojo> terminalPojos = terminalPojoMapper.selectByExample(terminalPojoExample);
//        List<String> terminalPojos = terminalPojoMapper.selectByArea();
//        ArrayList<String> terminals = new ArrayList<>();
//        int size = terminalPojos.size();
//        for (int i = 0; i < terminalPojos.size(); i++) {
//            if (i%10 == 0){
//                System.out.println(i*100/size+"%");
//            }
//            TerminalLogDto terminalLogDto = new TerminalLogDto();
//            terminalLogDto.setTerminalid(terminalPojos.get(i));
//            terminalLogDto.setStartTime("2021-04-01 00:00:00");
//            terminalLogDto.setEndTime("2021-04-14 23:59:59");
//            List<TerminalLogVO> terminalLogVOS = iTerminalLogService.selectTerminalLog(terminalLogDto, false);
//            if (!CollectionUtils.isEmpty(terminalLogVOS)){
//                int flag = 0;
//                for (TerminalLogVO terminalLogVO : terminalLogVOS) {
//                    if ("60H".equals(terminalLogVO.getOpkeyword())){
//                        flag++;
//                    }else {
//                        flag = 0;
//                    }
//                    if (flag >=8){
//                        terminals.add(terminalPojos.get(i));
//                        break;
//                    }
//                }
//            }
//        }
//        Workbook sheets = ExcelExportUtil.exportExcel(new ExportParams("登录异常车", "登录异常车"), String.class, terminals);
//        FileOutputStream fos = new FileOutputStream("/Users/shendawei/Desktop/登录异常车.xlsx");
//        sheets.write(fos);
    }
}
