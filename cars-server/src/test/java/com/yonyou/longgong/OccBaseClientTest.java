package com.yonyou.longgong;


import com.yonyou.longgong.afterservice.client.AfterServiceClient;
import com.yonyou.longgong.carsserver.client.OccBaseClient;
import com.yonyou.longgong.carsserver.occdto.FeignPage;
import com.yonyou.longgong.carsserver.occdto.OccOrgDto;
import com.yonyou.longgong.iotserver.service.ISiteService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shendawei
 * @创建时间 2019-12-09
 * @描述
 **/
@SpringBootTest(classes = CarsServerApplication.class)
@RunWith(SpringRunner.class)
public class OccBaseClientTest {

    @Autowired
    private OccBaseClient occBaseClient;

    @Autowired
    private AfterServiceClient afterServiceClient;

    @Autowired
    private ISiteService iSiteService;

    @Test
    public void OccTest(){
//        OccPersonInfoDto[] personInfo = occBaseClient.getPersonInfo("0WHXUY7u6obz8PHOwBhv", "person");
//        FeignPage<OccOrgDto> organization = occBaseClient.getOrgList("10000", "0", "organization");
        String s = afterServiceClient.updateStatus("111", true);
        return;
    }

    @Test
    public void SiteTest(){
        String cc = iSiteService.getSite("cc", true);
        return;
    }
}
