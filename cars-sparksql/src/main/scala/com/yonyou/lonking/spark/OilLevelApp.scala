package com.yonyou.lonking.spark

import java.sql.{Connection, DriverManager, PreparedStatement}
import java.util.{Calendar, Locale}

import com.yonyou.lonking.utils.UUIDGenerator
import org.apache.commons.lang3.time.FastDateFormat
import org.apache.spark.internal.Logging
import org.apache.spark.sql.SparkSession

/**
 *
 * @author shendawei
 * @创建时间 2020/10/9
 * @描述
 *
 **/
object OilLevelApp extends Logging{
  def main(args: Array[String]): Unit = {

    if(args.length != 1) {
      println("Usage: OilLevelApp <time>")
      System.exit(1)
    }

//    val url = "jdbc:oracle:thin:@10.10.10.217/LKSALESDEV"
    val url = "jdbc:oracle:thin:@10.10.10.200:1521:LKSALES"
    val user = "yonyou"
    val password = "yonyou"
//    val sparkSession = SparkSession.builder().appName("OilLevelApp").master("local[2]").getOrCreate()
    val sparkSession = SparkSession.builder().getOrCreate()
    try{
      val day1 = args(0)

      val formatTime = FastDateFormat.getInstance("yyyy-MM-dd", Locale.ENGLISH)
      val startDate = formatTime.parse(day1)
      val cal = Calendar.getInstance()
      cal.setTime(startDate)
      val startTime = startDate.getTime
      cal.add(Calendar.DATE,1)
      val endTime = cal.getTimeInMillis

      val df = sparkSession.sqlContext
        .read
        .format("org.apache.phoenix.spark")
        .options(Map("table" -> "TERMINAL_INFO", "zkUrl" -> "10.10.10.170:2181"))
        .load

      import org.apache.spark.sql.functions._
      val worktime = df.select("最后时间","DEVICE","发动机运行时间","PROVINCE","CITY","DISTRICT","TOWN")
        .filter(df.col("最后时间") between(startTime, endTime))
        .groupBy("DEVICE")
        .agg(last("发动机运行时间").as("max")
          , first("发动机运行时间").as("min")
          ,last("PROVINCE").as("PROVINCE")
          ,last("CITY").as("CITY")
          ,last("DISTRICT").as("DISTRICT")
          ,last("TOWN").as("TOWN"))
//      worktime.show(100)
      worktime.createOrReplaceTempView("worktime")
      val dataFrame = sparkSession
        .sql(" select device,max,min,format_number(max-min,2) as daily,PROVINCE,CITY,DISTRICT,TOWN from worktime ")
//      dataFrame.show(100)
//      dataFrame.printSchema()
      dataFrame.foreachPartition(rows => {
        Class.forName("oracle.jdbc.driver.OracleDriver").newInstance()
        val connection: Connection = DriverManager.getConnection(url,user,password)
        val prepareStatement: PreparedStatement = connection
          .prepareStatement("insert into iov_terminal_daily(id, device,year,month,day,worktime,province,city,district,town) values(?,?,?,?,?,?,?,?,?,?)")
        rows.foreach(row =>{
          val daily = row.getAs[String]("daily")
          val dailyFormat = daily.replaceAll(",", "")
          val dailyDouble = dailyFormat.toDouble
          if(dailyDouble>0.0 && dailyDouble<=24.0){
            prepareStatement.setString(1,UUIDGenerator.generateId())
            prepareStatement.setString(2,row.getAs[String]("device"))
            prepareStatement.setString(3,day1.substring(0,4))
            prepareStatement.setString(4,day1.substring(0,7))
            prepareStatement.setString(5,day1)
            prepareStatement.setDouble(6,dailyDouble)
            prepareStatement.setString(7,row.getAs[String]("PROVINCE"))
            prepareStatement.setString(8,row.getAs[String]("CITY"))
            prepareStatement.setString(9,row.getAs[String]("DISTRICT"))
            prepareStatement.setString(10,row.getAs[String]("TOWN"))
            prepareStatement.addBatch()
          }
        })
        prepareStatement.executeBatch()

        prepareStatement.close()
        connection.close()
      })
    }finally {
      sparkSession.close();
    }

  }
}
