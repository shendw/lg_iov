import java.sql.{Connection, DriverManager, PreparedStatement}
import java.util.{Calendar, Locale}

import com.yonyou.lonking.utils.UUIDGenerator
import org.apache.commons.lang3.time.FastDateFormat
import org.apache.spark.internal.Logging
import org.apache.spark.sql.catalyst.expressions.Log
import org.junit.Test

/**
 *
 * @author shendawei
 * @创建时间 2020/10/20
 * @描述
 *
 **/
object jdbcTest extends Logging{
  def main(args: Array[String]): Unit = {
    val url = "jdbc:oracle:thin:@10.10.10.217/LKSALESDEV"
    val user = "yonyou"
    val password = "yonyou"

    val day1 = "2020-10-20"

    val formatTime = FastDateFormat.getInstance("yyyy-MM-dd", Locale.ENGLISH)
    val startDate = formatTime.parse(day1)
    val cal = Calendar.getInstance()
    cal.setTime(startDate)

    Class.forName("oracle.jdbc.driver.OracleDriver").newInstance()
    val connection: Connection = DriverManager.getConnection(url,user,password)
    val prepareStatement: PreparedStatement = connection
      .prepareStatement("insert into OCC_ALL_1230.iov_terminal_daily(id, device,year,month,day,worktime,province,city,district,town) values(?,?,?,?,?,?,?,?,?,?)")

    for(i <- 0 to 10){
      prepareStatement.setString(1,UUIDGenerator.generateId())
      prepareStatement.setString(2,"11111111111")
      prepareStatement.setString(3,cal.get(Calendar.YEAR).toString)
      prepareStatement.setString(4,cal.get(Calendar.MONTH).toString)
      prepareStatement.setString(5,day1)
      prepareStatement.setDouble(6,2.2)
      prepareStatement.setString(7,"PROVINCE")
      prepareStatement.setString(8,"CITY")
      prepareStatement.setString(9,"DISTRICT")
      prepareStatement.setString(10,"TOWN")
      prepareStatement.addBatch()

    }

    prepareStatement.executeBatch()

    prepareStatement.close()
    connection.close()
  }
}
