import com.github.pagehelper.PageInfo;
import com.yonyou.lonking.HbaseApplication;
import com.yonyou.lonking.dto.BiForm4OneTerminalDto;
import com.yonyou.lonking.entity.WorkTimesEntity;
import com.yonyou.lonking.service.ITagDailyService;
import com.yonyou.lonking.service.IWorkTimesService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;

/**
 * @author shendawei
 * @创建时间 2020/10/16
 * @描述
 **/
@SpringBootTest(classes = HbaseApplication.class)
@RunWith(SpringRunner.class)
@Slf4j
public class OilLevelTest {

    @Autowired
    private ITagDailyService iTagDailyService;

    @Autowired
    private IWorkTimesService iWorkTimesService;

    @Test
    public void getoilLevel() throws ParseException {
        BiForm4OneTerminalDto oilLevelByDay = iTagDailyService.getOilLevelByDay("HLKM108A16989390", null,"2020-09-21");
        System.out.println(oilLevelByDay);
        BiForm4OneTerminalDto getworkTimeByDay = iTagDailyService.getworkTimeByDay("HLKM108A16989390", null,"2020-09-21");
        System.out.println(getworkTimeByDay);
        BiForm4OneTerminalDto machineStartByDay = iTagDailyService.getMachineStartByDay("HLKM108A16989390",null, "2020-09-21");
        System.out.println(machineStartByDay);
    }

    @Test
    public void getWorkTimes() throws ParseException {
        PageInfo<WorkTimesEntity> hlkm10425EC3DB70 = iWorkTimesService.getWorkTimes("HLKM10425EC3DB70",null, "2020-09-21", "2020-10-20", 1, 5);
//        PageInfo<WorkTimesEntity> hlkm10425EC3DB70 = iWorkTimesService.getWorkTimes("HLKM10425EC3DB70", null, null, 1, 5);
        System.out.println(hlkm10425EC3DB70);
    }
}
