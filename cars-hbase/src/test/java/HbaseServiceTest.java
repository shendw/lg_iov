import com.yonyou.lonking.HbaseApplication;
import com.yonyou.lonking.dao.phoenix.TerminalMapper;
import com.yonyou.lonking.entity.TerminalInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author shendawei
 * @创建时间 2020/4/11
 * @描述
 **/
@SpringBootTest(classes = HbaseApplication.class)
@RunWith(SpringRunner.class)
@Slf4j
public class HbaseServiceTest {

    @Autowired
    private TerminalMapper terminalMapper;

    @Test
    public void testGetOrders() {
        List<TerminalInfo> history = terminalMapper.getHistory();
        for (TerminalInfo terminalInfo : history) {
            System.out.println(terminalInfo);
        }
    }

    @Test
    public void testworkTime() throws ParseException {
        String terminalid = "HLKM11FB392E56F0%";
        String startDate = "2020-09-18 00:00:00";
        String endDate = "2020-09-30 00:00:00";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date startdate = df.parse(startDate);
        Date enddate = df.parse(endDate);
        Calendar cal = Calendar.getInstance();
        cal.setTime(startdate);
        long start = cal.getTimeInMillis();
        cal.setTime(enddate);
        long end = cal.getTimeInMillis();
        for (int i = 0; i < 5; i++) {
            List<TerminalInfo> history = terminalMapper.getWorkTime(String.valueOf(start),String.valueOf(end),terminalid,100,i*100);
            for (TerminalInfo terminalInfo : history) {
                System.out.println(terminalInfo);
            }
            System.out.println("--------------");
        }
    }
}
