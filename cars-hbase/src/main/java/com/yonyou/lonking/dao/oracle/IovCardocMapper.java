package com.yonyou.lonking.dao.oracle;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yonyou.lonking.entity.IovCardoc;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author shendw
 * @since 2020-10-30
 */
public interface IovCardocMapper extends BaseMapper<IovCardoc> {

}
