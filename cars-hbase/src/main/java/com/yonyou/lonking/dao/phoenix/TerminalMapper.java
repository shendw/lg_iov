package com.yonyou.lonking.dao.phoenix;

import com.yonyou.lonking.entity.TerminalInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2020/9/28
 * @描述
 **/
@Mapper
public interface TerminalMapper {
    List<TerminalInfo> getHistory();

    List<TerminalInfo> getWorkTime(@Param("startTime") String startTime, @Param("endTime") String endTime,@Param("rowkey") String rowkey
            ,@Param("limit")  int limit, @Param("offset")int offset);

    List<TerminalInfo> getOilLevelByDay(@Param("startRow") String startRow, @Param("endRow") String endRow);

    List<TerminalInfo> getWorkTimeByDay(@Param("startRow") String startRow, @Param("endRow") String endRow);

    List<TerminalInfo> getMachineStartByDay(@Param("startRow") String startRow, @Param("endRow") String endRow);
}
