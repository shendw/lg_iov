package com.yonyou.lonking.dao.oracle;

import com.yonyou.lonking.dto.display.ResultByConditionDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2020/12/3
 * @描述
 **/
@Mapper
public interface IovTerminalDailyMapper {

    List<ResultByConditionDto> getWorkTimeStatusticsByProvince(String day);

    List<ResultByConditionDto> getWorkingDaysByProvince(String month);

    List<ResultByConditionDto> getWorkingDaysByCity(@Param("month") String month, @Param("province") String province);

    List<ResultByConditionDto> getWorkingDaysByDistricts(@Param("month") String month, @Param("city") String city);

    List<ResultByConditionDto> getWorkingDaysByDistrict(@Param("month") String month, @Param("province") String province);

    List<ResultByConditionDto> getWorkingDaysByTown(@Param("month") String month, @Param("district") String district);

    List<ResultByConditionDto> getWorkingDaysByAgent(String month);

    List<ResultByConditionDto>  getAvgWorkTimeByCarmodel(String day);

    List<ResultByConditionDto> getAvgWorkTimeByCarmodelAndProvince(@Param("day") String day,@Param("province") String province);

    List<ResultByConditionDto> getAvgWorkTimeByCarmodelAndCity(@Param("day") String day,@Param("city") String city ,@Param("isMunicipalities") int isMunicipalitiess );
}
