package com.yonyou.lonking.dao.oracle;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yonyou.lonking.dto.display.CarInfoByProvinceDto;
import com.yonyou.lonking.dto.display.ResultByConditionDto;
import com.yonyou.lonking.entity.IovTerminalInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2020/12/3
 * @描述
 **/
@Mapper
public interface IovTerminalInfoMapper extends BaseMapper<IovTerminalInfo> {

    List<ResultByConditionDto> getCarNumByProvince();

    List<ResultByConditionDto> getCarNumByCity(String provinceCode);

    List<ResultByConditionDto> getCarNumByDistricts(String cityCode);

    List<ResultByConditionDto> getCarNumByDistrict(String provinceCode);

    List<ResultByConditionDto> getCarNumByDistrictCode(String districtCode);

    List<ResultByConditionDto> getCarNumByAgent();

    List<ResultByConditionDto> getCarNumOnlineByProvince();

    List<ResultByConditionDto> getCarNumOnlineByCity(String provinceCode);

    List<ResultByConditionDto> getCarNumOnlineByDistricts(String cityCode);

    List<ResultByConditionDto> getCarNumOnlineByDistrict(String provinceCode);

    List<ResultByConditionDto> getCarNumOnlineByDistrictCode(String districtCode);

    List<ResultByConditionDto> getCarNumOnlineByTown(String districtCode);

    List<ResultByConditionDto> getCarNumOnlineByAgent();

    List<ResultByConditionDto> getCarNumOfflineByProvince();

    List<ResultByConditionDto> getCarNumOfflineByCity(String provinceCode);

    List<ResultByConditionDto> getCarNumOfflineByAgent();

    int getCarNumByWorkStatus(@Param("workStatus") String workStatus);

    int getCarNumByWorkStatusAndProvince(@Param("workStatus") String workStatus,@Param("provinceCode") String provinceCode);

    int getCarNumByWorkStatusAndCity(@Param("workStatus") String workStatus,@Param("cityCode") String cityCode,@Param("isMunicipalities") int isMunicipalities);

    List<CarInfoByProvinceDto> getCarWorkInfoByProvince(String provinceCode);

    List<CarInfoByProvinceDto> getCarWorkInfoByCity(@Param("cityCode") String cityCode,@Param("isMunicipalities") int isMunicipalities);
}
