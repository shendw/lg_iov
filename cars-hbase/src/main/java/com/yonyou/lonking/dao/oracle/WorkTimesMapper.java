package com.yonyou.lonking.dao.oracle;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yonyou.lonking.dto.WorkTimeStatisticsDto;
import com.yonyou.lonking.entity.WorkTimesEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2020/10/22
 * @描述
 **/
public interface WorkTimesMapper extends BaseMapper<WorkTimesEntity> {

    /**
     * 单台车的工作时间
     * @param terminalId
     * @param startDate
     * @param endDate
     * @return
     */
    List<WorkTimesEntity> getWorkTimes(@Param(value = "device") String terminalId
            , @Param(value = "startDate") String startDate, @Param(value = "endDate") String endDate);

    /**
     * 按省份统计各省份所有车的总工时
     * @param date
     * @return
     */
    List<WorkTimeStatisticsDto> getSumWorkTimesByProvinceAndDay(String date);
    List<WorkTimeStatisticsDto> getSumWorkTimesByProvinceAndMonth(String date);
    List<WorkTimeStatisticsDto> getSumWorkTimesByProvinceAndYear(String date);

    /**
     * 多维度工时统计
     * @param date
     * @param key
     * @param func
     * @param dateField
     * @return
     */
    List<WorkTimeStatisticsDto> getWorkTimeStatistics(@Param(value = "date")String date
            , String key, String func, String dateField);
    /**
     * 单台车的开工天数
     * @param terminalId
     * @return
     */
    List<WorkTimeStatisticsDto> getOperatingRateByMonth(String terminalId, String year);

    String getTerminalIdByCarno(String carno);
}
