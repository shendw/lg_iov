package com.yonyou.lonking.dto.display;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2020/12/4
 * @描述
 **/
@Data
public class CarInfoByProvinceDto {
    private String carno;
    private String location;
    private String workStatus;
    /**
     * 机型
     */
//    @Excel(name = "机型")
    private String machinemodel;
    /**
     * 服务代理商
     */
    private String serviceAgent;
    /**
     * 客户
     */
    private String customer;
}
