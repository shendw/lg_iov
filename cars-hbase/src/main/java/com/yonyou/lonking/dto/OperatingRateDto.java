package com.yonyou.lonking.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2020/10/26
 * @描述
 **/
@Data
public class OperatingRateDto {

    @JsonIgnore
    public static String[] monthArray = new String[]{"01","02","03","04","05","06","07","08","09","10","11","12"};

    private String terminalId;

    private String[] month;

    private int[] value = new int[12];
}
