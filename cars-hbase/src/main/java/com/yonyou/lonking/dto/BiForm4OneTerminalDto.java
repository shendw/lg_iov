package com.yonyou.lonking.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2020/10/16
 * @描述
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BiForm4OneTerminalDto {

    private String terminalId;

    private List<String> date;

    private List<String> value;
}
