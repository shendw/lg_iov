package com.yonyou.lonking.dto.display;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2020/12/3
 * @描述
 **/
@Data
public class ResultByConditionDto {
    private String name;

    private Double value;
}
