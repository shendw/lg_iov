package com.yonyou.lonking.dto.display;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * @author shendawei
 * @创建时间 2020/12/4
 * @描述
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AvgWorkTimeDto {
    private int[] date;
    private Map<String,double[]> data;
}
