package com.yonyou.lonking.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2020/10/26
 * @描述
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WorkTimeStatisticsDto {
    private String key;

    private Double value;
}
