package com.yonyou.lonking.dto.display;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author shendawei
 * @创建时间 2020/12/4
 * @描述
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarNumDto {
    private int num;
    private int onlineNum;
}
