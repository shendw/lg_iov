package com.yonyou.lonking.dto;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2020/10/22
 * @描述
 **/
@Data
public class WorkTimesDto {

    private String device;

    private String date;

    private String workTime;

    private String oilConsumption;

    private String[] workTimes = new String[120];

}
