package com.yonyou.lonking.web;

import com.github.pagehelper.PageInfo;
import com.yonyou.lonking.dto.BiForm4OneTerminalDto;
import com.yonyou.lonking.entity.WorkTimesEntity;
import com.yonyou.lonking.service.ITagDailyService;
import com.yonyou.lonking.service.IWorkTimesService;
import com.yonyou.lonking.utils.ResponseCode;
import com.yonyou.lonking.utils.ResponseData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shendawei
 * @创建时间 2020/10/16
 * @描述
 **/
@RestController
@RequestMapping("/tagDaily")
@Slf4j
public class OilLevelController {

    @Autowired
    private ITagDailyService iTagDailyService;

    @Autowired
    private IWorkTimesService iWorkTimesService;

    @RequestMapping("/getOilLevel")
    public ResponseData getOilLevel(String terminalId,String date,String carno){
        try {
            BiForm4OneTerminalDto oilLevelByDay = iTagDailyService.getOilLevelByDay(terminalId,carno, date);
            return ResponseData.result(ResponseCode.SUCCESS.getCode(),oilLevelByDay);
        }catch (Exception e){
            log.error(e.getMessage());
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @RequestMapping("/getMachineStart")
    public ResponseData getMachineStart(String terminalId,String carno, String startDate, String endDate,@RequestParam int pageNum,@RequestParam int pageSize){
        try {
            PageInfo<WorkTimesEntity> workTimes = iWorkTimesService.getWorkTimes(terminalId,carno, startDate, endDate, pageNum, pageSize);
            return ResponseData.result(ResponseCode.SUCCESS.getCode(),workTimes);
        }catch (Exception e){
            log.error(e.getMessage());
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @RequestMapping("/getWorktime")
    public ResponseData getWorktime(String terminalId,String carno,String date){
        try {
            BiForm4OneTerminalDto workTimeByDay = iTagDailyService.getworkTimeByDay(terminalId,carno, date);
            return ResponseData.result(ResponseCode.SUCCESS.getCode(),workTimeByDay);
        }catch (Exception e){
            log.error(e.getMessage());
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }
}
