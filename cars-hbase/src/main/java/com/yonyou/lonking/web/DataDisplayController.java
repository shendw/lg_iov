package com.yonyou.lonking.web;

import com.yonyou.lonking.dto.BiForm4AllTErminalDto;
import com.yonyou.lonking.dto.display.AvgWorkTimeDto;
import com.yonyou.lonking.dto.display.CarInfoByProvinceDto;
import com.yonyou.lonking.dto.display.CarNumDto;
import com.yonyou.lonking.dto.display.ResultByConditionDto;
import com.yonyou.lonking.service.IDataDisplayService;
import com.yonyou.lonking.utils.ResponseCode;
import com.yonyou.lonking.utils.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2020/12/3
 * @描述
 **/
@RestController
@RequestMapping("/dataDisplay")
public class DataDisplayController {

    @Autowired
    private IDataDisplayService iDataDisplayService;

    @RequestMapping("/getCarNumByProvince")
    public ResponseData getCarNumByProvince(){
        List<ResultByConditionDto> carNumByProvince = iDataDisplayService.getCarNumByProvince();
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),carNumByProvince);
    }

    @RequestMapping("/getCarNumOnlineByCondition")
    public ResponseData getCarNumOnlineByCondition(String condition){
        BiForm4AllTErminalDto carNumOnlineByCondition = iDataDisplayService.getCarNumOnlineByCondition(condition);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),carNumOnlineByCondition);
    }

    @RequestMapping("/getCarNumOnlineByCity")
    public ResponseData getCarNumOnlineByCity(@RequestParam String provinceCode){
        BiForm4AllTErminalDto carNumOnlineByCity = iDataDisplayService.getCarNumOnlineByCity(provinceCode);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),carNumOnlineByCity);
    }

    @RequestMapping("/getCarNumOnlineByDistrict")
    public ResponseData getCarNumOnlineByDistrict(@RequestParam String cityCode){
        BiForm4AllTErminalDto carNumOnlineByCity = iDataDisplayService.getCarNumOnlineByDistrict(cityCode);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),carNumOnlineByCity);
    }

    @RequestMapping("/getActiveRateByCondition")
    public ResponseData getActiveRateByCondition(String condition){
        BiForm4AllTErminalDto activeRateByCondition = iDataDisplayService.getActiveRateByCondition(condition);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),activeRateByCondition);
    }

    @RequestMapping("/getActiveRateByCity")
    public ResponseData getActiveRateByCity(@RequestParam String provinceCode){
        BiForm4AllTErminalDto activeRateByCity = iDataDisplayService.getActiveRateByCity(provinceCode);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),activeRateByCity);
    }

    @RequestMapping("/getActiveRateByDistrict")
    public ResponseData getActiveRateByDistrict(@RequestParam String districtCode){
        BiForm4AllTErminalDto activeRateByCity = iDataDisplayService.getActiveRateByDistrict(districtCode);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),activeRateByCity);
    }

    @RequestMapping("/getWorkTimeStatusticsByProvince")
    public ResponseData getWorkTimeStatusticsByProvince(){
        List<ResultByConditionDto> workTimeStatusticsByProvince = iDataDisplayService.getWorkTimeStatusticsByProvince();
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),workTimeStatusticsByProvince);
    }

    @RequestMapping("/getWorkingDaysByCondition")
    public ResponseData getWorkingDaysByCondition(String condition){
        BiForm4AllTErminalDto workingDaysByCondition = iDataDisplayService.getWorkingDaysByCondition(condition);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),workingDaysByCondition);
    }

    @RequestMapping("/getWorkingDaysByCity")
    public ResponseData getWorkingDaysByCity(String provinceCode){
        BiForm4AllTErminalDto workingDaysByCity = iDataDisplayService.getWorkingDaysByCity(provinceCode);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),workingDaysByCity);
    }

    @RequestMapping("/getWorkingDaysByDistrict")
    public ResponseData getWorkingDaysByDistrict(String provinceCode,String district){
        BiForm4AllTErminalDto workingDaysByCity = iDataDisplayService.getWorkingDaysByDistrict(provinceCode,district);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),workingDaysByCity);
    }

    @RequestMapping("/getAvgWorkTimeByCarmodel")
    public ResponseData getAvgWorkTimeByCarmodel(){
        AvgWorkTimeDto avgWorkTime = iDataDisplayService.getAvgWorkTime();
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),avgWorkTime);
    }

    @RequestMapping("/getAvgWorkTimeProvince")
    public ResponseData getAvgWorkTimeProvince(@RequestParam String provinceCode){
        AvgWorkTimeDto avgWorkTime = iDataDisplayService.getAvgWorkTimeProvince(provinceCode);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),avgWorkTime);
    }

    @RequestMapping("/getAvgWorkTimeCity")
    public ResponseData getAvgWorkTimeCity(@RequestParam String provinceCode,String city){
        AvgWorkTimeDto avgWorkTime = iDataDisplayService.getAvgWorkTimeCity(provinceCode,city);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),avgWorkTime);
    }

    @RequestMapping("/getCarNum")
    public ResponseData getCarNum(){
        CarNumDto carNum = iDataDisplayService.getCarNum();
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),carNum);
    }

    @RequestMapping("/getCarNumWithProvince")
    public ResponseData getCarNumWithProvince(@RequestParam String provinceCode){
        CarNumDto carNum = iDataDisplayService.getCarNumByProvince(provinceCode+"0000");
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),carNum);
    }

    @RequestMapping("/getCarNumWithCity")
    public ResponseData getCarNumWithCity(@RequestParam String provinceCode,String cityCode){
        CarNumDto carNum = iDataDisplayService.getCarNumByCity(provinceCode,cityCode);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),carNum);
    }

    @RequestMapping("/getCarWorkInfoByProvince")
    public ResponseData getCarWorkInfoByProvince(@RequestParam String provinceCode){
        List<CarInfoByProvinceDto> carWorkInfoByProvince = iDataDisplayService.getCarWorkInfoByProvince(provinceCode+"0000");
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),carWorkInfoByProvince);
    }

    @RequestMapping("/getCarWorkInfoByCity")
    public ResponseData getCarWorkInfoByCity(@RequestParam String provinceCode,String cityCode){
        List<CarInfoByProvinceDto> carWorkInfoByProvince = iDataDisplayService.getCarWorkInfoByCity(provinceCode,cityCode);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),carWorkInfoByProvince);
    }
}
