package com.yonyou.lonking.web;

import com.yonyou.lonking.dto.BiForm4AllTErminalDto;
import com.yonyou.lonking.dto.OperatingRateDto;
import com.yonyou.lonking.service.IWorkTimesService;
import com.yonyou.lonking.utils.ResponseCode;
import com.yonyou.lonking.utils.ResponseData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Pattern;

/**
 * @author shendawei
 * @创建时间 2020/10/26
 * @描述
 **/
@RestController
@RequestMapping("/WorkTimesStatistics")
@Slf4j
@Validated
public class WorkTimesStatisticsController {

    @Autowired
    private IWorkTimesService iWorkTimesService;

    @RequestMapping("/getByProvinceAndDay")
    public ResponseData getByProvinceAndDay(@RequestParam
            @Pattern(regexp = "^\\d{4}(\\-|\\/|.)\\d{1,2}\\1\\d{1,2}$",message = "日期格式不对") String day){
        try {
            BiForm4AllTErminalDto workTimesByProvinceAndDay = iWorkTimesService.getWorkTimesByProvinceAndDay(day);
            return ResponseData.result(ResponseCode.SUCCESS.getCode(),workTimesByProvinceAndDay);
        }catch (Exception e){
            log.error(e.getMessage());
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @RequestMapping("/getByProvinceAndMonth")
    public ResponseData getByProvinceAndMonth(
            @Pattern(regexp = "^\\d{4}(\\-|\\/|.)\\d{1,2}$") String month){
        try {
            BiForm4AllTErminalDto workTimesByProvinceAndMonth = iWorkTimesService.getWorkTimesByProvinceAndMonth(month);
            return ResponseData.result(ResponseCode.SUCCESS.getCode(),workTimesByProvinceAndMonth);
        }catch (Exception e){
            log.error(e.getMessage());
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @RequestMapping("/getByProvinceAndYear")
    public ResponseData getByProvinceAndYear(@Pattern(regexp = "^\\d{4}$") String year){
        try {
            BiForm4AllTErminalDto workTimesByProvinceAndYear = iWorkTimesService.getWorkTimesByProvinceAndYear(year);
            return ResponseData.result(ResponseCode.SUCCESS.getCode(),workTimesByProvinceAndYear);
        }catch (Exception e){
            log.error(e.getMessage());
            return ResponseData.result(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
        }
    }

    @RequestMapping("/getWorkTimeStatistics")
    public ResponseData getWorkTimeStatistics(@RequestParam String date,@RequestParam String func,@RequestParam String key){
        String dayRegex = "^\\d{4}(\\-|\\/|.)\\d{1,2}\\1\\d{1,2}$";
        String monthRegex = "^\\d{4}(\\-|\\/|.)\\d{1,2}$";
        String yearRegex = "^\\d{4}$";
        String dayField;
        if (date.matches(dayRegex)){
            dayField = "day";
        }else if (date.matches(monthRegex)){
            dayField = "month";
        }else if (date.matches(yearRegex)){
            dayField = "year";
        }else {
            return ResponseData.result(ResponseCode.ERROR.getCode(),"查询日期格式错误！");
        }
        BiForm4AllTErminalDto workTimeStatistics = iWorkTimesService.getWorkTimeStatistics(date, dayField, key,func);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),workTimeStatistics);
    }

    @RequestMapping("/getOperatingRateByMonth")
    public ResponseData getOperatingRateByMonth(String terminalId,String carno,@Pattern(regexp = "\\d{4}$") String year){
        OperatingRateDto operatingRateByMonth = iWorkTimesService.getOperatingRateByMonth(terminalId,carno,year);
        return ResponseData.result(ResponseCode.SUCCESS.getCode(),operatingRateByMonth);
    }
}
