package com.yonyou.lonking.enums;

import lombok.Getter;

/**
 * @author shendawei
 * @创建时间 2020/12/18
 * @描述
 **/
@Getter
public enum ProvinceEnum {
//    中国省份代码表
//    代码 名    称          代码     名    称
//11       北京市          43       湖南省
//12       天津市          44       广东省
//13       河北省          45       广西壮族自治区
//14       山西省          46       海南省
//15       内蒙古自治区 50       重庆市
//21       辽宁省          51       四川省
//22       吉林省          52       贵州省
//23       黑龙江省       53       云南省
//31       上海市          54       西藏自治区
//32       江苏省          61       陕西省
//33       浙江省          62       甘肃省
//34       安徽省          63       青海省
//35       福建省          64       宁夏回族自治区
//36       江西省          65       新疆维吾尔自治区
//37       山东省          71       台湾省
//41       河南省          81       香港特别行政区
//42       湖北省          82       澳门特别行政区

    beijing("北京市","11",true),
    tainjin("天津市","12",true),
    hebei("河北省","13",false),
    shanxi("山西省","14",false),
    neimenggu("内蒙古自治区","15",false),
    liaoning("辽宁省","21",false),
    jiling("吉林省","22",false),
    heilongjiang("黑龙江省","23",false),
    shanghaishi("上海市","31",true),
    jiangsu("江苏省","32",false),
    zhejiang("浙江省","33",false),
    anhui("安徽省","34",false),
    fujian("福建省","35",false),
    jiangxi("江西省","36",false),
    shandong("山东省","37",false),
    henan("河南省","41",false),
    hubei("湖北省","42",false),

    hunan("湖南省","43",false),
    guangdong("广东省","44",false),
    guangxi("广西壮族自治区","45",false),
    hainan("海南省","46",false),
    chongqing("重庆市","50",true),
    sichuan("四川省","51",false),
    guizhou("贵州省","52",false),
    yunnan("云南省","53",false),
    xizang("西藏自治区","54",false),
    shanxi1("陕西省","61",false),
    gansu("甘肃省","62",false),
    qinghai("青海省","63",false),
    ningxia("宁夏回族自治区","64",false),
    xinjiang("新疆维吾尔自治区","65",false),
    taiwan("台湾省","71",false),
    xianggang("香港特别行政区","81",false),
    aomen("澳门特别行政区","82",false),
    ;




    private final String name;
    private final String code;
    private final boolean isMunicipalities;

    ProvinceEnum(String name, String code,boolean isMunicipalities) {
        this.code = code;
        this.name = name;
        this.isMunicipalities = isMunicipalities;
    }

    //根据枚举类的 code 值，获取枚举类型；当枚举类与switch结合使用时，此方法是必须的
    public static ProvinceEnum getEnumByCode(String code) {
        for (ProvinceEnum deviceType : ProvinceEnum.values()) {
            if (deviceType.getCode().equals(code)) {
                return deviceType;
            }
        }
        return null;
    }
}
