package com.yonyou.lonking.enums;

import lombok.Getter;

/**
 * @author shendawei
 * @创建时间 2020/10/30
 * @描述
 **/
@Getter
public enum CarModelEnum {

    SMALL("106100","小挖"),
    MIDDLE("106200","中挖"),
    LARGE("106300","大挖");

    private final String code;
    private final String desc;

    CarModelEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static CarModelEnum getEnumByCode(String code) {
        for (CarModelEnum carModel : CarModelEnum.values()) {
            if (carModel.getCode().equals(code)) {
                return carModel;
            }
        }
        return null;
    }
}
