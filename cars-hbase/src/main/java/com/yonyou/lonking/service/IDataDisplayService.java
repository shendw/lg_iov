package com.yonyou.lonking.service;

import com.yonyou.lonking.dto.BiForm4AllTErminalDto;
import com.yonyou.lonking.dto.display.AvgWorkTimeDto;
import com.yonyou.lonking.dto.display.CarInfoByProvinceDto;
import com.yonyou.lonking.dto.display.CarNumDto;
import com.yonyou.lonking.dto.display.ResultByConditionDto;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2020/12/3
 * @描述
 **/
public interface IDataDisplayService {
    /**
     * 根据省份获取车辆
     * @return
     */
    List<ResultByConditionDto> getCarNumByProvince();

    /**
     * 在线车辆排行
     * @return
     */
    BiForm4AllTErminalDto getCarNumOnlineByCondition(String condition);

    /**
     * 省份在线车辆排行
     * @param provinceCode
     * @return
     */
    BiForm4AllTErminalDto getCarNumOnlineByCity(String provinceCode);

    /**
     * 市在线车辆排行
     * @param cityCode
     * @return
     */
    BiForm4AllTErminalDto getCarNumOnlineByDistrict(String cityCode);

    /**
     * 车辆活跃率排行
     * @return
     */
    BiForm4AllTErminalDto getActiveRateByCondition(String condition);

    /**
     * 省份车辆活跃率排行
     * @param provinceCode
     * @return
     */
    BiForm4AllTErminalDto getActiveRateByCity(String provinceCode);

    /**
     * 市车辆活跃率排行
     * @param districtCode
     * @return
     */
    BiForm4AllTErminalDto getActiveRateByDistrict(String districtCode);

    /**
     * 根据省份获取工作时长
     * @return
     */
    List<ResultByConditionDto> getWorkTimeStatusticsByProvince();

    /**
     * 开工天数排行
     * @param condition
     * @return
     */
    BiForm4AllTErminalDto getWorkingDaysByCondition(String condition);

    /**
     * 开工天数排行(某省份)
     * @param provinceCode
     * @return
     */
    BiForm4AllTErminalDto getWorkingDaysByCity(String provinceCode);

    /**
     * 开工天数排行(某市)
     * @param provinceCode
     * @return
     */
    BiForm4AllTErminalDto getWorkingDaysByDistrict(String provinceCode,String district);

    /**
     * 全国平均工时统计
     * @return
     */
    AvgWorkTimeDto getAvgWorkTime();

    /**
     * 省份平均工时统计
     * @return
     */
    AvgWorkTimeDto getAvgWorkTimeProvince(String provinceCode);

    /**
     * 市平均工时统计
     * @return
     */
    AvgWorkTimeDto getAvgWorkTimeCity(String provinceCode,String city);

    /**
     * 全国车辆数量
     * @return
     */
    CarNumDto getCarNum();

    CarNumDto getCarNumByProvince(String provinceCode);

    CarNumDto getCarNumByCity(String provinceCode,String cityCode);

    List<CarInfoByProvinceDto> getCarWorkInfoByProvince(String provinceCode);

    List<CarInfoByProvinceDto> getCarWorkInfoByCity(String provinceCode,String cityCode);
}
