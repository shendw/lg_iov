package com.yonyou.lonking.service;

import com.yonyou.lonking.dto.BiForm4OneTerminalDto;

import java.text.ParseException;

/**
 * @author shendawei
 * @创建时间 2020/10/16
 * @描述
 **/
public interface ITagDailyService {

    BiForm4OneTerminalDto getOilLevelByDay(String terminalId,String carno, String date) throws ParseException;

    BiForm4OneTerminalDto getworkTimeByDay(String terminalId,String carno, String date) throws ParseException;

    BiForm4OneTerminalDto getMachineStartByDay(String terminalId,String carno, String date) throws ParseException;
}
