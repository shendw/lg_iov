package com.yonyou.lonking.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yonyou.lonking.dao.oracle.WorkTimesMapper;
import com.yonyou.lonking.dao.phoenix.TerminalMapper;
import com.yonyou.lonking.dto.BiForm4AllTErminalDto;
import com.yonyou.lonking.dto.OperatingRateDto;
import com.yonyou.lonking.dto.WorkTimeStatisticsDto;
import com.yonyou.lonking.entity.TerminalInfo;
import com.yonyou.lonking.entity.WorkTimesEntity;
import com.yonyou.lonking.enums.CarModelEnum;
import com.yonyou.lonking.exception.BusinessException;
import com.yonyou.lonking.service.IWorkTimesService;
import com.yonyou.lonking.utils.ResponseCode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author shendawei
 * @创建时间 2020/10/22
 * @描述
 **/
@Service("iWorkTimesService")
public class WorkTimesServiceImpl implements IWorkTimesService {

    @Autowired
    private WorkTimesMapper workTimesMapper;

    @Autowired
    private TerminalMapper terminalMapper;

    @Override
    public PageInfo<WorkTimesEntity> getWorkTimes(String terminalId,String carno, String startDate, String endDate, int pageNum, int pageSize) throws ParseException {
        if (StringUtils.isBlank(terminalId) && StringUtils.isBlank(carno)){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        if (StringUtils.isBlank(terminalId) && StringUtils.isNotBlank(carno)){
            terminalId = workTimesMapper.getTerminalIdByCarno(carno);
            if (StringUtils.isBlank(terminalId)){
                throw new BusinessException(ResponseCode.TERMINAL_CAR_IS_NONE);
            }
        }
        PageHelper.startPage(pageNum,pageSize);
        List<WorkTimesEntity> workTimes = workTimesMapper.getWorkTimes(terminalId, startDate, endDate);
        PageInfo<WorkTimesEntity> workTimesEntityPageInfo = new PageInfo<>(workTimes);
        if (!CollectionUtils.isEmpty(workTimes)){
            for (WorkTimesEntity workTime : workTimes) {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                Date startdate = df.parse(workTime.getDate());
                Calendar cal = Calendar.getInstance();
                cal.setTime(startdate);
                long start = cal.getTimeInMillis();
                cal.add(Calendar.DATE, 1);
                long end = cal.getTimeInMillis();
                String startRow = terminalId+"_"+(Long.MAX_VALUE-end);
                String endRow = terminalId+"_"+(Long.MAX_VALUE-start);
                List<TerminalInfo> machineStartByDay = terminalMapper.getMachineStartByDay(startRow, endRow);
                if (CollectionUtils.isEmpty(machineStartByDay)){
                    continue;
                }
                boolean[] worktimes = new boolean[120];
                for (TerminalInfo terminalInfo : machineStartByDay) {
                    if ("true".equals(terminalInfo.getMachineStart())){
                        String timeStamp = terminalInfo.getOrigin();
                        if (timeStamp.matches("^[0-9]+$")){
                            long origin = Long.parseLong(timeStamp);
                            int index = (int)Math.floor((origin-start)/720000L);
                            if (index <120){
                                worktimes[index] = true;
                            }
                        }
                    }
                }
                workTime.setWorkTimes(worktimes);
            }
        }
        return workTimesEntityPageInfo;
    }

    @Override
    public BiForm4AllTErminalDto getWorkTimesByProvinceAndDay(String day) {
        return toBiForm4AllTErminalDto(workTimesMapper.getSumWorkTimesByProvinceAndDay(day));
    }

    @Override
    public BiForm4AllTErminalDto getWorkTimesByProvinceAndMonth(String month) {
        return toBiForm4AllTErminalDto(workTimesMapper.getSumWorkTimesByProvinceAndMonth(month));
    }

    @Override
    public BiForm4AllTErminalDto getWorkTimesByProvinceAndYear(String year) {
        return toBiForm4AllTErminalDto(workTimesMapper.getSumWorkTimesByProvinceAndYear(year));
    }

    @Override
    public BiForm4AllTErminalDto getWorkTimeStatistics(String date,String dateField, String selectField, String func) {
//        String newSelectFeield = preDateFiel(IovCardoc.class, selectField);
//        if (newSelectFeield == null){
//            preDateFiel(IovTerminalDaily.class, selectField);
//        }
        if (!("carmodel".equalsIgnoreCase(selectField) || "province".equalsIgnoreCase(selectField)
                || "service_agent".equalsIgnoreCase(selectField) || "machinemodel".equalsIgnoreCase(selectField))){
            throw new BusinessException(ResponseCode.ERROR_PARAM);
        }
        if (!"sum".equalsIgnoreCase(func) && !"avg".equalsIgnoreCase(func)){
            throw new BusinessException(ResponseCode.ERROR_PARAM);
        }
        List<WorkTimeStatisticsDto> workTimeStatistics = workTimesMapper.getWorkTimeStatistics(date, selectField, func, dateField);
        if ("carmodel".equalsIgnoreCase(selectField) && !CollectionUtils.isEmpty(workTimeStatistics)){
            workTimeStatistics.forEach(x -> {
                CarModelEnum enumByCode = CarModelEnum.getEnumByCode(x.getKey());
                if (enumByCode != null){
                    x.setKey(enumByCode.getDesc());
                }
            });
        }
        return toBiForm4AllTErminalDto(workTimeStatistics);
    }

//    private String preDateFiel(Class<?> clazz,String selectField){
//        TableInfo tableInfo = TableInfoHelper.getTableInfo(clazz);
//        if (tableInfo == null){
//            return null;
//        }
//        boolean flag  = false;
//        List<TableFieldInfo> fieldList = tableInfo.getFieldList();
//        for (TableFieldInfo tableFieldInfo : fieldList) {
//            if (selectField.equals(tableFieldInfo.getColumn())){
//                return tableInfo.getTableName()+"."+selectField;
//            }
//        }
//        return null;
//    }

    private BiForm4AllTErminalDto toBiForm4AllTErminalDto(List<WorkTimeStatisticsDto> data){
        BiForm4AllTErminalDto<String, Double> biForm4AllTErminalDto = new BiForm4AllTErminalDto<>();
        if (!CollectionUtils.isEmpty(data)){
//            String[] keyArray = new String[data.size()];
//            Double[] valueArray = new Double[data.size()];
            ArrayList<String> keyList = new ArrayList<>(data.size());
            ArrayList<Double> valueList = new ArrayList<>(data.size());
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).getKey() != null){
//                    keyArray[i] = data.get(i).getKey();
//                    valueArray[i] = data.get(i).getValue();
                    keyList.add(data.get(i).getKey());
                    valueList.add(data.get(i).getValue());
                }
            }
            biForm4AllTErminalDto.setKey(keyList);
            biForm4AllTErminalDto.setValue(valueList);
        }
        return biForm4AllTErminalDto;
    }




    @Override
    public OperatingRateDto getOperatingRateByMonth(String terminalId,String carno,String year) {
        if (StringUtils.isBlank(terminalId) && StringUtils.isBlank(carno)){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        if (StringUtils.isBlank(terminalId) && StringUtils.isNotBlank(carno)){
            terminalId = workTimesMapper.getTerminalIdByCarno(carno);
            if (StringUtils.isBlank(terminalId)){
                throw new BusinessException(ResponseCode.TERMINAL_CAR_IS_NONE);
            }
        }
        List<WorkTimeStatisticsDto> operatingRateByMonth = workTimesMapper.getOperatingRateByMonth(terminalId,year);
        OperatingRateDto operatingRateDto = new OperatingRateDto();
        List<String> monthList = new ArrayList<>(12);
        for(int i = 0;i < 12;i++){
            monthList.add(i,year+"-"+OperatingRateDto.monthArray[i]);
        }
        operatingRateDto.setMonth(monthList.toArray(new String[12]));
        operatingRateDto.setTerminalId(terminalId);
        int[] value = operatingRateDto.getValue();
        if (!CollectionUtils.isEmpty(operatingRateByMonth)){
            operatingRateByMonth.forEach(x -> {
                value[monthList.indexOf(x.getKey())] = x.getValue().intValue();

            });
        }
        return operatingRateDto;
    }
}
