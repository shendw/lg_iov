package com.yonyou.lonking.service;

import com.github.pagehelper.PageInfo;
import com.yonyou.lonking.dto.BiForm4AllTErminalDto;
import com.yonyou.lonking.dto.OperatingRateDto;
import com.yonyou.lonking.entity.WorkTimesEntity;

import java.text.ParseException;

/**
 * @author shendawei
 * @创建时间 2020/10/22
 * @描述
 **/
public interface IWorkTimesService {
    /**
     * 获取工作时间段
     * @param terminalId
     * @param startDate
     * @param endDate
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageInfo<WorkTimesEntity> getWorkTimes(String terminalId,String carno, String startDate, String endDate, int pageNum, int pageSize) throws ParseException;

    BiForm4AllTErminalDto getWorkTimesByProvinceAndDay(String day);
    BiForm4AllTErminalDto getWorkTimesByProvinceAndMonth(String month);
    BiForm4AllTErminalDto getWorkTimesByProvinceAndYear(String year);

    /**
     * 多维度查询工时
     * @param date 年月日
     * @param dateField 日期类型，year,month,day
     * @param selectField 统计的维度
     * @param func 数值计算的函数类型
     * @return
     */
    BiForm4AllTErminalDto getWorkTimeStatistics(String date,String dateField,String selectField,String func);

    OperatingRateDto getOperatingRateByMonth(String terminalId,String carno,String year);
}
