package com.yonyou.lonking.service.impl;

import com.yonyou.lonking.dao.oracle.WorkTimesMapper;
import com.yonyou.lonking.dao.phoenix.TerminalMapper;
import com.yonyou.lonking.dto.BiForm4OneTerminalDto;
import com.yonyou.lonking.entity.TerminalInfo;
import com.yonyou.lonking.exception.BusinessException;
import com.yonyou.lonking.service.ITagDailyService;
import com.yonyou.lonking.utils.ResponseCode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author shendawei
 * @创建时间 2020/10/16
 * @描述
 **/
@Service("iOilLevelService")
public class TagDailyServiceImpl implements ITagDailyService {

    @Autowired
    private TerminalMapper terminalMapper;

    @Autowired
    private WorkTimesMapper workTimesMapper;

    @Override
    public BiForm4OneTerminalDto getOilLevelByDay(String terminalId,String carno, String date) throws ParseException {
        if (StringUtils.isBlank(terminalId) && StringUtils.isEmpty(carno)){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        if (StringUtils.isBlank(terminalId) && StringUtils.isNotBlank(carno)){
            terminalId = workTimesMapper.getTerminalIdByCarno(carno);
            if (StringUtils.isBlank(terminalId)){
                throw new BusinessException(ResponseCode.TERMINAL_CAR_IS_NONE);
            }
        }
        String[] rowkey = initRowkey(terminalId, date);
        List<TerminalInfo> oilLevel = terminalMapper.getOilLevelByDay(rowkey[0], rowkey[1]);
        List<String> dateList = new ArrayList<>();
        List<String> valueList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(oilLevel)){
            for (int i = oilLevel.size() - 1; i >= 0; i--) {
                String oil = oilLevel.get(i).getOil();
                String formatDate = oilLevel.get(i).getFormatDate();
                if (StringUtils.isBlank(oil) || StringUtils.isBlank(formatDate)){
                    continue;
                }
                dateList.add(formatDate);
                valueList.add(oil);
            }
        }
        return new BiForm4OneTerminalDto(terminalId,dateList,valueList);
    }

    @Override
    public BiForm4OneTerminalDto getworkTimeByDay(String terminalId,String carno, String date) throws ParseException {
        if (StringUtils.isBlank(terminalId) && StringUtils.isEmpty(carno)){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        if (StringUtils.isBlank(terminalId) && StringUtils.isNotBlank(carno)){
            terminalId = workTimesMapper.getTerminalIdByCarno(carno);
            if (StringUtils.isBlank(terminalId)){
                throw new BusinessException(ResponseCode.TERMINAL_CAR_IS_NONE);
            }
        }
        String[] rowkey = initRowkey(terminalId, date);
        List<TerminalInfo> workTimeByDay = terminalMapper.getWorkTimeByDay(rowkey[0], rowkey[1]);
        List<String> dateList = new ArrayList<>();
        List<String> valueList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(workTimeByDay)){
            for (int i = workTimeByDay.size() - 1; i >= 0; i--) {
                String worktime = workTimeByDay.get(i).getWorkTime();
                String formatDate = workTimeByDay.get(i).getFormatDate();
                if (StringUtils.isBlank(worktime) || StringUtils.isBlank(formatDate)){
                    continue;
                }
                dateList.add(formatDate);
                valueList.add(worktime);
            }
        }
        return new BiForm4OneTerminalDto(terminalId,dateList,valueList);
    }

    @Override
    public BiForm4OneTerminalDto getMachineStartByDay(String terminalId,String carno, String date) throws ParseException {
        if (StringUtils.isBlank(terminalId) && StringUtils.isEmpty(carno)){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        if (StringUtils.isBlank(terminalId) && StringUtils.isNotBlank(carno)){
            terminalId = workTimesMapper.getTerminalIdByCarno(carno);
            if (StringUtils.isBlank(terminalId)){
                throw new BusinessException(ResponseCode.TERMINAL_CAR_IS_NONE);
            }
        }
        String[] rowkey = initRowkey(terminalId, date);
        List<TerminalInfo> machineStartByDay = terminalMapper.getMachineStartByDay(rowkey[0], rowkey[1]);
        List<String> dateList = new ArrayList<>();
        List<String> valueList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(machineStartByDay)){
            for (int i = machineStartByDay.size() - 1; i >= 0; i--) {
                String machineStart = machineStartByDay.get(i).getMachineStart();
                String formatDate = machineStartByDay.get(i).getFormatDate();
                if (StringUtils.isBlank(machineStart) || StringUtils.isBlank(formatDate)){
                    continue;
                }
                dateList.add(formatDate);
                valueList.add(machineStart);
            }
        }
        return new BiForm4OneTerminalDto(terminalId,dateList,valueList);
    }

    private String[] initRowkey(String terminalId, String date) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date startdate = df.parse(date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(startdate);
        long start = cal.getTimeInMillis();
        cal.add(Calendar.DATE, 1);
        long end = cal.getTimeInMillis();
        String startRow = terminalId+"_"+(Long.MAX_VALUE-end);
        String endRow = terminalId+"_"+(Long.MAX_VALUE-start);
        return new String[]{startRow,endRow};
    }

}
