package com.yonyou.lonking.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yonyou.lonking.dao.oracle.IovTerminalDailyMapper;
import com.yonyou.lonking.dao.oracle.IovTerminalInfoMapper;
import com.yonyou.lonking.dto.BiForm4AllTErminalDto;
import com.yonyou.lonking.dto.display.AvgWorkTimeDto;
import com.yonyou.lonking.dto.display.CarInfoByProvinceDto;
import com.yonyou.lonking.dto.display.CarNumDto;
import com.yonyou.lonking.dto.display.ResultByConditionDto;
import com.yonyou.lonking.entity.IovTerminalInfo;
import com.yonyou.lonking.entity.TerminalInfo;
import com.yonyou.lonking.enums.ProvinceEnum;
import com.yonyou.lonking.exception.BusinessException;
import com.yonyou.lonking.service.IDataDisplayService;
import com.yonyou.lonking.utils.ResponseCode;
import com.yonyou.lonking.websocket.WebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author shendawei
 * @创建时间 2020/12/3
 * @描述
 **/
@Service("iDataDisplayService")
public class DataDisplayServiceImpl implements IDataDisplayService {

    @Autowired(required = false)
    private IovTerminalInfoMapper iovTerminalInfoMapper;

    @Autowired(required = false)
    private IovTerminalDailyMapper iovTerminalDailyMapper;

    @Override
    public List<ResultByConditionDto> getCarNumByProvince() {
        return iovTerminalInfoMapper.getCarNumByProvince();
    }

    @Override
    public BiForm4AllTErminalDto getCarNumOnlineByCondition(String condition) {
        if ("province".equals(condition)){
            return toBiForm4AllTErminalDto(iovTerminalInfoMapper.getCarNumOnlineByProvince());
        }else if ("agent".equals(condition)){
            return toBiForm4AllTErminalDto(iovTerminalInfoMapper.getCarNumOfflineByAgent());
        }
        return new BiForm4AllTErminalDto<>();
    }

    @Override
    public BiForm4AllTErminalDto getCarNumOnlineByCity(String provinceCode) {
        ProvinceEnum enumByCode = ProvinceEnum.getEnumByCode(provinceCode);
        if (enumByCode == null){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        provinceCode = provinceCode + "0000";
        if (enumByCode.isMunicipalities()){
            return toBiForm4AllTErminalDto(iovTerminalInfoMapper.getCarNumOnlineByDistrict(provinceCode));
        }else {
            return toBiForm4AllTErminalDto(iovTerminalInfoMapper.getCarNumOnlineByCity(provinceCode));
        }
    }

    @Override
    @SuppressWarnings("all")
    public BiForm4AllTErminalDto getCarNumOnlineByDistrict(String cityCode) {
        ProvinceEnum enumByCode = ProvinceEnum.getEnumByCode(cityCode.substring(0,2));
        if (enumByCode == null){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        if (enumByCode.isMunicipalities()){
            return toBiForm4AllTErminalDto(iovTerminalInfoMapper.getCarNumOnlineByTown(cityCode));
        }else {
            return toBiForm4AllTErminalDto(iovTerminalInfoMapper.getCarNumOnlineByDistricts(cityCode));
        }
    }

    @Override
    public BiForm4AllTErminalDto getActiveRateByCondition(String condition) {
        List<ResultByConditionDto> resultByConditionDtos = new ArrayList<>();
        if ("province".equals(condition)){
            List<ResultByConditionDto> carNumByProvince = iovTerminalInfoMapper.getCarNumByProvince();
            List<ResultByConditionDto> carNumOnlineByProvince = iovTerminalInfoMapper.getCarNumOnlineByProvince();
            carNumByProvince.forEach(x ->{
                carNumOnlineByProvince.forEach(y -> {
                    if(x.getName().equals(y.getName())){
                        x.setValue(new BigDecimal(y.getValue()/x.getValue()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                        resultByConditionDtos.add(x);
                        return;
                    }
                });
            });
        }else if ("agent".equals(condition)){
            List<ResultByConditionDto> carNumByAgent = iovTerminalInfoMapper.getCarNumByAgent();
            List<ResultByConditionDto> carNumOnlineByAgent = iovTerminalInfoMapper.getCarNumOnlineByAgent();
            carNumByAgent.forEach(x ->{
                carNumOnlineByAgent.forEach(y -> {
                    if(x.getName().equals(y.getName())){
                        x.setValue(new BigDecimal(y.getValue()/x.getValue()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                        resultByConditionDtos.add(x);
                        return;
                    }
                });
            });
        }else {
            return new BiForm4AllTErminalDto<>();
        }
        resultByConditionDtos.sort(Comparator.comparing(ResultByConditionDto::getValue).reversed());
        return toBiForm4AllTErminalDto(resultByConditionDtos);
    }

    @Override
    public BiForm4AllTErminalDto getActiveRateByCity(String provinceCode) {
        ProvinceEnum enumByCode = ProvinceEnum.getEnumByCode(provinceCode);
        if (enumByCode == null){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        provinceCode = provinceCode + "0000";
        List<ResultByConditionDto> resultByConditionDtos = new ArrayList<>();
        if (enumByCode.isMunicipalities()){
            List<ResultByConditionDto> carNumByCity = iovTerminalInfoMapper.getCarNumByDistrict(provinceCode);
            List<ResultByConditionDto> carNumOnlineByCity = iovTerminalInfoMapper.getCarNumOnlineByDistrict(provinceCode);
            carNumByCity.forEach(x ->{
                carNumOnlineByCity.forEach(y -> {
                    if(x.getName().equals(y.getName())){
                        x.setValue(new BigDecimal(y.getValue()/x.getValue()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                        resultByConditionDtos.add(x);
                        return;
                    }
                });
            });
        }else {
            List<ResultByConditionDto> carNumByCity = iovTerminalInfoMapper.getCarNumByCity(provinceCode);
            List<ResultByConditionDto> carNumOnlineByCity = iovTerminalInfoMapper.getCarNumOnlineByCity(provinceCode);
            carNumByCity.forEach(x ->{
                carNumOnlineByCity.forEach(y -> {
                    if(x.getName().equals(y.getName())){
                        x.setValue(new BigDecimal(y.getValue()/x.getValue()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                        resultByConditionDtos.add(x);
                        return;
                    }
                });
            });
        }
        resultByConditionDtos.sort(Comparator.comparing(ResultByConditionDto::getValue).reversed());
        return toBiForm4AllTErminalDto(resultByConditionDtos);
    }

    @Override
    @SuppressWarnings("all")
    public BiForm4AllTErminalDto getActiveRateByDistrict(String districtCode) {
        ProvinceEnum enumByCode = ProvinceEnum.getEnumByCode(districtCode.substring(0,2));
        if (enumByCode == null){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        List<ResultByConditionDto> resultByConditionDtos = new ArrayList<>();
        if (enumByCode.isMunicipalities()){
            List<ResultByConditionDto> carNumByDistrict = iovTerminalInfoMapper.getCarNumByDistrictCode(districtCode);
            List<ResultByConditionDto> carNumOnlineByDistrict = iovTerminalInfoMapper.getCarNumOnlineByDistrictCode(districtCode);
            carNumByDistrict.forEach(x ->{
                carNumOnlineByDistrict.forEach(y -> {
                    if(x.getName().equals(y.getName())){
                        x.setValue(new BigDecimal(y.getValue()/x.getValue()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                        resultByConditionDtos.add(x);
                        return;
                    }
                });
            });
        }else {
            List<ResultByConditionDto> carNumByDistrict = iovTerminalInfoMapper.getCarNumByDistricts(districtCode);
            List<ResultByConditionDto> carNumOnlineByDistrict = iovTerminalInfoMapper.getCarNumOnlineByDistricts(districtCode);
            carNumByDistrict.forEach(x ->{
                carNumOnlineByDistrict.forEach(y -> {
                    if(x.getName().equals(y.getName())){
                        x.setValue(new BigDecimal(y.getValue()/x.getValue()).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
                        resultByConditionDtos.add(x);
                        return;
                    }
                });
            });
        }
        resultByConditionDtos.sort(Comparator.comparing(ResultByConditionDto::getValue).reversed());
        return toBiForm4AllTErminalDto(resultByConditionDtos);
    }

    @Override
    public List<ResultByConditionDto> getWorkTimeStatusticsByProvince() {
        Calendar cal=Calendar.getInstance();
        cal.add(Calendar.DATE,-1);
        Date d=cal.getTime();
        SimpleDateFormat sp=new SimpleDateFormat("yyyy-MM-dd");
        String day=sp.format(d);
        return iovTerminalDailyMapper.getWorkTimeStatusticsByProvince(day);
    }


    @Override
    public BiForm4AllTErminalDto getWorkingDaysByCondition(String condition) {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        cal.add(Calendar.MONTH,-1);
        int month = cal.get(Calendar.MONTH )+1;
        String monthStr = year+"-"+(month<10 ? "0" + month : month);
        if ("province".equals(condition)){
            return toBiForm4AllTErminalDto(iovTerminalDailyMapper.getWorkingDaysByProvince(monthStr));
        }else if ("agent".equals(condition)){
            return toBiForm4AllTErminalDto(iovTerminalDailyMapper.getWorkingDaysByAgent(monthStr));
        }
        return new BiForm4AllTErminalDto<>();
    }

    @Override
    public BiForm4AllTErminalDto getWorkingDaysByCity(String provinceCode) {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        cal.add(Calendar.MONTH,-1);
        int month = cal.get(Calendar.MONTH )+1;
        String monthStr = year+"-"+(month<10 ? "0" + month : month);
        ProvinceEnum enumByCode = ProvinceEnum.getEnumByCode(provinceCode);
        if (enumByCode == null){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        if (enumByCode.isMunicipalities()){
            return toBiForm4AllTErminalDto(iovTerminalDailyMapper.getWorkingDaysByDistrict(monthStr,enumByCode.getName()));
        }else {
            return toBiForm4AllTErminalDto(iovTerminalDailyMapper.getWorkingDaysByCity(monthStr,enumByCode.getName()));
        }
    }

    @Override
    @SuppressWarnings("all")
    public BiForm4AllTErminalDto getWorkingDaysByDistrict(String provinceCode,String district) {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        cal.add(Calendar.MONTH,-1);
        int month = cal.get(Calendar.MONTH )+1;
        String monthStr = year+"-"+(month<10 ? "0" + month : month);
        ProvinceEnum enumByCode = ProvinceEnum.getEnumByCode(provinceCode);
        if (enumByCode == null){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        if (enumByCode.isMunicipalities()){
            return toBiForm4AllTErminalDto(iovTerminalDailyMapper.getWorkingDaysByTown(monthStr,district));
        }else {
            return toBiForm4AllTErminalDto(iovTerminalDailyMapper.getWorkingDaysByDistricts(monthStr,district));
        }
    }

    private BiForm4AllTErminalDto toBiForm4AllTErminalDto(List<ResultByConditionDto> data){
        BiForm4AllTErminalDto<String, Double> biForm4AllTErminalDto = new BiForm4AllTErminalDto<>();
        if (!CollectionUtils.isEmpty(data)){
            ArrayList<String> keyList = new ArrayList<>(data.size());
            ArrayList<Double> valueList = new ArrayList<>(data.size());
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).getName() != null){
                    keyList.add(data.get(i).getName());
                    valueList.add(data.get(i).getValue());
                }
            }
            biForm4AllTErminalDto.setKey(keyList);
            biForm4AllTErminalDto.setValue(valueList);
        }
        return biForm4AllTErminalDto;
    }

    @Override
    public AvgWorkTimeDto getAvgWorkTime() {
        Calendar cal=Calendar.getInstance();
        SimpleDateFormat sp=new SimpleDateFormat("yyyy-MM-dd");
        int[] date = new int[10];
        HashMap<String, double[]> data = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            cal.add(Calendar.DATE,-1);
            date[date.length-1-i] = cal.get(Calendar.DATE);
            Date d=cal.getTime();
            String day=sp.format(d);
            List<ResultByConditionDto> avgWorkTimeByCarmodel = iovTerminalDailyMapper.getAvgWorkTimeByCarmodel(day);
            if (!CollectionUtils.isEmpty(avgWorkTimeByCarmodel)){
                for (ResultByConditionDto resultByConditionDto : avgWorkTimeByCarmodel) {
                    String name = resultByConditionDto.getName();
                    double[] valueArray = data.get(name);
                    if (valueArray == null ){
                        double[] newValueArray = new double[10];
                        newValueArray[newValueArray.length-1-i] = resultByConditionDto.getValue();
                        data.put(name,newValueArray);
                    }else {
                        valueArray[valueArray.length-1-i] = resultByConditionDto.getValue();
                    }
                }
            }
        }
        data.put("小挖",data.remove("106100"));
        data.put("中挖",data.remove("106200"));
        data.put("大挖",data.remove("106300"));
        return new AvgWorkTimeDto(date,data);
    }

    @Override
    public AvgWorkTimeDto getAvgWorkTimeProvince(String provinceCode) {
        ProvinceEnum enumByCode = ProvinceEnum.getEnumByCode(provinceCode);
        if (enumByCode == null){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        Calendar cal=Calendar.getInstance();
        SimpleDateFormat sp=new SimpleDateFormat("yyyy-MM-dd");
        int[] date = new int[10];
        HashMap<String, double[]> data = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            cal.add(Calendar.DATE,-1);
            date[date.length-1-i] = cal.get(Calendar.DATE);
            Date d=cal.getTime();
            String day=sp.format(d);
            List<ResultByConditionDto> avgWorkTimeByCarmodel = iovTerminalDailyMapper.getAvgWorkTimeByCarmodelAndProvince(day,enumByCode.getName());
            if (!CollectionUtils.isEmpty(avgWorkTimeByCarmodel)){
                for (ResultByConditionDto resultByConditionDto : avgWorkTimeByCarmodel) {
                    String name = resultByConditionDto.getName();
                    double[] valueArray = data.get(name);
                    if (valueArray == null ){
                        double[] newValueArray = new double[10];
                        newValueArray[newValueArray.length-1-i] = resultByConditionDto.getValue();
                        data.put(name,newValueArray);
                    }else {
                        valueArray[valueArray.length-1-i] = resultByConditionDto.getValue();
                    }
                }
            }
        }
        data.put("小挖",data.remove("106100"));
        data.put("中挖",data.remove("106200"));
        data.put("大挖",data.remove("106300"));
        return new AvgWorkTimeDto(date,data);
    }

    @Override
    @SuppressWarnings("all")
    public AvgWorkTimeDto getAvgWorkTimeCity(String provinceCode,String city) {
        ProvinceEnum enumByCode = ProvinceEnum.getEnumByCode(provinceCode);
        if (enumByCode == null){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        boolean flag = false;
        if(enumByCode.isMunicipalities()){
            flag = true;
        }
        Calendar cal=Calendar.getInstance();
        SimpleDateFormat sp=new SimpleDateFormat("yyyy-MM-dd");
        int[] date = new int[10];
        HashMap<String, double[]> data = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            cal.add(Calendar.DATE,-1);
            date[date.length-1-i] = cal.get(Calendar.DATE);
            Date d=cal.getTime();
            String day=sp.format(d);
            List<ResultByConditionDto> avgWorkTimeByCarmodel = iovTerminalDailyMapper.getAvgWorkTimeByCarmodelAndCity(day,city,flag ? 1 : 2);
            if (!CollectionUtils.isEmpty(avgWorkTimeByCarmodel)){
                for (ResultByConditionDto resultByConditionDto : avgWorkTimeByCarmodel) {
                    String name = resultByConditionDto.getName();
                    double[] valueArray = data.get(name);
                    if (valueArray == null ){
                        double[] newValueArray = new double[10];
                        newValueArray[newValueArray.length-1-i] = resultByConditionDto.getValue();
                        data.put(name,newValueArray);
                    }else {
                        valueArray[valueArray.length-1-i] = resultByConditionDto.getValue();
                    }
                }
            }
        }
        data.put("小挖",data.remove("106100"));
        data.put("中挖",data.remove("106200"));
        data.put("大挖",data.remove("106300"));
        return new AvgWorkTimeDto(date,data);
    }

    @Override
    public CarNumDto getCarNum() {
        int num = iovTerminalInfoMapper.getCarNumByWorkStatus(null);
        int offlineNum = iovTerminalInfoMapper.getCarNumByWorkStatus("1");
        return new CarNumDto(num,num-offlineNum);
    }

    @Override
    public CarNumDto getCarNumByProvince(String provinceCode) {
        int num = iovTerminalInfoMapper.getCarNumByWorkStatusAndProvince(null, provinceCode);
        int offlineNum = iovTerminalInfoMapper.getCarNumByWorkStatusAndProvince("1", provinceCode);
        return new CarNumDto(num,num-offlineNum);
    }

    @Override
    public CarNumDto getCarNumByCity(String provinceCode,String cityCode) {
        ProvinceEnum enumByCode = ProvinceEnum.getEnumByCode(provinceCode);
        if (enumByCode == null){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        boolean flag = false;
        if(enumByCode.isMunicipalities()){
            flag = true;
        }
        int num = iovTerminalInfoMapper.getCarNumByWorkStatusAndCity(null, cityCode,flag?  1:2);
        int offlineNum = iovTerminalInfoMapper.getCarNumByWorkStatusAndCity("1", cityCode,flag?  1:2);
        return new CarNumDto(num,num-offlineNum);
    }

    @Override
    public List<CarInfoByProvinceDto> getCarWorkInfoByProvince(String provinceCode) {
        List<CarInfoByProvinceDto> carWorkInfoByProvince = iovTerminalInfoMapper.getCarWorkInfoByProvince(provinceCode);
        return carWorkInfoByProvince;
    }

    @Override
    public List<CarInfoByProvinceDto> getCarWorkInfoByCity(String provinceCode,String cityCode) {
        ProvinceEnum enumByCode = ProvinceEnum.getEnumByCode(provinceCode);
        if (enumByCode == null){
            throw new BusinessException(ResponseCode.ERROR_PARAM_IS_NULL);
        }
        boolean flag = false;
        if(enumByCode.isMunicipalities()){
            flag = true;
        }
        List<CarInfoByProvinceDto> carWorkInfoByProvince = iovTerminalInfoMapper.getCarWorkInfoByCity(cityCode,flag?  1:2);
        return carWorkInfoByProvince;
    }
}
