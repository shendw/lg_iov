package com.yonyou.lonking.message.receiver;

import com.yonyou.lonking.entity.IovTerminalInfo;
import com.yonyou.lonking.message.sink.OnlineMessageSink;
import com.yonyou.lonking.utils.DataTimeUtil;
import com.yonyou.lonking.websocket.WebSocketServer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.MessageHeaders;

import java.io.IOException;
import java.util.Date;

/**
 * @author shendawei
 * @创建时间 2020/12/7
 * @描述
 **/
@EnableBinding(OnlineMessageSink.class)
@Slf4j
public class OnlineMessageReceiver {

    @Autowired
    private WebSocketServer webSocketServer;

    @StreamListener(OnlineMessageSink.INPUT)
    public void receive(IovTerminalInfo playload, MessageHeaders messageHeaders) {
        String carno = playload.getMachineInformation();
        if (StringUtils.isNotBlank(carno)){
            try {
                carno = carno.replaceAll("\\\\u003e", "");
                carno = carno.replaceAll("\\\\u003c", "");
                String message = "整机编号：>%s< 上线，最新通讯时间:%s";
                WebSocketServer.BroadCastInfo(String.format(message,carno, DataTimeUtil.dateToStr(new Date())));
            } catch (IOException e) {
                log.error(e.getLocalizedMessage());
                e.printStackTrace();
            }
        }

    }
}
