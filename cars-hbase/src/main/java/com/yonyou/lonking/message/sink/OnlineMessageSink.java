package com.yonyou.lonking.message.sink;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author shendawei
 * @创建时间 2020/12/7
 * @描述
 **/
public interface OnlineMessageSink {

    String INPUT = "input-from-online-message";

    /**
     * 消息的输入通道。
     *
     * @return 消息的输入通道
     */
    @Input(OnlineMessageSink.INPUT)
    SubscribableChannel input();
}
