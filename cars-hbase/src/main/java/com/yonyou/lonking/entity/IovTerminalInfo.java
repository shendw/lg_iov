package com.yonyou.lonking.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author shendawei
 * @创建时间 2020/11/27
 * @描述
 **/
@Data
@Accessors(chain = true)
@TableName("IOV_TERMINAL_INFO")
public class IovTerminalInfo {

    @TableId("ID")
    private String id;

    @TableField( "CAR_INFORMATION")
    private String carno;

    @TableField( "CAR_TYPE")
    private String carType;

    //"整车制造商"
    @TableField("CAR_MANUFACTURER")
    private String carManufacturer;

    //"GPS终端版本"
    @TableField("GPS_VERSION")
    private String gpsVersion;

    //GPS终端制造商
    @TableField("GPS_MANUFACTURER")
    private String gpsManufacturer;

    //数采时钟间隔
    @TableField("ACQUISITION_INTERVAL")
    private String acquisitionInterval;

    //实时时钟
    @TableField("REAL_TIME")
    private String realTime;

    //GPS监控状态
    @TableField("GPS_MONITOR_STATUS")
    private String gpsMonitorStatus;

    //GPS天线故障
    @TableField("GPS_ANTENNA_ALARM")
    private String gpsAntennaFailureAlarm;

    //GPRS天线故障
    @TableField("GPRS_ANTENNA_ALARM")
    private String gprsAntennaFailureAlarm;

    //机器电压报警
    @TableField("MACHINE_VOLTAGE_ALARM")
    private String machineVoltageFaultAlarm;

    //RCM电池电压报警
    @TableField("RCM_BATTERY_VOLTAGE_ALARM")
    private String rcmBatteryVoltageFaultAlarm;

    //CAN通讯中断报警
    @TableField("COMMUNICATION_ALARM")
    private String communicationAlarm;

    //机器越界报警
    @TableField("MACHINE_OUT_OF_RANGE_ALARM")
    private String machineOutOfRangeAlarm;

    //机器启动
    @TableField("MACHINE_START")
    private String machineStart;

    //机器停机
    @TableField("MACHINE_SHUTDOWN")
    private String machineShutdown;

    //机器通电
    @TableField("MACHINE_ELECTRIFY")
    private String machineElectrify;

    //SMS启用
    @TableField("SMS_ENABLE")
    private String smsEnable;

    //GPS时间
    @TableField("GPS_TIME")
    private String gpsTime;

    //GPS定位状态
    @TableField("GPS_POSITION_STATUS")
    private String gpsPositionStatus;

    //经纬度
    @TableField("COORDINATES")
    private String coordinates;

    //地面速度
    @TableField("GROUND_SPEED")
	private String groundSpeed;

    //地面航向
    @TableField("GROUND_HEADING")
	private String groundHeading;

    //磁偏角
    @TableField("MAGNETIC_DECLINATION")
	private String magneticDeclination;

    //磁偏角方向
    @TableField("DECLINATION_DIRECTION")
	private String declinationDirection;

    //卫星个数
    @TableField("NUMBER_OF_SATELLITES")
	private String numberOfSatellites;

    //海拔高度
    @TableField("ALTITUDE")
	private String altitude;

    //GSM信号强度
    @TableField("GSM_SIGNAL_STRENGTH")
	private String gsmSignalStrength;

    //机器电压
    @TableField("MACHINE_VOLTAGE")
	private String machineVoltage;


    //工作小时 发动机运行时间
    @TableField("ENGINE_RUNNING_DURATION")
	private String engineRunningDuration;

    //发动机水温
    @TableField("ENGINE_WATER_TEMPERATURE")
	private String engineWaterTemperature;

    //液压油温
    @TableField("HYDRAULIC_OIL_TEMPERATURE")
	private String hydraulicOilTemperature;

    //机油压力
    @TableField("OIL_PRESSURE")
	private String oilPressure;

    //燃油油位
    @TableField("FUEL_LEVEL")
	private String fuelLevel;

    //发动机转速
    @TableField("ENGINE_SPEED")
	private String engineSpeed;

    //油门档位
    @TableField("THROTTLE_GEAR")
	private String throttleGear;

    //机器工作模式
    @TableField("MACHINE_OPERATION_MODE")
	private String machineOperationMode;

    //机器信息
    @TableField("MACHINE_INFORMATION")
	private String machineInformation;

    //机器时钟时间
    @TableField("MACHINE_CLOCK_TIME")
	private String machineClockTime;

    //RCM电池电压
    @TableField("RCM_BATTERY_VOLTAGE")
	private String rcmBatteryVoltage;

    //RCM时钟时间
    @TableField("RCM_CLOCK_TIME")
	private String rcmClockTime;

    //日流量
    @TableField("DAILY_DISCHARGE")
	private String dailyDischarge;

    //左上角经度或圆心经度
    @TableField("CENTER_LONGITUDE")
	private String centerLongitude;

    //左上角纬度或圆心纬度
    @TableField("CENTER_LATITUDE")
	private String centerLatitude;

    //右下角经度或半径
    @TableField("RADIUS")
	private String radius;

    //右下角纬度或识别常数
    @TableField("RECOGNITION_CONSTANT")
	private String recognitionConstant;

    //最新登录时间
    @TableField("LATEST_LANDING_TIME")
	private String latestLandingTime;

    //最后时间
    @TableField("LAST_TIME")
	private String lastTime;

    //通讯方式
    @TableField("COMMUNICATION_MODE")
	private String communicationMode;

    //通讯状态
    @TableField("COMMUNICATION_STATE")
	private String communicationState;

    //锁车状态
    @TableField("LOCKED_STATE")
	private String lockedState;

    @TableField( "LOCATION")
    private String location;

    @TableField( "PROVINCE")
    private String province;

    @TableField( "PROVINCE_CODE")
    private String provinceCode;

    @TableField( "CITY")
    private String city;

    @TableField( "CITY_CODE")
    private String cityCode;

    @TableField( "DISTRICT")
    private String district;

    @TableField( "DISTRICT_CODE")
    private String districtCode;

    @TableField( "TOWN")
    private String town;

    @TableField( "TOWN_CODE")
    private String townCode;

    @TableField( "WORKSTATUS")
    private String workStatus;
}
