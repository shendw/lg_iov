package com.yonyou.lonking.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author shendw
 * @since 2020-10-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("IOV_CARDOC")
public class IovCardoc implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 车辆档案主键
     */
    @TableId("PK_CARDOC")
    private String pkCardoc;

    /**
     * 车型
     */
    @TableField("CARMODEL")
    private String carmodel;

    /**
     * 机型
     */
    @TableField("MACHINEMODEL")
    private String machinemodel;

    /**
     * 整车编号
     */
    @TableField("CARNO")
    private String carno;

    /**
     * 服务代理商
     */
    @TableField("SERVICE_AGENT")
    private String serviceAgent;

    /**
     * 销售代理商
     */
    @TableField("SALE_AGENT")
    private String saleAgent;

    /**
     * 客户
     */
    @TableField("CUSTOMER")
    private String customer;

    /**
     * 流水号
     */
    @TableField("SERIAL")
    private String serial;

    /**
     * 流程状态
     */
    @TableField("STATUS")
    private Long status;

    /**
     * 入库日期
     */
    @TableField("INSTOCKTIME")
    private String instocktime;

    /**
     * 出厂日期
     */
    @TableField("OUTSTOCKTIME")
    private String outstocktime;

    /**
     * 销售日期
     */
    @TableField("SALETIME")
    private String saletime;

    /**
     * 销售公司
     */
    @TableField("SALECOMPANY")
    private String salecompany;

    /**
     * 最终客户
     */
    @TableField("FINALCUSTOMER")
    private String finalcustomer;

    /**
     * 最新通讯时间
     */
    @TableField("LASTTIME")
    private String lasttime;

    /**
     * 工作时长
     */
    @TableField("WORKTIME")
    private String worktime;

    /**
     * 地理位置
     */
    @TableField("LOCATION")
    private String location;

    @TableField("CREATIONTIME")
    private String creationtime;

    @TableField("CREATOR")
    private String creator;

    @TableField("MODIFIEDTIME")
    private String modifiedtime;

    @TableField("MODIFIER")
    private String modifier;

    @TableField("DR")
    private Long dr;

    @TableField("VERSION")
    private Long version;

    @TableField("PK_ORG")
    private String pkOrg;

    /**
     * 备注
     */
    @TableField("REMARK")
    private String remark;

    /**
     * 工作状态
     */
    @TableField("WORKSTATUS")
    private String workstatus;

    /**
     * 锁车状态
     */
    @TableField("LOCKSTATUS")
    private String lockstatus;

    /**
     * 下线人
     */
    @TableField("OUTLINEER")
    private String outlineer;

    /**
     * 下线时间
     */
    @TableField("OUTLINETIME")
    private String outlinetime;

    /**
     * 调试人
     */
    @TableField("DEBUGER")
    private String debuger;

    /**
     * 调试时间
     */
    @TableField("DEBUGTIME")
    private String debugtime;

    /**
     * 自动绑定时间
     */
    @TableField("AUTOBINDTIME")
    private String autobindtime;


}
