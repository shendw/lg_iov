package com.yonyou.lonking.entity;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2020/9/21
 * @描述
 **/
@Data
public class TerminalInfo {
    //"发动机运行时间","燃油油位","瞬时油耗",city
    private String id;
    private String device;
    private String origin;
    private String formatDate;
    //发动机运行时间
    private String workTime;
    private String oil;
    private String machineStart;
}
