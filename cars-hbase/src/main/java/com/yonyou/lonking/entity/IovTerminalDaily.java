package com.yonyou.lonking.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author shendw
 * @since 2020-10-30
 */
@Data
@Accessors(chain = true)
@TableName("IOV_TERMINAL_DAILY")
public class IovTerminalDaily{

    private static final long serialVersionUID = 1L;

    @TableId("ID")
    private String id;

    @TableField("DEVICE")
    private String device;

    @TableField("YEAR")
    private String year;

    @TableField("MONTH")
    private String month;

    @TableField("DAY")
    private String day;

    @TableField("WORKTIME")
    private BigDecimal worktime;

    @TableField("OIL_CONSUMPTION")
    private BigDecimal oilConsumption;

    @TableField("PROVINCE")
    private String province;

    @TableField("CITY")
    private String city;

    @TableField("DISTRICT")
    private String district;

    @TableField("TOWN")
    private String town;

    @TableField("CARNO")
    private String carno;

    @TableField("AGENT")
    private String agent;


}
