package com.yonyou.lonking.entity;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2020/10/22
 * @描述
 **/
@Data
public class WorkTimesEntity {
    private String device;

    private String date;

    private Double workTime;

    private Double oilConsumption;

    private boolean[] workTimes;
}
