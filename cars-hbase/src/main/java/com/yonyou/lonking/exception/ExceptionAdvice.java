package com.yonyou.lonking.exception;


import com.yonyou.lonking.utils.ResponseCode;
import com.yonyou.lonking.utils.ResponseData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author shendawei
 * @创建时间 2019-11-05
 * @描述
 **/
@RestControllerAdvice
@Slf4j
public class ExceptionAdvice {

    @ExceptionHandler(BusinessException.class)
    public ResponseData handleBusinessException(BusinessException e) {
        return ResponseData.result(e.getResponseCode().getCode(), e.getResponseCode().getDesc());
    }

    @ExceptionHandler(Exception.class)
    public ResponseData handleBusinessException(Exception e) {
        log.error(e.getMessage());
        return ResponseData.result(ResponseCode.ERROR.getCode(),e.getMessage());
    }

}
