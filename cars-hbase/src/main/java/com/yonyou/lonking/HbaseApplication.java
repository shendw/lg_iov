package com.yonyou.lonking;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringCloudApplication
@EnableTransactionManagement
@EnableFeignClients(basePackages = "com.yonyou.lonking.*")
@MapperScan(basePackages = "com.yonyou.lonking.dao.oracle")
public class HbaseApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(HbaseApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(HbaseApplication.class, args);
    }

}
