package com.yonyou.lonking.conf;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * @author shendawei
 * @创建时间 2020/10/22
 * @描述
 **/
@Configuration//声明该类是一个配置类
@MapperScan(basePackages = "com.yonyou.lonking.dao.phoenix", sqlSessionFactoryRef = "sqlSessionFactory1", sqlSessionTemplateRef = "sqlSessionTemplate1")
public class MybatisConfigPhoenix {
    @Resource(name = "dsPhoenix")
    DataSource dsPhoenix;

    @Bean
    @Primary
    SqlSessionFactory sqlSessionFactory1() {
        SqlSessionFactory sessionFactory = null;
        try {
            SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
            bean.setDataSource(dsPhoenix);
            bean.setMapperLocations(
                    // 设置mybatis的xml所在位置
                    new PathMatchingResourcePatternResolver().getResources("classpath*:mapper/phoenix/*.xml"));
            sessionFactory = bean.getObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sessionFactory;
    }

    @Bean
    @Primary
    SqlSessionTemplate sqlSessionTemplate1() {
        return new SqlSessionTemplate(sqlSessionFactory1());
    }

    // 创建该数据源的事务管理
//    @Bean(name = "phoenixTransactionManager")
//    @Primary
//    public DataSourceTransactionManager primaryTransactionManager() throws SQLException {
//        return new DataSourceTransactionManager(dsPhoenix);
//    }
}
