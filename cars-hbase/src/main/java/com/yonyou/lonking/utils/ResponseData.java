package com.yonyou.lonking.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-11-04
 * @描述
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseData {
    private String status;
    private String msg;
    private Object data;

    public ResponseData(String status, Object data) {
        this.data = data;
        this.status = status;
    }

    public ResponseData(String status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public ResponseData(String status, Object data, String msg) {
        this.data = data;
        this.status = status;
        this.msg = msg;
    }

    public static ResponseData result(String status, Object data) {
        if (data == null) {
            ResponseCode responseCode = ResponseCode.ERROR_NODATA;
            return new ResponseData(responseCode.getCode(), responseCode.getDesc());
        }
        return new ResponseData(status, data);
    }

    public static ResponseData result(String status, String msg) {
        return new ResponseData(status, msg);
    }

    public static ResponseData result(String status, Object data, String msg) {
        return new ResponseData(status, data, msg);
    }
}
