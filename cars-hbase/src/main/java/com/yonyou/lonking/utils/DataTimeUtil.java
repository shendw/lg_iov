package com.yonyou.lonking.utils;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * @author shendawei
 * @创建时间 2019-11-06
 * @描述
 **/
public class DataTimeUtil {
    public static final String DEFAULTSTARTTIME = " 00:00:00";
    public static final String DEFAULTENDTIME = " 23:59:59";
    public static final String STANDARD_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 将时间转换成时间戳
     *
     * @param str
     * @return
     * @throws ParseException
     */
    public static String dateToStamp(String str) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(STANDARD_FORMAT);
        Date date = simpleDateFormat.parse(str);
        long ts = date.getTime() / 1000;
        return String.valueOf(ts);
    }

    /**
     * 将时间戳转换成时间
     *
     * @param str
     * @return
     */
    public static String stampToDate(String str) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(STANDARD_FORMAT);
        long lt = new Long(str);
        Date date = new Date(lt);
        return simpleDateFormat.format(date);
    }

    /**
     * 计算两个时间的时间差
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @param duration  时间差单位
     * @return
     */
    public static long calculateTimeDifference(Date startTime, Date endTime, ChronoUnit duration) {
        return duration.between(startTime.toInstant(), endTime.toInstant());
    }

    public static Date strToDate(String dateTimeStr, String formatStr) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(formatStr);
        DateTime dateTime = dateTimeFormatter.parseDateTime(dateTimeStr);
        return dateTime.toDate();
    }

    public static String dateToStr(Date date, String formatStr) {
        if (date == null) {
            return StringUtils.EMPTY;
        }
        DateTime dateTime = new DateTime(date);
        return dateTime.toString(formatStr);
    }

    public static Date strToDate(String dateTimeStr) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern(STANDARD_FORMAT);
        DateTime dateTime = dateTimeFormatter.parseDateTime(dateTimeStr);
        return dateTime.toDate();
    }

    public static String dateToStr(Date date) {
        if (date == null) {
            return StringUtils.EMPTY;
        }
        DateTime dateTime = new DateTime(date);
        return dateTime.toString(STANDARD_FORMAT);
    }
}
