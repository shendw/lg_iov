package com.yonyou.lonking.exception;

import com.yonyou.lonking.utils.ResponseData;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author shendawei
 * @创建时间 2019-11-05
 * @描述
 **/
@RestControllerAdvice
public class ExceptionAdvice {

    @ExceptionHandler(BusinessException.class)
    public ResponseData handleBusinessException(BusinessException e) {
        return ResponseData.result(e.getResponseCode().getCode(), e.getResponseCode().getDesc());
    }

}
