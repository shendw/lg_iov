package com.yonyou.lonking.enums;

import lombok.Getter;

/**
 * @author shendawei
 * @创建时间 2019-12-06
 * @描述
 **/
@Getter
public enum CarWorkStatus {
    /**
     * 通电
     */
    ELECTRIFY("0","通电"),
    /**
     * 停机
     */
    STOP("1","停机"),
    /**
     * 怠速
     */
    SLOWSPEED("2","怠速"),
    /**
     * 启动
     */
    START("3","启动"),
    ;

    private final String code;
    private final String desc;

    CarWorkStatus(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
