package com.yonyou.lonking.enums;

import lombok.Getter;

/**
 * @author shendawei
 * @创建时间 2020/4/26
 * @描述
 **/
@Getter
public enum LockCarEnum {
    /**
     * 解车
     */
    UNLOCKCAR("0","解车"),
    /**
     * 锁车
     */
    LOCKCAR("1","锁车"),
    /**
     * 解车
     */
    LIMITSPEED("2","限速");

    private final String code;
    private final String desc;

    LockCarEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
