package com.yonyou.lonking.enums;

import lombok.Getter;

/**
 * @author shendawei
 * @创建时间 2019-12-18
 * @描述
 **/
@Getter
public enum CarProcessStatus {
    /**
     * 自动绑定
     */
    DEFAULT(0,"自动绑定"),
    /**
     * 自动绑定
     */
    AUTOBIND(0,"自动绑定"),
    /**
     * 下线
     */
    OUTLINE(1,"下线"),
    /**
     * 调试
     */
    TEST(2,"调试"),
    /**
     * 入库
     */
    INLINE(3,"入库"),
    /**
     * 代理商库存
     */
    STOCK(4,"代理库存"),
    /**
     * 已售
     */
    SOLD(5,"已售"),
    ;

    private final long code;
    private final String desc;

    CarProcessStatus(long code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
