package com.yonyou.lonking.enums;

import lombok.Getter;

/**
 * @author shendawei
 * @创建时间 2019-11-06
 * @描述
 **/
@Getter
public enum TerminalStatus {
    /**
     * 未标定
     */
    DEFEAT(0L),
    /**
     * 成品
     */
    FINISHED(1L),
    /**
     * 待修品
     */
    PENDING(2L),
    /**
     * 返修品
     */
    REPAIRED(3L),
    /**
     * 废弃品
     */
    SCRAP(4L),
    /**
     * 装机件
     */
    WORK(5L),

    ;

    private final Long code;

    TerminalStatus(Long code){
        this.code = code;
    }
}
