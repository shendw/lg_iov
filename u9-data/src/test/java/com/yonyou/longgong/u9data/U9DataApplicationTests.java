package com.yonyou.longgong.u9data;

import com.yonyou.longgong.u9data.dao.CardocPojoMapper;
import com.yonyou.longgong.u9data.pojo.U9CardocPojo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@SpringBootTest(classes = U9DataApplication.class)
@RunWith(SpringRunner.class)
public class U9DataApplicationTests {

	@Autowired
	private CardocPojoMapper cardocPojoMapper;

	@Test
	public void contextLoads() {
		List<String> strings = Arrays.asList(">LSW0075ELJ0081058<", ">LSW0365HPH0060658<");
		List<U9CardocPojo> u9CardocPojos = cardocPojoMapper.selectByCarNo(strings);
		System.out.println(u9CardocPojos);
	}

}
