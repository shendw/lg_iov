package com.yonyou.longgong.u9data.web;

import com.yonyou.longgong.u9data.pojo.U9CardocPojo;
import com.yonyou.longgong.u9data.service.ICardocService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-11-29
 * @描述
 **/
@RestController
@RequestMapping("/cardoc")
public class CardocController {

    @Autowired
    private ICardocService iCardocService;

    @RequestMapping("/getCardoc")
    public List<U9CardocPojo> getCardoc(@RequestBody List<U9CardocPojo> u9CardocPojos){
        List<U9CardocPojo> result = iCardocService.selectCardoc(u9CardocPojos);
        return result;
    }

    @RequestMapping("/getCardocStock")
    public List<U9CardocPojo> getCardocStock(@RequestBody List<U9CardocPojo> u9CardocPojos){
        List<U9CardocPojo> result = iCardocService.selectCardocStock(u9CardocPojos);
        return result;
    }
}
