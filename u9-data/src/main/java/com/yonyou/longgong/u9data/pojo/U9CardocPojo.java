package com.yonyou.longgong.u9data.pojo;

import lombok.Data;

/**
 * @author shendawei
 * @创建时间 2019-11-29
 * @描述
 **/
@Data
public class U9CardocPojo {
    private String pkOrg;

    private String carmodel;

    private String machinemodel;

    private String saleAgent;

    private String serviceAgent;

    private String customer;

    private String status = "0";

    private String instocktime;

    private String outstocktime;

    private String saletime;

    private String carno;
}
