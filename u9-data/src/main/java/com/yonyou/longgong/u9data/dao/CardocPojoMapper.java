package com.yonyou.longgong.u9data.dao;

import com.yonyou.longgong.u9data.pojo.U9CardocPojo;

import java.util.List;

public interface CardocPojoMapper {

    List<U9CardocPojo> selectByCarNo(List<String> carNo);

    List<U9CardocPojo> selectByCarNoNotSale(List<String> carNo);
}