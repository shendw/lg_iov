package com.yonyou.longgong.u9data.service;

import com.yonyou.longgong.u9data.pojo.U9CardocPojo;

import java.util.List;

/**
 * @author shendawei
 * @创建时间 2019-11-29
 * @描述
 **/
public interface ICardocService {
    /**
     * 已售车辆查询
     * @param u9CardocPojos
     * @return
     */
    List<U9CardocPojo> selectCardoc(List<U9CardocPojo> u9CardocPojos);

    /**
     * 库存车辆查询
     * @param u9CardocPojos
     * @return
     */
    List<U9CardocPojo> selectCardocStock(List<U9CardocPojo> u9CardocPojos);
}
