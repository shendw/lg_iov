package com.yonyou.longgong.u9data.service.impl;

import com.yonyou.longgong.u9data.dao.CardocPojoMapper;
import com.yonyou.longgong.u9data.pojo.U9CardocPojo;
import com.yonyou.longgong.u9data.service.ICardocService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author shendawei
 * @创建时间 2019-11-29
 * @描述
 **/
@Service("iCardocService")
public class CardocServiceImpl implements ICardocService {

    @Autowired
    private CardocPojoMapper cardocPojoMapper;

    @Override
    public List<U9CardocPojo> selectCardoc(List<U9CardocPojo> u9CardocPojos) {
        if (CollectionUtils.isEmpty(u9CardocPojos)){
            return null;
        }
        List<String> carNos = u9CardocPojos.stream().map(U9CardocPojo::getCarno).collect(Collectors.toList());
        return cardocPojoMapper.selectByCarNo(carNos);
    }

    @Override
    public List<U9CardocPojo> selectCardocStock(List<U9CardocPojo> u9CardocPojos) {
        if (CollectionUtils.isEmpty(u9CardocPojos)){
            return null;
        }
        List<String> carNos = u9CardocPojos.stream().map(U9CardocPojo::getCarno).collect(Collectors.toList());
        return cardocPojoMapper.selectByCarNoNotSale(carNos);
    }
}
